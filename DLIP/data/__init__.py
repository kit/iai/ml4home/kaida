"""
    Datasets to be used must be specified here to be loadable.
"""
from .base_classes.instance_segmentation.base_inst_seg_data_module import BaseInstanceSegmentationDataModule
from .base_classes.segmentation.base_seg_data_module import BaseSegmentationDataModule
from .base_classes.seed_detection.base_seed_data_module import BaseSeedDetectionDataModule
from .base_classes.panoptic_segmentation.base_panoptic_seg_data_module import BasePanopticSegmentationDataModule
