from pathlib import Path
from typing import Dict, List


class DiffParser:
    DIFF_DICT_KEYS = ["test", "train", "unlabeled", "meta_info"]

    def __init__(self, task: str):
        self.task = task

    def parse(self, diff: Dict[str, List[str]]) -> str:
        if self.task == "classification":
            return self._parse_classification(diff)
        return self._parse_segmentation(diff)

    @staticmethod
    def _parse_classification(diff: Dict[str, List[str]]) -> str:
        text = ""

        renamed_files = diff["Renamed"] if "Renamed" in diff else list()

        text += "New Labeled Data:\n"

        new_labels_counter = 0

        for file in renamed_files:
            if file.startswith("unlabeled"):
                renamed_file = Path(file.split("->")[1])
                text += "\t{} ({})\n".format(renamed_file.name, renamed_file.parent.name)
                new_labels_counter += 1

        if new_labels_counter == 0:
            text += "\tNone\n"

        text += "\nChanged Labels:\n"

        changed_labels_counter = 0

        for file in renamed_files:
            if not file.startswith("unlabeled"):
                original_file = Path(file.split("->")[0])
                renamed_file = Path(file.split("->")[1])

                text += "\t{} ({} -> {})\n".format(renamed_file.name,
                                                   original_file.parent.name,
                                                   renamed_file.parent.name)
                changed_labels_counter += 1

        if changed_labels_counter == 0:
            text += "\tNone\n"

        text += "\n"

        text += DiffParser._parse_added_data(diff)

        text += "\nDeleted Files:\n"
        empty = True

        if "Deleted" in diff:
            for file in diff["Deleted"]:
                text += "\t{}\n".format(Path(file).name)
                empty = False

        if empty:
            text += "\tNone\n"

        return text

    @staticmethod
    def _parse_segmentation(diff: Dict[str, List[str]]) -> str:
        text = ""

        text += "New Labeled Data:\n"

        label_files = list()

        if "Renamed" not in diff:
            text += "\tNone\n"
        else:
            renamed_files = diff["Renamed"]

            for file in renamed_files:
                file = Path(file.split("->")[0])
                text += "\t" + file.name + "\n"

                file_extension = file.name.split(".")[-1]
                label_files.append(file.name.replace(".{}".format(file_extension), "_label.{}".format(file_extension)))

        text += "\n"

        text += "Changed Labels:\n"
        empty = True

        if "Modified" in diff:
            modified_files = diff["Modified"]

            for file in modified_files:
                if str(Path(file)) in DiffParser.DIFF_DICT_KEYS:
                    continue

                text += "\t{}\n".format(Path(file).name)
                empty = False

        if empty:
            text += "\tNone\n"

        text += "\n"

        text += DiffParser._parse_added_data(diff, files_to_ignore=label_files)

        deleted_labels = list()
        deleted_files = list()

        if "Deleted" in diff:
            for file in [Path(f) for f in diff["Deleted"]]:
                if "_label" in file.name:
                    deleted_labels.append(file.name.replace("_label", ""))
                else:
                    deleted_files.append(file.name)

        deleted_labels = [f for f in deleted_labels if f not in deleted_files]

        text += "\n"
        text += "Discarded Labels:\n"

        if len(deleted_labels) == 0:
            text += "\tNone\n"
        else:
            for file in deleted_labels:
                text += "\t{}\n".format(file)

        text += "\n"
        text += "Deleted Files:\n"

        if len(deleted_files) == 0:
            text += "\tNone\n"
        else:
            for file in deleted_files:
                text += "\t{}\n".format(file)

        return text

    @staticmethod
    def _parse_added_data(diff: Dict[str, List[str]], files_to_ignore: List[str] = []):
        text = "Added Data:\n"

        added_data = diff["Added"] if "Added" in diff else list()

        added_files_counter = 0

        for file in added_data:
            if file.startswith("meta_info"):
                continue

            if Path(file).name in files_to_ignore:
                continue

            text += "\t{}\n".format(Path(file).name)
            added_files_counter += 1

        if added_files_counter == 0:
            text += "\tNone\n"

        return text
