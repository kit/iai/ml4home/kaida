from typing import List, Dict, Optional

from DLIP.data.version_control.dvc_wrapper.console_wrapper import ConsoleWrapper, COMMAND_BREAK
from DLIP.data.version_control.dvc_wrapper.git_wrapper import GitWrapper


class DvcWrapper(ConsoleWrapper):
    def __init__(self, project_folder):
        if not DvcWrapper.is_available():
            raise ModuleNotFoundError("DVC is not available")

        self.project_folder = project_folder

        self.base_command = f"cd \"{self.project_folder}\"" + COMMAND_BREAK

    @staticmethod
    def is_available() -> bool:
        try:
            import dvc as python_dvc_module
            return True
        except ModuleNotFoundError:
            try:
                DvcWrapper._run("dvc -h")
                return True
            except ValueError:
                return False

    def init(self, force: bool = False, using_git: bool = False) -> str:
        command = self.base_command
        command += f"dvc init {'-f' if force else ''} {'--no-scm' if not using_git else ''}"
        command += COMMAND_BREAK
        command += "dvc config core.analytics false"

        return DvcWrapper._run(command)

    def set_default_remote_storage(self, name: str, remote_storage: str) -> str:
        # dvc remote add -d <name> /path/to/use
        command = self.base_command
        command += f"dvc remote add -d {name} {remote_storage}"

        return DvcWrapper._run(command)

    def add_file_to_track(self, file_path: str) -> str:
        # dvc add data/path/data.file
        command = self.base_command
        command += f"dvc add {file_path}"

        return DvcWrapper._run(command)

    def add_change(self, file_path: str):
        self.add_file_to_track(file_path)

    def push(self) -> str:
        # dvc push
        command = self.base_command
        command += f"dvc push"

        return DvcWrapper._run(command)

    def status(self) -> List[str]:
        # dvc status
        command = self.base_command
        command += f"dvc status"

        output = DvcWrapper._run(command).splitlines()
        changed_data = list()

        if len(output) == 1:
            # Data and pipelines are up to date.
            return []

        for line in output:
            if line.endswith(".dvc:"):
                changed_data.append(line.replace(".dvc:", ""))

        return changed_data

    def diff(self) -> Optional[Dict[str, List[str]]]:
        if not GitWrapper.is_available():
            # dvc diff is only available when using git
            return None

        # dvc diff
        command = self.base_command
        command += f"dvc diff"

        output = DvcWrapper._run(command)

        if output == "":
            return dict()

        output = output.splitlines()
        changed_data = dict()

        current_key = ""
        for line in output:
            if line == "" or line.startswith("files summary") or line.startswith("\x1b"):
                continue
            if line.startswith(" ") or line.startswith("\t"):
                changed_data[current_key].append(line.replace(" ", ""))
            else:
                current_key = line.replace(":", "")
                changed_data[current_key] = list()

        return changed_data

    def checkout(self, force: bool = False):
        # dvc checkout
        command = self.base_command
        command += f"dvc checkout {'-f' if force else ''}"

        return DvcWrapper._run(command)

    def pull(self, force: bool = False):
        command = self.base_command
        command += f"dvc pull {'-f' if force else ''}"

        return DvcWrapper._run(command)
