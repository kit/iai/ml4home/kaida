from typing import List, Union, NoReturn
from pathlib import Path

from DLIP.data.version_control.dvc_wrapper.console_wrapper import ConsoleWrapper, COMMAND_BREAK


class GitWrapper(ConsoleWrapper):
    def __init__(self, project_folder):
        if not GitWrapper.is_available():
            raise ModuleNotFoundError("GIT is not available")

        self.project_folder = Path(project_folder)

        self.base_command = f"cd \"{str(self.project_folder)}\"" + COMMAND_BREAK

    @staticmethod
    def is_available() -> bool:
        try:
            GitWrapper._run("git --help")
            return True
        except ValueError:
            return False

    def init(self) -> str:
        command = self.base_command
        command += "git init"

        return GitWrapper._run(command)

    def add(self, file_name: str) -> str:
        command = self.base_command
        command += f"git add \"{file_name}\""

        return GitWrapper._run(command)

    def add_multiple_files(self, file_names: List[str]) -> str:
        command = self.base_command

        file_names = [f"\"{name}\"" for name in file_names]

        command += f"git add {' '.join(file_names)}"

        return GitWrapper._run(command)

    def commit(self, message: str) -> str:
        command = self.base_command
        command += f"git commit -m \"{message}\""

        return GitWrapper._run(command)

    def get_last_commit_id(self):
        command = self.base_command
        command += "git rev-parse HEAD"

        return GitWrapper._run(command)

    def checkout(self, commit_id: str) -> str:
        command = self.base_command
        command += f"git checkout {commit_id}"

        return GitWrapper._run(command)

    def status(self) -> str:
        command = self.base_command
        command += f"git status"

        return GitWrapper._run(command)

    def ignore(self, files: Union[List[str], str]) -> NoReturn:
        with open(self.project_folder.joinpath(".gitignore"), "a") as gitignore:
            if isinstance(files, str):
                files = [files]
            for file in files:
                gitignore.write(f"{file}\n")

    def stash(self):
        command = self.base_command
        command += f"git stash"

        return GitWrapper._run(command)
