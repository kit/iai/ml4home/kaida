from abc import ABC
import subprocess


COMMAND_BREAK = " && "


class ConsoleWrapper(ABC):
    @staticmethod
    def _run(command: str) -> str:
        ret = subprocess.run(command, capture_output=True, shell=True)

        if ret.returncode != 0:
            raise ValueError(ret.stderr.decode())

        return ret.stdout.decode()
