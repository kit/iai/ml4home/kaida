from sys import stderr
from typing import List, NoReturn, Dict, Any, Union, Optional
from pathlib import Path
import shutil
import json

from DLIP.data.version_control.dvc_wrapper.dvc_wrapper import DvcWrapper
from DLIP.data.version_control.dvc_wrapper.git_wrapper import GitWrapper
from DLIP.data.version_control.dvc_wrapper.file_utils import onerror


class DvcProject:
    DVC_VERSION_INFO_FILE_NAME = "dvc_project_version_info.json"
    DVC_INFO_FILE_KEYS = ["changed_data", "message", "last_version", "diff", "git_commit", "project_file"]

    def __init__(
            self,
            external_path: str,
            files_to_track: List[str],
            project_directory: str = None,
            do_init: bool = True,
            use_git: bool = False
    ):

        self.external_path = Path(external_path)
        self.files_to_track = [Path(file) for file in files_to_track]

        if project_directory is None:
            # we are assuming that all files to track are in the project directory
            self.project_folder = self.files_to_track[0].parent

            if any(self.project_folder != f.parent for f in self.files_to_track):
                raise ValueError("All files/folder to track need to be in the project directory")
        else:
            self.project_folder = Path(project_directory)

        if not do_init and self.project_folder.joinpath(".git").exists() and use_git or do_init and use_git:
            self.git_wrapper = GitWrapper(str(self.project_folder))
        else:
            if use_git:
                print("INFO: There is no git folder in the already initialized project. "
                      "Therefore, git is not used.", file=stderr)
            self.git_wrapper = None
        self.dvc_wrapper = DvcWrapper(str(self.project_folder))

        if not self.external_path.exists():
            if self.external_path.parent.exists():
                self.external_path.mkdir()
            else:
                raise ValueError("The given external path does not exist")

        if do_init:
            self.next_version = 0
            self.last_version = -1
            self.init()
        else:
            if not self.external_path.joinpath("versions").exists() or not self.external_path.joinpath("dvc").exists():
                raise ValueError("The external folder doesn't contain the needed files")
            self.next_version = max([int(x) for x in self.get_versions()] + [-1]) + 1

            if self.project_folder.joinpath(".dvc_version").exists():
                f = open(self.project_folder.joinpath(".dvc_version"), "r")
                self.last_version = int(f.read())
                f.close()
            else:
                self.last_version = self.next_version - 1

    def init(self) -> NoReturn:
        if self.git_wrapper is not None:
            self.git_wrapper.init()

            files_to_ignore = ["temp/", ".dvc_version"]
            self.git_wrapper.ignore(files_to_ignore)
            self.git_wrapper.add(".gitignore")
            self.git_wrapper.commit("initialized gitignore")

        self.dvc_wrapper.init(using_git=self.git_wrapper is not None)
        for file in self.files_to_track:
            self.dvc_wrapper.add_file_to_track(str(file.absolute()))

        if self.git_wrapper is not None:
            self.git_wrapper.add(str(self.project_folder.joinpath(".dvc").absolute()))
            for file in self.files_to_track:
                self.git_wrapper.add(str(file.absolute()) + ".dvc")
            self.git_wrapper.commit("initial commit")

        external_dvc_path = self.external_path.joinpath("dvc")
        external_dvc_path.mkdir()

        self.dvc_wrapper.set_default_remote_storage("remote_storage", str(external_dvc_path.absolute()))

        external_versions_path = self.external_path.joinpath("versions")
        external_versions_path.mkdir()

        f = open(self.project_folder.joinpath(".dvc_version"), "w")
        f.write(str(self.last_version))
        f.close()

    @staticmethod
    def is_already_initialized(external_path: str, files_to_track: List[str], project_directory: str = None,
                               use_git: bool = False) -> bool:
        external_path = Path(external_path)
        files_to_track = [Path(file) for file in files_to_track]

        if project_directory is None:
            # we are assuming that all files to track are in the project directory
            project_directory = files_to_track[0].parent
        else:
            project_directory = Path(project_directory)

        git_files_exist = True
        if use_git:
            git_files_exist = project_directory.joinpath(".git").exists()

        return (
                external_path.exists()
                and external_path.joinpath("versions").exists()
                and external_path.joinpath("dvc").exists()
                and project_directory.joinpath(".dvc").exists()
                and all(Path(str(f) + ".dvc").exists() for f in files_to_track)
                and git_files_exist
        )

    def get_versions(self) -> List[str]:
        external_versions_path = self.external_path.joinpath("versions")
        return [str(f.name) for f in external_versions_path.iterdir() if f.is_dir()]

    def get_versions_with_info(self) -> Dict[str, Dict[str, Any]]:
        versions = self.get_versions()
        version_infos = dict()

        for version in versions:
            external_version_path = self.external_path.joinpath("versions").joinpath(version)

            f = open(external_version_path.joinpath(self.DVC_VERSION_INFO_FILE_NAME))
            version_infos[version] = json.loads(f.read())

        return version_infos

    def commit_version(self, message: str = "", project_file: Dict[str, Any] = None) -> NoReturn:
        changed_data = self.dvc_wrapper.status()

        if len(changed_data) == 0 and self.next_version != 0:
            raise ValueError("No differences to commit")

        changed_data_diff = self.get_current_diff()

        for data in changed_data:
            self.dvc_wrapper.add_change(data)
        self.dvc_wrapper.push()

        if self.git_wrapper is not None:
            self.git_wrapper.add(str(self.project_folder.joinpath(".gitignore")))
            self.git_wrapper.add(str(self.project_folder.joinpath(".dvc")))
            self.git_wrapper.add(str(self.project_folder.joinpath(".dvcignore")))
            self.git_wrapper.add_multiple_files([str(file) + ".dvc" for file in self.files_to_track])
            self.git_wrapper.commit(f"v{self.next_version}: {message}")

        external_version_path = self.external_path.joinpath("versions").joinpath(str(self.next_version))

        external_version_path.mkdir()

        dvc_files = [Path(str(file) + ".dvc") for file in self.files_to_track] + \
                    [self.project_folder.joinpath(".dvc"), self.project_folder.joinpath(".dvcignore")] + \
                    [self.project_folder.joinpath(".dvc_version")]

        if self.git_wrapper is not None:
            files_to_copy = dvc_files + [self.project_folder.joinpath(".git"),
                                         self.project_folder.joinpath(".gitignore")]
        else:
            files_to_copy = dvc_files

        for file in files_to_copy:
            if file.is_dir():
                shutil.copytree(file, external_version_path.joinpath(file.name))
            else:
                shutil.copy(file, external_version_path)

        info_file = external_version_path.joinpath(self.DVC_VERSION_INFO_FILE_NAME)

        info_file_content = {
            "changed_data": changed_data,
            "message": message,
            "last_version": self.last_version,
            "diff": changed_data_diff,
            "git_commit": self.git_wrapper.get_last_commit_id() if self.git_wrapper is not None else None,
            "project_file": project_file
        }

        if not all([key in info_file_content.keys() for key in self.DVC_INFO_FILE_KEYS]):
            raise NotImplementedError("The dvc implementation is inconsistent. Please check the dvc info file keys.")

        file = open(info_file, "w")
        file.write(json.dumps(info_file_content))
        file.close()

        self.last_version = self.next_version
        self.next_version += 1

        f = open(self.project_folder.joinpath(".dvc_version"), "w")
        f.write(str(self.last_version))
        f.close()

    @staticmethod
    def load_external_version_data(
            external_version_path: Path,
            local_path: Path,
            files_to_track: List[Union[Path, str]],
            full: bool = True,
            load_git_files: bool = False
    ):
        files_to_track = [Path(f) for f in files_to_track]

        dvc_files = [Path(str(file.name) + ".dvc") for file in files_to_track]

        if full:
            dvc_files += [Path(".dvc"), Path(".dvcignore")] + \
                         [Path(".dvc_version")]

        if load_git_files and external_version_path.joinpath(".git").exists():
            dvc_files += [Path(".git")]

        if load_git_files and external_version_path.joinpath(".gitignore").exists():
            dvc_files += [Path(".gitignore")]

        DvcProject.download_files(external_version_path, local_path, dvc_files)

    @staticmethod
    def download_files(external_version_path, local_path, files):
        for file in files:
            if external_version_path.joinpath(file).is_dir():
                if local_path.joinpath(file).exists():
                    try:
                        shutil.rmtree(local_path.joinpath(file), ignore_errors=False)
                    except PermissionError:
                        # On Windows the .dvc folder cannot be removed, due to read only files
                        # Therefore an errorhandler has to be used that changes the rights for the needed files
                        shutil.rmtree(local_path.joinpath(file), ignore_errors=False, onerror=onerror)

                shutil.copytree(external_version_path.joinpath(file), local_path.joinpath(file))
            else:
                shutil.copy(external_version_path.joinpath(file), local_path)

    def checkout_version(self, version: str, full: bool = False) -> NoReturn:
        if version not in self.get_versions():
            raise ValueError(f"The given version '{version}' is not available")

        external_version_path = self.external_path.joinpath("versions").joinpath(version)

        if self.git_wrapper is not None:
            f = open(external_version_path.joinpath(DvcProject.DVC_VERSION_INFO_FILE_NAME))
            git_commit_id = json.loads(f.read())["git_commit"]
            f.close()

            try:
                self.git_wrapper.checkout(git_commit_id)
                print("[DVC_PROJECT] Did local checkout", file=stderr)
            except ValueError as err:
                if "Your local changes to the following files would be overwritten by checkout" in str(err.args[0]):
                    raise err

                print("[DVC_PROJECT] Loading external git files.", file=stderr)
                self.download_files(external_version_path, self.project_folder, [".git", ".gitignore"])
                self.git_wrapper.stash()
                self.git_wrapper.checkout(git_commit_id)

        else:
            self.load_external_version_data(external_version_path, self.project_folder, self.files_to_track,
                                            full=full, load_git_files=self.git_wrapper is not None)
            print("[DVC_PROJECT] Had to download all external files.", file=stderr)

            # downloading git files here, if git is used, but the remote version is newer,
            # because somebody else could have updated the remote files

        self.dvc_wrapper.pull(force=True)

        self.last_version = int(version)

        f = open(self.project_folder.joinpath(".dvc_version"), "w")
        f.write(str(self.last_version))
        f.close()

    def get_newest_remote_version(self):
        versions = self.get_versions()

        if len(versions) == 0:
            return -1

        return max([int(v) for v in self.get_versions()])

    def get_current_diff(self) -> Optional[Dict[str, List[str]]]:
        return self.dvc_wrapper.diff() if self.git_wrapper is not None else None
