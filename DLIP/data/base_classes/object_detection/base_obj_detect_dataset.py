import tifffile
import glob
import os
import numpy as np
import cv2
from DLIP.utils.helper_functions.gray_level_check import gray_redundand
from skimage import io

from DLIP.data.base_classes.base_dataset import BaseDataset
from DLIP.gui.backend.label_assistant.object_detection.ui_object_detection.obj_detect_ui import PascalVocReader

class BaseObjectDetectionDataset(BaseDataset):
    def __init__(
        self,
        root_dir: str,
        samples_dir: str = "samples",
        labels_dir: str = "labels",
        samples_data_format="tif",
        labels_data_format="xml",
        transforms = None,
        empty_dataset=False,
        labels_available=True,
        return_trafos=False,
        label_suffix="_label",
        label_prefix="",
    ):
        self.labels_available = labels_available
        self.root_dir = root_dir
        self.samples_dir = samples_dir
        self.labels_dir = labels_dir
        self.samples_data_format = samples_data_format
        self.labels_data_format = labels_data_format
        self.return_trafos = return_trafos
        self.transforms = transforms
        self.label_suffix=label_suffix
        self.label_prefix=label_prefix

        if transforms is None:
                self.transforms = lambda x, y: (x,y,0)
        if isinstance(transforms, list):
            self.transforms = transforms
        else:
            self.transforms = [self.transforms]

        self.samples = os.path.join(self.root_dir,self.samples_dir)
        self.labels  = os.path.join(self.root_dir,self.labels_dir)

        # Get all sample names sorted as integer values
        all_samples_sorted = sorted(
            glob.glob(f"{self.samples}{os.path.sep}*.{samples_data_format}"),
            key=lambda x: 
                x.split(f"{self.samples}{os.path.sep}")[1].split(
                    f".{samples_data_format}"
            ),
        )
        self.indices = []
        if not empty_dataset:
            # Extract indices from the sorted samples
            self.indices = [
                i.split(f"{self.samples}{os.path.sep}")[1].split(f".{samples_data_format}")[0]
                for i in all_samples_sorted
            ]
        self.raw_mode = False

    def __len__(self):
        return len(self.indices)

    def __getitem__(self, idx):
        # load sample
        sample_path = os.path.join(self.samples, f"{self.indices[idx]}.{self.samples_data_format}")
        sample_img = tifffile.imread(sample_path) if (self.samples_data_format=="tif" or self.samples_data_format=="tiff") else io.imread(sample_path)
        
        if sample_img.ndim>2 and gray_redundand(sample_img):
            sample_img = sample_img[:,:,0]

        sample_img_lst = []
        box_lst = []
        trafo_lst = []

        if self.labels_available:
            label_path = os.path.join(self.labels, f"{self.label_prefix}{self.indices[idx]}{self.label_suffix}.{self.labels_data_format}")
            pascal_voc_data = PascalVocReader(label_path)
            shapes = pascal_voc_data.shapes
            boxes = [ [box[0][0], box[0][1], box[1][0], box[2][1], class_label] for class_label, box,_,_,_ in shapes]

        # raw mode -> no transforms
        if self.raw_mode:
            if self.labels_available:
                return sample_img,boxes
            else:
                return sample_img
            
        for transform in self.transforms:
            im, bx, trafo = transform(sample_img, boxes=boxes)
            sample_img_lst.append(im)
            box_lst.append(bx)
            trafo_lst.append(trafo)

        if len(sample_img_lst) == 1:
            sample_img_lst = sample_img_lst[0]
            box_lst = box_lst[0] if len(box_lst) > 0 else box_lst
            trafo_lst = trafo_lst[0] if len(trafo_lst) > 0 else trafo_lst

        # sample_img_lst (optional: labels) (optional: trafos)
        if not self.return_trafos and not self.labels_available:
            return sample_img_lst
        if self.return_trafos and not self.labels_available:
            return sample_img_lst, trafo_lst
        if not self.return_trafos and self.labels_available:
            return sample_img_lst, box_lst
        if self.return_trafos and self.labels_available:
            return sample_img_lst, box_lst, trafo_lst

    def pop_sample(self, index):
        return self.indices.pop(index)

    def add_sample(self, new_sample):
        self.indices.append(new_sample)

    def get_samples(self):
        return self.indices