import os
import random

from DLIP.data.base_classes.base_pl_datamodule import BasePLDataModule
from DLIP.data.base_classes.classification.base_class_dataset import BaseClassificationDataset

class BaseClassificationDataModule(BasePLDataModule):
    def __init__(
        self,
        root_dir: str,
        class_lst: list,
        batch_size = 1,
        dataset_size = 1.0,
        val_to_train_ratio = 0,
        initial_labeled_ratio= 1.0,
        train_transforms=None,
        train_transforms_unlabeled=None,
        val_transforms=None,
        test_transforms=None,
        return_unlabeled_trafos=False,
        num_workers=0,
        pin_memory=False,
        shuffle=True,
        drop_last=False,
        single_batch_val_test = False,
        **kwargs
    ):
        super().__init__(
            dataset_size=dataset_size,
            batch_size = batch_size,
            val_to_train_ratio = val_to_train_ratio,
            num_workers = num_workers,
            pin_memory = pin_memory,
            shuffle = shuffle,
            drop_last = drop_last,
            initial_labeled_ratio = initial_labeled_ratio,
            single_batch_val_test = single_batch_val_test
        )
        if self.initial_labeled_ratio>=0:
            self.simulated_dataset = True
        else:
            self.simulated_dataset = False

        self.root_dir = root_dir

        self.train_labeled_root_dir     = os.path.join(self.root_dir, "train")
        if self.simulated_dataset:
            self.train_unlabeled_root_dir   = os.path.join(self.root_dir, "train")
        else:
            self.train_unlabeled_root_dir   = os.path.join(self.root_dir,  "unlabeled")
        self.test_labeled_root_dir      = os.path.join(self.root_dir, "test")
        self.train_transforms = train_transforms
        self.train_transforms_unlabeled = (
            train_transforms_unlabeled
            if train_transforms_unlabeled is not None
            else train_transforms
        )
        self.val_transforms = val_transforms
        self.test_transforms = test_transforms
        self.return_unlabeled_trafos = return_unlabeled_trafos
        self.labeled_train_dataset: BaseClassificationDataset = None
        self.unlabeled_train_dataset: BaseClassificationDataset = None
        self.val_dataset: BaseClassificationDataset = None
        self.test_dataset: BaseClassificationDataset = None
        self.class_lst = class_lst
        self.samples_data_format = self._determine_data_format()
        self._init_datasets()
        if self.simulated_dataset:
            self.assign_labeled_unlabeled_split()

    def _init_datasets(self):
        self.labeled_train_dataset = BaseClassificationDataset(
            root_dir=self.train_labeled_root_dir, 
            class_lst= self.class_lst,
            transforms=self.train_transforms,
            samples_data_format=self.samples_data_format,
        )

        for _ in range(int(len(self.labeled_train_dataset) * (1 - self.dataset_size))):
            self.labeled_train_dataset.pop_sample(random.randrange(len(self.labeled_train_dataset)))

        self.val_dataset = BaseClassificationDataset(
            root_dir=self.train_labeled_root_dir, 
            class_lst= self.class_lst,
            transforms=self.val_transforms,
            empty_dataset=True,
            samples_data_format=self.samples_data_format,
        )

        self.unlabeled_train_dataset = BaseClassificationDataset(
            root_dir=self.train_unlabeled_root_dir,
            class_lst= self.class_lst,
            transforms=self.train_transforms,
            labels_available=False,
            empty_dataset=True if self.simulated_dataset else False,
            return_trafos=self.return_unlabeled_trafos,
            samples_data_format=self.samples_data_format,
        )
        
        self.test_dataset = BaseClassificationDataset(
            root_dir=self.test_labeled_root_dir, 
            class_lst= self.class_lst,
            transforms=self.test_transforms,
            samples_data_format=self.samples_data_format,
        )

    def _determine_data_format(self):
        extensions =  list()

        for sub_dir in os.listdir(self.train_labeled_root_dir):
            for file in os.listdir(os.path.join(self.train_labeled_root_dir, sub_dir)):
                extensions.append(os.path.splitext(file)[1].replace(".", ""))

        for sub_dir in os.listdir(self.test_labeled_root_dir):
            for file in os.listdir(os.path.join(self.test_labeled_root_dir, sub_dir)):
                extensions.append(os.path.splitext(file)[1].replace(".", ""))

        for file in os.listdir(self.train_unlabeled_root_dir):
            extensions.append(os.path.splitext(file)[1].replace(".", ""))
    
        return max(extensions, key = extensions.count)