import tifffile
import os
import numpy as np
import cv2
from skimage import io

from DLIP.data.base_classes.base_dataset import BaseDataset


class BaseClassificationDataset(BaseDataset):
    def __init__(
        self,
        root_dir: str,
        class_lst: list,
        samples_data_format="tif",
        transforms = None,
        empty_dataset=False,
        labels_available=True,
        return_trafos=False,
    ):
        self.labels_available   = labels_available
        self.root_dir           = root_dir
        self.class_lst          = class_lst
        self.samples_data_format = samples_data_format
        self.return_trafos      = return_trafos
        self.transforms         = transforms

        self.indices = list()
        self.labels  = list()

        if not empty_dataset:
            if self.labels_available:
                for idx, tick_class in enumerate(self.class_lst):
                    all_samples = os.listdir(os.path.join(self.root_dir,tick_class))
                    self.indices += [
                        i.split(f".{samples_data_format}")[0]
                        for i in all_samples
                    ]
                    self.labels += [idx]*len(all_samples)
            else:
                all_samples = os.listdir(os.path.join(self.root_dir))
                self.indices += [
                        i.split(f".{samples_data_format}")[0]
                        for i in all_samples
                    ]

        self.raw_mode = False

        if transforms is None:
                self.transforms = lambda x, y: (x,y,0)
        if isinstance(transforms, list):
            self.transforms = transforms
        else:
            self.transforms = [self.transforms]

        self.raw_mode = False

    def __len__(self):
        return len(self.indices)

    def __getitem__(self, idx):
        # load sample
        if self.labels_available:
            sample_path = os.path.join(self.root_dir, self.class_lst[self.labels[idx]], f"{self.indices[idx]}.{self.samples_data_format}")
            label_one_hot = np.zeros((1,len(self.class_lst)), dtype=np.float32)
            label_one_hot[self.labels[idx]] = 1.0
        else:
            sample_path = os.path.join(self.root_dir, f"{self.indices[idx]}.{self.samples_data_format}")

        sample_img  = tifffile.imread(sample_path) if (self.samples_data_format=="tif" or self.samples_data_format=="tiff") else io.imread(sample_path)

        sample_img_lst = []
        trafo_lst = []

        # raw mode -> no transforms
        if self.raw_mode:
            if self.labels_available:
                return sample_img,label_one_hot
            else:
                return sample_img
            
        for transform in self.transforms:
            im, _,trafo = transform(sample_img)
            sample_img_lst.append(im)
            trafo_lst.append(trafo)

        if len(sample_img_lst) == 1:
            sample_img_lst = sample_img_lst[0]
            trafo_lst = trafo_lst[0] if len(trafo_lst) > 0 else trafo_lst
       
        if not self.return_trafos and not self.labels_available:
            return sample_img_lst
        if self.return_trafos and not self.labels_available:
            return sample_img_lst, trafo_lst
        if not self.return_trafos and self.labels_available:
            return sample_img_lst, label_one_hot
        if self.return_trafos and self.labels_available:
            return sample_img_lst, label_one_hot, trafo_lst

    def pop_sample(self, index):
        sample = self.indices[index]
        del self.indices[index]
        if not self.labels_available:
            return sample
        else:
            label = self.labels[index]
            del self.labels[index]
            return sample, label

    def add_sample(self, sample, label):
        self.indices.append(sample)
        self.labels.append(label)

    def get_samples(self):
        return self.indices