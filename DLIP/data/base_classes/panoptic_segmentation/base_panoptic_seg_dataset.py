import glob
import os

import cv2
import numpy as np
import tifffile
from skimage import io
from albumentations import ReplayCompose

from DLIP.data.base_classes.base_dataset import BaseDataset


class BasePanopticSegmentationDataset(BaseDataset):
    def __init__(
        self,
        root_dir: str,
        instance_map_look_up: dict,
        semantic_map_look_up: dict,
        samples_dir: str = "samples",
        instance_labels_dir: str = "instance_labels",
        semantic_labels_dir: str = "semantic_labels",
        samples_data_format: str = "tif",
        instance_labels_data_format: str = "tif",
        semantic_labels_data_format: str = "tif",
        transforms=None,
        empty_dataset: bool = False,
        instance_labels_available: bool = True,
        semantic_labels_available: bool = True,
        return_trafos: bool = False,
        instance_label_suffix: str = "_instance_label",
        instance_label_prefix: str = "",
        semantic_label_suffix: str = "_semantic_label",
        semantic_label_prefix: str = "",
        use_instance_one_hot: bool = True,
        use_instance_dist_map: bool = True,
    ):
        self.instance_labels_available = instance_labels_available
        self.semantic_labels_available = semantic_labels_available
        self.root_dir = root_dir
        self.samples_dir = samples_dir
        self.instance_labels_dir = instance_labels_dir
        self.semantic_labels_dir = semantic_labels_dir
        self.samples_data_format = samples_data_format
        self.instance_labels_data_format = instance_labels_data_format
        self.semantic_labels_data_format = semantic_labels_data_format
        self.return_trafos = return_trafos
        self.transforms = transforms
        self.instance_map_look_up = instance_map_look_up
        self.semantic_map_look_up = semantic_map_look_up
        self.instance_label_suffix = instance_label_suffix
        self.instance_label_prefix = instance_label_prefix
        self.semantic_label_suffix = semantic_label_suffix
        self.semantic_label_prefix = semantic_label_prefix
        self.use_instance_one_hot = use_instance_one_hot
        self.use_instance_dist_map = use_instance_dist_map

        if transforms is None:
            self.transforms = lambda x, y: (x, y, 0)
        if isinstance(transforms, list):
            self.transforms = transforms
        else:
            self.transforms = [self.transforms]

        self.samples = os.path.join(self.root_dir, self.samples_dir)
        self.instance_labels = os.path.join(self.root_dir, self.instance_labels_dir)
        self.semantic_labels = os.path.join(self.root_dir, self.semantic_labels_dir)

        # Get all sample names sorted as integer values
        all_samples_sorted = sorted(
            glob.glob(f"{self.samples}{os.path.sep}*.{samples_data_format}"),
            key=lambda x: x.split(f"{self.samples}{os.path.sep}")[1].split(f".{samples_data_format}"),
        )
        self.indices = []
        if not empty_dataset:
            # Extract indices from the sorted samples
            self.indices = [
                i.split(f"{self.samples}{os.path.sep}")[1].split(f".{samples_data_format}")[0]
                for i in all_samples_sorted
            ]
        self.raw_mode = False

    def __len__(self):
        return len(self.indices)

    def __getitem__(self, idx):
        # load sample
        sample_path = os.path.join(self.samples, f"{self.indices[idx]}.{self.samples_data_format}")
        sample_img = tifffile.imread(sample_path) if self.samples_data_format == "tif" else io.imread(sample_path)

        sample_img_lst = []
        instance_label_lst = []
        semantic_label_lst = []
        trafo_lst = []

        if self.instance_labels_available:
            # load instance label map
            if self.use_instance_dist_map:
                instance_label_path = os.path.join(
                    self.instance_labels+"_dist_map",
                    f"{self.instance_label_prefix}{self.indices[idx]}{self.instance_label_suffix}.{self.instance_labels_data_format}",
                )
            else:
                instance_label_path = os.path.join(
                    self.instance_labels,
                    f"{self.instance_label_prefix}{self.indices[idx]}{self.instance_label_suffix}.{self.instance_labels_data_format}",
                )
            instance_label_img = (
                tifffile.imread(instance_label_path).squeeze()
                if self.instance_labels_data_format == "tif"
                else cv2.imread(instance_label_path, -1).squeeze()
            )

            if self.use_instance_one_hot:
                instance_label_one_hot = np.zeros(
                    (instance_label_img.shape[0], instance_label_img.shape[1], len(self.instance_map_look_up)),
                    dtype=np.float32,
                )
                for key, value in self.instance_map_look_up.items():
                    instance_label_one_hot[instance_label_img == value, int(key)] = 1.0
            else:
                # not the optimal naming
                if len(instance_label_img.shape) == 2:
                    instance_label_one_hot = np.expand_dims(instance_label_img.astype(float), axis=2)
                else:
                    instance_label_one_hot = instance_label_img.astype(float)
        else:
            instance_label_one_hot = np.zeros((sample_img.shape))

        if self.semantic_labels_available:
            # load semantic label map
            semantic_label_path = os.path.join(
                self.semantic_labels,
                f"{self.semantic_label_prefix}{self.indices[idx]}{self.semantic_label_suffix}.{self.semantic_labels_data_format}",
            )
            semantic_label_img = (
                tifffile.imread(semantic_label_path).squeeze()
                if self.semantic_labels_data_format == "tif"
                else cv2.imread(semantic_label_path, -1).squeeze()
            )
            semantic_label_one_hot = np.zeros(
                (semantic_label_img.shape[0], semantic_label_img.shape[1], len(self.semantic_map_look_up)),
                dtype=np.float32,
            )
            for key, value in self.semantic_map_look_up.items():
                semantic_label_one_hot[semantic_label_img == value, int(key)] = 1.0
        else:
            semantic_label_one_hot = np.zeros((sample_img.shape))

        # raw mode -> no transforms
        if self.raw_mode:
            if self.instance_labels_available and self.semantic_labels_available:
                return sample_img, instance_label_one_hot, semantic_label_one_hot
            elif self.instance_labels_available:
                return sample_img, instance_label_one_hot
            elif self.semantic_labels_available:
                return sample_img, semantic_label_one_hot
            else:
                return sample_img

        for transform in self.transforms:
            fused_labels = np.concatenate((instance_label_one_hot, semantic_label_one_hot), axis=2)
            im, fused_lbl, trafo = transform(sample_img.transpose((1, 2, 0)), fused_labels)

            instance_lbl = fused_lbl[:,:,0][:,:,None]
            semantic_lbl = fused_lbl[:,:,1:]

            sample_img_lst.append(im)
            instance_label_lst.append(instance_lbl)
            semantic_label_lst.append(semantic_lbl)
            trafo_lst.append(trafo)

        if len(sample_img_lst) == 1:
            sample_img_lst = sample_img_lst[0]
            instance_label_lst = instance_label_lst[0] if len(instance_label_lst) > 0 else instance_label_lst
            semantic_label_lst = semantic_label_lst[0] if len(semantic_label_lst) > 0 else semantic_label_lst
            trafo_lst = trafo_lst[0] if len(trafo_lst) > 0 else trafo_lst

        # sample_img_lst (optional: labels) (optional: trafos)
        if not self.return_trafos and not self.instance_labels_available and not self.semantic_labels_available:
            return sample_img_lst
        if self.return_trafos and not self.instance_labels_available and not self.semantic_labels_available:
            return sample_img_lst, trafo_lst
        if not self.return_trafos and self.instance_labels_available and not self.semantic_labels_available:
            return sample_img_lst, instance_label_lst
        if not self.return_trafos and not self.instance_labels_available and self.semantic_labels_available:
            return sample_img_lst, semantic_label_lst
        if self.return_trafos and self.instance_labels_available and not self.semantic_labels_available:
            return sample_img_lst, instance_label_lst, trafo_lst
        if self.return_trafos and not self.instance_labels_available and self.semantic_labels_available:
            return sample_img_lst, semantic_label_lst, trafo_lst
        if not self.return_trafos and self.instance_labels_available and self.semantic_labels_available:
            return sample_img_lst, instance_label_lst, semantic_label_lst
        if self.return_trafos and self.instance_labels_available and self.semantic_labels_available:
            return sample_img_lst, instance_label_lst, semantic_label_lst, trafo_lst

    def pop_sample(self, index):
        return self.indices.pop(index)

    def add_sample(self, new_sample):
        self.indices.append(new_sample)

    def get_samples(self):
        return self.indices

    def _update_label_maps(self):
        map_lst = list()
        for file in os.listdir(self.instance_labels):
            file_path = os.path.join(self.instance_labels, file)
            instance_label_img = (
                tifffile.imread(file_path) if self.labels_data_format == "tif" else cv2.imread(file_path, -1)
            )
            map_lst.extend(np.unique(instance_label_img))

        map_lst = sorted(map_lst)

        if len(self.instance_map_look_up) > 1:
            for i in range(len(self.instance_map_look_up)):
                if len(self.instance_map_look_up) != len(map_lst):
                    raise ValueError("Instance Label mapping incomplete")
                else:
                    self.instance_map_look_up[i] = map_lst[i]
        else:
            if len(len(self.instance_map_look_up)) == 0:
                raise ValueError("Instance Label mapping incomplete")
            else:
                self.instance_map_look_up[0] = map_lst[-1]

        map_lst = list()
        for file in os.listdir(self.semantic_labels):
            file_path = os.path.join(self.semantic_labels, file)
            semantic_label_img = (
                tifffile.imread(file_path) if self.labels_data_format == "tif" else cv2.imread(file_path, -1)
            )
            map_lst.extend(np.unique(semantic_label_img))

        map_lst = sorted(map_lst)

        if len(self.semantic_map_look_up) > 1:
            for i in range(len(self.semantic_map_look_up)):
                if len(self.semantic_map_look_up) != len(map_lst):
                    raise ValueError("Semantic Label mapping incomplete")
                else:
                    self.semantic_map_look_up[i] = map_lst[i]
        else:
            if len(len(self.semantic_map_look_up)) == 0:
                raise ValueError("Semantic Label mapping incomplete")
            else:
                self.semantic_map_look_up[0] = map_lst[-1]
