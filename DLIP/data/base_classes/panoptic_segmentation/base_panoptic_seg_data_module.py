import os
import random

import cv2
import numpy as np
import tifffile

from DLIP.data.base_classes.base_pl_datamodule import BasePLDataModule
from DLIP.data.base_classes.panoptic_segmentation.base_panoptic_seg_dataset import BasePanopticSegmentationDataset


class BasePanopticSegmentationDataModule(BasePLDataModule):
    def __init__(
        self,
        root_dir: str,
        n_classes: int,
        batch_size: int = 1,
        dataset_size: float = 1.0,
        val_to_train_ratio: float = 0,
        initial_labeled_ratio: float = 1.0,
        train_transforms=None,
        train_transforms_unlabeled=None,
        val_transforms=None,
        test_transforms=None,
        return_unlabeled_trafos: bool = False,
        num_workers: int = 0,
        pin_memory: bool = False,
        shuffle: bool = True,
        drop_last: bool = False,
        instance_map_look_up=None,
        samples_dir: str = "samples",
        instance_labels_dir: str = "instance_labels",
        instance_label_suffix: str = "_instance_label",
        instance_label_prefix: str = "",
        semantic_map_look_up=None,
        semantic_labels_dir: str = "semantic_labels",
        semantic_label_suffix: str = "_semantic_label",
        semantic_label_prefix: str = "",
        use_instance_one_hot: bool = True,
        use_instance_dist_map: bool = True,
        single_batch_val_test = False,
        **kwargs
    ):
        super().__init__(
            dataset_size=dataset_size,
            batch_size = batch_size,
            val_to_train_ratio = val_to_train_ratio,
            num_workers = num_workers,
            pin_memory = pin_memory,
            shuffle = shuffle,
            drop_last = drop_last,
            initial_labeled_ratio = initial_labeled_ratio,
            single_batch_val_test = single_batch_val_test
        )
        super().__init__(
            dataset_size=dataset_size,
            batch_size=batch_size,
            val_to_train_ratio=val_to_train_ratio,
            num_workers=num_workers,
            pin_memory=pin_memory,
            shuffle=shuffle,
            drop_last=drop_last,
            initial_labeled_ratio=initial_labeled_ratio,
        )
        if self.initial_labeled_ratio >= 0:
            self.simulated_dataset = True
        else:
            self.simulated_dataset = False

        self.root_dir = root_dir
        self.samples_dir = samples_dir
        self.instance_labels_dir = instance_labels_dir
        self.semantic_labels_dir = semantic_labels_dir

        self.train_labeled_root_dir = os.path.join(self.root_dir, "train")
        if self.simulated_dataset:
            self.train_unlabeled_root_dir = os.path.join(self.root_dir, "train")
        else:
            self.train_unlabeled_root_dir = os.path.join(self.root_dir, "unlabeled")
        self.test_labeled_root_dir = os.path.join(self.root_dir, "test")

        self.train_transforms = train_transforms
        self.train_transforms_unlabeled = (
            train_transforms_unlabeled
            if train_transforms_unlabeled is not None
            else train_transforms
        )
        self.val_transforms = val_transforms
        self.test_transforms = test_transforms
        self.return_unlabeled_trafos = return_unlabeled_trafos
        self.labeled_train_dataset: BasePanopticSegmentationDataset = None
        self.unlabeled_train_dataset: BasePanopticSegmentationDataset = None
        self.val_dataset: BasePanopticSegmentationDataset = None
        self.test_dataset: BasePanopticSegmentationDataset = None
        self.n_classes = n_classes
        (
            self.samples_data_format,
            self.instance_labels_data_format,
            self.semantic_labels_data_format,
        ) = self._determine_data_format()

        if instance_map_look_up is None or semantic_map_look_up is None:
            determined_instance_map_look_up, determined_semantic_map_look_up = self._determine_label_maps()
            if instance_map_look_up is None:
                instance_map_look_up = determined_instance_map_look_up
            if semantic_map_look_up is None:
                semantic_map_look_up = determined_semantic_map_look_up

        self.instance_map_look_up = instance_map_look_up
        self.semantic_map_look_up = semantic_map_look_up

        self.instance_label_suffix = instance_label_suffix
        self.instance_label_prefix = instance_label_prefix
        self.semantic_label_suffix = semantic_label_suffix
        self.semantic_label_prefix = semantic_label_prefix

        self.use_instance_one_hot = use_instance_one_hot
        self.use_instance_dist_map = use_instance_dist_map

        self._init_datasets()
        if self.simulated_dataset:
            self.assign_labeled_unlabeled_split()

    def _init_datasets(self):
        self.labeled_train_dataset = BasePanopticSegmentationDataset(
            root_dir=self.train_labeled_root_dir,
            transforms=self.train_transforms,
            samples_dir=self.samples_dir,
            instance_labels_dir=self.instance_labels_dir,
            semantic_labels_dir=self.semantic_labels_dir,
            samples_data_format=self.samples_data_format,
            instance_labels_data_format=self.instance_labels_data_format,
            semantic_labels_data_format=self.semantic_labels_data_format,
            instance_map_look_up=self.instance_map_look_up,
            semantic_map_look_up=self.semantic_map_look_up,
            instance_label_suffix=self.instance_label_suffix,
            instance_label_prefix=self.instance_label_prefix,
            semantic_label_suffix=self.semantic_label_suffix,
            semantic_label_prefix=self.semantic_label_prefix,
            use_instance_one_hot=self.use_instance_one_hot,
            use_instance_dist_map=self.use_instance_dist_map,
        )

        for _ in range(int(len(self.labeled_train_dataset) * (1 - self.dataset_size))):
            self.labeled_train_dataset.pop_sample(random.randrange(len(self.labeled_train_dataset)))

        self.val_dataset = BasePanopticSegmentationDataset(
            root_dir=self.train_labeled_root_dir,
            transforms=self.val_transforms,
            samples_dir=self.samples_dir,
            instance_labels_dir=self.instance_labels_dir,
            semantic_labels_dir=self.semantic_labels_dir,
            empty_dataset=True,
            samples_data_format=self.samples_data_format,
            instance_labels_data_format=self.instance_labels_data_format,
            semantic_labels_data_format=self.semantic_labels_data_format,
            instance_map_look_up=self.instance_map_look_up,
            semantic_map_look_up=self.semantic_map_look_up,
            instance_label_suffix=self.instance_label_suffix,
            instance_label_prefix=self.instance_label_prefix,
            semantic_label_suffix=self.semantic_label_suffix,
            semantic_label_prefix=self.semantic_label_prefix,
            use_instance_one_hot=self.use_instance_one_hot,
            use_instance_dist_map=self.use_instance_dist_map,
        )

        self.unlabeled_train_dataset = BasePanopticSegmentationDataset(
            root_dir=self.train_unlabeled_root_dir,
            transforms=self.train_transforms,
            samples_dir=self.samples_dir,
            instance_labels_dir=self.instance_labels_dir,
            semantic_labels_dir=self.semantic_labels_dir,
            instance_labels_available=False,
            semantic_labels_available=False,
            empty_dataset=True if self.simulated_dataset else False,
            return_trafos=self.return_unlabeled_trafos,
            samples_data_format=self.samples_data_format,
            instance_labels_data_format=self.instance_labels_data_format,
            semantic_labels_data_format=self.semantic_labels_data_format,
            instance_map_look_up=self.instance_map_look_up,
            semantic_map_look_up=self.semantic_map_look_up,
            instance_label_suffix=self.instance_label_suffix,
            instance_label_prefix=self.instance_label_prefix,
            semantic_label_suffix=self.semantic_label_suffix,
            semantic_label_prefix=self.semantic_label_prefix,
            use_instance_one_hot=self.use_instance_one_hot,
            use_instance_dist_map=self.use_instance_dist_map,
        )

        self.test_dataset = BasePanopticSegmentationDataset(
            root_dir=self.test_labeled_root_dir,
            transforms=self.test_transforms,
            samples_dir=self.samples_dir,
            instance_labels_dir=self.instance_labels_dir,
            semantic_labels_dir=self.semantic_labels_dir,
            samples_data_format=self.samples_data_format,
            instance_labels_data_format=self.instance_labels_data_format,
            semantic_labels_data_format=self.semantic_labels_data_format,
            instance_map_look_up=self.instance_map_look_up,
            semantic_map_look_up=self.semantic_map_look_up,
            instance_label_suffix=self.instance_label_suffix,
            instance_label_prefix=self.instance_label_prefix,
            semantic_label_suffix=self.semantic_label_suffix,
            semantic_label_prefix=self.semantic_label_prefix,
            use_instance_one_hot=self.use_instance_one_hot,
            use_instance_dist_map=False
        )

    def _determine_data_format(self):
        extensions = {
            self.samples_dir: list(),
            self.instance_labels_dir: list(),
            self.semantic_labels_dir: list(),
        }

        for folder in extensions.keys():
            for file in os.listdir(os.path.join(self.train_labeled_root_dir, folder)):
                # print(os.path.splitext(file)[1].replace(".", ""))
                extensions[folder].append(os.path.splitext(file)[1].replace(".", ""))

            for file in os.listdir(os.path.join(self.train_unlabeled_root_dir, folder)):
                extensions[folder].append(os.path.splitext(file)[1].replace(".", ""))

        if len(extensions[self.instance_labels_dir]) == 0:
            extensions[self.instance_labels_dir].append("tif")
        if len(extensions[self.semantic_labels_dir]) == 0:
            extensions[self.semantic_labels_dir].append("tif")

        return (
            max(set(extensions[self.samples_dir]), key=extensions[self.samples_dir].count),
            max(set(extensions[self.instance_labels_dir]), key=extensions[self.instance_labels_dir].count,),
            max(set(extensions[self.semantic_labels_dir]), key=extensions[self.semantic_labels_dir].count,),
        )

    def _determine_label_maps(self):
        map_lst = list()
        for file in os.listdir(os.path.join(self.train_labeled_root_dir, "instance_labels")):
            file_path = os.path.join(os.path.join(self.train_labeled_root_dir, "instance_labels"), file)
            instance_label_img = (
                tifffile.imread(file_path)
                if self.instance_labels_data_format == "tif"
                else cv2.imread(file_path, -1)
            )
            map_lst.extend(np.unique(instance_label_img))

        for file in os.listdir(os.path.join(self.train_unlabeled_root_dir, "instance_labels")):
            file_path = os.path.join(os.path.join(self.train_labeled_root_dir, "instance_labels"), file)
            instance_label_img = (
                tifffile.imread(file_path)
                if self.instance_labels_data_format == "tif"
                else cv2.imread(file_path, -1)
            )
            map_lst.extend(np.unique(instance_label_img))

        map_lst = sorted(map_lst)

        instance_map_look_up = dict()
        if self.n_classes > 1:
            if len(map_lst) < self.n_classes:
                for i in range(self.n_classes):
                    instance_map_look_up[i] = i
            for i in range(self.n_classes):
                if len(instance_map_look_up) != len(map_lst):
                    instance_map_look_up[i] = None
                else:
                    instance_map_look_up[i] = map_lst[i]
        else:
            if len(map_lst) == 0:
                instance_map_look_up[0] = None
            else:
                instance_map_look_up[0] = map_lst[-1]

        map_lst = list()
        for file in os.listdir(os.path.join(self.train_labeled_root_dir, "semantic_labels")):
            file_path = os.path.join(os.path.join(self.train_labeled_root_dir, "semantic_labels"), file)
            semantic_label_img = (
                tifffile.imread(file_path)
                if self.semantic_labels_data_format == "tif"
                else cv2.imread(file_path, -1)
            )
            map_lst.extend(np.unique(semantic_label_img))

        for file in os.listdir(os.path.join(self.train_unlabeled_root_dir, "semantic_labels")):
            file_path = os.path.join(os.path.join(self.train_labeled_root_dir, "semantic_labels"), file)
            semantic_label_img = (
                tifffile.imread(file_path)
                if self.semantic_labels_data_format == "tif"
                else cv2.imread(file_path, -1)
            )
            map_lst.extend(np.unique(semantic_label_img))

        map_lst = sorted(np.unique(map_lst))

        semantic_map_look_up = dict()
        if self.n_classes > 1:
            for i in range(self.n_classes):
                if i < len(map_lst):
                    semantic_map_look_up[i] = map_lst[i]
                else:
                    print(f"WARNING: class {i} has no corresponding value in the given dataset")
                    semantic_map_look_up[i] = None

        return instance_map_look_up, semantic_map_look_up
