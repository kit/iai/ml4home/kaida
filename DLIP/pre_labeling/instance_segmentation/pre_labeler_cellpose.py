from DLIP.pre_labeling.abstract_pre_labeling import AbstractPreLabeler
from cellpose import models

class Cellpose(AbstractPreLabeler):
    def __init__(
        self,
        use_gpu,    
        model_type
    ):
        super(Cellpose,self).__init__()
        self.model = models.Cellpose(gpu=use_gpu, model_type=model_type)

    def predict(self, img, status_bar):
        instances, _, _, _= self.model.eval(img, diameter=None, channels=[0,0])
        return instances