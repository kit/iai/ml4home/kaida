from DLIP.pre_labeling.segmentation.pre_labeler_percentile_seg import PreLabelerPercentileSeg
from skimage import measure

class PreLabelerPercentileInst(PreLabelerPercentileSeg):
    def predict(self, img, status_bar):
        pre_labeled_img = super().predict(img, status_bar)
        pre_labeled_img_inst = measure.label(pre_labeled_img, background=0)
        return pre_labeled_img_inst