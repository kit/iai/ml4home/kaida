import numpy as np
import torch
import cv2
from copy import deepcopy
from DLIP.utils.helper_functions.window_inference import window_inference
from DLIP.utils.helper_functions.gray_level_check import gray_redundand
from DLIP.pre_labeling.segmentation.pre_label_unet import UnetSemanticPreLabeler


class UnetInstancePreLabeler(UnetSemanticPreLabeler):
    def __init__(
        self,
        model_tar_path,   
        resize 
    ):
        super(UnetInstancePreLabeler,self).__init__(
            model_tar_path,
            resize
        )

    def predict(self, img, status_bar):
        if img.ndim>2 and gray_redundand(img):
            img = img[:,:,0]
            img = np.expand_dims(img, axis=-1)

        if len(img.shape)==2:
            img_shape_0 = img.shape
        else: 
            channel_id = np.argmin(img.shape)
            img_shape_0 = img.shape[:2] if channel_id==2 else img.shape[1:] 

        trafo = deepcopy(self.trafo)
        if not self.resize:
            trafo_names = [elem.__class__.__name__ for elem in trafo[0].transform["pre"].transforms.transforms]
            if "Resize" in trafo_names:
                ind = trafo_names.index("Resize")
                del trafo[0].transform["pre"].transforms.transforms[ind]

        input_tensor,_,_ = trafo[0](img)


        prediction_raw = window_inference(self.model,input_tensor, n_classes=1, window_size=512, use_gpu=torch.cuda.is_available()).squeeze().numpy()
        instances = self.model.post_pro.process(prediction_raw, None)
        instances = cv2.resize(instances, (img_shape_0[1],img_shape_0[0]), interpolation=cv2.INTER_NEAREST)
        return instances