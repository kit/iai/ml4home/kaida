from DLIP.pre_labeling.segmentation.pre_labeler_otsu_seg import PreLabelerOtsuSeg
from skimage import measure

class PreLabelerOtsuInst(PreLabelerOtsuSeg):
    def predict(self, img, status_bar):
        pre_labeled_img = super().predict(img, status_bar)
        pre_labeled_img_inst = measure.label(pre_labeled_img, background=0)
        return pre_labeled_img_inst