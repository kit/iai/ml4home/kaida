import numpy as np
import torch
import cv2
from copy import deepcopy
from DLIP.utils.helper_functions.window_inference import window_inference
from DLIP.utils.helper_functions.gray_level_check import gray_redundand
from DLIP.pre_labeling.segmentation.pre_label_unet import UnetSemanticPreLabeler
from DLIP.utils.data_preparation.label_pre_processing.centroid_post_pro import centroid_postprocessing
from DLIP.utils.metrics.seed_metrics import create_gt_area

class UnetSeedPreLabeler(UnetSemanticPreLabeler):
    def __init__(
        self,
        model_tar_path,   
        resize,
        seed_radius 
    ):
        super(UnetSeedPreLabeler,self).__init__(
            model_tar_path,
            resize
        )
        self.seed_radius = seed_radius

    def predict(self, img, status_bar):
        if img.ndim>2 and gray_redundand(img):
            img = img[:,:,0]
            img = np.expand_dims(img, axis=-1)

        if len(img.shape)==2:
            img_shape_0 = img.shape
        else: 
            channel_id = np.argmin(img.shape)
            img_shape_0 = img.shape[:2] if channel_id==2 else img.shape[1:] 

        trafo = deepcopy(self.trafo)
        if not self.resize:
            trafo_names = [elem.__class__.__name__ for elem in trafo[0].transform["pre"].transforms.transforms]
            if "Resize" in trafo_names:
                ind = trafo_names.index("Resize")
                del trafo[0].transform["pre"].transforms.transforms[ind]

        input_tensor,_,_ = trafo[0](img)

        prediction_raw = window_inference(self.model,input_tensor, n_classes=1, window_size=512, use_gpu=torch.cuda.is_available()).squeeze().numpy()

        prediction =  centroid_postprocessing(prediction_raw, do_binarize=True).astype(np.uint8)

        seeds = cv2.resize(prediction, (img_shape_0[1],img_shape_0[0]), interpolation=cv2.INTER_NEAREST)
        seeds = create_gt_area(centroid_postprocessing(seeds), gt_radius=self.seed_radius)

        seeds = (seeds>0).astype(np.uint8)

        return seeds