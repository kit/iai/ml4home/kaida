from DLIP.pre_labeling.abstract_pre_labeling import AbstractPreLabeler
import tarfile
import os
from copy import deepcopy
import numpy as np

from pytorch_lightning.utilities.seed import seed_everything
from DLIP.utils.loading.initialize_wandb import initialize_wandb
from DLIP.utils.loading.load_transforms import load_transforms
from DLIP.utils.loading.load_model import load_model
from DLIP.utils.loading.merge_configs import merge_configs
from DLIP.utils.loading.split_parameters import split_parameters
from DLIP.utils.helper_functions.window_inference import window_inference
from DLIP.utils.helper_functions.gray_level_check import gray_redundand
import cv2
import torch

class UnetSemanticPreLabeler(AbstractPreLabeler):
    def __init__(
        self,
        model_tar_path,    
        resize
    ):
        super(UnetSemanticPreLabeler,self).__init__()
        self.model, self.trafo = self._load_model_trafo_post_pro(model_tar_path)
        self.resize = resize

    def predict(self, img, status_bar):
        # if img.ndim>2 and gray_redundand(img):
        #     img = img[:,:,0]
        #     img = np.expand_dims(img, axis=-1)

        if len(img.shape)==2:
            img_shape_0 = img.shape
        else: 
            channel_id = np.argmin(img.shape)
            img_shape_0 = img.shape[:2] if channel_id==2 else img.shape[1:] 

        trafo = deepcopy(self.trafo)
        if not self.resize:
            trafo_names = [elem.__class__.__name__ for elem in trafo[0].transform["pre"].transforms.transforms]
            if "Resize" in trafo_names:
                ind = trafo_names.index("Resize")
                del trafo[0].transform["pre"].transforms.transforms[ind]

        input_tensor,_,_ = trafo[0](img)
        prediction = window_inference(self.model,input_tensor, n_classes=1, window_size=512, use_gpu=torch.cuda.is_available()).squeeze().numpy()
        prediction =  (prediction>0.5).astype(np.uint8)
        prediction = cv2.resize(prediction, (img_shape_0[1],img_shape_0[0]), interpolation=cv2.INTER_NEAREST)
        return prediction

    def _load_model_trafo_post_pro(self,model_tar_path):
        tar = tarfile.open(model_tar_path, "r:")
        base_path = os.path.dirname(model_tar_path)
        tar.extractall(path=base_path)
        tar.close()

        # load model
        cfg_yaml = merge_configs(os.path.join(base_path, "cfg.yaml"))

        config = initialize_wandb(
            cfg_yaml=cfg_yaml,
            disabled=True,
            experiment_dir=None,
            config_name=None,
        )

        seed_everything(seed=cfg_yaml['experiment.seed']['value'])
        parameters_splitted = split_parameters(config, ["model", "train", "data"])

        model = load_model(
            parameters_splitted["model"], 
            checkpoint_path_str=os.path.join(base_path, "dnn_weights.ckpt")
        )

        if torch.cuda.is_available():
            model.cuda()
        model.eval()

        _, _, test_trafos = load_transforms(parameters_splitted["data"])

        os.remove(os.path.join(base_path, "dnn_weights.ckpt"))
        os.remove(os.path.join(base_path, "cfg.yaml"))

        return model, test_trafos