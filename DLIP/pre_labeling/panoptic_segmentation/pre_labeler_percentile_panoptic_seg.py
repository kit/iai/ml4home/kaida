from DLIP.pre_labeling.abstract_pre_labeling import AbstractPreLabeler
import numpy as np
from DLIP.utils.helper_functions.gray_level_check import is_gray, gray_redundand
import cv2
from skimage import measure

class PreLabelerPercentilePanopticSeg(AbstractPreLabeler):
    def __init__(
        self,
        percentile_threshold,
        inverse=False
    ):
        super(PreLabelerPercentilePanopticSeg,self).__init__()
        self.percentile_threshold = percentile_threshold
        self.inverse = inverse

    def predict(self, img, status_bar):
        if not is_gray(img):
            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            status_bar.showMessage("Conversion to gray level image in order to do pre-labeling",)

        if gray_redundand(img):
            img = img [:,:,0].copy()

        if self.inverse:
            pre_labeled_img = cv2.threshold(
                    img,
                    np.percentile(img,self.percentile_threshold, interpolation='linear'),1,
                    cv2.THRESH_BINARY_INV)[1]
        else:
            pre_labeled_img = cv2.threshold(
                    img,
                    np.percentile(img,self.percentile_threshold, interpolation='linear'),1,
                    cv2.THRESH_BINARY)[1]

        return pre_labeled_img