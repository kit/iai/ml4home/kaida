from abc import ABC, abstractmethod
class AbstractPreLabeler(ABC):
    def __init__(self):
        super().__init__()

    @abstractmethod
    def predict(self, img, status_bar):
        raise NotImplementedError("Process fcn needs to be implemented")