from abc import abstractmethod

from DLIP.pre_labeling.abstract_pre_labeling import AbstractPreLabeler

from DLIP.utils.loading.initialize_wandb import initialize_wandb
from DLIP.utils.loading.load_transforms import load_transforms
from DLIP.utils.loading.load_model import load_model
from DLIP.utils.loading.merge_configs import merge_configs
from DLIP.utils.loading.split_parameters import split_parameters

class AbstractInferenceModule(AbstractPreLabeler):
    def __init__(self, 
        config_files,
        ckpt_path,
        use_gpu,
        window_size,
        ):
        super().__init__()
        self.use_gpu = use_gpu
        self.window_size = window_size

        cfg_yaml = merge_configs(config_files)

        config = initialize_wandb(
            cfg_yaml=cfg_yaml,
            disabled=True,
            experiment_dir=None,
            config_name=None,
        )

        parameters_splitted = split_parameters(config, ["model", "train", "data"])

        self.model = load_model(parameters_splitted["model"], checkpoint_path_str=ckpt_path)
        _, _, self.trafos = load_transforms(parameters_splitted["data"])

        if self.use_gpu:
            self.model.cuda()
        self.model.eval()

    def predict(self, img, status_bar=None):
        img_pre = self.pre_process(img)
        pred = self.inference(img_pre)
        pred_post = self.post_process(pred)
        return pred_post

    @abstractmethod
    def pre_process(self, img):
        raise NotImplementedError("Process fcn needs to be implemented")

    @abstractmethod
    def post_process(self, pred):
        raise NotImplementedError("Process fcn needs to be implemented")

    @abstractmethod
    def inference(self, img_pre):
        raise NotImplementedError("Process fcn needs to be implemented")