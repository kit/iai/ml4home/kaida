from DLIP.pre_labeling.abstract_pre_labeling import AbstractPreLabeler
import numpy as np

class PreLabelerPreviousLabel(AbstractPreLabeler):
    def __init__(
        self,
    ):
        super(PreLabelerPreviousLabel,self).__init__()
        self.previous_label = None
        self.remember_label = True

    def predict(self, img, status_bar):
        if self.previous_label is not None:
            return self.previous_label
        else:
            return np.zeros((img.shape[0],img.shape[1])).astype("uint8")