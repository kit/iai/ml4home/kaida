import torchvision.models as models
import torch
from torch import nn
from torchvision import transforms as T
import cv2
import numpy as np
import os
import tifffile

import matplotlib.pyplot as plt

if __name__ == "__main__":
    img_ref = cv2.imread("/home/ws/sc1357/data/2017_ISIC_Derma/train/samples/ISIC_0015260.jpg")

    pretrained_model = models.resnet18(pretrained=True)

    normalize = T.Normalize(mean=[0.485, 0.456, 0.406],
                                    std=[0.229, 0.224, 0.225])

    transform = T.Compose([T.ToPILImage(), T.Resize(224), T.ToTensor(), normalize])

    feature_extractor = nn.Sequential(*list(pretrained_model.children())[:-1])
    feature_extractor.eval()

    feat_ref = feature_extractor(transform(img_ref).unsqueeze(0)).squeeze().detach().numpy()
    name_list = list()
    feature_list = list()

    for file in os.listdir("/home/ws/sc1357/data/2017_ISIC_Derma/train/samples/"):
        img_nb = cv2.imread(os.path.join("/home/ws/sc1357/data/2017_ISIC_Derma/train/samples/", file))
        if img_nb is not None:
            feat_nb = feature_extractor(transform(img_nb).unsqueeze(0)).squeeze().detach().numpy()
            feature_list.append(np.dot(feat_ref, feat_nb)/(np.linalg.norm(feat_ref)*np.linalg.norm(feat_nb)))
            name_list.append(file)

    
    n = 5
    feature_list = np.array(feature_list)
    name_list = np.array(name_list)

    # similar
    print(name_list[np.argsort(feature_list)[:n]])

    # dissimilar
    print(name_list[np.argsort(-feature_list)[:n]])

    # img = tifffile.imread("/home/ws/sc1357/data/2021_06_25_CLL_Cells/2021_12_03_CLL_Instance_Detection/Hoechst/L1_ICC_RAW_ch00.tif")[300:600,300:600]

    # img = random_noise(img, var=0.0000005)

    # plt.imshow(img, cmap="gray")
    # plt.show()

    # # Estimate the average noise standard deviation across color channels.
    # sigma_est = estimate_sigma(img, average_sigmas=True)

    # img_denoise = denoise_bilateral(img,  sigma_color=sigma_est*3)

    # fig, ax = plt.subplots(1,2)
    # ax[0].imshow(img, cmap="gray")
    # ax[1].imshow(img_denoise, cmap="gray")
    # plt.show()


    
