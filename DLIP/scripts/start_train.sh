export RESULT_DIR="/home/ws/sc1357/data/dma_spheroid_test"
export CFG_BASE="/home/ws/sc1357/projects/devel/src/kaida/DLIP/experiments/configurations/pre-anno-test/dma_cells_fl.yaml"

python train.py --config_files "\
$CFG_BASE \
" \
--result_dir $RESULT_DIR