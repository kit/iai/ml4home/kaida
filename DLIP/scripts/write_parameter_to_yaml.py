import yaml
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Update cfg file')
    parser.add_argument('--cfg_file_path')
    parser.add_argument('--dataset_path')
    parser.add_argument('--device')

    args = parser.parse_args()

    dataset_path = args.dataset_path
    cfg_file_path = args.cfg_file_path
    device = args.device

    with open(cfg_file_path, "r") as file_descriptor:
        cfg_file = yaml.safe_load(file_descriptor)

    if device == "local"  :
        cfg_file["data.datamodule.root_dirs"]["value"]["local"] = dataset_path
    else:
        cfg_file["data.datamodule.root_dirs"]["value"]["horeka"] = dataset_path

    with open(cfg_file_path, 'w') as file_descriptor:
        yaml.safe_dump(cfg_file, file_descriptor)