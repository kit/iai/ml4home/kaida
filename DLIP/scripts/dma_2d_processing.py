import os
import numpy as np
import pandas as pd

import tifffile
import cv2

from DLIP.pre_labeling.instance_segmentation import UnetInstancePreLabeler
import yaml
from skimage.measure import label, regionprops
from skimage.color import label2rgb
import matplotlib.pyplot as plt
from DLIP.utils.visualization.inst_seg_contour import visualize_instances_map

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)



# if __name__ == "__main__":
#     tar_path = "/home/ws/sc1357/data/Maryam-HaCaTcells/results/20220825_1007_0018_dnn_model.tar"
#     unet = UnetInstancePreLabeler(model_tar_path=tar_path, resize=False)

#     processing_dir = "/home/ws/sc1357/lsdf/levkin-screening/Maryam for Marcel/SCC25 cells/RAW/"

#     result_dir = os.path.join(processing_dir, "results")

#     if not os.path.exists(result_dir):
#         os.makedirs(result_dir)

#     for file in os.listdir(processing_dir):
#         img_path = os.path.join(processing_dir, file)
#         img_name, ext = os.path.splitext(os.path.basename(img_path))

#         if ext != ".tif" or "ch03" in img_name or  "ch00" in img_name or  "ch02" in img_name:
#             continue
#         img = tifffile.imread(img_path)
#         pred = unet.predict(img,None)
#         mask = label(pred)
#         regions =  regionprops(mask)

#         properties = {
#             "num_instances": None, 
#         }


#         properties["num_instances"] = int(len(regions))

#         overlay = visualize_instances_map(cv2.cvtColor(normalize(img), cv2.COLOR_GRAY2RGB),mask)

#         tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), overlay)

#         with open(os.path.join(result_dir, img_name+"_props.yaml"), 'w') as f:
#             yaml.dump(properties, f)
        




if __name__ == "__main__":

    processing_dir = "/home/ws/sc1357/lsdf/levkin-screening/Maryam for Marcel/SCC25 cells/RAW/"

    result_dir = os.path.join(processing_dir, "results")

    summary = pd.DataFrame()
    

    for file in os.listdir(result_dir):
        file_path = os.path.join(result_dir, file)
        file_name, ext = os.path.splitext(os.path.basename(file_path))

        if ext != ".yaml":
            continue
    
        with open(file_path, 'r') as f:
            properties= yaml.safe_load(f)
            properties["name"] = file_name.replace("_props", "")

        print(properties)
        
        summary = summary.append(properties, ignore_index=True)

    summary = summary.set_index("name")
    summary.to_csv(os.path.join(result_dir, "summary.csv"))