import logging
import os
import tarfile

import wandb
from pytorch_lightning.utilities.seed import seed_everything

from DLIP.utils.cross_validation.cv_trainer import CVTrainer
from DLIP.utils.helper_functions.coupling import create_model_package
from DLIP.utils.loading.initialize_wandb import initialize_wandb
from DLIP.utils.loading.load_data_module import load_data_module
from DLIP.utils.loading.load_model import load_model
from DLIP.utils.loading.load_trainer import load_trainer
from DLIP.utils.loading.merge_configs import merge_configs
from DLIP.utils.loading.parse_arguments import parse_arguments
from DLIP.utils.loading.prepare_directory_structure import prepare_directory_structure
from DLIP.utils.loading.split_parameters import split_parameters


def run():
    logging.basicConfig(level=logging.INFO)
    logging.info("Initalizing model")

    args = parse_arguments()
    config_files, result_dir = args["config_files"], args["result_dir"]

    cfg_yaml = merge_configs(config_files)
    base_path = os.path.expandvars(result_dir)
    experiment_name = cfg_yaml['experiment.name']['value']

    experiment_dir, config_name = prepare_directory_structure(
        base_path=base_path,
        experiment_name=experiment_name,
        data_module_name=cfg_yaml['data.datamodule.name']['value'],
        model_name=cfg_yaml['model.name']['value']
    )

    config = initialize_wandb(
        cfg_yaml=cfg_yaml,
        experiment_dir=experiment_dir,
        config_name=config_name
    )

    seed_everything(seed=config['experiment.seed'])
    parameters_splitted = split_parameters(config, ["model", "train", "data"])

    if "train.aem.method" in config.keys():
        if config["train.aem.method"] == "pretrained_model":
            if config["train.aem.method.params.path"].endswith('.tar'):
                tar = tarfile.open(config["train.aem.method.params.path"], "r:")
                base_path = os.path.dirname(config["train.aem.method.params.path"])
                tar.extractall(path=base_path)
                tar.close()
                ckpt_path = os.path.join(base_path, "dnn_weights.ckpt")
            else:
                ckpt_path = config["train.aem.method.params.path"]
        else:
            ckpt_path = None
    else:
        ckpt_path = None

    model = load_model(parameters_splitted["model"], checkpoint_path_str=ckpt_path)

    if "train.aem.method" in config.keys():
        if config["train.aem.method"] == "pretrained_model":
            if config["train.aem.method.params.path"].endswith('.tar'):
                os.remove(os.path.join(base_path, "dnn_weights.ckpt"))
                os.remove(os.path.join(base_path, "cfg.yaml"))

    data = load_data_module(parameters_splitted["data"])

    data.labeled_train_dataset.indices = data.labeled_train_dataset.indices*30

    trainer = load_trainer(parameters_splitted['train'], experiment_dir, wandb.run.name, data)

    if 'train.cross_validation.n_splits' in cfg_yaml:
        cv_trainer = CVTrainer(
            trainer=trainer,
            n_splits=cfg_yaml['train.cross_validation.n_splits']['value']
        )
        cv_trainer.fit(model=model, datamodule=data)
    else:
        trainer.fit(model, data)
        try:
            model = load_model(
                parameters_splitted["model"],
                checkpoint_path_str=trainer.checkpoint_callback.best_model_path
            )
            _ = trainer.test(model=model, dataloaders=data.test_dataloader())
        except Exception:
            logging.info("Testing was not successful!")
    wandb.finish()

    create_model_package(result_dir, experiment_dir, cfg_yaml, config_name)


if __name__ == "__main__":
    run()
