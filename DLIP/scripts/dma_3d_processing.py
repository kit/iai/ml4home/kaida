import os
import numpy as np
import pandas as pd

import tifffile
import cv2

from DLIP.pre_labeling.segmentation.pre_label_unet import UnetSemanticPreLabeler
import yaml
from skimage.measure import label, regionprops
from skimage.color import label2rgb
import matplotlib.pyplot as plt

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)



# if __name__ == "__main__":
#     tar_path = "/home/ws/sc1357/data/datasets/2022_DMA_Maryam_3D_Staining_annotated/results/20220810_1244_0000_dnn_model.tar"
#     unet = UnetSemanticPreLabeler(model_tar_path=tar_path)

#     processing_dir = "/home/ws/sc1357/lsdf/levkin-screening/Maryam for Marcel/3D/raw/TileScan 2/"

#     result_dir = os.path.join(processing_dir, "results")

#     if not os.path.exists(result_dir):
#         os.makedirs(result_dir)

#     for file in os.listdir(processing_dir):
#         img_path = os.path.join(processing_dir, file)
#         img_name, ext = os.path.splitext(os.path.basename(img_path))

#         if ext != ".tif" or "ch03" in img_name:
#             continue
#         img = tifffile.imread(img_path)
#         pred = unet.predict(img,None)
#         mask = label(pred)
#         regions =  sorted(regionprops(mask), key=lambda r: r.area, reverse=True) 

#         properties = {
#             "num_instances": None, "area": None, "axis_major_length": None, "axis_minor_length": None, 
#             "mean": None, "std": None, "norm_std": None, "norm_edge_ratio": None,
#         }
#         properties["num_instances"] = len(regions)
#         if properties["num_instances"] > 0:
#             for props in regions:
#                 properties["area"] = int(props.area)
#                 properties["axis_major_length"] = props.axis_major_length
#                 properties["axis_minor_length"] = props.axis_minor_length
#                 mask[mask!=props.label] = 0
#                 break
#             properties["mean"], properties["std"] = float(np.mean(img[mask>0])), float(np.std(img[mask>0]))
#             properties["norm_std"] = float(properties["std"]/properties["mean"])
#             img_uint8 = (img/255).astype("uint8")
#             edge = cv2.Canny(img_uint8,60/255*np.max(img_uint8),120/255*np.max(img_uint8))
#             edge = cv2.morphologyEx(edge, cv2.MORPH_CLOSE, np.ones((3,3)), iterations=3)
#             properties["norm_edge_ratio"] = float(np.sum(edge[mask>0]>0)/properties["area"])
#             # print(properties)
#             # fig, ax = plt.subplots(1,3)

#             # ax[0].imshow(edge)
#             # ax[1].imshow(img)
#             # ax[2].imshow(mask>0)
#             # plt.show()

            

#         else:
#             properties["area"], properties["axis_major_length"], properties["axis_minor_length"], \
#             properties["mean"], properties["std"],properties["norm_edge_ratio"], properties["norm_std"]  = -1, -1, -1, -1, -1,-1, -1


 
#         overlay = label2rgb(mask>0, image=normalize(img))

#         tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), (255*overlay).astype(np.uint8))

#         with open(os.path.join(result_dir, img_name+"_props.yaml"), 'w') as f:
#             yaml.dump(properties, f)
        




if __name__ == "__main__":

    processing_dir = "/home/ws/sc1357/lsdf/levkin-screening/Maryam for Marcel/3D/raw/TileScan 2/"

    result_dir = os.path.join(processing_dir, "results")

    summary = pd.DataFrame()
    

    for file in os.listdir(result_dir):
        file_path = os.path.join(result_dir, file)
        file_name, ext = os.path.splitext(os.path.basename(file_path))

        if ext != ".yaml":
            continue
    
        with open(file_path, 'r') as f:
            properties= yaml.safe_load(f)
            properties["name"] = file_name.replace("_props", "")

        print(properties)
        
        summary = summary.append(properties, ignore_index=True)

    summary = summary.set_index("name")
    summary.to_csv(os.path.join(result_dir, "summary.csv"))

