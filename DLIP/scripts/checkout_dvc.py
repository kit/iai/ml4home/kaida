import os
import argparse
from DLIP.data.version_control.dvc_wrapper.dvc_project import DvcProject
from pathlib import Path

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Checkout dvc data')
    parser.add_argument('--dataset_path')

    args = parser.parse_args()

    dvc_proj = DvcProject(
                external_path = str(Path(args.dataset_path)),
                files_to_track = list(),
                project_directory = os.path.dirname(str(Path(args.dataset_path))) ,
                do_init = False,
                use_git = False
    )

    version_file = os.path.join(str(Path(args.dataset_path)), ".dvc_version")
    with open("sample.txt", "r") as file:
        version = file.readline()


    dvc_proj.checkout_version(version)