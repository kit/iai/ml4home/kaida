import logging
import os
import platform
from pathlib import Path
import sys
from argparse import Namespace

from PyQt5 import QtCore, QtWidgets

from DLIP.gui.backend.utils import SplashScreen


def get_initial_ui():
    path_project = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

    path_parent = os.path.dirname(os.path.dirname(path_project))

    seg_annotation_tool_properties = {
        "dir": {"Linux": "ImageLabelingTool_Ubuntu", "Windows": "ImageLabelingTool"},
        "exec": {"Linux": "ImageLabelingTool.sh", "Windows": "ImageLabelingTool.exe"},
    }

    path_seg_annotation_tool = os.path.join(
        path_parent,
        seg_annotation_tool_properties["dir"][platform.system()],
        seg_annotation_tool_properties["exec"][platform.system()]
    )

    task_property_seg = Namespace(**{
        "name": "Segmentation",
        "folder_name": "segmentation",
        "gui_name": "AssistedLabelingSegmentationGui",
        "datamodule_name": "BaseSegmentationDataModule"
    })

    task_property_inst_seg = Namespace(**{
        "name": "Instance Segmentation",
        "folder_name": "instance_segmentation",
        "gui_name": "AssistedLabelingInstanceSegmentationGui",
        "datamodule_name": "BaseInstanceSegmentationDataModule"
    })

    task_property_panoptic_seg = Namespace(**{
        "name": "Panoptic Segmentation",
        "folder_name": "panoptic_segmentation",
        "gui_name": "AssistedLabelingPanopticSegmentationGui",
        "datamodule_name": "BasePanopticSegmentationDataModule"
    })

    task_property_class = Namespace(**{
        "name": "Classification",
        "folder_name": "classification",
        "gui_name": "AssistedLabelingClassificationGui",
        "datamodule_name": "BaseClassificationDataModule"
    })

    task_property_seed = Namespace(**{
        "name": "Seed Detection",
        "folder_name": "seed_detection",
        "gui_name": "AssistedLabelingSeedDetectionGui",
        "datamodule_name": "BaseSeedDetectionDataModule"
    })

    task_property_obj_detect = Namespace(**{
        "name": "Object Detection",
        "folder_name": "object_detection",
        "gui_name": "AssistedLabelingObjectDetectionGui",
        "datamodule_name": "BaseObjectDetectionDataModule"
    })

    task_properties = {
        "segmentation": task_property_seg,
        "instance_segmentation": task_property_inst_seg,
        "panoptic_segmentation": task_property_panoptic_seg,
        "classification": task_property_class,
        "seed_detection": task_property_seed,
        "object_detection": task_property_obj_detect,
    }

    return initialUi(
        path_project,
        path_seg_annotation_tool,
        task_properties,
    )


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    if QtCore.QCoreApplication.instance() is not None:
        app = QtCore.QCoreApplication.instance()
    else:
        app = QtWidgets.QApplication(sys.argv)

    splash_screen_img = str(Path(__file__).parents[2].joinpath(
        "img").joinpath("kaida_logo.png"))
    splash_screen = SplashScreen(splash_screen_img)
    splash_screen.show()

    # Doing time-consuming imports (even when not needed here) while showing a splash screen
    # This way, it's not slowing down the program somewhere else.

    from DLIP.gui.backend.initial import initialUi
    from DLIP.data.base_classes.base_pl_datamodule import BasePLDataModule
    from DLIP.gui.backend.label_assistant.base_assisted_labeling_masks import BaseAssistedLabelingGuiMasks
    from DLIP.sampler.generic import *

    init_gui = get_initial_ui()

    splash_screen.close()

    init_gui.show()

    sys.exit(app.exec_())