#!/bin/bash
#SBATCH --partition=haicore-gpu4
#SBATCH --time=48:00:00
#SBATCH --nodes=1
#SBATCH --gres=gpu:1
#SBATCH --mail-type=ALL
#SBATCH --mail-user=marcel.schilling@kit.edu
#SBATCH --error=%j_error.txt
#SBATCH --output=%j_output.txt
#SBATCH --job-name=first_shot
#SBATCH --constraint=LSDF

# INPUT FROM JOB SUBMISSION
# -> DATASET, RESULT_DIR, MODEL

# remove all modules
module purge
module load compiler/intel/19.1 mpi/openmpi/4.0

# activate cuda
module load devel/cuda/11.2

# activate conda env
source /home/hk-project-sppo/sc1357/miniconda3/etc/profile.d/conda.sh
conda activate kaida

# move to script dir
cd /home/hk-project-sppo/sc1357/devel/kaida/DLIP/scripts

# checkout dvc 
if [ "$DVC" = "1" ]; then
    python checkout_dvc.py --dataset_path $DATASET
fi

export CFG_FILE="/home/hk-project-sppo/sc1357/devel/kaida/DLIP/experiments/configurations/models/{${MODEL}.yaml}"
# update cfg file
python write_parameter_to_yaml.py --cfg_file_path $CFG_FILE --dataset_path $DATASET

# generate dist maps
if [ "$MODEL" = "instance_segmentation/u_net" ]; then
    cd /home/hk-project-sppo/sc1357/devel/kaida/DLIP/utils/data_preparation/label_pre_processing
    python generate_distance_maps.py --dataset_path $DATASET
    cd /home/hk-project-sppo/sc1357/devel/kaida/DLIP/scripts
fi

export CFG_ADD="${DATASET}/train_cfg/selected_train_cfg.yaml}"
# start train
python train.py --config_files "$CFG_FILE $CFG_ADD" --result_dir $RESULT_DIR