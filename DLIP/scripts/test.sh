export CFG_BASE="/home/ws/sc1357/projects/devel/src/kaida/DLIP/experiments/configurations/models/instance_segmentation/u_net_base.yaml"     
export CFG_ADD="/home/ws/sc1357/data/2022_DMA_Spheroid_Detection_split/train_cfg/selected_train_cfg.yaml"   
export RESULT_DIR="/home/ws/sc1357/data/2022_DMA_Spheroid_Detection_split/results/"    

python /home/ws/sc1357/projects/devel/src/kaida/DLIP/scripts/train.py --config_files "$CFG_BASE $CFG_ADD" --result_dir $RESULT_DIR