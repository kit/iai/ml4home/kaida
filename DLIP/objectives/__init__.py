"""
    Objectives to be used must be specified here to be loadable.
"""
from .bce_dice_loss import BceDiceLoss
from .dice_loss import DiceLoss
from .weighted_dice_loss import WeightedDiceLoss
