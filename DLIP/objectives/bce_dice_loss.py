import torch
import torch.nn as nn
from DLIP.objectives.dice_loss import DiceLoss


class BceDiceLoss(nn.Module):
    """ Combination of BCE and Dice Loss. 

    """
    def __init__(self, alpha=0.5, eps=1e-6) -> None:
        super(BceDiceLoss, self).__init__()
        self.eps = eps
        self.alpha = alpha
        self.dice_loss = DiceLoss()
        self.bce_loss = nn.BCELoss()


    def forward(
            self,
            y_pred: torch.Tensor,
            y_true: torch.Tensor
        ):
        """
        :param y_pred: NxCxHxW
        :param y_true: NxHxW
        :return: scalar
        """
        return self.bce_loss.forward(y_pred, y_true) + self.alpha*self.dice_loss.forward(y_pred, y_true)