from typing import List

import torch
import torch.nn as nn


class WeightedDiceLoss(nn.Module):
    """Default Dice Loss."""

    def __init__(self, weights: List[float], eps=1e-6, device="cuda" if torch.cuda.is_available() else "cpu") -> None:
        super(WeightedDiceLoss, self).__init__()
        self.weights = torch.tensor(weights, device=device)
        self.eps = eps

    def forward(self, y_pred: torch.Tensor, y_true: torch.Tensor):
        """
        :param y_pred: NxCxHxW
        :param y_true: NxCxHxW
        :return: scalar
        """
        batch_size = y_true.size(0)
        num_channels = y_true.size(1)
        dims = (0, 2)  # sum over batch dim and image dimensions (not channels/classes)

        if num_channels != len(self.weights):
            raise ValueError(f"Got input with {num_channels} channels but weightings for {len(self.weights)} classes!")

        y_true = y_true.view(batch_size, num_channels, -1)  # NxCx(H*W)
        y_pred = y_pred.view(batch_size, num_channels, -1)  # NxCx(H*W)

        intersection = torch.sum(y_pred * y_true, dims)  # intersection per channel/class
        cardinality = torch.sum(y_pred + y_true, dims)  # cardinality per channel/class

        dice_score = (2.0 * intersection + self.eps) / (cardinality + self.eps)
        weighted_dice_loss = self.weights * (1.0 - dice_score)
        return torch.mean(weighted_dice_loss)