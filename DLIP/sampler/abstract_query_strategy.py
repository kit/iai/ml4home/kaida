from abc import ABC, abstractmethod
from DLIP.data.base_classes.base_dataset import BaseDataset

class AbstractQueryStrategy(ABC):
    @abstractmethod
    def get_samples_to_label(self, unlabeled_dataset: BaseDataset, labeled_dataset: BaseDataset, num_samples:int, **kwargs):
        raise NotImplementedError("Rank Samples to Label needs to be implemented.")