import torchvision.models as models
from torch import nn
import torch
from torchvision import transforms as T
import numpy as np

from DLIP.data.base_classes.base_dataset import BaseDataset
from DLIP.sampler.abstract_query_strategy import (
    AbstractQueryStrategy,
)

import cv2
from sklearn.metrics import pairwise_distances
from sklearn.decomposition import PCA
from tqdm import tqdm

class HeterogenitySelectorResNet(AbstractQueryStrategy):
    def __init__(self, resnet_name: str, metric: str, pca_dim=None, **kwargs):
        super().__init__()
        if resnet_name=="resnet50":
            pretrained_model = models.resnet50(pretrained=True)
        else:
            pretrained_model = models.resnet18(pretrained=True)

        self.feature_extractor = nn.Sequential(*list(pretrained_model.children())[:-1])
        self.feature_extractor.eval()

        if torch.cuda.is_available():
            self.feature_extractor.cuda() 
        
        normalize = T.Normalize(mean=[0.485, 0.456, 0.406],
                                    std=[0.229, 0.224, 0.225])

        self.transform = T.Compose([T.ToPILImage(), T.Resize(224), T.ToTensor(), normalize])
        self.metric = metric
        self.pca_dim = pca_dim
        self.features = None
        self.already_selected = list()
        self.min_distances = None

    def _calculate_features(self, unlabeled_dataset: BaseDataset, labeled_dataset: BaseDataset):
        unlabeled_dataset.set_raw_mode(True)
        for i in range(len(unlabeled_dataset)):
            img = unlabeled_dataset[i]

            features_temp = self._img_to_feature(img)
            if i == 0:
                self.features = features_temp.copy()
            else:
                self.features = np.vstack([self.features,features_temp.copy()])

        labeled_dataset.set_raw_mode(True)
        for i in tqdm(range(len(labeled_dataset)), desc="Calc features"):
            img,_ = labeled_dataset[i]
            features_temp = self._img_to_feature(img)
            self.features = np.vstack([self.features,features_temp.copy()])
   
        if self.pca_dim is not None:
            self._pca_compression()

    def _img_to_feature(self, img):
        if len(img.shape) < 3 or img.shape[2]==1:
            if not(img.dtype == np.dtype("uint8") or img.dtype == np.uint8):
                img = cv2.convertScaleAbs(img, alpha=255/65535)
        
            img = cv2.cvtColor(img,cv2.COLOR_GRAY2RGB)

        with torch.no_grad():
            input_tensor = self.transform(img).unsqueeze(0).cuda()  if torch.cuda.is_available() else self.transform(img).unsqueeze(0)
            features_temp = self.feature_extractor(input_tensor).squeeze().detach().cpu().numpy()
    
        return features_temp
            
    def _pca_compression(self):
        features_hdim = self.features
        n_components = min(features_hdim.shape[0], self.pca_dim)
        pca = PCA(n_components=n_components)
        features_ldim = pca.fit_transform(features_hdim)
        self.features = features_ldim.copy()
        
    def update_distances(self, cluster_centers, only_new=True, reset_dist=False):
        """Update min distances given cluster centers.
        Args:
        cluster_centers: indices of cluster centers
        only_new: only calculate distance for newly selected points and update
            min_distances.
        rest_dist: whether to reset min_distances.
        """

        if reset_dist:
            self.min_distances = None
        if only_new:
            cluster_centers = [d for d in cluster_centers if d not in self.already_selected]
        if cluster_centers:
            # Update min_distances for all examples given new cluster center.
            x = self.features[cluster_centers]
            dist = pairwise_distances(self.features, x, metric=self.metric)

            if self.min_distances is None:
                self.min_distances = np.min(dist, axis=1).reshape(-1,1)
            else:
                self.min_distances = np.minimum(self.min_distances, dist)

        
    def get_samples_to_label(self, unlabeled_dataset: BaseDataset, labeled_dataset: BaseDataset, num_samples: int):
        self.all_samples = unlabeled_dataset.get_samples().copy()
        self.all_samples.extend(labeled_dataset.get_samples().copy())

        self._calculate_features(unlabeled_dataset, labeled_dataset)

        self.update_distances(
            [ix for ix in range(len(unlabeled_dataset),self.features.shape[0])], 
            only_new=False, 
            reset_dist=True)

        self.already_selected = list(range(len(unlabeled_dataset),self.features.shape[0]))

        new_batch = []

        for _ in tqdm(range(num_samples), desc="Determine sampling order"):
            if len(self.already_selected)==0:
                # Initialize centers with a randomly selected datapoint
                ind = np.random.choice(np.arange(len(unlabeled_dataset)))
            else:
                ind = np.argmax(self.min_distances)
            # New examples should not be in already selected since those points
            # should have min_distance of zero to a cluster center.
            assert ind not in self.already_selected

            self.update_distances([ind], only_new=True, reset_dist=False)
            self.already_selected.append(ind)
            new_batch.append(self.all_samples[ind])

        return new_batch