#GUI for selecting images

from DLIP.sampler.abstract_query_strategy import (
    AbstractQueryStrategy,
)


from PIL import Image
from PyQt5 import QtWidgets, uic, QtGui, QtCore
import os
import tifffile
from skimage import io
from DLIP.utils.helper_functions.create_qimage import array_to_qimage
import numpy as np
from DLIP.pre_img_processing.generic.img_pre_pro_normalize import Normalize

class SelectionWindow(QtWidgets.QDialog):
    def __init__(self,samples,path_to_dataset,data_type):
        super(SelectionWindow, self).__init__()
        currFilepath = os.path.dirname(os.path.realpath(__file__))
        uiFile=currFilepath + os.path.sep + 'cherry_picking_gui.ui'
        uic.loadUi(uiFile, self)

        self.samples=samples
        self.path =path_to_dataset
        self.data_type = data_type
        self.ls_imNumPage = list()
        self.ranking_dict = dict()

        self.ls_label=list()
        self.ls_graphic=list()
        self.ls_check=list()
        self.ls_scene=list()
        for i in range(1,25):
            itemName='label_'+str(i)
            self.ls_label.append(self.findChild(QtWidgets.QLabel,itemName))
            itemName='graphicsView_'+str(i)
            self.ls_graphic.append(self.findChild(QtWidgets.QGraphicsView,itemName))
            itemName='checkBox_'+str(i)
            self.ls_check.append(self.findChild(QtWidgets.QCheckBox,itemName))
            self.ls_scene.append(QtWidgets.QGraphicsScene(self))

        self.lbl_currPage=self.findChild(QtWidgets.QLabel,'lbl_currPage')
        self.btn_nextPage=self.findChild(QtWidgets.QPushButton,'btn_nextPage')
        self.btn_nextPage.clicked.connect(lambda: self.changePage(1))
        self.btn_prevPage=self.findChild(QtWidgets.QPushButton,'btn_prevPage')
        self.btn_prevPage.clicked.connect(lambda: self.changePage(-1))

        self.btn_saveSelection=self.findChild(QtWidgets.QPushButton,'btn_saveSelection')
        self.btn_saveSelection.clicked.connect(self.saveSelection)

        self.numImage=len(self.samples)
        self.maxPage=int(self.numImage / 24) + (self.numImage % 24 > 0)
        for i in range(1,self.maxPage):
            self.ls_imNumPage.append(range((i-1)*24,i*24))
        self.currPage=1

        self.ls_imNumPage.append(range((self.maxPage-1)*24,self.numImage))
        self.slctFiles=[False]*len(self.samples)
        self.updateGUI()

        self.exec_()
        
    def changePage(self,direction):
        currPosition=self.ls_imNumPage[self.currPage-1]
        for i in range(0,len(currPosition)):
            self.slctFiles[currPosition[i]]=self.ls_check[i].isChecked()
        
        if self.currPage+direction < 1 or self.currPage+direction > self.maxPage:
            return
        else:
            self.currPage= self.currPage+direction
        self.updateGUI()
        

    def updateGUI(self):

        for i in range(0,24):
            self.ls_scene[i].clear()
            self.ls_scene[i]=QtWidgets.QGraphicsScene(self)
            self.ls_label[i].setText('')
            self.ls_check[i].setChecked(False)

            self.ls_label[i].setVisible(False)
            self.ls_check[i].setVisible(False)
            self.ls_graphic[i].setVisible(False)

        currPosition=self.ls_imNumPage[self.currPage-1]
        self.lbl_currPage.setText(str(self.currPage)+'/'+str(self.maxPage))
            
        for i in range(0,len(currPosition)):
            self.ls_label[i].setVisible(True)
            self.ls_check[i].setVisible(True)
            self.ls_graphic[i].setVisible(True)

            if self.slctFiles[currPosition[i]]:
                self.ls_check[i].setChecked(True)
            else:
                self.ls_check[i].setChecked(False)

            self.ls_label[i].setText(self.samples[currPosition[i]])

            imNumber=currPosition[i]
            imPath=self.path+os.path.sep+self.samples[imNumber]+'.'+self.data_type


            im=Image.open(imPath)
            w,h=im.size

            img = tifffile.imread(imPath) if self.data_type == "tif" else io.imread(imPath)

            do_normalize = True

            if do_normalize:
                img = Normalize(None).process(img.copy(), None)

            pix = QtGui.QPixmap.fromImage(array_to_qimage(np.uint8(img/65535.0*255.0) if (img.dtype == np.uint16 or img.dtype == np.dtype("uint16"))  else img))

            pix = pix.scaledToWidth(150)
            item = QtWidgets.QGraphicsPixmapItem(pix)
            self.ls_scene[i].addItem(item)
            self.ls_graphic[i].setScene(self.ls_scene[i])
            #self.ls_graphic[i].fitInView(QtCore.QRectF(0, 0, 150, 150),QtCore.Qt.KeepAspectRatio)
            self.ls_scene[i].update()

    def saveSelection(self):
        currPosition=self.ls_imNumPage[self.currPage-1]
        for i in range(0,len(currPosition)):
            self.slctFiles[currPosition[i]]=self.ls_check[i].isChecked()
        for i in range(0,len(self.slctFiles)):
            if self.slctFiles[i]:
                self.ranking_dict[self.samples[i]]=1
            else:
                self.ranking_dict[self.samples[i]]=0
        
        self.close()

    def resize_fcn(self, event):
        if self.overlay_img_raw is not None:
            self.original_img_lbl.setPixmap(
                QtGui.QPixmap.fromImage(
                    self.overlay_img_raw.scaled(
                        0.9*self.frameGeometry().width(),0.9*self.frameGeometry().height(), 
                        aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
            )

    def get_selection(self):
        return self.ranking_dict

class CherryPicker(AbstractQueryStrategy):
    def __init__(self):
        super().__init__()

    def get_samples_to_label(self, unlabeled_dataset, labeled_dataset, num_samples):
        samples = unlabeled_dataset.get_samples().copy()
        try:
            path_to_dataset=unlabeled_dataset.samples
        except:
            path_to_dataset=unlabeled_dataset.root_dir
        data_type = unlabeled_dataset.samples_data_format

        self.slct_window = SelectionWindow(samples,path_to_dataset,data_type)

        ranking_dict = self.slct_window.get_selection()

        relevant_list = list()
        irrelevant_list = list()


        for (key,value) in ranking_dict.items():
            if value==1:
                relevant_list.append(key)
            else:
                irrelevant_list.append(key)

        return relevant_list+irrelevant_list        