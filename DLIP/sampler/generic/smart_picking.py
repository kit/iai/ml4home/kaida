# GUI for selecting images

from DLIP.utils.helper_functions.create_qimage import array_to_qimage
from DLIP.sampler.abstract_query_strategy import (
    AbstractQueryStrategy,
)
from DLIP.models.zoo.encoder.resnet_encoder import ResNetEncoder
from PyQt5 import QtWidgets, QtCore, QtGui
from pyqtgraph import PlotWidget, plot
import pyqtgraph as pg
from PIL import Image
import numpy as np
import torch
import os
from sklearn import decomposition
from sklearn import manifold
from tqdm import tqdm
import umap.umap_ as umap
import cv2
from DLIP.utils.helper_functions.create_qimage import array_to_qimage

class SelectionWindow(QtWidgets.QDialog):

    def __init__(
            self, 
            samples,
            path_to_dataset,
            data_type,
            **kwargs
        ):
        super(SelectionWindow, self).__init__(**kwargs)
        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setBackground('w')
        self.layout = QtWidgets.QVBoxLayout()
        self.samples = samples
        self.selected_samples = np.zeros(len(self.samples))
        self.path_to_dataset = path_to_dataset
        self.data_type = data_type
        self.all_results = []
        self.setMinimumSize(1000, 1000)
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

        
        self.loading_text = QtWidgets.QLabel(self)
        self.loading_text.setAlignment(QtCore.Qt.AlignCenter)
        # self.loading_text.setGeometry(QtCore.QRect(-1000, -1000, 220, 220))
        self.loading_text.setStyleSheet("border: 1px solid black;")
        #make the font size bigger
        font = QtGui.QFont()
        font.setPointSize(100)
        self.loading_text.setFont(font)
        self.layout.addWidget(self.loading_text)




        
        #add a dropdown menu to select the dimensionality reduction method (PCA, t-SNE, UMAP)
        self.comboBox = QtWidgets.QComboBox(self)
        self.comboBox.addItem("UMAP")
        self.comboBox.addItem("t-SNE")
        self.comboBox.addItem("PCA")
        self.comboBox.activated[str].connect(self.setReductionMethod)
        self.comboBox.setGeometry(QtCore.QRect(0, 0, 150, 150))
        self.layout.addWidget(self.comboBox)
        self.layout.addWidget(self.graphWidget)

        self.graphWidget.scene().sigMouseClicked.connect(self.mouseClickedOnReducedData)

        self.setWindowTitle("Select the samples you want to prioritize")
        self.setStyleSheet("background-color: white;")
        self.setLayout(self.layout)
        self.vb = self.graphWidget.plotItem.vb
        self.vb.aspectLocked = True
        

        ResNetModel = ResNetEncoder(3)
        ResNetModel.to(self.device)
        ResNetModel.eval()
        
        with torch.no_grad():
            for i in tqdm(range(len(self.samples))):
                img = Image.open(os.path.join(self.path_to_dataset,self.samples[i] + '.' + self.data_type))
                img = img.resize((224, 224))
                img = np.array(img, dtype=np.float32)
                #if image is grayscale
                if len(img.shape) == 2:
                    img = np.stack((img,)*3, axis=-1)
                if np.amax(img > 1):
                    img = img / np.amax(img)
                #do z-score normalization
                img = (img - np.mean(img)) / np.std(img)
                img = img.transpose(2, 0, 1)
                img = torch.from_numpy(img).float()
                img = img.unsqueeze(0)
                output = ResNetModel(img.to(self.device))
                output = output[0].squeeze(0)
                output = output.cpu().numpy()
                self.all_results.append(output)
                
             
        self.all_results = np.array(self.all_results)
        self.all_results = self.all_results.reshape(self.all_results.shape[0], -1)
        self.setReductionMethod(self.comboBox.currentText())


        self.exec_()






    def mouseClickedOnReducedData(self, event):
        #get how much the user zoomed in
        temp_x_min_ = self.vb.state['viewRange'][0][0]
        temp_x_max_ = self.vb.state['viewRange'][0][1]
        temp_y_min_ = self.vb.state['viewRange'][1][0]
        temp_y_max_ = self.vb.state['viewRange'][1][1]
        self.temp_range_x = abs(temp_x_max_ - temp_x_min_)
        self.temp_range_y = abs(temp_y_max_ - temp_y_min_)
        self.temp_mul_factor_x = self.temp_range_x / self.initial_range_x
        self.temp_mul_factor_y = self.temp_range_y / self.initial_range_y


        self.distance_threshold_temp_scroll_x = self.distance_threshold_x * self.temp_mul_factor_x
        self.distance_threshold_temp_scroll_y = self.distance_threshold_y * self.temp_mul_factor_y
        if event.button() == QtCore.Qt.LeftButton:
            pos = event.scenePos()
            pos = self.vb.mapSceneToView(pos)
            x = pos.x()
            y = pos.y()

            for i in range(len(self.final_results)):
                if ((self.final_results[i, 0] - self.distance_threshold_temp_scroll_x / 2) < x and 
                    x <= (self.final_results[i, 0] + self.distance_threshold_temp_scroll_x / 2)and 
                    (self.final_results[i, 1] - self.distance_threshold_temp_scroll_y / 2) < y and
                    y <= (self.final_results[i, 1] + self.distance_threshold_temp_scroll_y / 2)    
                        ):
                    #change the color the point
                    self.graphWidget.plot([self.final_results[i, 0]], [self.final_results[i, 1]], pen=None, symbol='s', symbolPen=pg.mkPen(
                        color="r"), symbolBrush=(0, 0, 0, 255), symbolSize=10, pxMode=True)

                    self.selected_sample = self.samples[i]
                    msgBox = QtWidgets.QMessageBox()
                    # img = Image.open(os.path.join(self.path_to_dataset, self.samples[i] + '.' + self.data_type))
                    img = cv2.imread(os.path.join(self.path_to_dataset, self.samples[i] + '.' + self.data_type))
                    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                    # img = img.resize((300, 300))
                    img = np.array(img)
                    temp_min = np.amin(img)
                    temp_max = np.amax(img)
                    img = (img - temp_min) / (temp_max - temp_min)
                    img = img * 255
                    img= img.astype(np.uint8)
                    pix = QtGui.QPixmap.fromImage(array_to_qimage(img))
                    pix = pix.scaled(300, 300, QtCore.Qt.KeepAspectRatio)
                    msgBox.setIconPixmap(pix)
                    
                    msgBox.setWindowTitle("prioritize this sample?")
                    self.removeButton = QtWidgets.QPushButton("Remove")
                    if self.selected_samples[i] == 1:
                        self.removeButton = QtWidgets.QPushButton("Remove")
                        self.removeButton.clicked.connect(self.removeSample)
                        msgBox.addButton(self.removeButton, QtWidgets.QMessageBox.ActionRole)
                    msgBox.setStandardButtons(
                        QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)

                    returnValue = msgBox.exec()
                    if returnValue == QtWidgets.QMessageBox.Ok:
                        self.selected_samples[i] = 1
                        self.graphWidget.plot([self.final_results[i, 0]], [self.final_results[i, 1]], pen=None, symbol='s', symbolPen=pg.mkPen(
                            color="r"), symbolBrush=(0, 255, 0, 255), symbolSize=10, pxMode=True)
                    else:
                        if self.selected_samples[i] != 1:
                            self.selected_samples[i] = 0
                            self.graphWidget.plot([self.final_results[i, 0]], [self.final_results[i, 1]], pen=None, symbol='s', symbolPen=pg.mkPen(
                                color="r"), symbolBrush=(255, 0, 0, 255), symbolSize=10, pxMode=True)
                        else:
                            self.graphWidget.plot([self.final_results[i, 0]], [self.final_results[i, 1]], pen=None, symbol='s', symbolPen=pg.mkPen(
                                color="r"), symbolBrush=(0, 255, 0, 255), symbolSize=10, pxMode=True)

                    

                    break
        else:
            pass
    
    def removeSample(self):
        i = self.samples.index(self.selected_sample)
        self.selected_samples[i] = 0
        self.graphWidget.plot([self.final_results[i, 0]], [self.final_results[i, 1]], pen=None,
                              symbol='s', symbolPen=pg.mkPen(color="r"), symbolBrush=(255, 0, 0, 255), symbolSize=10, pxMode=True)

    def get_pca(self, data, n_components=2):
        pca = decomposition.PCA()
        pca.n_components = n_components
        pca_data = pca.fit_transform(data)
        return pca_data

    def get_tsne(self, data, n_components=2):
        if len(self.samples) < 10:
            perplexity = 2
        elif len(self.samples) < 60:
            perplexity = 5
        elif len(self.samples) >= 60:
            perplexity = 30
        tsne = manifold.TSNE(n_components=n_components,
                             random_state=0, perplexity=perplexity)
        tsne_data = tsne.fit_transform(data)
        return tsne_data

    def get_umap(self, data):
        n_neighbors = 2
        if len(self.samples) < 5:
            n_neighbors = 2
        elif len(self.samples) < 20:
            n_neighbors = 5
        elif len(self.samples) >= 60:
            n_neighbors = 15
        reducer = umap.UMAP(n_neighbors=n_neighbors)
        embedding = reducer.fit_transform(data)
        return embedding
    def setReductionMethod(self, text="t-SNE"):
        #move loading text to the middle of the screen
        self.graphWidget.hide()
        self.loading_text.setHidden(False)
        self.loading_text.move(self.width()/2 - self.loading_text.width()/2, self.height()/2 - self.loading_text.height()/2)
        self.loading_text.setText("Loading...")
        self.loading_text.repaint()
        self.graphWidget.clear()

        if text == "PCA":
            self.final_results = self.get_pca(self.all_results)
        elif text == "t-SNE":
            self.final_results = self.get_tsne(self.all_results)
        elif text == "UMAP":
            self.final_results = self.get_umap(self.all_results)
        # self.graphWidget.clear()
        # self.final_results = np.random.rand(100, 2)
        # self.final_results = self.final_results * 2000
        self.graphWidget.plot(self.final_results[:, 0], self.final_results[:, 1], pen=None, symbol='s', symbolPen=pg.mkPen(
            color="r"), symbolBrush=(255, 0, 0, 255), symbolSize=10 , pxMode=True)
        self.graphWidget.show()
        self.vb = self.graphWidget.getViewBox()
        self.vb_geometry = self.vb.screenGeometry()
        self.vb_width = self.vb_geometry.width()
        self.vb_height = self.vb_geometry.height()
        #hide the loading_text label
        temp_x_min = self.vb.state['viewRange'][0][0]
        temp_x_max = self.vb.state['viewRange'][0][1]
        temp_y_min = self.vb.state['viewRange'][1][0]
        temp_y_max = self.vb.state['viewRange'][1][1]
        self.initial_range_x = abs(temp_x_max - temp_x_min)
        self.initial_range_y = abs(temp_y_max - temp_y_min)
        #dividing by viewbox height / width and multiplying by symbol size to get the threshold
        self.distance_threshold_x = (self.initial_range_x / self.vb_width) * 10
        self.distance_threshold_y = (
            self.initial_range_y / self.vb_height) * 10
        self.loading_text.setHidden(True)
        self.loading_text.setText("")
        self.loading_text.repaint()



class SmartPicker(AbstractQueryStrategy):
    def __init__(self):
        super().__init__()

    def get_samples_to_label(self, unlabeled_dataset, labeled_dataset, num_samples):
        samples = unlabeled_dataset.get_samples().copy()
        try:
            path_to_dataset = unlabeled_dataset.samples
        except:
            path_to_dataset = unlabeled_dataset.root_dir
        data_type = unlabeled_dataset.samples_data_format

        self.slct_window = SelectionWindow(samples, path_to_dataset, data_type)
        # ranking_dict = self.slct_window.get_selection()
        selected_samples = self.slct_window.selected_samples
        ranking_dict = {samples[i]: selected_samples[i] for i in range(len(samples))}

        relevant_list = list()
        irrelevant_list = list()

        for (key, value) in ranking_dict.items():
            if value == 1:
                relevant_list.append(key)
            else:
                irrelevant_list.append(key)
        # print(f"list: {relevant_list+irrelevant_list}")
        return relevant_list+irrelevant_list
