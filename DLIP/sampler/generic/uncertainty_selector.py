from DLIP.data.base_classes.base_dataset import BaseDataset
from DLIP.sampler.abstract_query_strategy import (
    AbstractQueryStrategy,
)

import tarfile
import os
import numpy as np
import torch
import torch.nn as nn

from pytorch_lightning.utilities.seed import seed_everything
from DLIP.utils.loading.initialize_wandb import initialize_wandb
from DLIP.utils.loading.load_transforms import load_transforms
from DLIP.utils.loading.load_model import load_model
from DLIP.utils.loading.merge_configs import merge_configs
from DLIP.utils.loading.split_parameters import split_parameters
from DLIP.utils.metrics.uncertainty_metrics import get_idais

def apply_dropout(m):
    if type(m) == nn.Dropout or type(m) == nn.Dropout2d:
        m.train()

class UncertaintySelector(AbstractQueryStrategy):
    def __init__(self, num_mc_samples, model_path, **kwargs):
        super().__init__()
        self.num_mc_samples = num_mc_samples
        self._load_model_trafo(model_path)

    def get_samples_to_label(
            self, 
            unlabeled_dataset: BaseDataset, 
            labeled_dataset: BaseDataset, 
            num_samples: int):

        samples = unlabeled_dataset.get_samples().copy()
        unlabeled_dataset.set_raw_mode(True)

        unc_lst = list()
        for i in range(len(unlabeled_dataset)):
            img = unlabeled_dataset[i]
            unc_lst.append(self._calculate_uncertainty(img))

        ind_sorted = sorted(range(len(unc_lst)), key=lambda k: 1-unc_lst[k])
        
        return [samples[ind] for ind in ind_sorted]

    def _calculate_uncertainty(self, img):
        with torch.no_grad():
            input_tensor = self.trafos[0](img)[0].unsqueeze(0).cuda()  if torch.cuda.is_available() else self.trafos(img)[0].unsqueeze(0)

            y_pred_sim_mcd = np.zeros((self.num_mc_samples, 1, input_tensor.shape[2], input_tensor.shape[3]))
            for i_mc in range(self.num_mc_samples):
                y_pred_sim_mcd[i_mc,0,:] = self.model(input_tensor).squeeze(0).detach().cpu().numpy()

            unc = get_idais(y_pred_sim_mcd)
        return unc

    def _load_model_trafo(self,model_tar_path):
        tar = tarfile.open(model_tar_path, "r:")
        base_path = os.path.dirname(model_tar_path)
        tar.extractall(path=base_path)
        tar.close()

        # load model
        cfg_yaml = merge_configs(os.path.join(base_path, "cfg.yaml"))

        config = initialize_wandb(
            cfg_yaml=cfg_yaml,
            disabled=True,
            experiment_dir=None,
            config_name=None,
        )

        seed_everything(seed=config['experiment.seed'])
        parameters_splitted = split_parameters(config, ["model", "train", "data"])

        self.model = load_model(
            parameters_splitted["model"], 
            checkpoint_path_str=os.path.join(base_path, "dnn_weights.ckpt")
        )

        if torch.cuda.is_available():
            self.model.cuda()
        
        self.model.eval()

        _, _, self.trafos = load_transforms(parameters_splitted["data"])

        os.remove(os.path.join(base_path, "dnn_weights.ckpt"))
        os.remove(os.path.join(base_path, "cfg.yaml"))