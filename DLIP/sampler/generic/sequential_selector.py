import logging
from DLIP.sampler.abstract_query_strategy import (
    AbstractQueryStrategy,
)

from DLIP.data.base_classes.base_dataset import BaseDataset

class SequentialSelector(AbstractQueryStrategy):
    def __init__(self):
        super().__init__()

    def get_samples_to_label(
            self, 
            unlabeled_dataset: BaseDataset, 
            labeled_dataset: BaseDataset, 
            num_samples: int):
        samples = unlabeled_dataset.get_samples().copy()
        try:
            samples = sorted(samples, key =lambda x: int(x))
        except:
            samples = sorted(samples)
            logging.warning("There may be sorting issues since there are no integer values...")
        return samples