import os
from torch import Tensor
import torch
import yaml

from DLIP.data.base_classes.base_dataset import BaseDataset
from DLIP.models.autoencoder.autoencoder import AutoEncoder

from DLIP.sampler.abstract_query_strategy import (
    AbstractQueryStrategy,
)

from DLIP.utils.clustering.abstract_clustering import AbstractClustering

from DLIP.utils.clustering.clustering_metrics import get_sequence

import numpy as np

class HeterogenitySelector_Autoencoder(AbstractQueryStrategy):
    def __init__(self, clustering_algorithm: AbstractClustering, autoencoder: AutoEncoder, features_checkpoint_dir: str, **kwargs):
        super().__init__()
        self.clustering_algorithm = clustering_algorithm
        self.ae = autoencoder
        self.features_checkpoint_dir = features_checkpoint_dir

    def rank_samples_to_label(self, unlabeled_dataset: BaseDataset):
        samples = unlabeled_dataset.get_samples().copy()
        
        if not os.path.exists(os.path.join(self.features_checkpoint_dir, 'features_checkpoint.yaml')):
            self.init = True
            features = {}
        else:
            self.init = False
            with open(os.path.join(self.features_checkpoint_dir, 'features_checkpoint.yaml')) as file:
                features = yaml.load(file, Loader=yaml.FullLoader)

        for i in range(len(samples)):
            img = unlabeled_dataset[i]
            img = torch.unsqueeze(img, dim=0)
            if self.init:
                features_temp = self.feature_extractor(img)
                features[samples[i]] = features_temp.tolist()
            else:
                features_temp = np.array(features[samples[i]])
            if i == 0:
                features_samples = features_temp
            else:
                features_samples = np.vstack([features_samples,features_temp])
        
        if self.init:
            with open(os.path.join(self.features_checkpoint_dir, 'features_checkpoint.yaml'), "w") as file:
                yaml.dump(features, file)

        labels, n_clusters = self.clustering_algorithm.get_clustering(features_samples)
        seq = get_sequence(labels)

        ranking_dict = dict()
        for i in range(len(samples)):
            ranking_dict[samples[i]] = seq.index(i)

        return ranking_dict

    def feature_extractor(self, img:Tensor):
        z_temp = self.ae.get_features(img)
        z_temp = z_temp.detach().cpu().numpy()
        return z_temp