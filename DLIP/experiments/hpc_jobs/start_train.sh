#!/bin/bash
#SBATCH --partition=haicore-gpu8
#SBATCH --time=48:00:00
#SBATCH --ntasks=252
#SBATCH --nodes=1
#SBATCH --gres=gpu:4
#SBATCH --mail-type=ALL
#SBATCH --mail-user=marcel.schilling@kit.edu
#SBATCH --error=%j_error.txt
#SBATCH --output=%j_output.txt
#SBATCH --job-name=ssl
#SBATCH --constraint=LSDF

export CFG_FILE="/home/hk-project-sppo/sc1357/devel/kaida/DLIP/experiments/configurations/pre-anno-test/dma_cells_fl.yaml"
export RESULT_DIR="/lsdf/kit/iai/projects/iai-aida/Daten_Schilling/per_anno_exp"
export SWEEPID="kit-iai-ibcs-dl/pre-anno/x82p0s73"

# remove all modules
module purge

# activate cuda
module load devel/cuda/11.2

# activate conda env
source /home/hk-project-sppo/sc1357/miniconda3/etc/profile.d/conda.sh
conda activate kaida

# move to script dir
cd /home/hk-project-sppo/sc1357/devel/kaida/DLIP/scripts

# start train
CUDA_VISIBLE_DEVICES=0 wandb agent $SWEEPID &
CUDA_VISIBLE_DEVICES=1 wandb agent $SWEEPID &
CUDA_VISIBLE_DEVICES=2 wandb agent $SWEEPID &
CUDA_VISIBLE_DEVICES=3 wandb agent $SWEEPID 

wait < <(jobs -p)
