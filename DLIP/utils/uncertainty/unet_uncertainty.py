from abc import ABC, abstractmethod
import torch
import torch.nn as nn
import numpy as np
from DLIP.objectives import DiceLoss
from DLIP.utils.metrics.uncertainty_metrics import (
    get_pixelwise_sample_variance,
    get_pixelwise_entropy,
    get_pixelwise_mutual_information,
    get_idais,
    get_iiou
)

class Uncertainty(ABC):
    def __init__(self, model: nn.Module):
        pass

    @abstractmethod
    def get_pixelwise_uncertainty(self):
        raise NotImplementedError("This function needs to be implemented")

    @abstractmethod
    def get_imagewise_uncertainty(self):
        raise NotImplementedError("This function needs to be implemented")

class McdUncertainty(Uncertainty): 
    def __init__(self, model: nn.Module, x_in, T: int = 20):
        super(McdUncertainty, self).__init__(model)
        model.eval()
        for m in model.modules():
            if type(m) == nn.Dropout or type(m) == nn.Dropout2d:
                m.train()
        size = [1,1,256,256] # list(x_in.size())
        size.append(T)
        self.pred = np.empty(size)
        for t in range(T):
            next = model(x_in)
            next = next.detach().cpu().numpy()
            self.pred[...,t] = next

    def get_prediction(self):
        return self.pred
    
    def get_pixelwise_uncertainty(self, type: str = "entropy"):
        pred = self.pred
        if type == "sample_variance":
            uncertainty = get_pixelwise_sample_variance(pred)
        elif type == "entropy":
            uncertainty = get_pixelwise_entropy(pred)
        elif type == "mutual_information":
            uncertainty = get_pixelwise_mutual_information(pred)
        else:
            print("uncertainty measurement type not known")
        return uncertainty

    def get_binary_prediction(self, threshold: float = 0.5):
        pred = self.pred
        pred = np.mean(pred,-1)
        pred[pred<threshold]=0
        pred[pred>=threshold]=1
        return pred

    def get_imagewise_uncertainty(self, type: str = "iDais"):
        pred = self.pred
        if type == "iDais":
            score = get_idais(pred)
        elif type == "iIoU":
            score = get_iiou(pred)
        return score
