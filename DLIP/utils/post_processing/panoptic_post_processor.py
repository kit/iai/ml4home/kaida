import numpy as np

from DLIP.utils.post_processing.distmap2inst import DistMapPostProcessor


class PanopticPostProcessor:
    def __init__(self, **kwargs) -> None:
        self.instance_post_processor = DistMapPostProcessor(**kwargs)

    def process(
        self,
        original_image: np.ndarray,
        y_pred_dist_map: np.ndarray,
        y_pred_semantic: np.ndarray
    ) -> np.ndarray:
        y_pred_semantic = np.argmax(y_pred_semantic, axis=0)

        instance_channel = self.instance_post_processor.process(y_pred_dist_map, original_image)

        instance_ids = np.unique(instance_channel)[1:]  # ignore background
        semantic_channel = np.zeros_like(instance_channel)

        for instance_id in instance_ids:
            class_values, class_counts = np.unique(
                y_pred_semantic[instance_channel[0] == instance_id],
                return_counts=True
            )

            semantic_channel[instance_channel == instance_id] = class_values[np.argmax(class_counts)]

        return np.concatenate((instance_channel, semantic_channel), axis=0)
