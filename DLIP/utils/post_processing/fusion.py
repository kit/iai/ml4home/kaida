import numpy as np
from skimage import measure
from sklearn.neighbors import NearestNeighbors
from skimage import measure

def inst_mask2centroid_lst(instance_mask):
    inst_lst = list()
    props = measure.regionprops(measure.label(instance_mask))
    for prop in props:
        y, x = prop["centroid"]
        inst_lst.append([x,y])

    return inst_lst

class StainedCellFusion:
    def __init__(self) -> None:
        self.distance_threshold = 6

    def process(self, img_dict, result_dict):
        ref_nuclei_props        = measure.regionprops(measure.label(result_dict["Hoechst"]["inst"]))
        calcein_nuclei_lst      = inst_mask2centroid_lst(result_dict["Calcein"]["inst"])
        pi_nuclei_lst           = inst_mask2centroid_lst(result_dict["PI"]["inst"])

        neigh_calcein = NearestNeighbors(n_neighbors=1)
        if len(calcein_nuclei_lst)>0:
            neigh_calcein.fit(np.array(calcein_nuclei_lst))
        else:
            neigh_calcein = None

        neigh_pi = NearestNeighbors(n_neighbors=1)
        if len(pi_nuclei_lst)>0:
            neigh_pi.fit(np.array(pi_nuclei_lst))
        else:
            neigh_pi = None

        result_lst = list()

        for prop in ref_nuclei_props:
            y, x = prop["centroid"]
            point = np.array([x, y]).reshape(1,-1)

            if neigh_calcein is not None:
                dist_calcein, _ = neigh_calcein.kneighbors(point, return_distance=True) 
            else:
                dist_calcein = np.inf

            if neigh_pi is not None:     
                dist_pi, _      = neigh_pi.kneighbors(point, return_distance=True)
            else:
                dist_pi = np.inf

            result_lst_i    = [x,y]

            # calculate median intensities
            for key in ["Hoechst", "Calcein", "PI"]:
                result_lst_i.append(
                    np.median(
                        img_dict[key][result_dict["Hoechst"]["inst"]==prop.label], 
                        axis=0)
                    )

            if dist_calcein<=self.distance_threshold:
                result_lst_i.append(1)
                if dist_pi>self.distance_threshold:
                    result_lst_i.append(1)
                else:
                    result_lst_i.append(0)
                result_lst.append(result_lst_i)

        return np.array(result_lst)