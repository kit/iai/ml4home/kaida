# -------------------------------------------------------------------------------
# Code adapted from: https://github.com/vqdang/hover_net
#
# MIT License
#
# Copyright (c) 2020 vqdang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# -------------------------------------------------------------------------------

from collections import OrderedDict
from typing import Optional, Tuple

import cv2
import numpy as np
from scipy import ndimage
from scipy.ndimage import measurements
from scipy.ndimage.morphology import binary_fill_holes
from skimage.segmentation import watershed


class HVMapPostProcessor:
    def __init__(self, nr_types: Optional[int] = None, return_centroids: bool = False) -> None:
        self.nr_types = nr_types
        self.return_centroids = return_centroids

    @staticmethod
    def _remove_small_objects(pred, min_size=64, connectivity=1):
        """Remove connected components smaller than the specified size.
        This function is taken from skimage.morphology.remove_small_objects, but the warning
        is removed when a single label is provided.
        Args:
            pred: input labelled array
            min_size: minimum size of instance in output array
            connectivity: The connectivity defining the neighborhood of a pixel.

        Returns:
            out: output array with instances removed under min_size
        """
        out = pred

        if min_size == 0:  # shortcut for efficiency
            return out

        if out.dtype == bool:
            selem = ndimage.generate_binary_structure(pred.ndim, connectivity)
            ccs = np.zeros_like(pred, dtype=np.int32)
            ndimage.label(pred, selem, output=ccs)
        else:
            ccs = out

        try:
            component_sizes = np.bincount(ccs.ravel())
        except ValueError:
            raise ValueError(
                "Negative value labels are not supported. Try "
                "relabeling the input with `scipy.ndimage.label` or "
                "`skimage.morphology.label`."
            )

        too_small = component_sizes < min_size
        too_small_mask = too_small[ccs]
        out[too_small_mask] = 0

        return out

    @staticmethod
    def _get_bounding_box(img):
        """Get bounding box coordinate information."""
        rows = np.any(img, axis=1)
        cols = np.any(img, axis=0)
        rmin, rmax = np.where(rows)[0][[0, -1]]
        cmin, cmax = np.where(cols)[0][[0, -1]]
        # due to python indexing, need to add 1 to max
        # else accessing will be 1px in the box, not out
        rmax += 1
        cmax += 1
        return [rmin, rmax, cmin, cmax]

    @staticmethod
    def _proc_np_hv(pred_np, pred_hv):
        """Process Nuclei Prediction with XY Coordinate Map.

        Args:
            pred_np:
                channel 0 contain probability map of nuclei
            pred_hv
                channel 0 containing the regressed X-map
                channel 1 containing the regressed Y-map

        """
        pred_hv = np.array(pred_hv, dtype=np.float32)

        blb_raw = np.array(pred_np[..., 0], dtype=np.float32)
        h_dir_raw = pred_hv[..., 0]
        v_dir_raw = pred_hv[..., 1]

        # processing
        blb = np.array(blb_raw >= 0.5, dtype=np.int32)

        blb = measurements.label(blb)[0]
        blb = HVMapPostProcessor._remove_small_objects(blb, min_size=10)
        blb[blb > 0] = 1  # background is 0 already

        h_dir = cv2.normalize(
            h_dir_raw, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F
        )
        v_dir = cv2.normalize(
            v_dir_raw, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F
        )

        sobelh = cv2.Sobel(h_dir, cv2.CV_64F, 1, 0, ksize=21)
        sobelv = cv2.Sobel(v_dir, cv2.CV_64F, 0, 1, ksize=21)

        sobelh = 1 - (
            cv2.normalize(
                sobelh, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F
            )
        )
        sobelv = 1 - (
            cv2.normalize(
                sobelv, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F
            )
        )

        overall = np.maximum(sobelh, sobelv)
        overall = overall - (1 - blb)
        overall[overall < 0] = 0

        dist = (1.0 - overall) * blb
        # nuclei values form mountains so inverse to get basins
        dist = -cv2.GaussianBlur(dist, (3, 3), 0)

        overall = np.array(overall >= 0.4, dtype=np.int32)

        marker = blb - overall
        marker[marker < 0] = 0
        marker = binary_fill_holes(marker).astype("uint8")
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (5, 5))
        marker = cv2.morphologyEx(marker, cv2.MORPH_OPEN, kernel)
        marker = measurements.label(marker)[0]
        marker = HVMapPostProcessor._remove_small_objects(marker, min_size=10)

        proced_pred = watershed(dist, markers=marker, mask=blb)

        return proced_pred

    def process(self, pred_dict: OrderedDict) -> Tuple[np.ndarray, dict]:
        """Post processing script for image tiles.

        Returns:
            pred_inst:     pixel-wise nuclear instance segmentation prediction
            pred_type_out: pixel-wise nuclear type prediction
        """

        if self.nr_types is not None:
            pred_type = pred_dict["tp"].detach().cpu().numpy()

        pred_inst_hv = pred_dict["hv"].detach().cpu().numpy()
        pred_inst_np = pred_dict["np"].detach().cpu().numpy()

        pred_inst = self._proc_np_hv(pred_inst_np, pred_inst_hv)

        inst_info_dict = None
        if self.return_centroids or self.nr_types is not None:
            inst_id_list = np.unique(pred_inst)[1:]  # exlcude background
            inst_info_dict = {}
            for inst_id in inst_id_list:
                inst_map = pred_inst == inst_id
                # TODO: chane format of bbox output
                rmin, rmax, cmin, cmax = self._get_bounding_box(inst_map)
                inst_bbox = np.array([[rmin, cmin], [rmax, cmax]])
                inst_map = inst_map[
                    inst_bbox[0][0]: inst_bbox[1][0], inst_bbox[0][1]: inst_bbox[1][1]
                ]
                inst_map = inst_map.astype(np.uint8)
                inst_moment = cv2.moments(inst_map)
                inst_contour = cv2.findContours(
                    inst_map, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
                )
                # * opencv protocol format may break
                inst_contour = np.squeeze(inst_contour[0][0].astype("int32"))
                # < 3 points dont make a contour, so skip, likely artifact too
                # as the contours obtained via approximation => too small or sthg
                if inst_contour.shape[0] < 3:
                    continue
                if len(inst_contour.shape) != 2:
                    continue  # ! check for trickery shape
                inst_centroid = [
                    (inst_moment["m10"] / inst_moment["m00"]),
                    (inst_moment["m01"] / inst_moment["m00"]),
                ]
                inst_centroid = np.array(inst_centroid)
                inst_contour[:, 0] += inst_bbox[0][1]  # X
                inst_contour[:, 1] += inst_bbox[0][0]  # Y
                inst_centroid[0] += inst_bbox[0][1]  # X
                inst_centroid[1] += inst_bbox[0][0]  # Y
                inst_info_dict[inst_id] = {  # inst_id should start at 1
                    "bbox": inst_bbox,
                    "centroid": inst_centroid,
                    "contour": inst_contour,
                    "type_prob": None,
                    "type": None,
                }

        if self.nr_types is not None:
            # * Get class of each instance id, stored at index id-1
            for inst_id in list(inst_info_dict.keys()):
                rmin, cmin, rmax, cmax = (inst_info_dict[inst_id]["bbox"]).flatten()
                inst_map_crop = pred_inst[rmin:rmax, cmin:cmax]
                inst_type_crop = pred_type[rmin:rmax, cmin:cmax]
                inst_map_crop = (
                    inst_map_crop == inst_id
                )  # TODO: duplicated operation, may be expensive
                inst_type = inst_type_crop[inst_map_crop]
                type_list, type_pixels = np.unique(inst_type, return_counts=True)
                type_list = list(zip(type_list, type_pixels))
                type_list = sorted(type_list, key=lambda x: x[1], reverse=True)
                inst_type = type_list[0][0]
                if inst_type == 0:  # ! pick the 2nd most dominant if exist
                    if len(type_list) > 1:
                        inst_type = type_list[1][0]
                type_dict = {v[0]: v[1] for v in type_list}
                type_prob = type_dict[inst_type] / (np.sum(inst_map_crop) + 1.0e-6)
                inst_info_dict[inst_id]["type"] = int(inst_type)
                inst_info_dict[inst_id]["type_prob"] = float(type_prob)

        # inst_id in the dict maps to the same value in the `pred_inst`
        return pred_inst, inst_info_dict
