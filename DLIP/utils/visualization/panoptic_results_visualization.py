from pathlib import Path
from typing import Dict, Tuple

import cv2
import numpy as np
import tifffile
from skimage.morphology import binary_erosion


class PanopticResultsVisualization:
    def __init__(
        self,
        class_color_mapping: Dict[int, Tuple[float, float, float]] = None,
        class_name_mapping: Dict[int, str] = None,
    ) -> None:
        self.class_color_mapping = class_color_mapping

        if class_color_mapping is None:
            self.class_color_mapping = {
                1: (0, 0, 1),
                2: (0, 1, 0),
                3: (0, 1, 1),
                4: (1, 0, 0),
                5: (1, 0, 1),
                6: (1, 1, 0),
                7: (0.25, 0.60, 0.25),
                8: (0.60, 0.25, 0.25),
                9: (0.25, 0.25, 0.60),
            }

        self.class_name_mapping = class_name_mapping

    @staticmethod
    def colorize_segmentation(semantic_label: np.ndarray, class_colors: Dict[int, Tuple[float, float, float]]):
        colored_image = np.zeros((*semantic_label.shape[:2], 3))

        for key in class_colors:
            for channel in range(3):
                colored_image[semantic_label == key, channel] = class_colors[key][channel]

        return colored_image

    @staticmethod
    def get_cell_overlay(
        instance_label: np.ndarray, semantic_label: np.ndarray, class_colors: Dict[int, Tuple[float, float, float]],
    ) -> np.ndarray:
        colored_image = PanopticResultsVisualization.colorize_segmentation(semantic_label, class_colors)
        result_image = np.zeros_like(colored_image)

        instance_ids = np.unique(instance_label)[1:]

        for instance_id in instance_ids:
            current_image = instance_label == instance_id
            eroded_image = binary_erosion(current_image)
            result_image[np.logical_and(current_image, np.invert(eroded_image)), :] = colored_image[current_image][0]

        return result_image

    def make_legend(self, images: np.ndarray) -> np.ndarray:
        images = images.squeeze()
        font = cv2.FONT_HERSHEY_PLAIN

        result_images = list()

        for img in images:
            # img = cv2.resize(img, (img.shape[0] * 2, img.shape[1] * 2), interpolation=cv2.INTER_LINEAR)
            legend_start = img.shape[1] + 10 + 10

            img = np.pad(
                img,
                ((10, 10), (10, 240), (0, 0)),
                "constant",
                constant_values=((255, 255), (255, 255), (0, 0)),
            )

            img = cv2.putText(img, "Classes:", (legend_start, 20), font, 1, (0, 0, 0), thickness=1)
            legend_start += 10

            for i, key in enumerate(self.class_color_mapping.keys()):
                color = tuple([int(self.class_color_mapping[key][idx] * 255) for idx in range(3)])

                img = cv2.putText(img, "o", (legend_start, 20 + (i + 1) * 20), font, 1, color, thickness=1)

                class_label = f"class {key}" if self.class_name_mapping is None else self.class_name_mapping[key]

                img = cv2.putText(
                    img, class_label, (legend_start + 10, 20 + (i + 1) * 20), font, 1, (0, 0, 0), thickness=1,
                )

            result_images.append(img)

        return np.stack((result_images[0][None, ...], result_images[1][None, ...], result_images[2][None, ...]))

    def save_result_image(
        self,
        original_image: np.ndarray,
        network_prediction: np.ndarray,
        image_name: str,
        result_dir: str,
    ) -> None:
        assert len(original_image.shape) == 3 and original_image.shape[2] == 3
        assert len(network_prediction.shape) == 3 and network_prediction.shape[0] == 2

        result_images = list()
        result_dir = Path(result_dir)

        cell_overlay = self.get_cell_overlay(
            network_prediction[0],
            network_prediction[1],
            self.class_color_mapping,
        )

        for channel_idx in range(original_image.shape[2]):
            result_image = np.concatenate([original_image[..., channel_idx][..., None]] * 3, axis=2)

            result_image[cell_overlay != 0] = cell_overlay[cell_overlay != 0]
            result_images.append((result_image * 255).astype(np.uint8))

        final_image = np.stack((result_images[0][None, ...], result_images[1][None, ...], result_images[2][None, ...]))
        final_image = self.make_legend(final_image)

        tifffile.imwrite(result_dir.joinpath(image_name), final_image, imagej=True)
