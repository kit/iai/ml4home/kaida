import yaml
from typing import Dict, List, Union
import sys

import numpy as np
from wandb import Config
from PyQt5 import QtCore, QtGui, QtWidgets, uic
import ast
import os




class QHLine(QtWidgets.QFrame):
    def __init__(self):
        super(QHLine, self).__init__()
        
        self.setFrameShape(QtWidgets.QFrame.HLine)
        self.setFrameShadow(QtWidgets.QFrame.Sunken)

class ParameterTable(QtWidgets.QDialog):
    def __init__(self, path_project, yaml_path: str):
        super(ParameterTable,self).__init__()
        uic.loadUi(f"{path_project}/utils/helper_gui/parameter_table_gui.ui", self)

        self.yaml_path      = yaml_path

        self.used_categories= list()

        self.win = QtWidgets.QWidget()

        self.outerlayout = QtWidgets.QVBoxLayout()
        self.grid = QtWidgets.QGridLayout()

        self.edits = list()
        self.buttons_dict = dict()
        self.inner_layouts = dict()
        self.var_types = list()

        self.rcount=0
        self.index_count = 0

        self.yaml2dict(self.yaml_path)
        self.cfg=self.read(self.cfg_yaml)
        self.keys=list(self.cfg_yaml.keys())

        self.win.setLayout(self.grid)

        self.scrollArea = QtWidgets.QScrollArea()
        self.scrollArea.setGeometry(QtCore.QRect(0, 0, 1131, 951))
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollArea.setEnabled(True)

        self.scrollArea.setWidget(self.win)

        self.outerlayout.addWidget(self.scrollArea)
                          
        self.setLayout(self.outerlayout)

    def yaml2dict(self,yaml_path: str):
        self.cfg_yaml = yaml.load(open(yaml_path), Loader=yaml.FullLoader)

    def read(self,parameters: Union[Dict, Config], numpy_mode: bool = True):
        categories = dict()

        for item in dict(parameters).items():
            key, value = item

            if isinstance(value, list):
                value = np.array(value) if numpy_mode else value
            if isinstance(value, dict):
                if len(value.keys()) >=1 and "type" in value:
                    v_type = value["type"]
                else:
                    v_type = None
                if len(value.keys()) >=1 and "options" in value:
                    options = value["options"]
                else:
                    options = None
                if len(value.keys()) >= 1 and "value" in value:
                    value = value["value"]

            key_split = [x for x in (key.split("."))]

            self._add_value(categories, key_split, value, v_type,options,False)

        return categories

    def _add_value(self, categories: Dict, rest: List[str], value, v_type,options,loop):
        key = rest.pop(0)

        if len(rest) > 0 and key not in categories:
            categories[key] = dict()
        elif len(rest) > 0 and not isinstance(categories[key], dict):
            old_val = categories[key]
            categories[key] = dict()
            categories[key]["value"] = old_val

        if len(rest) > 0:
            if not key in self.used_categories:
                if not loop:
                    layout = QtWidgets.QVBoxLayout()
                    label = QtWidgets.QLabel(text=key, alignment=QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
                    label.setStyleSheet("font-weight: bold")
                    layout.addWidget(label)
                    layout.addWidget(QHLine())
                    widget = QtWidgets.QWidget()
                    widget.setLayout(layout)
                    self.grid.addWidget(widget,self.rcount,0)
                else:
                    label = QtWidgets.QLabel(text=key)
                    label.setStyleSheet("font-weight: bold")
                    self.grid.addWidget(label,self.rcount,0)
                self.used_categories.append(key)
                self.rcount+=1
            self._add_value(categories[key], rest, value, v_type,options,True)
        else:
            categories[key] = value
            self.grid.addWidget(QtWidgets.QLabel(text=key),self.rcount,0)
            self.build_editable(value, v_type,options)
            self.rcount+=1
    
    def build_editable(self,value,v_type,options):
        if v_type == "comboBox":
            self.var_types.append(type(value)) 
            self.edits.append(QtWidgets.QComboBox())
            if not options == None:
                self.edits[self.index_count].addItems(list(map(str,options)))
                self.edits[self.index_count].setCurrentIndex(self.edits[self.index_count].findText(str(value)))
            else:
                self.edits[self.index_count].addItems(list(map(str, value)))
            self.edits[self.index_count].currentIndexChanged.connect(lambda _, idx=self.index_count: self.change_parameter(idx))
            self.grid.addWidget(self.edits[self.index_count],self.rcount,1)
            self.index_count+=1
        elif v_type == "file_path" or (type(value) == str and value == "FILE_PATH" and v_type==None):
            self.var_types.append(type(value))
            self.edits.append(QtWidgets.QLabel())
            self.edits[self.index_count].setStyleSheet("background-color: white")
            self.edits[self.index_count].setText(value)
            self.buttons_dict[str(self.rcount)] = QtWidgets.QPushButton()
            self.buttons_dict[str(self.rcount)].setSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
            self.buttons_dict[str(self.rcount)].setText("...")
            self.inner_layouts[str(self.rcount)] = QtWidgets.QHBoxLayout()
            self.inner_layouts[str(self.rcount)].addWidget(self.edits[self.index_count])
            self.inner_layouts[str(self.rcount)].addWidget(self.buttons_dict[str(self.rcount)])
            
            self.buttons_dict[str(self.rcount)].clicked.connect(lambda _, idx=self.index_count: self.choose_file(idx))

            self.grid.addLayout(self.inner_layouts[str(self.rcount)],self.rcount,1)
            self.index_count+=1
        
        else:
            self.var_types.append(type(value))
            self.edits.append(QtWidgets.QLineEdit())
            self.edits[self.index_count].setText(str(value))
            self.edits[self.index_count].textEdited.connect(lambda _, idx=self.index_count: self.change_parameter(idx))
            self.grid.addWidget(self.edits[self.index_count],self.rcount,1)
            self.index_count+=1

    def change_parameter(self, idx):
        if type(self.edits[idx]) == QtWidgets.QComboBox:
            var=self.edits[idx].currentText()
        elif type(self.edits[idx]) == QtWidgets.QLineEdit:
            var=self.edits[idx].text()
        self.cfg_yaml[self.keys[idx]]["value"] = self.convert_variable_to_original(var,idx)
        with open(self.yaml_path, "w", encoding = "utf-8") as yaml_file:
            yaml.dump(self.cfg_yaml, yaml_file, default_flow_style=None)

    def choose_file(self,idx):
        var = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select File")[0])
        if not os.path.exists(var):
            return
        self.edits[idx].setText(var)
        self.cfg_yaml[self.keys[idx]] = var

    def convert_variable_to_original(self, var, idx):
        if var == "":
            return None

        v_type = self.var_types[idx]
        if v_type == list:
            var = ast.literal_eval(var)
        elif v_type == bool:
            var = var.lower() == "true"
        elif v_type == type(None):
            var = None
        else:
            var=v_type(var)
        return var


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    #yaml_path= os.path.join("C:\\Users\\bq_sschwindt\\Documents\\Projects\\kaida\\DLIP\\experiments\\configurations\\models\\instance_segmentation","u_net.yaml")
    yaml_path = os.path.join("C:\\Users\\bq_sschwindt\\Documents\\Traindata\\KaIDA_test_big\\temp","u_net.yaml")
    tbl = ParameterTable("C:\\Users\\bq_sschwindt\\Documents\Projects\\kaida\\DLIP",yaml_path)
    tbl.show()
    sys.exit(app.exec_())