from PyQt5 import QtCore, QtGui, QtWidgets, uic
import ast
import os

class ParameterTable(QtWidgets.QDialog):
    def __init__(self, path_project,parameter: dict, users_cfg: dict):
        super(ParameterTable,self).__init__()
        uic.loadUi(f"{path_project}/utils/helper_gui/parameter_table_gui.ui", self)

        self.parameter      = parameter
        self.users_cfg      = users_cfg

        win = QtWidgets.QWidget()
        outerlayout = QtWidgets.QVBoxLayout()
        buttons = QtWidgets.QHBoxLayout()
        grid = QtWidgets.QGridLayout()

        self.keys = list(self.parameter.keys())
        self.edits = list()
        self.buttons = dict()
        self.inner_layouts = dict()
        self.var_types = list()

        for i in range(len(self.keys)):
            grid.addWidget(QtWidgets.QLabel(text=self.keys[i]),i,0)
            value=self.parameter[self.keys[i]]
            if type(value) == list and not type(value[0]) == list:  #kreiert Combobox wenn in Variable eine Liste gespeichert ist
                self.var_types.append(type(value[0])) 
                self.edits.append(QtWidgets.QComboBox())
                self.edits[i].addItems(list(map(str, value)))
                if self.users_cfg[self.keys[i]] is not None:
                    self.edits[i].setCurrentIndex(self.edits[i].findText(str(self.users_cfg[self.keys[i]])))
                else:
                    self.users_cfg[self.keys[i]]=value[0]
                self.edits[i].currentIndexChanged.connect(lambda _, idx=i: self.change_parameter(idx))
                grid.addWidget(self.edits[i],i,1)
            elif type(value) == str and value == "FILE_PATH":
                self.var_types.append(type(value))
                self.edits.append(QtWidgets.QLabel())
                self.edits[i].setStyleSheet("background-color: white")
                if self.users_cfg[self.keys[i]] is not None:
                    self.edits[i].setText(str(self.users_cfg[self.keys[i]]))
                else:
                    self.edits[i].setText("FILE_PATH")
                self.buttons[str(i)] = QtWidgets.QPushButton()
                self.buttons[str(i)].setSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
                self.buttons[str(i)].setText("...")
                self.inner_layouts[str(i)] = QtWidgets.QHBoxLayout()
                self.inner_layouts[str(i)].addWidget(self.edits[i])
                self.inner_layouts[str(i)].addWidget(self.buttons[str(i)])
                
                self.buttons[str(i)].clicked.connect(lambda _, idx=i: self.choose_file(idx))

                grid.addLayout(self.inner_layouts[str(i)],i,1)

                
            else:
                if type(value) == list:
                    value=value[0]
                self.var_types.append(type(value))
                self.edits.append(QtWidgets.QLineEdit())
                if self.users_cfg[self.keys[i]] is not None:
                    self.edits[i].setText(str(self.users_cfg[self.keys[i]]))
                elif self.parameter[self.keys[i]] is not None:
                    self.edits[i].setText(str(value))
                    self.users_cfg[self.keys[i]]=value
                self.edits[i].textEdited.connect(lambda _, idx=i: self.change_parameter(idx))
                grid.addWidget(self.edits[i],i,1)
        
        buttons.addWidget(self.button_box)
        outerlayout.addLayout(grid)
        outerlayout.addLayout(buttons)
                    
        self.setLayout(outerlayout)

    def get_custom_parameter(self):
        return self.users_cfg

    def change_parameter(self, idx):
        if type(self.edits[idx]) == QtWidgets.QComboBox:
            var=self.edits[idx].currentText()
        elif type(self.edits[idx]) == QtWidgets.QLineEdit:
            var=self.edits[idx].text()
        self.users_cfg[self.keys[idx]] = self.convert_variable_to_original(var,idx)

    def choose_file(self,idx):
        var = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select File")[0])
        if not os.path.exists(var):
            return
        self.edits[idx].setText(var)
        self.users_cfg[self.keys[idx]] = var

    def convert_variable_to_original(self,var,idx):
        v_type=self.var_types[idx]
        if v_type == list:
            var = ast.literal_eval(var)
        elif v_type == bool:
            var = var.lower() == "true"
        elif v_type == type(None):
            var = None
        else:
            var=v_type(var)
        return var