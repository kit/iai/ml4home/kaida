import torch
import torch.nn as nn
import numpy as np
from DLIP.objectives import DiceLoss

def get_pixelwise_sample_variance(pred: np.ndarray):
    uncertainty = np.mean(np.square(pred), -1) - np.square(np.mean(pred, -1))
    return uncertainty

def get_pixelwise_entropy(pred: np.ndarray):
    pred = np.repeat(np.expand_dims(pred, -1), 2, -1)
    pred[..., 1] = 1 - pred[..., 0] #0 = foreground, 1 = background
    uncertainty = -np.sum(np.mean(pred, -2) * np.log(np.mean(pred, -2) + 1e-5), -1)
    return uncertainty

def get_pixelwise_mutual_information(pred: np.ndarray):
    pred = np.repeat(np.expand_dims(pred, -1), 2, -1)
    pred[..., 1] = 1 - pred[..., 0] #0 = foreground, 1 = background
    entropy = -np.sum(np.mean(pred, -2) * np.log(np.mean(pred, -2) + 1e-5), -1)
    expected_entropy = -np.mean(np.sum(pred * np.log(pred + 1e-5), -1), -1)
    uncertainty = entropy - expected_entropy
    return uncertainty

def get_idais(pred: np.ndarray):
    dice = DiceLoss()
    T = pred.shape[-1]
    pred = torch.from_numpy(pred)
    M = 0
    for i in range(T):
        for j in range(T):
            if j > i:
                M = M + dice.forward(pred[...,i],pred[...,j])*(i!=j)
    score = 2/(T*(T-1)) * M 
    score = score.detach().cpu().numpy()
    return 1-score

def get_iiou(pred: np.ndarray):
    C = 2 #number of classes
    threshold = 0.5 #nur möglich für 2 Klassen-Probleme
    pred[pred<threshold]=0
    pred[pred>=threshold]=1
    T = pred.shape[-1]
    M = 0
    for i in range(C):
        indicator_array = np.zeros(pred.shape)
        indicator_array[pred==i] = 1
        intersection = np.sum(np.prod(indicator_array,-1))
        cardinality = np.sum(indicator_array,-1)
        cardinality[cardinality>=1] = 1
        cardinality = np.sum(cardinality)
        M = M + (intersection + 1e-5)/(cardinality + 1e-5)
    score = 1 - 1/C * M
    return score