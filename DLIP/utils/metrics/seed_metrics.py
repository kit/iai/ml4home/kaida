"""
Copyright 2022 Tim Scherr tim.scherr@kit.edu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import numpy as np
from itertools import chain
from skimage import measure
from scipy.ndimage import binary_dilation
from scipy.ndimage.morphology import generate_binary_structure
from skimage import measure
from skimage.morphology import disk
from skimage.segmentation import watershed

def get_particle_ids(img):
    """ Get particle ids in intensity-coded label image.
    :param img: Intensity-coded nuclei image.
        :type:
    :return: List of nucleus ids.
    """

    values = np.unique(img)
    values = values[values > 0]

    return values


def create_gt_area(gt, gt_radius=3):
    """ Create area for ground truth particles.
    :param gt: Intensity-coded ground truth image.
    :return: Intensity-coded ground truth area.
    """
    ground_truth_markers = measure.label(gt, connectivity=1, background=0)
    if gt_radius == 1:
        ground_truth_area = binary_dilation(gt, generate_binary_structure(2, 1))
    elif gt_radius == 1.5:
        ground_truth_area = binary_dilation(gt, generate_binary_structure(2, 2))
    elif gt_radius >= 2:
        ground_truth_area = binary_dilation(gt, disk(gt_radius))
    else:
        raise Exception('Radius not supported')
    gt_area = watershed(image=ground_truth_area, markers=ground_truth_markers, mask=ground_truth_area,
                                watershed_line=False).astype(np.uint16)

    return gt_area


def metric_scores(prediction,ground_truth):
    """ Calculate metrics
    
    :param prediction: Particle centroid prediction.
    :param ground_truth: Intensity-coded ground truth area.
    :return: Metrics (precision, recall, f-score, splits, added, false negatives, true positives, false positives,
    number of predicted particles, number of ground truth particles)
    """

    # Assign each predicted particle centroid a unique id
    prediction = measure.label(prediction, connectivity=1, background=0)
    
    # Get the ground truth particle ids and the prediction particle seeds ids
    gt_ids, pred_ids = get_particle_ids(ground_truth), get_particle_ids(prediction)

    # Preallocate lists for used particles and for split particles
    used_ids_pred, used_ids_gt, split_ids_pred, split_ids_gt = [], [], [], []

    if np.max(ground_truth)==0:
        if np.max(prediction)==0:
            return 0
        else:
            return 1

    # Get particle centroid predictions that lie in a ground truth area, which is intensity-coded
    seeds_in_gt = ground_truth * (prediction > 0)
    seeds_in_gt_hist = np.histogram(seeds_in_gt, bins=range(1, gt_ids[-1] + 2), range=(1, gt_ids[-1] + 1))

    # Get prediction particle ids that match a gt particle (regardless of splits, ...)
    used_ids_pred.append(get_particle_ids(prediction * (seeds_in_gt > 0)))

    # Get gt particles that have (at least one) predicted particle centroid inside
    used_ids_gt.append(get_particle_ids(seeds_in_gt))

    # Find split particles (ids of the multiple predicted particles and of the corresponding gt particle)
    for i, num_particles in enumerate(seeds_in_gt_hist[0]):
        if num_particles > 1:
            split_ids_gt.append(seeds_in_gt_hist[1][i])
            split_ids_pred.append(get_particle_ids(prediction * (seeds_in_gt == seeds_in_gt_hist[1][i])))

    # Count split particles and check for multiple splits within one ground truth particles (the splitting of a gt 
    # particle into three seeds is counted as two splits, ...)
    num_split = 0
    for i in range(len(split_ids_pred)):
        num_split += len(split_ids_pred[i]) - 1

    # Find missing particles (gt particle not marked as used or split)
    num_missing = 0
    ids_gt = list(chain.from_iterable(used_ids_gt)) + split_ids_gt
    for particle_id in gt_ids:
        if particle_id not in ids_gt:
            num_missing += 1

    # Find added particles (predicted particle not marked as used or split)
    num_added = 0
    ids_pred = list(chain.from_iterable(used_ids_pred)) + list(chain.from_iterable(split_ids_pred))
    for particle in pred_ids:
        if particle not in ids_pred:
            num_added += 1

    # Calculate true positives, false positives and false negatives
    tp = len(pred_ids) - num_split - num_added
    fp = num_split + num_added
    fn = num_missing

    # Precision, recall and f_score of the image
    precision = tp / (tp + fp) if (tp + fp) > 0 else 0
    recall = tp / (tp + fn) if (tp + fn) > 0 else 0
    f_score = 2 * precision * recall / (precision + recall) if (recall + precision) > 0 else 0

    return f_score