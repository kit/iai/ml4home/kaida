from typing import List

import numpy as np
from sklearn import metrics

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.matching import InstanceMatching


def confusion_matrix(
    y_inst_pred: np.ndarray,
    y_inst_true: np.ndarray,
    y_sem_pred: np.ndarray,
    y_sem_true: np.ndarray,
    labels: List[int]
) -> np.ndarray:
    y_pred = list()
    y_true = list()

    matching = InstanceMatching.match(y_inst_pred, y_inst_true)

    for pred_inst_id in matching.paired_instance_ids.keys():
        true_inst_id = matching.paired_instance_ids[pred_inst_id][0]
        y_pred.append(np.unique(y_sem_pred[matching.get_user_instance_label_map(pred_inst_id) != 0])[0])
        y_true.append(np.unique(y_sem_true[matching.get_network_instance_label_map(true_inst_id) != 0])[0])

    for pred_inst_id in matching.unpaired_user_annotations:
        y_pred.append(np.unique(y_sem_pred[matching.get_user_instance_label_map(pred_inst_id) != 0])[0])
        y_true.append(-1)

    for true_inst_id in matching.unpaired_network_predictions:
        y_pred.append(-1)
        y_true.append(np.unique(y_sem_true[matching.get_network_instance_label_map(true_inst_id) != 0])[0])

    return metrics.confusion_matrix(y_true, y_pred, labels=labels + [-1])
