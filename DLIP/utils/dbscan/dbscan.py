import logging
import numpy as np
import matplotlib.pyplot as plt

from sklearn.cluster import DBSCAN
from scipy import spatial

# metric kann auch vorberechnete Distanzmatrix sein
def get_dbscan(data, metric):
    eps = 0.0001
    if metric == 'euclidean' or metric == 'cosine':
        if metric == 'euclidean':
            m = euclidean
        else:
            m = cosine
        distance = get_distance(data, m)
        distance[distance == 0] = np.nan
        eps = 0.4 * np.nanmedian(distance)
    else:
        logging.warn('Metric \'' + metric + '\' not known. Use \'euclidean\' or \'cosine\'.')

    print(distance)

    db = DBSCAN(eps=eps, min_samples=2, metric=metric).fit(data)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    print(labels)

    n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise = list(labels).count(-1)

    print('Estimated number of clusters: %d' % n_clusters)
    print('Estimated number of noise points: %d' % n_noise)

    return labels, core_samples_mask, n_clusters

def euclidean(a, b):
    return np.linalg.norm(a-b)

def cosine(a,b):
    return spatial.distance.cosine(a, b)

def get_distance(data : np.ndarray, metric = euclidean):
    distance = np.zeros((len(data),len(data)))
    for i in range(len(data)):
        for j in range(len(data)):
            distance[i,j] = metric(data[i],data[j])
    return distance

def get_sequence(labels : np.ndarray):
    size = len(labels)
    seq = []
    unseen = list(range(size))
    
    cluster = 0 if len(set(labels[unseen])) - (1 if -1 in labels else 0) != 0 else -1

    for _ in range(size):
        next_item = next((a for a in np.where(labels==cluster)[0] if a in unseen), None)
        seq.append(next_item)
        unseen.remove(next_item)
        cluster = next((x[1] for x in enumerate(set(labels[unseen])) if x[1] > cluster), -1 if len(set(labels[unseen])) == 0 else list(set(labels[unseen]))[0])        
    return seq

def get_visualization(labels, data, core_samples_mask, n_clusters):
    set_labels = set(labels)
    colors = [plt.cm.Spectral(each) for each in np.linspace(0, 1, len(set_labels))]
    for k, col in zip(set_labels, colors):
        class_member_mask = (labels == k)

        if k == -1:
            col = [0,0,0,1]
            xy = data[class_member_mask]
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                    markeredgecolor='k', markersize=4)
        else:
            xy = data[class_member_mask & core_samples_mask]
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                    markeredgecolor='k', markersize=10)

            xy = data[class_member_mask & ~core_samples_mask]
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                    markeredgecolor='k', markersize=6)
    
    plt.title('Estimated number of clusters: %d' % n_clusters)
    plt.savefig('test.png',dpi=200)