import numpy as np

from sklearn.cluster import Birch
from DLIP.utils.clustering.abstract_clustering import AbstractClustering

class BirchCluster(AbstractClustering):
    def __init__(self, n_clusters = 10, **kwargs) -> None:
        super().__init__()
        self.n_clusters = n_clusters

    def get_clustering(self, data):
        print('starting Birch')
        clustering = Birch(n_clusters=self.n_clusters).fit(data)
        labels = clustering.labels_

        cluster, size = np.unique(labels, return_counts = True)
        sizes = dict(zip(cluster, size))

        print('Estimated number of clusters: %d' % self.n_clusters)
        for i in range(self.n_clusters):
            print('Estimated points in cluster ' + str(i+1) + ': %d' % sizes[i])

        return labels, self.n_clusters