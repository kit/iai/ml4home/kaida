import logging
from turtle import distance
import numpy as np
from scipy.spatial.distance import pdist

from sklearn.cluster import DBSCAN

from DLIP.utils.clustering.abstract_clustering import AbstractClustering

class DBSCANCluster(AbstractClustering):
    def __init__(self, metric = 'cosine', eps=None, **kwargs) -> None:
        super().__init__()
        self.metric = metric
        self.eps = eps

    def get_clustering(self, data):
        if self.eps is None:
            if self.metric == 'euclidean' or self.metric == 'cosine':
                distance = pdist(data, metric=self.metric)
                eps = np.quantile(distance,0.001)
            else:
                logging.warn('Metric \'' + self.metric + '\' not known. Use \'euclidean\' or \'cosine\'.')
        else:
            eps = self.eps

        print('starting DBSCAN')
        db = DBSCAN(eps=eps, min_samples=5, metric=self.metric).fit(data)
        labels = db.labels_

        n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise = list(labels).count(-1)

        cluster, size = np.unique(labels, return_counts = True)
        sizes = dict(zip(cluster, size))

        print('Estimated number of clusters: %d' % n_clusters)
        print('Estimated number of noise points: %d' % n_noise)
        for i in range(n_clusters):
            print('Estimated points in cluster ' + str(i+1) + ': %d' % sizes[i])
            
        return labels, n_clusters