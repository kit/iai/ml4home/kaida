from abc import ABC, abstractmethod

class AbstractClustering(ABC):
    @abstractmethod
    def get_clustering(self, data):
        raise NotImplementedError("Getting clusters needs to be implemented.")