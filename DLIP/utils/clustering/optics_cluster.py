import logging
import numpy as np

from sklearn.cluster import OPTICS

from DLIP.utils.clustering.abstract_clustering import AbstractClustering

class OPTICSCluster(AbstractClustering):
    def __init__(self, metric = 'cosine', **kwargs) -> None:
        super().__init__()
        self.metric = metric

    def get_clustering(self, data):
        if not (self.metric == 'euclidean' or self.metric == 'cosine'):
            logging.warn('Metric \'' + self.metric + '\' not known. Use \'euclidean\' or \'cosine\'.')

        print('starting OPTICS')
        clustering = OPTICS(min_samples=5, metric=self.metric).fit(data)
        labels = clustering.labels_

        n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
        n_noise = list(labels).count(-1)

        cluster, size = np.unique(labels, return_counts = True)
        sizes = dict(zip(cluster, size))

        print('Estimated number of clusters: %d' % n_clusters)
        print('Estimated number of noise points: %d' % n_noise)
        for i in range(n_clusters):
            print('Estimated points in cluster ' + str(i+1) + ': %d' % sizes[i])

        return labels, n_clusters