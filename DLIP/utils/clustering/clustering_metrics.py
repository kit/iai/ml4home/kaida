import numpy as np
import matplotlib.pyplot as plt

from scipy import spatial

def euclidean(a, b):
    return np.linalg.norm(a-b)

def cosine(a,b):
    return spatial.distance.cosine(a, b)

def get_distance(data : np.ndarray, metric = euclidean):
    distance = np.zeros((len(data),len(data)))
    for i in range(len(data)):
        for j in range(len(data)):
            distance[i,j] = metric(data[i],data[j])

    return distance

def get_sequence(labels : np.ndarray):
    size = len(labels)
    seq = []
    unseen = list(range(size))
    
    cluster = 0 if len(set(labels[unseen])) - (1 if -1 in labels else 0) != 0 else -1

    for _ in range(size):
        next_item = next((a for a in np.where(labels==cluster)[0] if a in unseen), None)
        seq.append(next_item)
        unseen.remove(next_item)
        cluster = next((x[1] for x in enumerate(set(labels[unseen])) if x[1] > cluster), -1 if len(set(labels[unseen])) == 0 else list(set(labels[unseen]))[0])

    return seq

def get_visualization(labels, data, n_clusters):
    set_labels = set(labels)
    colors = [plt.cm.Spectral(each) for each in np.linspace(0, 1, len(set_labels))]
    for k, col in zip(set_labels, colors):
        class_member_mask = (labels == k)

        if k == -1:
            col = [0,0,0,1]
            xy = data[class_member_mask]
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                    markeredgecolor='k', markersize=4)
        else:
            xy = data[class_member_mask]
            plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col),
                    markeredgecolor='k', markersize=6)
    
    plt.title('Estimated number of clusters: %d' % n_clusters)
    plt.savefig('test.png',dpi=200)