import numpy as np


def norm_percentile(image, eps: float = 10**-8):
    image = image.astype("float32")

    min_val = np.percentile(image, 1)
    max_val = np.percentile(image, 99)

    image[image < min_val] = min_val
    image[image > max_val] = max_val

    if max_val - min_val < eps:
        if max_val != 0:
            return image / max_val
        return image

    image = (image - min_val) / (max_val - min_val)

    return image
