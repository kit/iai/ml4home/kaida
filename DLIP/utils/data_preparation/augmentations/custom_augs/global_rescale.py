from albumentations import RandomScale
import albumentations.augmentations.functional as F
import cv2



class GlobalRescale(RandomScale):
    """Crop a random part of the input.

    Args:
        scale_factor (float): factor to rescale image.
        p (float): probability of applying the transform. Default: 1.

    Targets:
        image, mask, bboxes, keypoints

    Image types:
        uint8, float32
    """

    def __init__(self, scale_factor, interpolation=cv2.INTER_LINEAR, always_apply=False, p=1.0):
        super().__init__(always_apply, p)
        self.scale_factor = scale_factor
        self.interpolation = interpolation

    def get_params(self):
        return {"scale": self.scale_factor}

    def apply(self, img, scale=0, interpolation=cv2.INTER_LINEAR, **params):
        return F.scale(img, scale, interpolation)