from albumentations import RandomCrop
import albumentations.augmentations.functional as F
import random
import numpy as np


class RandomCropLabelAware(RandomCrop):
    """Crop a random part of the input.

    Args:
        height (int): height of the crop.
        width (int): width of the crop.
        p (float): probability of applying the transform. Default: 1.

    Targets:
        image, mask, bboxes, keypoints

    Image types:
        uint8, float32
    """

    def __init__(self, height, width, p_not_empty, always_apply=False, p=1.0):
        super().__init__(always_apply, p)
        self.height = height
        self.width = width
        self.p_not_empty= p_not_empty

    def apply(self, img, h_start=0, w_start=0, **params):
        return F.random_crop(img, self.height, self.width, h_start, w_start)

    def get_params_dependent_on_targets(self, params):
        mask = params["mask"]
        h_start, w_start =  random.random(), random.random()
        continue_crop_selection = True
        while continue_crop_selection:
            crop = F.random_crop(mask, self.height, self.width, h_start, w_start)
            if np.max(crop)>0:
                continue_crop_selection = False
            else: 
                if random.random()<=self.p_not_empty:
                    h_start, w_start =  random.random(), random.random()
                else:
                    continue_crop_selection = False

        return {"h_start": h_start, "w_start": w_start}

    @property
    def targets_as_params(self):
        return ["mask"]


    def apply_to_mask(self, img, h_start=0, w_start=0, **params):
        return F.random_crop(img, self.height, self.width, h_start, w_start)