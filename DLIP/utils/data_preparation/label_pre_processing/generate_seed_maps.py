"""
Copyright 2022 Tim Scherr tim.scherr@kit.edu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


import os
import numpy as np
import matplotlib.pyplot as plt
import cv2
from skimage.color import label2rgb
from DLIP.utils.data_preparation.label_pre_processing.centroid_post_pro import centroid_postprocessing
from scipy.ndimage import binary_dilation
from scipy.ndimage.morphology import generate_binary_structure
from skimage import measure
from skimage.morphology import disk
import tifffile
import argparse


class SeedMapGenerator:
    def __init__(
            self, 
            root_dir,
            label_dir = "labels") -> None:
        root_dir = root_dir
        self.label_dir = label_dir
        self.data_dirs = [os.path.join(root_dir,"train"), os.path.join(root_dir,"test")]

    def generate_labels(self, folder_name):
        for data_dir in self.data_dirs:
            if not os.path.exists(os.path.join(data_dir, folder_name)):
                os.makedirs(os.path.join(data_dir, folder_name))
            for file in os.listdir(os.path.join(data_dir,self.label_dir)):
                file_path_src = os.path.join(data_dir, self.label_dir, file)
                file_path_dst = os.path.join(data_dir, folder_name, file).replace("png","tif")

                if os.path.exists(file_path_dst):
                    continue

                label_raw = tifffile.imread(file_path_src).squeeze()
                label_raw = measure.label(label_raw)

                label_center = centroid_postprocessing(label_raw)
                label_seed = self._calc_seed_map(label_center)
         

                img= tifffile.imread(os.path.join(data_dir, "samples", file.replace("_label.tif",".tif")))

                tifffile.imwrite(file_path_dst,label_seed)
                # plt.imshow(label2rgb(label_seed,(img-img.min())/(img.max()-img.min()),bg_label=0)) 
                # plt.show()
    
        print("Done")


    def _calc_seed_map(self, label, label_type='disk_5'):
        if label_type == '1x1':
            label_img = label.astype(np.uint8)
        elif label_type == '1x1_dilation':
            label_img = binary_dilation(label, generate_binary_structure(2, 1)) > 0
            label_img = label_img.astype(np.uint8)
        elif label_type == '3x3':
            label_img = binary_dilation(label, generate_binary_structure(2, 2)) > 0
            label_img = label_img.astype(np.uint8)
        elif "disk_" in label_type:
            label_img = binary_dilation(label,  disk(int(label_type.replace("disk_", "")))) > 0
            label_img = label_img.astype(np.uint8)
        else:
            raise Exception('Label type not known')
        return label_img

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Make seed maps')
    parser.add_argument('--dataset_path')
    args = parser.parse_args()

    # dataset_path = "/home/ws/sc1357/Downloads/Microglia_Theresa/" #args.dataset_path
    
    dmg_obj = SeedMapGenerator(args.dataset_path)
    dmg_obj.generate_labels("labels_mask")