import argparse
import os

import numpy as np
import tifffile
from scipy.ndimage.morphology import distance_transform_edt
from skimage import measure


class DistanceMapGenerator:
    def __init__(self, root_dir, label_dir="labels") -> None:
        root_dir = root_dir
        self.label_dir = label_dir
        self.data_dirs = [os.path.join(root_dir, "train"), os.path.join(root_dir, "test")]

    def generate_labels(self, folder_name):
        for data_dir in self.data_dirs:
            if not os.path.exists(os.path.join(data_dir, folder_name)):
                os.makedirs(os.path.join(data_dir, folder_name))
            for file in os.listdir(os.path.join(data_dir, self.label_dir)):
                file_path_src = os.path.join(data_dir, self.label_dir, file)
                file_path_dst = os.path.join(data_dir, folder_name, file).replace("png", "tif")

                if os.path.exists(file_path_dst):
                    continue

                print(file)
                label_raw = tifffile.imread(file_path_src)
                # label_raw = measure.label(label_raw)

                label_dist = self.apply_dist_transform(label_raw)
                # label_rgb = label2rgb(label_raw[:,:,0],bg_label=0)

                tifffile.imwrite(file_path_dst, label_dist.astype(np.float32))
                # plt.imshow(label_dist)
                # plt.show()

        print("Done")

    @staticmethod
    def apply_dist_transform(label):
        # Preallocation
        label_dist = np.zeros(shape=label.shape, dtype=np.float)

        # Find centroids, calculate distance transforms
        props = measure.regionprops(label)
        for i in range(len(props)):
            # Get nucleus and Euclidean distance transform for each nucleus
            nucleus = label == props[i].label
            # centroid, diameter = np.round(props[i].centroid), int(np.ceil(props[i].equivalent_diameter))
            nucleus_crop_dist = distance_transform_edt(nucleus)
            if np.max(nucleus_crop_dist) > 0:
                nucleus_crop_dist = nucleus_crop_dist / np.max(nucleus_crop_dist)
            label_dist += nucleus_crop_dist

        return label_dist


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Make distance maps")
    parser.add_argument("--dataset_path")
    parser.add_argument("--instance_labels_dir", default="labels")
    args = parser.parse_args()

    dataset_path = args.dataset_path

    dmg_obj = DistanceMapGenerator(dataset_path, label_dir=args.instance_labels_dir)
    dmg_obj.generate_labels(f"{args.instance_labels_dir}_dist_map")