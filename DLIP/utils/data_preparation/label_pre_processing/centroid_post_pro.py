"""
Copyright 2022 Tim Scherr tim.scherr@kit.edu

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import numpy as np
from skimage import measure

def centroid_postprocessing(prediction, do_binarize=False
                            ):
    """ Post-processing for centroid extraction.
    :param prediction: Marker prediction.
    :return: Centroid image
    """
    # Binarize the channels
    if do_binarize:
        prediction_bin = prediction > 0.5
    else:
        prediction_bin = prediction
    
    # Label markers
    markers = measure.label(prediction_bin, connectivity=1, background=0)

    # Allocate results array
    mask = np.zeros_like(prediction_bin)

    # Get seeds
    props = measure.regionprops(markers)
    for i in range(len(props)):
        centroid = props[i].centroid
        mask[tuple(np.round(centroid).astype(np.uint16))] = True

    return mask