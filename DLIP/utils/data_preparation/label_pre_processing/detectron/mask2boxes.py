import numpy as np
from detectron2.structures import Instances
from detectron2.structures import Boxes, BitMasks
import torch

def mask2boxes(mask_img):
    mask_img = mask_img.numpy().squeeze()

    obj_ids = np.unique(mask_img)

    # first id is the background, so remove it
    #obj_ids = obj_ids[1:]
    obj_ids = obj_ids[1:]

    # split the color-encoded mask into a set
    # of binary masks
    masks = mask_img == obj_ids[:, None, None]
    
    # get bounding box coordinates for each mask
    num_objs = len(obj_ids)

    #print(num_objs)
    # print(num_objs)
    boxes = []
    rel_inst = []
    for i in range(num_objs):
        pos = np.where(masks[i])
        xmin = np.min(pos[1])
        xmax = np.max(pos[1])
        ymin = np.min(pos[0])
        ymax = np.max(pos[0])
        if xmax-xmin>1 and ymax-ymin>1:
            rel_inst.append(i)
            
        boxes.append([xmin, ymin, xmax, ymax])

    boxes = [boxes[rel_id] for rel_id in rel_inst]
    masks = masks[rel_inst,:]
    num_objs = len(rel_inst)

    target = Instances(mask_img.shape)
    target.gt_boxes = Boxes(boxes)
    target.gt_classes = torch.zeros((num_objs,), dtype=torch.int64)

    if num_objs>0:
        target.gt_masks = BitMasks(
                torch.stack([torch.from_numpy(np.ascontiguousarray(x)) for x in masks])
            )
    else:
        target.gt_masks = BitMasks(
                torch.zeros(0,mask_img.shape[0],mask_img.shape[1])
        )
        
    return  target 
