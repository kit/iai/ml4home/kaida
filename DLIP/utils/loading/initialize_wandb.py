import tempfile
import yaml
import wandb
import platform

def initialize_wandb(cfg_yaml:dict, experiment_dir:str, config_name:str, disabled=False):
    if platform.system() == "Linux":
        with tempfile.NamedTemporaryFile(mode = "w", suffix='.yaml') as tmp:
            yaml.dump(cfg_yaml, tmp)
            wandb.init(
                config=tmp.name,
                allow_val_change=True,
                project=None if disabled else cfg_yaml['wandb.project_name']['value'],
                entity= None if disabled else cfg_yaml['wandb.entity']['value'],
                tags= None if disabled else cfg_yaml['wandb.tags']['value'],
                notes= None if disabled else cfg_yaml['wandb.notes']['value'],
                mode="disabled"if disabled else  cfg_yaml['wandb.mode']['value'],
                dir=None if disabled else experiment_dir,
            )
    else:
        with tempfile.NamedTemporaryFile(mode = "w", suffix='.yaml', delete=False) as tmp:
            yaml.dump(cfg_yaml, tmp)
            wandb.init(
                config=tmp.name,
                allow_val_change=True,
                project=None if disabled else cfg_yaml['wandb.project_name']['value'],
                entity= None if disabled else cfg_yaml['wandb.entity']['value'],
                tags= None if disabled else cfg_yaml['wandb.tags']['value'],
                notes= None if disabled else cfg_yaml['wandb.notes']['value'],
                mode="disabled"if disabled else  cfg_yaml['wandb.mode']['value'],
                dir=None if disabled else experiment_dir,
            )

    if not disabled:
        wandb.run.name = f"{cfg_yaml['experiment.name']['value']}_{cfg_yaml['data.datamodule.name']['value']}_{cfg_yaml['model.name']['value']}_{config_name}"
        wandb.run.save()
        # if 'train.cross_validation.active' in cfg_yaml:
        #     wandb.tensorboard.patch(tensorboardX=False, pytorch=True, root_logdir=experiment_dir)
        # else:
        #     wandb.tensorboard.patch(pytorch=True)
    return wandb.config