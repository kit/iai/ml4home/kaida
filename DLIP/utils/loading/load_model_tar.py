
import tarfile
import os

from pytorch_lightning.utilities.seed import seed_everything
from DLIP.utils.loading.initialize_wandb import initialize_wandb
from DLIP.utils.loading.load_transforms import load_transforms
from DLIP.utils.loading.load_model import load_model
from DLIP.utils.loading.merge_configs import merge_configs
from DLIP.utils.loading.split_parameters import split_parameters
import torch
     
def load_model_trafos_tar(model_tar_path):
    tar = tarfile.open(model_tar_path, "r:")
    base_path = os.path.dirname(model_tar_path)
    tar.extractall(path=base_path)
    tar.close()

    # load model
    cfg_yaml = merge_configs(os.path.join(base_path, "cfg.yaml"))

    config = initialize_wandb(
        cfg_yaml=cfg_yaml,
        disabled=True,
        experiment_dir=None,
        config_name=None,
    )

    seed_everything(seed=cfg_yaml['experiment.seed']['value'])
    parameters_splitted = split_parameters(config, ["model", "train", "data"])

    model = load_model(
        parameters_splitted["model"], 
        checkpoint_path_str=os.path.join(base_path, "dnn_weights.ckpt")
    )

    if torch.cuda.is_available():
        model.cuda()
    model.eval()

    _, _, test_trafos = load_transforms(parameters_splitted["data"])

    os.remove(os.path.join(base_path, "dnn_weights.ckpt"))
    os.remove(os.path.join(base_path, "cfg.yaml"))

    return model, test_trafos