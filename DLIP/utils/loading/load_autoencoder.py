from DLIP.models.autoencoder.autoencoder import AutoEncoder
from DLIP.utils.loading.dict_to_config import dict_to_config
from DLIP.utils.loading.split_parameters import split_parameters


def load_autoencoder(autoencoder_params: dict, checkpoint_path_str = None):
    ae = AutoEncoder(**autoencoder_params)

    if checkpoint_path_str is not None:
        ae.add_pretrained('temp',checkpoint_path_str)
        ae = ae.from_pretrained('temp')

    return ae
