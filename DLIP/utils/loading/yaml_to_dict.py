from DLIP.utils.loading.config_parser import ConfigParser
import yaml

def yaml2dict(yaml_path: str):
    cfg_yaml = yaml.load(open(yaml_path), Loader=yaml.FullLoader)
    return ConfigParser.read(cfg_yaml)