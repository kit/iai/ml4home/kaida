import importlib
from DLIP.utils.loading.load_submodule import load_submodule
from difflib import get_close_matches
import os

def load_functions(module_path: str, task: str, project_path: str):
    """ Loads a class from a given module and object name.

    Args:
        module_path (str): Defines the module path. Expected to start after DLIP.
        class_name (str): The name of the class to be loaded.

    Raises:
        ModuleNotFoundError: If the given class_name is not found

    Returns:
        (Class): The class.
    """
    module_lst = list()
    generic_module_path = os.path.join(
        os.path.dirname(project_path), 
        module_path.replace(".", os.sep), 
        "generic"
    )
    # adaptive loading of folder generic
    if os.path.exists(generic_module_path):
        module_lst.extend(load_submodule(f"{module_path}.{'generic'}"))

    module_lst.extend(load_submodule(f"{module_path}.{task}"))


    # try:
    #     module_lst.extend(load_submodule(f"{module_path}.{task}"))
    # except:
    #     pass

    return module_lst