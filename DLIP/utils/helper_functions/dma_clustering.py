import os 

def get_dma_list(file_lst, img_format=".tif"):
    dma_list = []

    for f in file_lst:
        ext = os.path.splitext(f)[1]  
        if ext.lower() != img_format:
            continue

        img_name = os.path.splitext("_".join(f.split("_")[:-1]))[0]
        dma_list.append(img_name)
    
    return list(set(dma_list))