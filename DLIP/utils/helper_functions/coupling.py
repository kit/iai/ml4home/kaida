import os
import shutil
import yaml
import datetime
import shutil

def create_model_package(result_dir,experiment_dir, cfg_yaml, config_name):
    d = datetime.datetime.now()
    timestamp = "%04d%02d%02d_%02d%02d" % (d.year, d.month, d.day, d.hour, d.minute)
    package_path = os.path.join(result_dir, f"{timestamp}_{config_name}_dnn_model")
    os.makedirs(package_path)
    
    with open(os.path.join(package_path, "cfg.yaml"), 'w') as file_descriptor:
        yaml.safe_dump(cfg_yaml, file_descriptor)#

    src_weight_path = os.path.join(experiment_dir, "dnn_weights.ckpt")
    dst_weight_path = os.path.join(package_path, "dnn_weights.ckpt")

    shutil.copy2(src_weight_path,dst_weight_path)
    shutil.make_archive(f"{package_path}", 'tar', package_path)

    shutil.rmtree(package_path)
    shutil.rmtree(experiment_dir)