from abc import ABC, abstractmethod

class AbstractImgPreProcessor(ABC):
    def __init__(self, project):
        super().__init__()
        self.project = project
        self.do_always = True

    @abstractmethod
    def process(self, img, status_bar):
        raise NotImplementedError("Process fcn needs to be implemented")