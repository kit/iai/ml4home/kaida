from DLIP.pre_img_processing.abstract_img_pre_processor import AbstractImgPreProcessor
from DLIP.utils.helper_functions.gray_level_check import is_gray
import numpy as np

class ConstantNoiseFilter(AbstractImgPreProcessor):
    def __init__(self, project, noise_threshold):
        super(ConstantNoiseFilter, self).__init__(project)
        self.noise_threshold = noise_threshold

    def process(self, img, status_bar=None):
        
        if is_gray(img):
            img[img<=self.noise_threshold] = 0
            return img
        else:
            if status_bar is not None:
                status_bar.showMessage("Pre image processing not possible for RGB image",)
            return img