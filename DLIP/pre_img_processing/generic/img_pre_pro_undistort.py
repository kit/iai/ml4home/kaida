from DLIP.pre_img_processing.abstract_img_pre_processor import AbstractImgPreProcessor
import cv2

def load_coefficients(path):
    '''Loads camera matrix and distortion coefficients.'''
    # FILE_STORAGE_READ
    cv_file = cv2.FileStorage(path, cv2.FILE_STORAGE_READ)

    # note we also have to specify the type to retrieve other wise we only get a
    # FileNode object back instead of a matrix
    camera_matrix = cv_file.getNode('K').mat()
    dist_matrix = cv_file.getNode('D').mat()

    cv_file.release()
    return [camera_matrix, dist_matrix]


class Undistort(AbstractImgPreProcessor):
    def __init__(
        self, 
        project,
        calibration_yml):
        super(Undistort, self).__init__(project)
        self.do_always = False
        self.mtx, self.dist = load_coefficients(calibration_yml)

    def process(self, img, status_bar):
        return cv2.undistort(img, self.mtx, self.dist, None, None)