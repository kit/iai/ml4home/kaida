from DLIP.pre_img_processing.abstract_img_pre_processor import AbstractImgPreProcessor
from DLIP.utils.helper_functions.gray_level_check import is_gray
import numpy as np
 
class NormalizePercentile(AbstractImgPreProcessor):
    def __init__(self, project):
        super(NormalizePercentile, self).__init__(project)
 
    def process(self, img, status_bar=None):
        if is_gray(img):
            # normalize to 1, 99 percentile and afterwards to range of uint16
            min_val, max_val = self.extract_percentile(img, 1, 99)
 
            img[img<min_val] = min_val
            img[img>max_val] = max_val
            img_norm = ((img - min_val) / (max_val - min_val))
            if img.dtype == np.dtype("uint8") or img.dtype == np.uint8:
                return (img_norm*255).astype(np.uint8)
            else:
                return (img_norm*65535).astype(np.uint16)
        else:
            if status_bar is not None:
                status_bar.showMessage("Pre image processing not possible for RGB image",)
        return img
 
    def extract_percentile(self, data, percentile_min, percentile_max):
        min_value = np.percentile(data, percentile_min)
        max_value = np.percentile(data, percentile_max)
        return min_value, max_value