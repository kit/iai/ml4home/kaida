from DLIP.pre_img_processing.abstract_img_pre_processor import AbstractImgPreProcessor
from skimage.transform import rescale
import cv2

class Resample(AbstractImgPreProcessor):
    def __init__(
        self, 
        project,
        resample_factor):
        super(Resample, self).__init__(project)
        self.resample_factor = resample_factor
        self.do_always = False

    def process(self, img, status_bar):
        self.old_size = img.shape[0:2]
        if img.ndim==2:
            return rescale(img, scale=self.resample_factor,order=2 ,preserve_range=True).astype(img.dtype)
        return rescale(img, scale=self.resample_factor,order=2 ,preserve_range=True,multichannel=True).astype(img.dtype)

    def remap_label(self, label):
        label_remap = cv2.resize(label.squeeze(),(self.old_size[1],self.old_size[0]), interpolation=cv2.INTER_NEAREST)
        return label_remap