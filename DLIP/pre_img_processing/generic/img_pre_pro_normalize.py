from DLIP.pre_img_processing.abstract_img_pre_processor import AbstractImgPreProcessor
from DLIP.utils.helper_functions.gray_level_check import is_gray, gray_redundand
import numpy as np

class Normalize(AbstractImgPreProcessor):
    def __init__(self, project):
        super(Normalize, self).__init__(project)

    def process(self, img, status_bar=None):
        if is_gray(img):
            min_val = np.min(img)
            max_val = np.max(img)
            if min_val!=max_val:
                img_norm = (img-min_val)/(max_val-min_val)

            if img.dtype == np.dtype("uint8") or img.dtype == np.uint8:
                return (img_norm*255).astype(np.uint8)
            else:
                return (img_norm*65535).astype(np.uint16)
        else:
            if status_bar is not None:
                status_bar.showMessage("Pre image processing not possible for RGB image",)
            return img