from DLIP.pre_img_processing.abstract_img_pre_processor import AbstractImgPreProcessor
import sys
import cv2
import numpy as np


class Crop(AbstractImgPreProcessor):
    def __init__(self, project,permanent):
        super(Crop, self).__init__(project)
        self.permanent=permanent
        self.r=None
        self.do_always = False
        self.old_size = None

    def process(self, img, status_bar):
        if self.permanent == False or self.r == None:
            # Select ROI
            cv2.namedWindow('Select ROI (Press enter to finalize ROI)', cv2.WINDOW_NORMAL)
            cv2.resizeWindow('Select ROI (Press enter to finalize ROI)',500,500)
            self.r = cv2.selectROI(
                "Select ROI (Press enter to finalize ROI)", cv2.cvtColor(img,cv2.COLOR_BGR2RGB))

	    # Crop image
        self.old_size = img.shape[0:2]
        img_crop = img[int(self.r[1]):int(self.r[1]+self.r[3]), int(self.r[0]):int(self.r[0]+self.r[2])]
        cv2.destroyAllWindows()
        return img_crop

    def remap_label(self, label):
        label_remap = np.zeros(self.old_size, dtype=label.dtype) 
        label_remap[int(self.r[1]):int(self.r[1]+self.r[3]), int(self.r[0]):int(self.r[0]+self.r[2])] = label 
        return label_remap