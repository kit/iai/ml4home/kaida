from DLIP.pre_img_processing.abstract_img_pre_processor import AbstractImgPreProcessor
from skimage.restoration import denoise_bilateral,estimate_sigma

class BilateralNoiseFilter(AbstractImgPreProcessor):
    def __init__(self, project):
        super(BilateralNoiseFilter, self).__init__(project)

    def process(self, img, status_bar):
        sigma_est = estimate_sigma(img, average_sigmas=True)
        if img.ndim == 3:
            multichnl = True
        else:
            multichnl = False
        img_denoise = denoise_bilateral(img,  sigma_color=sigma_est, multichannel=multichnl)
        return img_denoise
   