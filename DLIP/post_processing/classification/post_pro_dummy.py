from DLIP.post_processing.abstract_post_processor import AbstractPostProcessor
import numpy as np

class Dummy(AbstractPostProcessor):
    def __init__(
        self,
        project,
    ):
        super(Dummy,self).__init__(project)

    def process(self, label, status_bar):
        label_post = label.copy()
        label_post[3] = 0.3
        label_post[4] = 0.7
        return label_post


# Interphase,Prophase,Metaphase,Anaphase,Telophase,Cytokinesis