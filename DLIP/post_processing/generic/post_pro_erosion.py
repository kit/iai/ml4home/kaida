from DLIP.post_processing.abstract_post_processor import AbstractPostProcessor
import numpy as np
import cv2

class Erosion(AbstractPostProcessor):
    def __init__(
        self,
        project,
        kernel_size,
        iterations=1
    ):
        super(Erosion,self).__init__(project)
        self.iterations = iterations
        self.kernel = np.ones((kernel_size,kernel_size),np.uint8) 

    def process(self, label, status_bar):
        post_pro_label = cv2.erode(
            label, 
            self.kernel, 
            iterations=self.iterations
        )
        return post_pro_label