from DLIP.post_processing.abstract_post_processor import AbstractPostProcessor
import numpy as np
from skimage.morphology import remove_small_objects

class RemoveSmallObjects(AbstractPostProcessor):
    def __init__(
        self,
        project,
        object_area_threshold,
    ):
        super(RemoveSmallObjects,self).__init__(project)
        self.object_area_threshold = object_area_threshold

    def process(self, label, status_bar):
        post_pro_label = np.zeros_like(label)

        for elem in range(1,np.max(label)+1):
            post_pro_label += elem*remove_small_objects(label==elem, min_size=self.object_area_threshold).astype("uint16")

        cv2.imwrite("pp_label.png", post_pro_label)
        cv2.imwrite("raw_label.png", label)
        print("sdsd")
        return post_pro_label