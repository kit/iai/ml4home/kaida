from DLIP.post_processing.abstract_post_processor import AbstractPostProcessor
import numpy as np
from skimage.morphology import remove_small_holes

class RemoveHoles(AbstractPostProcessor):
    def __init__(
        self,
        project,
        hole_area_threshold,
    ):
        super(RemoveHoles,self).__init__(project)
        self.hole_area_threshold = hole_area_threshold

    def process(self, label, status_bar):
        post_pro_label = np.zeros_like(label)

        for elem in range(1,np.max(label)+1):
            post_pro_label += elem*remove_small_holes(label==elem, area_threshold=self.hole_area_threshold).astype("uint16")
            
        return post_pro_label