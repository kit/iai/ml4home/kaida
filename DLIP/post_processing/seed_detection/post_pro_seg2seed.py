from DLIP.post_processing.abstract_post_processor import AbstractPostProcessor
import numpy as np
from skimage.measure import regionprops
from skimage.measure import label as label_fcn
from skimage.draw import disk

class SegmentToSeed(AbstractPostProcessor):
    def __init__(
        self,
        project,
        seed_size,
    ):
        super(SegmentToSeed,self).__init__(project)
        self.seed_size = seed_size


    def process(self, label, status_bar):   
        label_seed = np.zeros_like(label, dtype=np.uint8)
        region_probs = regionprops(label_fcn(label, connectivity=2))

        for prop in region_probs:
            (row, col) = prop["centroid"]
            yy, xx = disk((row, col), radius=self.seed_size, shape=label_seed.shape[0:2])
            label_seed[yy, xx] = 1

        return label_seed