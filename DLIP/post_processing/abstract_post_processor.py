from abc import ABC, abstractmethod
class AbstractPostProcessor(ABC):
    def __init__(self, project):
        super().__init__()
        self.project = project

    @abstractmethod
    def process(self, label, status_bar):
        raise NotImplementedError("Process fcn needs to be implemented")