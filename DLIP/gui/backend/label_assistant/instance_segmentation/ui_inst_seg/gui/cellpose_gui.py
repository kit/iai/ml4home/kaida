"""
Copyright © 2020 Howard Hughes Medical Institute

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of HHMI nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import glob
import os
import pathlib
import sys
import warnings
from typing import Tuple

import cv2
import numpy as np
import pyqtgraph as pg
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import QMainWindow, QApplication, QSizePolicy, QSpacerItem, QWidget, QComboBox, QGridLayout, \
    QFrame, QCheckBox, QLabel
from natsort import natsorted
from skimage import measure

from DLIP.gui.backend.label_assistant.instance_segmentation.ui_inst_seg.gui import guiparts, io, menus
from DLIP.gui.backend.label_assistant.instance_segmentation.ui_inst_seg.utils import masks_to_outlines

try:
    import matplotlib.pyplot as plt
    MATPLOTLIB = True
except:
    MATPLOTLIB = False


class QHLine(QFrame):
    def __init__(self):
        super(QHLine, self).__init__()
        self.setFrameShape(QFrame.HLine)
        self.setFrameShadow(QFrame.Sunken)


def avg3d(C):
    """ smooth value of c across nearby points
        (c is center of grid directly below point)
        b -- a -- b
        a -- c -- a
        b -- a -- b
    """
    Ly, Lx = C.shape
    # pad T by 2
    T = np.zeros((Ly+2, Lx+2), np.float32)
    M = np.zeros((Ly, Lx), np.float32)
    T[1:-1, 1:-1] = C.copy()
    y,x = np.meshgrid(np.arange(0, Ly, 1, int), np.arange(0, Lx, 1, int), indexing='ij')
    y += 1
    x += 1
    a = 1./2  # /(z**2 + 1)**0.5
    b = 1./(1+2**0.5)  # (z**2 + 2)**0.5
    c = 1.
    M = (
            b * T[y-1, x-1] + a * T[y-1, x] + b * T[y-1, x+1] +
            a * T[y, x-1] + c * T[y, x] + a * T[y, x+1] +
            b * T[y+1, x-1] + a * T[y+1, x] + b * T[y+1, x+1]
    )

    M /= 4 * a + 4 * b + c
    return M


def interpZ(mask, zdraw):
    """ find nearby planes and average their values using grid of points
        zfill is in ascending order
    """
    ifill = np.ones(mask.shape[0], np.bool)
    zall = np.arange(0, mask.shape[0], 1, int)
    ifill[zdraw] = False
    zfill = zall[ifill]
    zlower = zdraw[np.searchsorted(zdraw, zfill, side='left')-1]
    zupper = zdraw[np.searchsorted(zdraw, zfill, side='right')]
    for k, z in enumerate(zfill):
        Z = zupper[k] - zlower[k]
        zl = (z-zlower[k])/Z
        plower = avg3d(mask[zlower[k]]) * (1-zl)
        pupper = avg3d(mask[zupper[k]]) * zl
        mask[z] = (plower + pupper) > 0.33
        # Ml, norml = avg3d(mask[zlower[k]], zl)
        # Mu, normu = avg3d(mask[zupper[k]], 1-zl)
        # mask[z] = (Ml + Mu) / (norml + normu)  > 0.5
    return mask, zfill


def make_bwr():
    # make a bwr colormap
    b = np.append(255 * np.ones(128), np.linspace(0, 255, 128)[::-1])[:, np.newaxis]
    r = np.append(np.linspace(0, 255, 128), 255*np.ones(128))[:, np.newaxis]
    g = np.append(np.linspace(0, 255, 128), np.linspace(0, 255, 128)[::-1])[:, np.newaxis]
    color = np.concatenate((r, g, b), axis=-1).astype(np.uint8)
    bwr = pg.ColorMap(pos=np.linspace(0.0, 255, 256), color=color)
    return bwr


def make_spectral():
    # make spectral colormap
    r = np.array([0,4,8,12,16,20,24,28,32,36,40,44,48,52,56,60,64,68,72,76,80,84,88,92,96,100,104,108,112,116,120,124,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,120,112,104,96,88,80,72,64,56,48,40,32,24,16,8,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,7,11,15,19,23,27,31,35,39,43,47,51,55,59,63,67,71,75,79,83,87,91,95,99,103,107,111,115,119,123,127,131,135,139,143,147,151,155,159,163,167,171,175,179,183,187,191,195,199,203,207,211,215,219,223,227,231,235,239,243,247,251,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255])
    g = np.array([0,1,2,3,4,5,6,7,8,9,10,9,9,8,8,7,7,6,6,5,5,5,4,4,3,3,2,2,1,1,0,0,0,7,15,23,31,39,47,55,63,71,79,87,95,103,111,119,127,135,143,151,159,167,175,183,191,199,207,215,223,231,239,247,255,247,239,231,223,215,207,199,191,183,175,167,159,151,143,135,128,129,131,132,134,135,137,139,140,142,143,145,147,148,150,151,153,154,156,158,159,161,162,164,166,167,169,170,172,174,175,177,178,180,181,183,185,186,188,189,191,193,194,196,197,199,201,202,204,205,207,208,210,212,213,215,216,218,220,221,223,224,226,228,229,231,232,234,235,237,239,240,242,243,245,247,248,250,251,253,255,251,247,243,239,235,231,227,223,219,215,211,207,203,199,195,191,187,183,179,175,171,167,163,159,155,151,147,143,139,135,131,127,123,119,115,111,107,103,99,95,91,87,83,79,75,71,67,63,59,55,51,47,43,39,35,31,27,23,19,15,11,7,3,0,8,16,24,32,41,49,57,65,74,82,90,98,106,115,123,131,139,148,156,164,172,180,189,197,205,213,222,230,238,246,254])
    b = np.array([0,7,15,23,31,39,47,55,63,71,79,87,95,103,111,119,127,135,143,151,159,167,175,183,191,199,207,215,223,231,239,247,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,251,247,243,239,235,231,227,223,219,215,211,207,203,199,195,191,187,183,179,175,171,167,163,159,155,151,147,143,139,135,131,128,126,124,122,120,118,116,114,112,110,108,106,104,102,100,98,96,94,92,90,88,86,84,82,80,78,76,74,72,70,68,66,64,62,60,58,56,54,52,50,48,46,44,42,40,38,36,34,32,30,28,26,24,22,20,18,16,14,12,10,8,6,4,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,8,16,24,32,41,49,57,65,74,82,90,98,106,115,123,131,139,148,156,164,172,180,189,197,205,213,222,230,238,246,254])
    color = np.vstack((r, g, b)).T.astype(np.uint8)
    spectral = pg.ColorMap(pos=np.linspace(0.0, 255, 256), color=color)
    return spectral


def make_cmap(cm=0):
    # make a single channel colormap
    r = np.arange(0, 256)
    color = np.zeros((256, 3))
    color[:, cm] = r
    color = color.astype(np.uint8)
    cmap = pg.ColorMap(pos=np.linspace(0.0, 255, 256), color=color)
    return cmap


def run(image=None):
    # Always start by initializing Qt (only once per application)
    warnings.filterwarnings("ignore")
    app = QApplication(sys.argv)
    os.environ['MXNET_CUDNN_AUTOTUNE_DEFAULT'] = '0'

    InstanceSegUi(image=image)
    ret = app.exec_()
    sys.exit(ret)


def get_unique_points(set):
    cps = np.zeros((len(set), 3), np.int32)
    for k, pp in enumerate(set):
        cps[k, :] = np.array(pp)
    set = list(np.unique(cps, axis=0))
    return set


class InstanceSegUi(QMainWindow):
    def __init__(
            self,
            image=None,
            _save_label=None,
            on_label_ui_closed=None,
            ui_shape: Tuple[int, int] = None,
            ui_position: Tuple[int, int] = None,
            parent: QWidget = None
    ):
        super(InstanceSegUi, self).__init__(parent)

        self.save_label = _save_label
        self.on_label_ui_closed = on_label_ui_closed

        pg.setConfigOptions(imageAxisOrder="row-major")

        self.setGeometry(50, 50, 1200, 1000)

        if ui_shape is not None:
            self.setMinimumWidth(ui_shape[0])
            self.setMinimumHeight(ui_shape[1])

        if ui_position is not None:
            self.move(*ui_position)

        self.setWindowTitle("cellpose")
        self.cp_path = os.path.dirname(os.path.realpath(__file__))
        app_icon = QtGui.QIcon()
        icon_path = pathlib.Path.home().joinpath('.cellpose', 'logo.png')
        icon_path = str(icon_path.resolve())
        app_icon.addFile(icon_path, QtCore.QSize(16, 16))
        app_icon.addFile(icon_path, QtCore.QSize(24, 24))
        app_icon.addFile(icon_path, QtCore.QSize(32, 32))
        app_icon.addFile(icon_path, QtCore.QSize(48, 48))
        app_icon.addFile(icon_path, QtCore.QSize(64, 64))
        app_icon.addFile(icon_path, QtCore.QSize(256, 256))
        self.setWindowIcon(app_icon)
        
        menus.mainmenu(self)
        menus.editmenu(self)

        self.setStyleSheet("QMainWindow {background: 'black';}")
        self.stylePressed = (
            "QPushButton {Text-align: left; "
            "background-color: rgb(100,50,100); "
            "border-color: white;"
            "color:white;}"
        )
        self.styleUnpressed = (
            "QPushButton {Text-align: left; "
            "background-color: rgb(50,50,50); "
            "border-color: white;"
            "color:white;}"
        )
        self.styleInactive = (
            "QPushButton {Text-align: left; "
            "background-color: rgb(30,30,30); "
            "border-color: white;"
            "color:rgb(80,80,80);}"
        )
        self.loaded = False

        # ---- MAIN WIDGET LAYOUT ---- #
        self.cwidget = QWidget(self)
        self.l0 = QGridLayout()
        self.cwidget.setLayout(self.l0)
        self.setCentralWidget(self.cwidget)
        self.l0.setVerticalSpacing(6)

        self.imask = 0

        b = self.make_buttons()

        # ---- drawing area ---- #
        self.win = pg.GraphicsLayoutWidget()
        self.l0.addWidget(self.win, 0, 3, b, 20)
        self.win.scene().sigMouseClicked.connect(self.plot_clicked)
        self.win.scene().sigMouseMoved.connect(self.mouse_moved)
        self.make_viewbox()
        bwrmap = make_bwr()
        self.bwr = bwrmap.getLookupTable(start=0.0, stop=255.0, alpha=False)
        self.cmap = []
        # spectral colormap
        self.cmap.append(make_spectral().getLookupTable(start=0.0, stop=255.0, alpha=False))
        # single channel colormaps
        for i in range(3):
            self.cmap.append(make_cmap(i).getLookupTable(start=0.0, stop=255.0, alpha=False))

        if MATPLOTLIB:
            self.colormap = (plt.get_cmap('gist_ncar')(np.linspace(0.0, .9, 1000)) * 255).astype(np.uint8)
        else:
            np.random.seed(42)  # make colors stable
            self.colormap = ((np.random.rand(1000, 3) * 0.8 + 0.1) * 255).astype(np.uint8)
        self.reset("default")

        self.is_stack = True # always loading images of same FOV
        # if called with image, load it
        if image is not None:
            self.filename = image
            io._load_image(self, self.filename)

        self.setAcceptDrops(True)
        self.win.show()
        self.show()

    def closeEvent(self, event):
        event.accept()
        self.save_label()
        self.on_label_ui_closed()
        QApplication.setOverrideCursor(QtCore.Qt.ArrowCursor) 
        # self.par._save_label()
        # self.par.on_label_ui_closed()

    def help_window(self):
        HW = guiparts.HelpWindow(self)
        HW.show()

    def gui_window(self):
        EG = guiparts.ExampleGUI(self)
        EG.show()

    def make_buttons(self):
        label_style = """QLabel{
                            color: white
                            } 
                         QToolTip { 
                           background-color: black; 
                           color: white; 
                           border: black solid 1px
                           }"""
        self.boldfont = QtGui.QFont("Arial", 12, QtGui.QFont.Bold)
        self.medfont = QtGui.QFont("Arial", 10)
        self.smallfont = QtGui.QFont("Arial", 8)
        self.headings = ('color: rgb(150,255,150);')
        self.dropdowns = (
            "color: white;"
            "background-color: rgb(40,40,40);"
            "selection-color: white;"
            "selection-background-color: rgb(50,100,50);"
        )
        self.checkstyle = "color: rgb(190,190,190);"

        label = QLabel('Views:')  # [\u2191 \u2193]')
        label.setStyleSheet(self.headings)
        label.setFont(self.boldfont)
        self.l0.addWidget(label, 0, 0, 1, 1)

        label = QLabel('[up/down or W/S]')
        label.setStyleSheet(label_style)
        label.setFont(self.smallfont)
        self.l0.addWidget(label, 1, 0, 1, 1)

        label = QLabel('[pageup/down]')
        label.setStyleSheet(label_style)
        label.setFont(self.smallfont)
        self.l0.addWidget(label, 1, 1, 1, 1)

        b=2
        self.view = 0  # 0=image, 1=flowsXY, 2=flowsZ, 3=cellprob
        self.color = 0  # 0=RGB, 1=gray, 2=R, 3=G, 4=B
        self.RGBChoose = guiparts.RGBRadioButtons(self, b, 1)
        self.RGBDropDown = QComboBox()
        self.RGBDropDown.addItems(["RGB", "gray", "spectral", "red", "green", "blue"])
        self.RGBDropDown.setFont(self.medfont)
        self.RGBDropDown.currentIndexChanged.connect(self.color_choose)
        self.RGBDropDown.setFixedWidth(60)
        self.RGBDropDown.setStyleSheet(self.dropdowns)

        self.l0.addWidget(self.RGBDropDown, b, 0, 1, 1)
        b += 3

        self.resize = -1
        self.X2 = 0

        b += 1
        line = QHLine()
        line.setStyleSheet('color: white;')
        self.l0.addWidget(line, b, 0, 1, 2)
        b += 1
        label = QLabel('Drawing:')
        label.setStyleSheet(self.headings)
        label.setFont(self.boldfont)
        self.l0.addWidget(label, b, 0, 1, 2)

        b += 1
        self.brush_size = 3
        self.BrushChoose = QComboBox()
        self.BrushChoose.addItems(["1", "3", "5", "7", "9"])
        self.BrushChoose.currentIndexChanged.connect(self.brush_choose)
        self.BrushChoose.setFixedWidth(60)
        self.BrushChoose.setStyleSheet(self.dropdowns)
        self.BrushChoose.setFont(self.medfont)
        self.l0.addWidget(self.BrushChoose, b, 1, 1, 1)
        label = QLabel('brush size: [, .]')
        label.setStyleSheet(label_style)
        label.setFont(self.medfont)
        self.l0.addWidget(label, b, 0, 1, 1)

        # cross-hair
        self.vLine = pg.InfiniteLine(angle=90, movable=False)
        self.hLine = pg.InfiniteLine(angle=0, movable=False)

        b += 1
        # turn on delete mode
        self.DCheckBox = QCheckBox('erase')
        self.DCheckBox.setStyleSheet(self.checkstyle)
        self.DCheckBox.setFont(self.medfont)
        self.DCheckBox.toggled.connect(self.delete_on)
        self.l0.addWidget(self.DCheckBox, b, 0, 1, 2)

        b += 1
        # turn on crosshairs
        self.CHCheckBox = QCheckBox('crosshair')
        self.CHCheckBox.setStyleSheet(self.checkstyle)
        self.CHCheckBox.setFont(self.medfont)
        self.CHCheckBox.toggled.connect(self.cross_hairs)
        self.l0.addWidget(self.CHCheckBox, b, 0, 1, 1)

        b += 1
        # turn off masks
        self.layer_off = False
        self.masksOn = True
        self.MCheckBox = QCheckBox('masks on [X]')
        self.MCheckBox.setStyleSheet(self.checkstyle)
        self.MCheckBox.setFont(self.medfont)
        self.MCheckBox.setChecked(True)
        self.MCheckBox.toggled.connect(self.toggle_masks)
        self.l0.addWidget(self.MCheckBox, b, 0, 1, 2)

        b += 1
        # define opacity
        self.opacity = 125
        self.OpacityChoose = QComboBox()
        self.OpacityChoose.addItems([".1", ".3", ".5", ".7", ".9"])
        self.OpacityChoose.currentIndexChanged.connect(self.opacity_choose)
        self.OpacityChoose.setFixedWidth(60)
        self.OpacityChoose.setStyleSheet(self.dropdowns)
        self.OpacityChoose.setFont(self.medfont)
        self.l0.addWidget(self.OpacityChoose, b, 1, 1, 1)
        label = QLabel('opacity: [, .]')
        label.setStyleSheet(label_style)
        label.setFont(self.medfont)
        self.l0.addWidget(label, b, 0, 1, 1)

        b += 1
        # turn off outlines
        self.outlinesOn = False  # turn off by default
        self.OCheckBox = QCheckBox('outlines on [Z]')
        self.OCheckBox.setStyleSheet(self.checkstyle)
        self.OCheckBox.setFont(self.medfont)
        self.l0.addWidget(self.OCheckBox, b, 0, 1, 2)
        
        self.OCheckBox.setChecked(False)
        self.OCheckBox.toggled.connect(self.toggle_masks) 

        b += 1
        verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.l0.addItem(verticalSpacer, b, 0, 1, 2)
        
        b += 1
        line = QHLine()
        line.setStyleSheet('color: white;')
        line.setVisible(False)
        self.l0.addWidget(line, b, 0, 1, 2)

        return b

    def keyPressEvent(self, event):
        if self.loaded:
            # self.p0.setMouseEnabled(x=True, y=True)
            if (event.modifiers() != QtCore.Qt.ControlModifier and
                event.modifiers() != QtCore.Qt.ShiftModifier and
                event.modifiers() != QtCore.Qt.AltModifier) and not self.in_stroke:
                updated = False
                if len(self.current_point_set) > 0:
                    if event.key() == QtCore.Qt.Key_Return:
                        self.add_set()
                    if self.NZ > 1:
                        if event.key() == QtCore.Qt.Key_Left:
                            self.currentZ = max(0, self.currentZ-1)
                            self.zpos.setText(str(self.currentZ))
                        elif event.key() == QtCore.Qt.Key_Right:
                            self.currentZ = min(self.NZ-1, self.currentZ+1)
                            self.zpos.setText(str(self.currentZ))
                else:
                    if event.key() == QtCore.Qt.Key_X:
                        self.MCheckBox.toggle()
                    if event.key() == QtCore.Qt.Key_Z:
                        self.OCheckBox.toggle()
                    if event.key() == QtCore.Qt.Key_Left:
                        if self.NZ == 1:
                            self.get_prev_image()
                        else:
                            self.currentZ = max(0, self.currentZ-1)
                            self.scroll.setValue(self.currentZ)
                            updated = True
                    elif event.key() == QtCore.Qt.Key_Right:
                        if self.NZ == 1:
                            self.get_next_image()
                        else:
                            self.currentZ = min(self.NZ-1, self.currentZ+1)
                            self.scroll.setValue(self.currentZ)
                            updated = True
                    elif event.key() == QtCore.Qt.Key_A:
                        if self.NZ == 1:
                            self.get_prev_image()
                        else:
                            self.currentZ = max(0, self.currentZ-1)
                            self.scroll.setValue(self.currentZ)
                            updated = True
                    elif event.key() == QtCore.Qt.Key_D:
                        if self.NZ == 1:
                            self.get_next_image()
                        else:
                            self.currentZ = min(self.NZ-1, self.currentZ+1)
                            self.scroll.setValue(self.currentZ)
                            updated = True

                    elif event.key() == QtCore.Qt.Key_PageDown:
                        self.view = (self.view + 1) % len(self.RGBChoose.bstr)
                        self.RGBChoose.button(self.view).setChecked(True)
                    elif event.key() == QtCore.Qt.Key_PageUp:
                        self.view = (self.view - 1) % len(self.RGBChoose.bstr)
                        self.RGBChoose.button(self.view).setChecked(True)

                # can change background or stroke size if cell not finished
                if event.key() == QtCore.Qt.Key_Up or event.key() == QtCore.Qt.Key_W:
                    self.color = (self.color - 1) % 6
                    self.RGBDropDown.setCurrentIndex(self.color)
                elif event.key() == QtCore.Qt.Key_Down or event.key() == QtCore.Qt.Key_S:
                    self.color = (self.color + 1) % 6
                    self.RGBDropDown.setCurrentIndex(self.color)
                elif (event.key() == QtCore.Qt.Key_Comma or
                        event.key() == QtCore.Qt.Key_Period):
                    count = self.BrushChoose.count()
                    gci = self.BrushChoose.currentIndex()
                    if event.key() == QtCore.Qt.Key_Comma:
                        gci = max(0, gci-1)
                    else:
                        gci = min(count-1, gci+1)
                    self.BrushChoose.setCurrentIndex(gci)
                    self.brush_choose()
                if not updated:
                    self.update_plot()
                elif event.modifiers() == QtCore.Qt.ControlModifier:
                    if event.key() == QtCore.Qt.Key_Z:
                        self.undo_action()
                    if event.key() == QtCore.Qt.Key_0:
                        self.clear_all()
        if event.key() == QtCore.Qt.Key_Minus or event.key() == QtCore.Qt.Key_Equal:
            self.p0.keyPressEvent(event)

    def toggle_removals(self):
        pass
        # if self.ncells>0:
        #     #self.ClearButton.setEnabled(True)
        #     #self.remcell.setEnabled(True)
        #     #self.undo.setEnabled(True)
        # else:
        #     #self.ClearButton.setEnabled(False)
        #     #self.remcell.setEnabled(False)
        #     #self.undo.setEnabled(False)

    def remove_action(self):
        if self.selected > 0:
            self.remove_cell(self.selected)

    def undo_action(self):
        if len(self.strokes) > 0 and self.strokes[-1][0][0] == self.currentZ:
            self.remove_stroke()
        else:
            # remove previous cell
            if self.ncells > 0:
                self.remove_cell(self.ncells)

    def undo_remove_action(self):
        self.undo_remove_cell()

    def get_files(self):
        images = []
        images.extend(glob.glob(os.path.dirname(self.filename) + '/*.png'))
        images.extend(glob.glob(os.path.dirname(self.filename) + '/*.jpg'))
        images.extend(glob.glob(os.path.dirname(self.filename) + '/*.jpeg'))
        images.extend(glob.glob(os.path.dirname(self.filename) + '/*.tif'))
        images.extend(glob.glob(os.path.dirname(self.filename) + '/*.tiff'))
        images = natsorted(images)
        fnames = [os.path.split(images[k])[-1] for k in range(len(images))]
        f0 = os.path.split(self.filename)[-1]
        idx = np.nonzero(np.array(fnames) == f0)[0][0]
        return images, idx

    def get_prev_image(self):
        images, idx = self.get_files()
        idx = (idx - 1) % len(images)
        io._load_image(self, filename=images[idx])

    def get_next_image(self):
        images, idx = self.get_files()
        idx = (idx+1) % len(images)
        io._load_image(self, filename=images[idx])

    def toggle_masks(self):
        if self.MCheckBox.isChecked():
            self.masksOn = True
        else:
            self.masksOn = False
        if self.OCheckBox.isChecked():
            self.outlinesOn = True
        else:
            self.outlinesOn = False
        if not self.masksOn and not self.outlinesOn:
            self.p0.removeItem(self.layer)
            self.layer_off = True
        else:
            if self.layer_off:
                self.p0.addItem(self.layer)
            self.redraw_masks(masks=self.masksOn, outlines=self.outlinesOn)
        if self.loaded:
            self.update_plot()

    def make_viewbox(self):
        self.p0 = guiparts.ViewBoxNoRightDrag(
            parent=self,
            lockAspect=True,
            name="plot1",
            border=[100, 100, 100],
            invertY=True
        )
        self.brush_size = 3
        self.win.addItem(self.p0, 0, 0)
        self.p0.setMenuEnabled(False)
        self.p0.setMouseEnabled(x=True, y=True)
        self.img = pg.ImageItem(viewbox=self.p0, parent=self)
        self.img.autoDownsample = False
        self.layer = guiparts.ImageDraw(viewbox=self.p0, parent=self)
        self.layer.setLevels([0, 255])
        self.scale = pg.ImageItem(viewbox=self.p0, parent=self)
        self.scale.setLevels([0, 255])
        self.p0.scene().contextMenuItem = self.p0
        # self.p0.setMouseEnabled(x=False,y=False)
        self.Ly, self.Lx = 512, 512
        self.p0.addItem(self.img)
        self.p0.addItem(self.layer)
        self.p0.addItem(self.scale)

        guiparts.make_quadrants(self)

    def reset(self,mode=None):
        # ---- start sets of points ---- #
        self.selected = 0
        self.X2 = 0
        self.resize = -1
        self.onechan = False
        self.loaded = False
        self.channel = [0, 1]
        self.current_point_set = []
        self.in_stroke = False
        self.strokes = []
        self.stroke_appended = True
        self.ncells = 0
        self.zdraw = []
        self.removed_cell = []
        self.cellcolors = [np.array([255, 255, 255])]

        if mode == "default":
            # -- set menus to default -- #
            self.color = 0
            self.RGBDropDown.setCurrentIndex(self.color)
            self.view = 0
            self.RGBChoose.button(self.view).setChecked(True)
            self.BrushChoose.setCurrentIndex(1)
            self.OpacityChoose.setCurrentIndex(2)
            self.CHCheckBox.setChecked(False)
            self.autosave = True
        self.DCheckBox.setChecked(False)
        self.delete = False

        # -- zero out image stack -- #
        # self.opacity = 128 # how opaque masks should be
        self.outcolor = [200, 200, 255, 200]
        self.NZ, self.Ly, self.Lx = 1, 512, 512
        """if self.autobtn.isChecked():
            self.saturation = [[0,255] for n in range(self.NZ)]"""
        self.currentZ = 0
        self.flows = [[], [], [], [], [[]]]
        self.stack = np.zeros((1, self.Ly, self.Lx, 3))
        # masks matrix
        self.layers = 0 * np.ones((1, self.Ly, self.Lx, 4), np.uint8)
        # image matrix with a scale disk
        self.radii = 0 * np.ones((self.Ly, self.Lx, 4), np.uint8)
        self.cellpix = np.zeros((1, self.Ly, self.Lx), np.uint16)
        self.outpix = np.zeros((1, self.Ly, self.Lx), np.uint16)
        self.ismanual = np.zeros(0, np.bool)
        self.update_plot()
        self.filename = []
        self.loaded = False

    def brush_choose(self):
        self.brush_size = self.BrushChoose.currentIndex() * 2 + 1
        if self.loaded:
            self.layer.setDrawKernel(kernel_size=self.brush_size)
            self.update_plot()
    
    def opacity_choose(self):
        self.opacity = int((self.OpacityChoose.currentIndex()*.2 + .1)*255)
        print(self.opacity)
        if self.loaded:
            self.update_plot()
            self.redraw_masks(masks=self.masksOn, outlines=self.outlinesOn)

    def delete_on(self):
        if self.DCheckBox.isChecked():
            self.delete = True
        else:
            self.delete = False

    def cross_hairs(self):
        if self.CHCheckBox.isChecked():
            self.p0.addItem(self.vLine, ignoreBounds=True)
            self.p0.addItem(self.hLine, ignoreBounds=True)
        else:
            self.p0.removeItem(self.vLine)
            self.p0.removeItem(self.hLine)

    def clear_all(self):
        self.prev_selected = 0
        self.selected = 0
        # self.layers_undo, self.cellpix_undo, self.outpix_undo = [],[],[]
        self.layers = 0 * np.ones((self.NZ, self.Ly, self.Lx, 4), np.uint8)
        self.cellpix = np.zeros((self.NZ, self.Ly, self.Lx), np.uint16)
        self.outpix = np.zeros((self.NZ, self.Ly, self.Lx), np.uint16)
        self.cellcolors = [np.array([255, 255, 255])]
        self.ncells = 0
        print('removed all cells')
        self.toggle_removals()
        self.update_plot()

    def select_cell(self, idx):
        self.prev_selected = self.selected
        self.selected = idx
        if self.selected > 0:
            self.layers[self.cellpix == idx] = np.array([255, 255, 255, self.opacity])
            # if self.outlinesOn:
            #    self.layers[self.outpix==idx] = np.array(self.outcolor)
            self.update_plot()

    def unselect_cell(self):
        if self.selected > 0:
            idx = self.selected
            if idx < self.ncells + 1:
                self.layers[self.cellpix == idx] = np.append(self.cellcolors[idx], self.opacity)
                if self.outlinesOn:
                    self.layers[self.outpix == idx] = np.array(self.outcolor).astype(np.uint8)
                    # [0,0,0,self.opacity])
                self.update_plot()
        self.selected = 0

    def remove_cell(self, idx):
        # remove from manual array
        self.selected = 0
        for z in range(self.NZ):
            cp = self.cellpix[z] == idx
            op = self.outpix[z] == idx
            # remove from mask layer
            self.layers[z, cp] = np.array([0, 0, 0, 0])
            # remove from self.cellpix and self.outpix
            self.cellpix[z, cp] = 0
            self.outpix[z, op] = 0
            # reduce other pixels by -1
            self.cellpix[z, self.cellpix[z] > idx] -= 1
            self.outpix[z, self.outpix[z] > idx] -= 1
        self.update_plot()
        if self.NZ == 1:
            self.removed_cell = [self.ismanual[idx-1], self.cellcolors[idx], np.nonzero(cp), np.nonzero(op)]
            self.redo.setEnabled(True)
        # remove cell from lists
        self.ismanual = np.delete(self.ismanual, idx-1)
        del self.cellcolors[idx]
        del self.zdraw[idx-1]
        self.ncells -= 1
        print('removed cell %d' % (idx-1))
        if self.ncells==0:
            pass
            # self.ClearButton.setEnabled(False)
        if self.NZ == 1:
            io._save_sets(self)

    def merge_cells(self, idx):
        self.prev_selected = self.selected
        self.selected = idx
        if self.selected != self.prev_selected:
            for z in range(self.NZ):
                ar0, ac0 = np.nonzero(self.cellpix[z] == self.prev_selected)
                ar1, ac1 = np.nonzero(self.cellpix[z] == self.selected)
                touching = np.logical_and((ar0[:, np.newaxis] - ar1) == 1,
                                          (ac0[:, np.newaxis] - ac1) == 1).sum()
                print(touching)
                ar = np.hstack((ar0, ar1))
                ac = np.hstack((ac0, ac1))
                if touching:
                    mask = np.zeros((np.ptp(ar)+4, np.ptp(ac)+4), np.uint8)
                    mask[ar-ar.min()+2, ac-ac.min()+2] = 1
                    contours = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                    pvc, pvr = contours[-2][0].squeeze().T            
                    vr, vc = pvr + ar.min() - 2, pvc + ac.min() - 2
                else:
                    print('Segments must touch each other for being merged.')
                    return
                    """vr0, vc0 = np.nonzero(self.outpix[z]==self.prev_selected)
                    vr1, vc1 = np.nonzero(self.outpix[z]==self.selected)
                    vr = np.hstack((vr0, vr1))
                    vc = np.hstack((vc0, vc1))"""
                color = self.cellcolors[self.prev_selected]
                self.draw_mask(z, ar, ac, vr, vc, color, idx=self.prev_selected)
            self.remove_cell(self.selected)
            print('merged two cells')
            self.update_plot()
            io._save_sets(self)
            self.undo.setEnabled(False)      
            self.redo.setEnabled(False)    

    def undo_remove_cell(self):
        if len(self.removed_cell) > 0:
            z = 0
            ar, ac = self.removed_cell[2]
            vr, vc = self.removed_cell[3]
            color = self.removed_cell[1]
            self.draw_mask(z, ar, ac, vr, vc, color)
            self.toggle_mask_ops()
            self.cellcolors.append(color)
            self.ncells += 1
            self.ismanual = np.append(self.ismanual, self.removed_cell[0])
            self.zdraw.append([])
            print('added back removed cell')
            self.update_plot()
            io._save_sets(self)
            self.removed_cell = []
            self.redo.setEnabled(False)

    def remove_stroke(self, delete_points=True):
        # self.current_stroke = get_unique_points(self.current_stroke)
        stroke = np.array(self.strokes[-1])
        cZ = stroke[0, 0]
        outpix = self.outpix[cZ][stroke[:, 1], stroke[:, 2]] > 0
        self.layers[cZ][stroke[~outpix, 1], stroke[~outpix, 2]] = np.array([0, 0, 0, 0])
        if self.masksOn:
            cellpix = self.cellpix[cZ][stroke[:, 1], stroke[:, 2]]
            ccol = np.array(self.cellcolors.copy())
            if self.selected > 0:
                ccol[self.selected] = np.array([255, 255, 255])
            col2mask = ccol[cellpix]
            col2mask = np.concatenate((col2mask, self.opacity*(cellpix[:, np.newaxis] > 0)), axis=-1)
            self.layers[cZ][stroke[:, 1], stroke[:, 2], :] = col2mask
        if self.outlinesOn:
            self.layers[cZ][stroke[outpix,1],stroke[outpix,2]] = np.array(self.outcolor)
        if delete_points:
            self.current_point_set = self.current_point_set[:-1*(stroke[:, -1] == 1).sum()]
        del self.strokes[-1]
        self.update_plot()

    def plot_clicked(self, event):
        if event.double():
            if event.button() == QtCore.Qt.LeftButton:
                if (event.modifiers() != QtCore.Qt.ShiftModifier and
                    event.modifiers() != QtCore.Qt.AltModifier):
                    try:
                        self.p0.setYRange(0, self.Ly+self.pr)
                    except:
                        self.p0.setYRange(0, self.Ly)
                    self.p0.setXRange(0, self.Lx)

    def mouse_moved(self, pos):
        items = self.win.scene().items(pos)
        for x in items:
            if x == self.p0:
                mousePoint = self.p0.mapSceneToView(pos)
                if self.CHCheckBox.isChecked():
                    self.vLine.setPos(mousePoint.x())
                    self.hLine.setPos(mousePoint.y())
            # else:
            #    QtWidgets.QApplication.restoreOverrideCursor()
                # QtWidgets.QApplication.setOverrideCursor(QtCore.Qt.DefaultCursor)


    def color_choose(self):
        self.color = self.RGBDropDown.currentIndex()
        self.view = 0
        self.RGBChoose.button(self.view).setChecked(True)
        self.update_plot()

    def update_ztext(self):
        zpos = self.currentZ
        try:
            zpos = int(self.zpos.text())
        except:
            print('ERROR: zposition is not a number')
        self.currentZ = max(0, min(self.NZ-1, zpos))
        self.zpos.setText(str(self.currentZ))
        self.scroll.setValue(self.currentZ)

    def update_plot(self):
        self.Ly, self.Lx, _ = self.stack[self.currentZ].shape
        if self.view == 0:
            image = self.stack[self.currentZ]
            if self.color == 0:
                if self.onechan:
                    # show single channel
                    image = self.stack[self.currentZ][:, :, 0]
                self.img.setImage(image, autoLevels=False, lut=None)
            elif self.color == 1:
                image = image.astype(np.float32).mean(axis=-1).astype(np.uint8)
                self.img.setImage(image, autoLevels=False, lut=None)
            elif self.color == 2:
                image = image.astype(np.float32).mean(axis=-1).astype(np.uint8)
                self.img.setImage(image, autoLevels=False, lut=self.cmap[0])
            elif self.color > 2:
                image = image[:, :, self.color-3]
                self.img.setImage(image, autoLevels=False, lut=self.cmap[self.color-2])
            # self.img.setLevels(self.saturation[self.currentZ])
        else:
            image = np.zeros((self.Ly, self.Lx), np.uint8)
            if len(self.flows) >= self.view-1 and len(self.flows[self.view-1]) > 0:
                image = self.flows[self.view-1][self.currentZ]
            if self.view > 1:
                self.img.setImage(image, autoLevels=False, lut=self.bwr)
            else:
                self.img.setImage(image, autoLevels=False, lut=None)
            self.img.setLevels([0.0, 255.0])
        self.scale.setImage(self.radii, autoLevels=False)
        self.scale.setLevels([0.0, 255.0])
        # self.img.set_ColorMap(self.bwr)
        if self.masksOn or self.outlinesOn:
            self.layer.setImage(self.layers[self.currentZ], autoLevels=False)
        # self.slider.setLow(self.saturation[self.currentZ][0])
        # self.slider.setHigh(self.saturation[self.currentZ][1])
        self.win.show()
        self.show()

    def add_set(self):
        if len(self.current_point_set) > 0:
            self.current_point_set = np.array(self.current_point_set)
            while len(self.strokes) > 0:
                self.remove_stroke(delete_points=False)
            if len(self.current_point_set) > 2:
                # np.random.seed(42) # make colors stable
                col_rand = np.random.randint(1000)
                color = self.colormap[col_rand,:3]
                median = self.add_mask(points=self.current_point_set, color=color)
                if median is not None:
                    self.removed_cell = []
                    self.toggle_mask_ops()
                    self.cellcolors.append(color)
                    self.ncells += 1
                    self.ismanual = np.append(self.ismanual, True)
                    if self.NZ==1:
                        # only save after each cell if single image
                        io._save_sets(self)
            self.current_stroke = []
            self.strokes = []
            self.current_point_set = []
            self.update_plot()

    def add_mask(self, points=None, color=(100, 200, 50)):
        # loop over z values
        median = []
        if points.shape[1] < 3:
            points = np.concatenate((np.zeros((points.shape[0], 1), np.int32), points), axis=1)

        zdraw = np.unique(points[:, 0])
        zrange = np.arange(zdraw.min(), zdraw.max() + 1, 1, int)
        zmin = zdraw.min()
        pix = np.zeros((2,0), np.uint16)
        mall = np.zeros((len(zrange), self.Ly, self.Lx), np.bool)
        k = 0
        for z in zdraw:
            iz = points[:, 0] == z
            vr = points[iz, 1]
            vc = points[iz, 2]
            # get points inside drawn points
            mask = np.zeros((np.ptp(vr)+4, np.ptp(vc)+4), np.uint8)
            pts = np.stack((vc-vc.min()+2, vr-vr.min()+2), axis=-1)[:, np.newaxis, :]
            mask = cv2.fillPoly(mask, [pts], (255, 0, 0))
            ar, ac = np.nonzero(mask)
            ar, ac = ar+vr.min()-2, ac+vc.min()-2
            # get dense outline
            contours = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            pvc, pvr = contours[-2][0].squeeze().T            
            vr, vc = pvr + vr.min() - 2, pvc + vc.min() - 2
            # concatenate all points
            ar, ac = np.hstack((np.vstack((vr, vc)), np.vstack((ar, ac))))
            # if these pixels are overlapping with another cell, reassign them
            if self.delete == False:
                ioverlap = self.cellpix[z][ar, ac] > 0
                if (~ioverlap).sum() < 8:
                    print('ERROR: cell too small without overlaps, not drawn')
                    return None
                elif ioverlap.sum() > 0:
                    ar, ac = ar[~ioverlap], ac[~ioverlap]
                    # compute outline of new mask
                    mask = np.zeros((np.ptp(ar)+4, np.ptp(ac)+4), np.uint8)
                    mask[ar-ar.min()+2, ac-ac.min()+2] = 1
                    contours = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                    pvc, pvr = contours[-2][0].squeeze().T            
                    vr, vc = pvr + ar.min() - 2, pvc + ac.min() - 2
            if self.delete:
                self.erase_mask(z, ar, ac, vr, vc)
                median = None
            else:
                self.draw_mask(z, ar, ac, vr, vc, color)
                median.append(np.array([np.median(ar), np.median(ac)]))
            mall[z-zmin, ar, ac] = True
            pix = np.append(pix, np.vstack((ar, ac)), axis=-1)

        mall = mall[:, pix[0].min():pix[0].max()+1, pix[1].min():pix[1].max()+1].astype(np.float32)
        ymin, xmin = pix[0].min(), pix[1].min()
        if len(zdraw) > 1:
            mall, zfill = interpZ(mall, zdraw - zmin)
            for z in zfill:
                mask = mall[z].copy()
                ar, ac = np.nonzero(mask)
                ioverlap = self.cellpix[z+zmin][ar+ymin, ac+xmin] > 0
                if (~ioverlap).sum() < 5:
                    print('WARNING: stroke on plane %d not included due to overlaps' % z)
                elif ioverlap.sum() > 0:
                    mask[ar[ioverlap], ac[ioverlap]] = 0
                    ar, ac = ar[~ioverlap], ac[~ioverlap]
                # compute outline of mask
                outlines = masks_to_outlines(mask)
                vr, vc = np.nonzero(outlines)
                vr, vc = vr+ymin, vc+xmin
                ar, ac = ar+ymin, ac+xmin
                if self.delete:
                    self.erase_mask(z+zmin, ar, ac, vr, vc)
                else:
                    self.draw_mask(z+zmin, ar, ac, vr, vc, color)
                    
        if not self.delete:
            self.zdraw.append(zdraw)

        return median

    def draw_mask(self, z, ar, ac, vr, vc, color, idx=None):
        ''' draw single mask using outlines and area '''
        if idx is None:
            idx = self.ncells+1
        self.cellpix[z][vr, vc] = idx
        self.cellpix[z][ar, ac] = idx
        self.outpix[z][vr, vc] = idx
        if self.masksOn:
            self.layers[z][ar, ac, :3] = color
            self.layers[z][ar, ac, -1] = self.opacity
        if self.outlinesOn:
            self.layers[z][vr, vc] = np.array(self.outcolor)

    def erase_mask(self, z, ar, ac, vr, vc):
        self.layers[z][ar, ac, :] = np.array([0, 0, 0, 0])
        self.layers[z][vr, vc, :] = np.array([0, 0, 0, 0])
        # remove from self.cellpix and self.outpix
        old_props = measure.regionprops(self.cellpix[z])
        old_areas = [prop.area for prop in old_props]
        self.cellpix[z][vr, vc] = 0
        self.cellpix[z][ar, ac] = 0
        self.outpix[z][vr, vc] = 0
        
        new = measure.label(self.cellpix[z])
        new_props = measure.regionprops(self.cellpix[z])
        new_areas = [prop.area for prop in new_props]
        change_idxs = [i for i in range(0,len(old_areas)) if not old_areas[i] == new_areas[i]]

        for i in change_idxs:
            temp = measure.label(self.cellpix[z] == i+1)
            temp_contoure = measure.find_contours(self.cellpix[z] == i+1)
            zdraw = self.zdraw[i]
            segments = np.unique(temp)
            if len(segments) > 2:
                outlines = masks_to_outlines(temp == 1)
                vr, vc = np.nonzero(outlines)
                self.outpix[z][vr, vc] = i+1
                if self.outlinesOn:
                    self.layers[z][vr, vc] = np.array(self.outcolor)
                for seg in segments[2:]:
                    outlines = masks_to_outlines(temp == seg)
                    vr, vc = np.nonzero(outlines)
                    self.ncells += 1
                    col_rand = np.random.randint(1000)
                    col = self.colormap[col_rand, :3]
                    self.cellcolors.append(col)
                    self.ismanual = np.append(self.ismanual, True)
                    self.zdraw.append(zdraw)
                    self.cellpix[z][temp == seg] = self.ncells
                    self.outpix[z][vr, vc] = self.ncells
                    if self.masksOn:
                        self.layers[z][temp == seg, :3] = col
                    if self.outlinesOn:
                        self.layers[z][vr, vc] = np.array(self.outcolor)

    def redraw_masks(self, masks=True, outlines=True):
        if not outlines and masks:
            self.draw_masks()
            self.cellcolors = np.array(self.cellcolors)
            self.layers[..., :3] = self.cellcolors[self.cellpix, :]
            self.layers[..., 3] = self.opacity * (self.cellpix > 0).astype(np.uint8)
            self.cellcolors = list(self.cellcolors)
            if self.selected > 0:
                self.layers[self.cellpix == self.selected] = np.array([255, 255, 255, self.opacity])
        else:
            if masks:
                self.layers[..., 3] = self.opacity * (self.cellpix > 0).astype(np.uint8)
            else:
                self.layers[..., 3] = 0
            self.layers[self.outpix > 0] = np.array(self.outcolor).astype(np.uint8)

    def draw_masks(self):
        self.cellcolors = np.array(self.cellcolors)
        self.layers[..., :3] = self.cellcolors[self.cellpix, :]
        self.layers[..., 3] = self.opacity * (self.cellpix > 0).astype(np.uint8)
        self.cellcolors = list(self.cellcolors)
        self.layers[self.outpix > 0] = np.array(self.outcolor)
        if self.selected > 0:
            self.layers[self.outpix == self.selected] = np.array([0, 0, 0, self.opacity])

    def compute_saturation(self):
        # compute percentiles from stack
        self.saturation = []
        for n in range(len(self.stack)):
            # changed to use omnipose convention 
            self.saturation.append([np.percentile(self.stack[n].astype(np.float32), 0.01),
                                    np.percentile(self.stack[n].astype(np.float32), 99.99)])

    def chanchoose(self, image):
        if image.ndim > 2:
            if self.ChannelChoose[0].currentIndex() == 0:
                image = image.astype(np.float32).mean(axis=-1)[..., np.newaxis]
            else:
                chanid = [self.ChannelChoose[0].currentIndex()-1]
                if self.ChannelChoose[1].currentIndex() > 0:
                    chanid.append(self.ChannelChoose[1].currentIndex()-1)
                image = image[:, :, chanid].astype(np.float32)
        return image

    def enable_buttons(self):
        # self.X2Up.setEnabled(True)
        # self.X2Down.setEnabled(True)
        # self.ModelButton.setEnabled(True)
        # self.SizeButton.setEnabled(True)
        # self.ModelButton.setStyleSheet(self.styleUnpressed)
        """self.SizeButton.setStyleSheet(self.styleUnpressed)
        self.loadMasks.setEnabled(True)
        self.saveSet.setEnabled(True)
        self.savePNG.setEnabled(True)
        self.saveOutlines.setEnabled(True)"""
        self.toggle_mask_ops()

        self.update_plot()
        self.setWindowTitle(self.filename)

    def toggle_mask_ops(self):
        self.toggle_removals()
