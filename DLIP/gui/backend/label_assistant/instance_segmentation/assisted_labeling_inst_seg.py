import os

import cv2
import tifffile

from DLIP.gui.backend.label_assistant.base_assisted_labeling_masks import BaseAssistedLabelingGuiMasks
from DLIP.gui.backend.label_assistant.instance_segmentation.ui_inst_seg.gui import io
from DLIP.gui.backend.label_assistant.instance_segmentation.ui_inst_seg.gui.cellpose_gui import InstanceSegUi


class AssistedLabelingInstanceSegmentationGui(BaseAssistedLabelingGuiMasks):
    def __init__(
        self,
        parent,
        project,
        path_project,
        data_module,
        task_property,
        path_seg_annotation_tool,
        open_gui
    ):

        super(AssistedLabelingInstanceSegmentationGui, self).__init__(
            parent,
            project,
            path_project,
            data_module,
            task_property,
            path_seg_annotation_tool,
            open_gui
        )

        self.setWindowTitle("KaIDA - Instance Segmentation")

        self.inst_seg_ui = None

    def _read_label(self, file_path: str):
        return tifffile.imread(file_path) if (self.data_module.labels_data_format == "tif" or self.data_module.labels_data_format == "tiff" ) \
            else cv2.imread(file_path,-1)

    def _write_label(self, file_path, label):
        tifffile.imwrite(file_path, label) if (self.data_module.labels_data_format == "tif" or self.data_module.labels_data_format == "tiff" ) \
            else cv2.imwrite(file_path, label)

    def _update_label_ui(self):
        temp_folder = os.path.join(self.project["data_dir"], "temp")

        label_file = None
        img_file = None
        
        for file in os.listdir(temp_folder):
            if "_label" in file:
                label_file = os.path.join(temp_folder, file)
            else:
                img_file = os.path.join(temp_folder, file)

        io._load_image(self.inst_seg_ui, img_file)

        if label_file is not None:
            io._load_masks(self.inst_seg_ui, label_file)

    def _start_label_ui(self):
        if self.inst_seg_ui is None:
            self.inst_seg_ui = InstanceSegUi(
                image=None,
                _save_label=self._save_label,
                on_label_ui_closed=self.on_label_ui_closed,
                ui_position=self.annotation_tool_pos,
                ui_shape=self.annotation_tool_shape
            )
        
        self._update_label_ui()
        self.label_ui_active = True

    def _save_label(self):
        io._save_label(self.inst_seg_ui)
