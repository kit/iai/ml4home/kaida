from PyQt5.QtCore import QObject, pyqtSignal

from DLIP.utils.loading.load_functions import load_functions


class BaseLabelingGuiInitializationWorker(QObject):
    finished = pyqtSignal()
    data = pyqtSignal(dict, dict)

    def __init__(self, project, task_property, path_project):
        super(BaseLabelingGuiInitializationWorker, self).__init__()
        self.project = project
        self.task_property = task_property
        self.path_project = path_project

    def run(self):
        function_lsts = {
            "sampler": None,
            "pre_img_processing": None,
            "pre_labeling": None,
            "post_inspection": None,
            "post_processing": None
        }

        cfg = dict()

        for key in function_lsts:
            cfg[key] = dict()
            function_lsts[key] = load_functions(f"DLIP.{key}", self.task_property.folder_name, self.path_project)
        cfg["split_percent"] = dict()

        if "cfg" in self.project:
            cfg = self.project["cfg"]

        self.data.emit(function_lsts, cfg)
        self.finished.emit()
