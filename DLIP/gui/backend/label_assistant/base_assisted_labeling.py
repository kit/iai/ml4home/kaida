import copy
import gc
import json
import os
import shutil
from abc import abstractmethod
import time
from threading import Thread
import FreeSimpleGUI as sg
import tkinter as tk
from tkinter import messagebox

import numpy as np
import pandas as pd
import tifffile
import torch
from PyQt5 import uic, QtCore
from PyQt5.QtCore import QThread, QCoreApplication
from PyQt5.QtWidgets import QMainWindow, QDialog, QMessageBox, QFileDialog, QPushButton
from pynput.keyboard import Key, Controller
from skimage import io
import cv2

from DLIP.gui.backend.dvc_interface.dvc_interface import DvcInterface
from DLIP.gui.backend.label_assistant.base_labeling_gui_initialization_worker import BaseLabelingGuiInitializationWorker
from DLIP.utils.helper_gui.parameter_table import ParameterTable
from DLIP.utils.loading.yaml_to_dict import yaml2dict
from scipy.interpolate import interp1d


class BaseAssistedLabelingGui(QMainWindow, QDialog):
    def __init__(self, parent, project, path_project, data_module, task_property, open_gui) -> None:
        super(BaseAssistedLabelingGui, self).__init__(parent=None,)
        uic.loadUi(f"{path_project}/gui/layouts/gui_assisted_labeling.ui", self)

        self.parent = parent  # the initial ui

        self.calc_status = False

        self.path_project = path_project
        self.project = project
        self.data_module = data_module
        self.task_property = task_property
        self.open_gui = open_gui

        self.sampler = None

        self.pre_labeler = None
        self.pre_img_processor = None

        self.post_processor = None
        self.post_inspector = None

        self.post_pro_gui = None
        self.post_insp_gui = None
        
        self.considered_sample = None
        self.current_index = None

        self.time_duration_status_bar = 4000
        self.sampling_done = False
        self.label_ui_active = False

        self.data_frame = None
        self.data_frame_file = None

        self.reinspect = False
        self.all_labeled = False

        self.next_sample_btn_2.setEnabled(False)
        self.prev_sample_btn.setEnabled(False)


        self.dataset_type = "train"

        self.cfg = dict()

        self.keyboard = Controller()

        self.currently_doing_reinspect = False

        self.action_add_new_samples.triggered.connect(self.add_new_samples)

        self.finished_initialization_for = {
            "basic_gui": False,
            "dvc": False
        }

        self._configure_ui_size()

        self._init_functions()
        self._init_dvc()

    def _configure_ui_size(self):
        screen_geometry = QCoreApplication.instance().desktop().availableGeometry()
        screen_width, screen_height = screen_geometry.width(), screen_geometry.height()

        # if sys.platform.startswith("win32"):
        #     # at least on Windows 11 with a bottom task bar availableGeometry is not right
        #     possible_height = int(0.965 * screen_height)
        # else:
        possible_height = int(0.965 * screen_height)

        self.setMinimumWidth(int(0.25 * screen_width))
        self.setMinimumHeight(possible_height)
        self.move(0, 0)

        self.annotation_tool_shape = (int(0.75 * screen_width), possible_height)
        self.annotation_tool_pos = (int(0.25 * screen_width) + 1, 0)

    def _init_functions(self):
        self.gui_init_thread = QThread()
        self.gui_init_worker = BaseLabelingGuiInitializationWorker(self.project, self.task_property, self.path_project)
        self.gui_init_worker.moveToThread(self.gui_init_thread)

        self.gui_init_thread.started.connect(self.gui_init_worker.run)
        self.gui_init_worker.finished.connect(self.gui_init_thread.quit)
        self.gui_init_worker.data.connect(self._finish_gui_initialization)

        self.gui_init_thread.start()

    def _finish_gui_initialization(self, function_lsts, cfg):
        self.function_lsts = function_lsts
        self.cfg = cfg

        self._init_combo_boxes()
        self._init_data_frame()

        self.finished_initialization_for["basic_gui"] = True
        self._finished_initialization()

    def _init_combo_boxes(self):
        self.initiation = True
        cfg = copy.deepcopy(self.cfg)
        self.sampler_combobox.addItems([fcn.__name__ for fcn in self.function_lsts["sampler"]])
        self.pre_img_processing_combobox.addItems(
            ["None", *[fcn.__name__ for fcn in self.function_lsts["pre_img_processing"]]]
        )
        self.pre_labeling_combobox.addItems(
            ["None", *[fcn.__name__ for fcn in self.function_lsts["pre_labeling"]]]
        )
        self.label_inspection_combobox.addItems(
            ["None", *[fcn.__name__ for fcn in self.function_lsts["post_inspection"]]]
        )
        self.post_label_processing_combobox.addItems(
            ["None", *[fcn.__name__ for fcn in self.function_lsts["post_processing"]]]
        )
        self.train_test_select_combobox.addItems(["train", "test"])
        self.train_test_select_combobox.setCurrentIndex(self.train_test_select_combobox.findText(self.dataset_type))

        if "selected" in cfg["sampler"]:
            self.sampler_combobox.blockSignals(True)
            self.sampler_combobox.setCurrentIndex(self.sampler_combobox.findText(cfg["sampler"]["selected"]))
            self.sampler_combobox.blockSignals(False)
            self.load_sampler(cfg["sampler"]["selected"])
        if "selected" in cfg["pre_img_processing"]:
            self.pre_img_processing_combobox.blockSignals(True)
            self.pre_img_processing_combobox.setCurrentIndex(
                self.pre_img_processing_combobox.findText(cfg["pre_img_processing"]["selected"])
            )
            self.pre_img_processing_combobox.blockSignals(False)
            self.load_pre_img_processor(cfg["pre_img_processing"]["selected"])
        if "selected" in cfg["pre_labeling"]:
            self.pre_labeling_combobox.blockSignals(True)
            self.pre_labeling_combobox.setCurrentIndex(
                self.pre_labeling_combobox.findText(cfg["pre_labeling"]["selected"])
            )
            self.pre_labeling_combobox.blockSignals(False)
            self.load_pre_labeler(cfg["pre_labeling"]["selected"])
        if "selected" in cfg["post_inspection"]:
            self.label_inspection_combobox.blockSignals(True)
            self.label_inspection_combobox.setCurrentIndex(
                self.label_inspection_combobox.findText(cfg["post_inspection"]["selected"])
            )
            self.label_inspection_combobox.blockSignals(False)
            self.load_post_inspector(cfg["post_inspection"]["selected"])
        if "selected" in cfg["post_processing"]:
            self.post_label_processing_combobox.blockSignals(True)
            self.post_label_processing_combobox.setCurrentIndex(
                self.post_label_processing_combobox.findText(cfg["post_processing"]["selected"])
            )
            self.post_label_processing_combobox.blockSignals(False)
            self.load_post_processor(cfg["post_processing"]["selected"])
        if "split_percent" not in cfg:
            cfg["split_percent"] = dict()
            self.cfg = cfg
        if "selected" in cfg["split_percent"]:
            self.percent_test_lineedit.setText(cfg["split_percent"]["selected"])
        
        self.initiation = False

    def _init_data_frame(self):
        self.data_frame = pd.DataFrame({
                "dataset_type": ["train_unlabeled" for _ in range(len(self.data_module.unlabeled_train_dataset))],
                "sampling_score": [np.nan for _ in range(len(self.data_module.unlabeled_train_dataset))]
            }, index=self.data_module.unlabeled_train_dataset.get_samples()
        )

        self.data_frame = self.data_frame.append(pd.DataFrame({
                "dataset_type": ["train_labeled" for _ in range(len(self.data_module.labeled_train_dataset))],
                "sampling_score": [0.0 for _ in range(len(self.data_module.labeled_train_dataset))]
            }, index=self.data_module.labeled_train_dataset.get_samples()
        ))

        self.data_frame = self.data_frame.append(pd.DataFrame({
                "dataset_type": ["test_labeled" for _ in range(len(self.data_module.test_dataset))],
                "sampling_score": [0.0 for _ in range(len(self.data_module.test_dataset))]
            }, index=self.data_module.test_dataset.get_samples()
        ))

        arr = self.data_frame.index.duplicated(keep='first')
        name_list = []
        while len(np.where(arr == True)[0]) > 0:
            pos = np.where(arr == True)[0][0]
            name_list.append(self.data_frame.index[pos])
            self.data_frame = self.data_frame.drop(self.data_frame.index[pos])

            arr = self.data_frame.index.duplicated(keep='first')
        if len(name_list) > 0:
            text = 'The program will not work reliably since images with the same name were detected:'
            root = tk.Tk()
            root.withdraw()
            for i in name_list:
                text = text + '\n' + '-' + i
            messagebox.showerror('Error', text)


        meta_info_path = os.path.join(self.project["data_dir"], "meta_info")
        if not os.path.exists(meta_info_path):
            os.mkdir(meta_info_path)

        self.data_frame_file = os.path.join(meta_info_path, "data_frame.h5")

        if os.path.exists(self.data_frame_file):
            self.statusBar().showMessage("Reading available sampling scores", self.time_duration_status_bar)
            self.repaint()
            df = pd.read_hdf(self.data_frame_file, "df")
            for ind in range(len(df)):
                if df.index.values[ind] in self.data_frame.index:
                    self.data_frame.at[df.index.values[ind], "sampling_score"] = \
                        df.loc[df.index.values[ind]]["sampling_score"]

        self.data_frame = self.data_frame.sort_values(by=["dataset_type", "sampling_score"], ascending=[True, False])

    def _init_dvc(self):
        if self.project["dvc_server_dir"] is not None:
            self.finished_initialization_for["dvc"] = False

            local_dir2track_lst = [
                self.data_module.unlabeled_train_dataset.root_dir, 
                self.data_module.labeled_train_dataset.root_dir,
                self.data_module.test_dataset.root_dir,
                os.path.join(self.project["data_dir"], "meta_info")
            ]

            self.dvc_interface = DvcInterface(
                self.project["dvc_server_dir"],
                local_dir2track_lst,
                self
            )

            self.action_load_dataset_version.triggered.connect(self.dvc_interface.load_version)
            self.action_save_dataset_version.triggered.connect(self.dvc_interface.save_version)
        else:
            self.finished_initialization_for["dvc"] = True

            self.dvc_interface = None
            self.menu_dataset_versioning.setEnabled(False)

    def dvc_initialized(self):
        self.finished_initialization_for["dvc"] = True
        self._finished_initialization()

    def _finished_initialization(self):
        if not all([self.finished_initialization_for[key] for key in self.finished_initialization_for]):
            return

        self.parent.spinner_widget.stop()

        if self.open_gui:
            self.parent.close()

            self.show()

            del self.parent

    def _update_displays(self):
        self.progress_bar.setMinimum(0)

        self.progress_bar.setMaximum(
            self.data_frame.shape[0]
        )

        self.progress_bar.setValue(self.data_frame[self.data_frame["dataset_type"] != "train_unlabeled"].shape[0])

        self.num_labeled_text.setText(
            f"{self.data_frame[self.data_frame['dataset_type']!='train_unlabeled'].shape[0]}/{self.data_frame.shape[0]}"
        )

        self.labeled_sample_list.clear()

        if self.dataset_type == "train":
            for index in self.data_module.labeled_train_dataset.indices:
                self.labeled_sample_list.addItem(str(index))
        else:
            for index in self.data_module.test_dataset.indices:
                self.labeled_sample_list.addItem(str(index))
        self.repaint()

    def reload(self):
        # reload data and update display (for example when a different version is checked out using dvc)
        self.data_module.reload()

        self._init_data_frame()
        self._update_displays()

        self.current_index = None

    def _load_config(self):
        if "cfg" in self.project:
            self.cfg=self.project["cfg"]

    def save_config(self,key,selection_str):
        if str(selection_str)!="None":
            try:
                default_args = yaml2dict(os.path.join(self.path_project,"experiments", "configurations", f"{key}.yaml"))[str(selection_str)]
                default_keys = list(default_args.keys())

                if str(selection_str) not in self.cfg[key]:
                    args = copy.copy(default_args)
                    for default_key in default_keys:
                        args[default_key]=None
                else:
                    args = self.cfg[key][str(selection_str)]
                    args_keys = list(args.keys())
                    missing_keys = set(default_keys) - set(args_keys)
                    for missing_key in missing_keys:
                        args[missing_key] = None
                        # args[missing_key] = default_args[missing_key]

                if len(args) > 0 and not self.initiation:
                    parameter_tbl = ParameterTable(self.path_project, default_args, args)
                    result = parameter_tbl.exec_()
                    if result == QDialog.Accepted:
                        self.calc_status = True
                        thread = Thread(target = self.check_status)
                        thread.start()

                        self.cfg[key][str(selection_str)] = parameter_tbl.get_custom_parameter()
                    else:
                        return False
                elif len(args) > 0 and self.initiation:
                    self.cfg[key][str(selection_str)] = args
                else:
                    self.cfg[key][str(selection_str)] = dict()
            except:
                self.cfg[key][str(selection_str)] = dict()

        self.cfg[key]["selected"] = str(selection_str)

        self.project["cfg"] = self.cfg
        with open(self.project["prj_file_name"], 'w') as f:
            json.dump(self.project, f)

        return True

    def check_status(self):
        layout = [[sg.Text("Calculating....")]]
        window = sg.Window("Demo", layout, finalize= True)
        while self.calc_status:
            event, values = window.read(timeout=1) 

        window.close()

    def sampling(self):
        self.statusBar().showMessage("Sampling ...")
        self.repaint()
        data_frame_idx = (self.data_frame.sampling_score.isna()) & (self.data_frame.dataset_type == "train_unlabeled")
        unlabeled_unsampled_subset = self.data_frame[data_frame_idx]

        if len(unlabeled_unsampled_subset) > 0:
            ordered_lst = self.sampler.get_samples_to_label(
                self.data_module.unlabeled_train_dataset, 
                self.data_module.labeled_train_dataset,
                len(self.data_module.unlabeled_train_dataset)
            )

            m = interp1d([0, len(self.data_module.unlabeled_train_dataset)], [1, 0])

            for ix, elem in enumerate(ordered_lst):
                self.data_frame.at[elem, "sampling_score"] = m(ix)

            self.data_frame = self.data_frame.sort_values(
                by=["dataset_type", "sampling_score"],
                ascending=[True, False]
            )

            self.data_frame.to_hdf(self.data_frame_file, key="df")
            self.statusBar().showMessage("Sampling done", self.time_duration_status_bar)
            self.repaint()
        else:
            self.data_frame = self.data_frame.sort_values(
                by=["dataset_type", "sampling_score"],
                ascending=[True, False]
            )

            self.statusBar().showMessage("Sampling already done before", self.time_duration_status_bar)
            self.repaint()

        self.sampling_done = True
        self.next_sample_btn_2.setEnabled(True)
        self.prev_sample_btn.setEnabled(True)
        self.next_sample()

    def next_sample(self):
        if self.dataset_type == "train":
            if not self.sampling_done:
                self.statusBar().showMessage("Autostart of sampling", self.time_duration_status_bar)
                self.repaint()
                self.sampling()

            new_index = self._determine_index("forward")
            if new_index == len(self.data_frame) - 1:
                self.next_sample_btn_2.setEnabled(False)

            if new_index == self.current_index:
                self.statusBar().showMessage("No more samples to label", self.time_duration_status_bar)
                self.repaint()

            else:
                self.current_index = new_index
            
                self._label_img(self.data_frame.index.values[self.current_index])
        else:
            self.statusBar().showMessage("Disabled in test mode", self.time_duration_status_bar)
            self.repaint()

        
    def previous_sample(self):
        if self.dataset_type == "train":
            self.next_sample_btn_2.setEnabled(True)
            if not self.sampling_done:
                self.statusBar().showMessage("Autostart of sampling", self.time_duration_status_bar)
                self.repaint()
                self.sampling()

            new_index = self._determine_index("backward")

            if new_index == self.current_index:
                self.statusBar().showMessage("Not possible to shift backwards, limit reached",
                                             self.time_duration_status_bar)
                self.repaint()
            elif self.current_index is None:
                # At the start of the ui the current index is still uninitialized.
                # Therefore, use new_index - 1 i.e. the last labeled image
                self.current_index = max(0, new_index - 1)
                self.statusBar().showMessage(f"Using last labeled image.", self.time_duration_status_bar)
                self._label_img(self.data_frame.index.values[self.current_index])
            elif self.current_index > 0:
                self.current_index = new_index
                self._label_img(self.data_frame.index.values[self.current_index])
        else:
            self.statusBar().showMessage("Disabled in test mode", self.time_duration_status_bar)
            self.repaint()
            
    def reinspect_sample(self):
        self.next_sample_btn_2.setEnabled(False)
        self.prev_sample_btn.setEnabled(False)
        
        self.reinspect = True
        selected_samples = self.labeled_sample_list.selectedItems()
        if len(selected_samples) == 1:
            self._label_img(selected_samples[0].text())
        else:
            self.statusBar().showMessage("Select ONE sample to reinspect", self.time_duration_status_bar)
            self.repaint()

    def save_split_percent(self):
        key = "split_percent"
        selection_str = self.percent_test_lineedit.text()
        saved = self.save_config(key, selection_str)

    def load_sampler(self, selection_str):
        key = "sampler"
        saved = self.save_config(key, selection_str)
        if saved == False:
            item = self.cfg[key]["selected"]
            idx = self.sampler_combobox.findText(item, QtCore.Qt.MatchFixedString)
            self.sampler_combobox.blockSignals(True)
            self.sampler_combobox.setCurrentIndex(idx)
            self.sampler_combobox.blockSignals(False)
            return
        if str(selection_str) != "None":
            fcn_names = [fcn.__name__ for fcn in self.function_lsts["sampler"]]
            args = self.cfg["sampler"][str(selection_str)]

            if self.sampler is not None:
                del self.sampler
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
                gc.collect()

            self.sampler = self.function_lsts["sampler"][fcn_names.index(str(selection_str))](
                **args
            )
        self.calc_status = False

    def load_pre_labeler(self, selection_str):
        key = "pre_labeling"
        saved = self.save_config(key, selection_str)
        if saved == False:
            item = self.cfg[key]["selected"]
            idx = self.pre_labeling_combobox.findText(item, QtCore.Qt.MatchFixedString)
            self.pre_labeling_combobox.blockSignals(True)
            self.pre_labeling_combobox.setCurrentIndex(idx)
            self.pre_labeling_combobox.blockSignals(False)
            return
        if str(selection_str) != "None":
            fcn_names = [fcn.__name__ for fcn in self.function_lsts["pre_labeling"]]
            args = self.cfg["pre_labeling"][str(selection_str)]

            if self.pre_labeler is not None:
                del self.pre_labeler
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
                gc.collect()

            self.pre_labeler = self.function_lsts["pre_labeling"][fcn_names.index(str(selection_str))](
                **args
            )
        else:
            self.pre_labeler = None
        
        self.calc_status = False

    def load_pre_img_processor(self, selection_str):
        key = "pre_img_processing"
        saved = self.save_config(key, selection_str)
        if saved == False:
            item = self.cfg[key]["selected"]
            idx = self.pre_img_processing_combobox.findText(item, QtCore.Qt.MatchFixedString)
            self.pre_img_processing_combobox.blockSignals(True)
            self.pre_img_processing_combobox.setCurrentIndex(idx)
            self.pre_img_processing_combobox.blockSignals(False)
            return
        if str(selection_str) != "None":
            fcn_names = [fcn.__name__ for fcn in self.function_lsts["pre_img_processing"]]
            args = self.cfg["pre_img_processing"][str(selection_str)]

            if self.pre_img_processor is not None:
                del self.pre_img_processor
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
                gc.collect()

            self.pre_img_processor = self.function_lsts["pre_img_processing"][fcn_names.index(str(selection_str))](
                **{"project": self.project},
                **args
            )
        else:
            self.pre_img_processor = None
        
        self.calc_status = False

    def load_post_processor(self, selection_str):
        key = "post_processing"
        saved = self.save_config(key, selection_str)
        if saved == False:
            item = self.cfg[key]["selected"]
            idx = self.post_label_processing_combobox.findText(item, QtCore.Qt.MatchFixedString)
            self.post_label_processing_combobox.blockSignals(True)
            self.post_label_processing_combobox.setCurrentIndex(idx)
            self.post_label_processing_combobox.blockSignals(False)
            return
        if str(selection_str) != "None":
            fcn_names = [fcn.__name__ for fcn in self.function_lsts["post_processing"]]
            args = self.cfg["post_processing"][str(selection_str)]

            if self.post_processor is not None:
                del self.post_processor
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
                gc.collect()

            self.post_processor = self.function_lsts["post_processing"][fcn_names.index(str(selection_str))](
                **{"project": self.project},
                **args
            )
        else:
            self.post_processor = None

    def load_post_inspector(self, selection_str):
        key = "post_inspection"
        saved = self.save_config(key, selection_str)
        if saved == False:
            item = self.cfg[key]["selected"]
            idx = self.label_inspection_combobox.findText(item, QtCore.Qt.MatchFixedString)
            self.label_inspection_combobox.blockSignals(True)
            self.label_inspection_combobox.setCurrentIndex(idx)
            self.label_inspection_combobox.blockSignals(False)
            return
        if str(selection_str) != "None":
            fcn_names = [fcn.__name__ for fcn in self.function_lsts["post_inspection"]]
            args = self.cfg["post_inspection"][str(selection_str)]

            if self.post_inspector is not None:
                del self.post_inspector
                if torch.cuda.is_available():
                    torch.cuda.empty_cache()
                gc.collect()

            self.post_inspector = self.function_lsts["post_inspection"][fcn_names.index(str(selection_str))](
                **{"project": self.project},
                **args
            )
        else:
            self.post_inspector = None
    
    def _read_img(self, file_path: str):
        return tifffile.imread(file_path) if (self.data_module.samples_data_format == "tif" or  self.data_module.samples_data_format == "tiff") else cv2.cvtColor(cv2.imread(file_path),cv2.COLOR_BGR2RGB)

    def _write_img(self, file_path: str, img):
        tifffile.imwrite(file_path, img) if (self.data_module.samples_data_format == "tif" or  self.data_module.samples_data_format == "tiff") else io.imsave(file_path, img)

    def _clean_temp_dir(self):
        for filename in os.listdir(os.path.join(self.project["data_dir"], "temp")):
            if filename != "labeling.json":
                os.remove(
                    os.path.join(
                        os.path.join(self.project["data_dir"], "temp"),
                        filename)
                )

    def _determine_index(self, direction, status_bar=None):
        if self.current_index is None:
            for ind in range(len(self.data_frame)):
                if self.data_frame.at[self.data_frame.index.values[ind], "dataset_type"] == "train_unlabeled":
                    """if direction=="forward":
                        return ind
                    else:"""
                    return ind
            return len(self.data_frame) - 1
        else:
            if direction == "backward":
                return max(self.current_index - 1, 0)
            else:
                if self.current_index + 1 <= len(self.data_frame) - 1:
                    return self.current_index + 1
                    """for ind in range(self.current_index+1, len(self.data_frame)):
                        if self.data_frame.at[self.data_frame.index.values[ind],'dataset_type'] == "train_unlabeled":
                            return ind"""
                return len(self.data_frame) - 1

    def set_dataset_type(self, selection_str):
        self.dataset_type = selection_str
        self._update_displays()

    def ctrl_handler(self):
        if self.ctrl_pressed.isChecked():
            self.keyboard.press(Key.ctrl)
        else:
            self.keyboard.release(Key.ctrl)

    @abstractmethod
    def _update_label_ui(self):
        pass

    @abstractmethod
    def _save_label(self):
        pass

    @abstractmethod    
    def _read_label(self, file_path: str):
        pass

    @abstractmethod    
    def _write_label(self, file_path: str, label):
        pass

    @abstractmethod 
    def _start_label_ui(self):
        pass

    @abstractmethod 
    def _finalize_file_handling(self):
        pass

    @abstractmethod 
    def _finalize_labeling(self):
        pass

    @abstractmethod 
    def on_label_ui_closed(self):
        pass

    @abstractmethod 
    def _label_img(self, sample_name: str):
        pass

    @abstractmethod 
    def _compose_file_name(self, sample_name, data_type, folder):
        pass

    @abstractmethod     
    def split_train_test(self):
        pass

    @abstractmethod
    def edit_single_sample(self, sample_name, reply):
        pass

    def edit_sample(self):
        if self.dataset_type == "train":
            msg_box = QMessageBox()
            msg_box.setWindowTitle("Edit Annotations")
            msg_box.setText('Shift to not annotated dataset or remove completely?')
            msg_box.addButton(QPushButton('Shift to not annotated'), QMessageBox.YesRole)
            msg_box.addButton(QPushButton('Remove completely'), QMessageBox.DestructiveRole)
            msg_box.addButton(QPushButton('Cancel'), QMessageBox.RejectRole)
            reply = msg_box.exec_()

            items = self.labeled_sample_list.selectedItems()
            sample_names = list()
            for item in items:
                sample_names.append(item.text())

            for sample_name in sample_names:
                self.edit_single_sample(sample_name, reply)

            self.data_frame = self.data_frame.sort_values(by=['dataset_type', 'sampling_score'],
                                                          ascending=[True, False])
            self.current_index = None
   
    def add_new_samples(self):
        file_names, _ = QFileDialog().getOpenFileNames(self, "Select New Files To Add")

        if len(file_names) == 0:
            return

        for file in file_names:
            if hasattr(self.data_module, 'samples_dir'):
                dst = os.path.join(self.data_module.train_unlabeled_root_dir, 
                                   self.data_module.samples_dir,
                                   os.path.basename(file))
            else:
                dst = os.path.join(self.data_module.train_unlabeled_root_dir,
                                   os.path.basename(file))

            shutil.copy(file, dst)

        self.reload()

    def _do_preprocessing_and_pre_labeling(self, img, labels_available: bool):
        if self.pre_img_processor is not None:
           if self.pre_img_processor.do_always or (not labels_available):
                self.statusBar().showMessage("Pre-processing ...")
                self.repaint()
                img = self.pre_img_processor.process(img, self.statusBar())
                self.statusBar().showMessage("Pre-processing finished", self.time_duration_status_bar)
                self.repaint()

        if self.pre_labeler is not None:
            self.statusBar().showMessage("Pre-annotating ...")
            self.repaint()
            if self.project["task"] == "panoptic_segmentation":
                """ FOR THE TIME BEING, THERE WILL TWO BRANCHES FOR INSTANCE AND SEMANTIC SEGMENTATION """
                instance_label = self.pre_labeler.predict(img, self.statusBar())
                semantic_label = self.pre_labeler.predict(img, self.statusBar())
            else:
                label = self.pre_labeler.predict(img, self.statusBar())
            self.statusBar().showMessage("Pre-annotating finished", self.time_duration_status_bar)
            self.repaint()
        else:
            label = None
            instance_label = None
            semantic_label = None
        if self.project["task"] == "panoptic_segmentation":
            return img, instance_label, semantic_label
        else:
            return img, label

    def do_post_inspection(self, img, label):
        reinspect = False

        if self.post_inspector.inspect(label, img, self.statusBar()) < self.threshold_warning.value():
            self.post_insp_gui.do_inspection(label, img)
            if self.post_insp_gui.relabeling:
                reinspect = True
        self.statusBar().showMessage("")
        self.repaint()

        return reinspect

    def save_before_quit_dialog(self):
        reply = QMessageBox.question(self, "Quit", "Do you want to save this annotation?",
                                     QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            return True
        else:
            return False

    def show(self):
        if self.dvc_interface is not None:
            self.dvc_interface.check_for_newer_versions()

        self._update_displays()
        super(BaseAssistedLabelingGui, self).show()