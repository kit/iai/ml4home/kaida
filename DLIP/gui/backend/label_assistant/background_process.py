import subprocess

from PyQt5 import QtCore


class BackgroundProcess(QtCore.QObject):
    """Monitor a process running in the background."""

    finished = QtCore.pyqtSignal()
    failed = QtCore.pyqtSignal(int)

    def __init__(self, cmd):
        super(BackgroundProcess, self).__init__()
        self.cmd = cmd 

    def run(self):
        process = subprocess.Popen(self.cmd)

        while True:
            if process.poll() is not None:
                if process.returncode == 1:
                    self.failed.emit(process.returncode)
                else:
                    self.finished.emit()
                break
