import os
import random
import shutil
import time

from DLIP.gui.backend.post_inspection.mask.post_insp_ui_masks import PostInspectionMaskGui
from DLIP.gui.backend.post_processing.mask.post_pro_ui_masks import PostProcessingMaskGui
from .base_assisted_labeling import BaseAssistedLabelingGui

import cv2
import numpy as np
import time


class BaseAssistedLabelingGuiMasks(BaseAssistedLabelingGui):
    def __init__(self, parent, project, path_project, data_module, task_property, path_seg_annotation_tool, open_gui) -> None:
        super(BaseAssistedLabelingGuiMasks, self).__init__(
            parent,
            project,
            path_project,
            data_module, 
            task_property,
            open_gui
        )

        self.path_seg_annotation_tool = path_seg_annotation_tool

        self.post_pro_gui = PostProcessingMaskGui(path_project, project)
        self.post_insp_gui = PostInspectionMaskGui(path_project, project)

        self.save_backwards = True

        self.resize_shape = None

    def previous_sample(self):
        label_status = self.data_frame['dataset_type'][self.current_index].split('_')[1]
        if label_status == 'unlabeled':
            self.save_backwards = self.save_before_quit_dialog()
        super().previous_sample()
        self.save_backwards = True

    def _label_img(self, sample_name: str):
        # process and save previous labeled sample
        if self.considered_sample is not None:
            self._save_label()
            time.sleep(.5)
            reinspect_needed = self._finalize_labeling()
            if reinspect_needed:
                sample_name_reinspect = self.considered_sample
                self.considered_sample = None
                self._label_img(sample_name_reinspect)
                return None

        # start processing of next sample
        self.considered_sample = sample_name

        img = self._read_img(
            self._compose_file_name(sample_name, "sample", self.data_frame.at[sample_name, "dataset_type"])
        )

        

        if self.data_frame.at[sample_name, "dataset_type"] != "train_unlabeled":
            labels_available = True
            if(self.project["task"] == "panoptic_segmentation"):
                instance_label = self._read_label(
                    self._compose_file_name(sample_name, "instance_label", self.data_frame.at[sample_name, "dataset_type"])
                )
                semantic_label = self._read_label(
                    self._compose_file_name(sample_name, "semantic_label", self.data_frame.at[sample_name, "dataset_type"])
                )
            else:
                label = self._read_label(
                    self._compose_file_name(sample_name, "label", self.data_frame.at[sample_name, "dataset_type"])
                )
        else:
            labels_available = False


        if(self.project["task"] == "panoptic_segmentation"):
            img, pre_instance_label, pre_semantic_label = self._do_preprocessing_and_pre_labeling(img, labels_available)
            if not labels_available: 
                instance_label, semantic_label = pre_instance_label, pre_semantic_label  
        else:
            img, pre_label = self._do_preprocessing_and_pre_labeling(img, labels_available)
            if not labels_available: 
                label  =  pre_label        





        # if self.use_keep_pre_pro_img.isChecked(): 
        #     if self.data_frame.at[sample_name, "dataset_type"] != "train_unlabeled":
        #         if(self.project["task"] == "panoptic_segmentation"):
        #             instance_label = self._read_label(
        #                 self._compose_file_name(sample_name, "instance_label", self.data_frame.at[sample_name, "dataset_type"])
        #             )
        #             semantic_label = self._read_label(
        #                 self._compose_file_name(sample_name, "semantic_label", self.data_frame.at[sample_name, "dataset_type"])
        #             )
        #         else:
        #             label = self._read_label(
        #                 self._compose_file_name(sample_name, "label", self.data_frame.at[sample_name, "dataset_type"])
        #             )
        #     else:
        #         if(self.project["task"] == "panoptic_segmentation"):
        #             img, instance_label, semantic_label = self._do_preprocessing_and_pre_labeling(img, use_pre_processor=True)
        #         else:
        #             img, label = self._do_preprocessing_and_pre_labeling(img, use_pre_processor=True)
        # else:
        #     if self.data_frame.at[sample_name, "dataset_type"] != "train_unlabeled":
        #         if(self.project["task"] == "panoptic_segmentation"):
        #             instance_label = self._read_label(
        #                 self._compose_file_name(sample_name, "instance_label", self.data_frame.at[sample_name, "dataset_type"])
        #             )
        #             semantic_label = self._read_label(
        #                 self._compose_file_name(sample_name, "semantic_label", self.data_frame.at[sample_name, "dataset_type"])
        #             )
        #         else:
        #             label = self._read_label(
        #                 self._compose_file_name(sample_name, "label", self.data_frame.at[sample_name, "dataset_type"])
        #             )
        #     else:
        #         if(self.project["task"] == "panoptic_segmentation"):
        #             img, instance_label, semantic_label = self._do_preprocessing_and_pre_labeling(img, use_pre_processor=False)
        #         else:
        #             img, label = self._do_preprocessing_and_pre_labeling(img, use_pre_processor=False)



            # if self.pre_img_processor is not None:
            #     self.statusBar().showMessage("Pre-processing ...")
            #     self.repaint()
            #     ori_img = img.copy()
            #     img = self.pre_img_processor.process(img.copy(), self.statusBar())
            #     if ori_img.shape != img.shape:
            #         self.resize_shape = ori_img.shape
            #         if(self.project["task"] == "panoptic_segmentation"):
            #             if (instance_label and semantic_label) is not None:
            #                 instance_label = cv2.resize(instance_label.squeeze(),(img.shape[1],img.shape[0]), interpolation=cv2.INTER_NEAREST)
            #                 instance_label = np.expand_dims(instance_label, axis=0)
            #                 semantic_label = cv2.resize(semantic_label.squeeze(),(img.shape[1],img.shape[0]), interpolation=cv2.INTER_NEAREST)
            #                 semantic_label = np.expand_dims(semantic_label, axis=0)
            #         else:
            #             if label is not None:
            #                 label = cv2.resize(label.squeeze(),(img.shape[1],img.shape[0]), interpolation=cv2.INTER_NEAREST)
            #                 label = np.expand_dims(label, axis=0)
                    # self.statusBar().showMessage("Not possible to keep original image if shape is changed!")
                    # self.repaint()
                    # self.considered_sample = None
                    # if self.current_index == 0:
                    #     self.current_index = None
                    # else:
                    #     self.current_index = self.current_index-1
                    # self._clean_temp_dir()
                    # return
                # self.statusBar().showMessage("Pre-processing finished", self.time_duration_status_bar)
                # self.repaint()

        # save pre-processed img
        if self.pre_img_processor is not None:
            self._write_img(self._compose_file_name(sample_name, "sample", "temp"), img)
        else:
            shutil.copy(
                self._compose_file_name(sample_name, "sample", self.data_frame.at[sample_name, "dataset_type"]),
                self._compose_file_name(sample_name, "sample", "temp")
                )

        # save label information if available
        if(self.project["task"] == "panoptic_segmentation"):
            if (instance_label is not None) and  (semantic_label is not None):
                self._write_label(self._compose_file_name(sample_name, "instance_label", "temp"), instance_label)
                self._write_label(self._compose_file_name(sample_name, "semantic_label", "temp"), semantic_label)
        else:
            if label is not None:
                self._write_label(self._compose_file_name(sample_name, "label", "temp"), label)

        # start label ui if not active
        if not self.label_ui_active:
            # print("start new")
            self._start_label_ui()
        else:
            # print("update new")
            self._update_label_ui()

    def on_label_ui_closed(self):
        self.label_ui_active = False
        save_before_quit = self.save_before_quit_dialog()

        if save_before_quit:
            # auto-saving active -> no self._save_label() required
            reinspect_needed = self._finalize_labeling()
            if reinspect_needed:
                sample_name_reinspect = self.considered_sample
                self.considered_sample = None
                self._label_img(sample_name_reinspect)
                return
            self.considered_sample = None
        else:
            self.on_label_ui_closed_without_saving()
        
        self.reinspect = False
        self.next_sample_btn_2.setEnabled(False)
        self.prev_sample_btn.setEnabled(False)


    def on_label_ui_closed_without_saving(self):
        self.label_ui_active = False

        if not self.currently_doing_reinspect:
            self.current_index -= 1
        else:
            self.currently_doing_reinspect = False
        self._clean_temp_dir()

        self.considered_sample = None

    def _finalize_labeling(self):
        img = self._read_img(self._compose_file_name(self.considered_sample, "sample", "temp"))
        if self.post_processor is not None:
            self.statusBar().showMessage("Post-processing ...")
            self.repaint()
            if(self.project["task"] == "panoptic_segmentation"):
                instance_label = self._read_label(self._compose_file_name(self.considered_sample, "instance_label", "temp"))
                semantic_label = self._read_label(self._compose_file_name(self.considered_sample, "semantic_label", "temp"))
                instance_label_post_pro, semantic_label_post_pro = self.post_processor.process(instance_label, semantic_label, self.statusBar())
                self.statusBar().showMessage("Post-processing finished", self.time_duration_status_bar)
                self.repaint()
                if self.check_comparison.isChecked():
                    self.post_pro_gui.compare_labels(instance_label, instance_label_post_pro, img)
                    if self.post_pro_gui.accept_processing:
                        self._write_label(self._compose_file_name(self.considered_sample, "instance_label", "temp"), instance_label_post_pro)
                        self._write_label(self._compose_file_name(self.considered_sample, "semantic_label", "temp"), semantic_label_post_pro)
                else:
                    self._write_label(self._compose_file_name(self.considered_sample, "instance_label", "temp"), instance_label_post_pro)
                    self._write_label(self._compose_file_name(self.considered_sample, "semantic_label", "temp"), semantic_label_post_pro)
            else:
                label = self._read_label(self._compose_file_name(self.considered_sample, "label", "temp"))
                label_post_pro = self.post_processor.process(label, self.statusBar())
                self.statusBar().showMessage("Post-processing finished", self.time_duration_status_bar)
                self.repaint()
                if self.check_comparison.isChecked():
                    self.post_pro_gui.compare_labels(label, label_post_pro, img)
                    if self.post_pro_gui.accept_processing:
                        self._write_label(self._compose_file_name(self.considered_sample, "label", "temp"), label_post_pro)
                else:
                    self._write_label(self._compose_file_name(self.considered_sample, "label", "temp"), label_post_pro)

        # post inspection
        reinspect = False
        if self.post_inspector is not None:
            self.statusBar().showMessage("Annotation inspection ...")
            self.repaint()
            if self.use_keep_pre_pro_img.isChecked():
                img = self._read_img(self._compose_file_name(self.considered_sample, "sample", "temp"))
            else:
                img = self._read_img(
                    self._compose_file_name(
                        self.considered_sample,
                        "sample",
                        self.data_frame.at[self.considered_sample, "dataset_type"]
                    )
                )
            if(self.project["task"] == "panoptic_segmentation"):
                instance_label = self._read_label(self._compose_file_name(self.considered_sample, "instance_label", "temp"))
                semantic_label = self._read_label(self._compose_file_name(self.considered_sample, "semantic_label", "temp"))
                reinspect = self.do_post_inspection(img, instance_label, semantic_label)
            else:
                label = self._read_label(self._compose_file_name(self.considered_sample, "label", "temp"))

                reinspect = self.do_post_inspection(img, label)

        if self.save_backwards:
            self._finalize_file_handling()
            if self.data_frame.at[self.considered_sample, "dataset_type"] == "train_unlabeled":        
                self.data_module.unlabeled_train_dataset.pop_sample(
                    self.data_module.unlabeled_train_dataset.get_samples().index(self.considered_sample)
                )
                self.data_module.labeled_train_dataset.add_sample(
                    self.considered_sample
                )
                self.data_frame.at[self.considered_sample, "dataset_type"] = "train_labeled"
        else:
            self._clean_temp_dir()

        self._update_displays()

        return reinspect
        
    def _finalize_file_handling(self):
        train_sample_is_unlabeled = self.data_frame.at[self.considered_sample, "dataset_type"] == "train_unlabeled"

        if self.pre_img_processor is not None:
            if (not self.use_keep_pre_pro_img.isChecked()) and train_sample_is_unlabeled and (not self.pre_img_processor.do_always):
                if(self.project["task"] == "panoptic_segmentation"):
                    instance_label = self._read_label(self._compose_file_name(self.considered_sample, "instance_label", "temp")).squeeze()
                    semantic_label = self._read_label(self._compose_file_name(self.considered_sample, "semantic_label", "temp")).squeeze()
                    instance_label_post_pro = self.pre_img_processor.remap_label(instance_label)
                    semantic_label_post_pro = self.pre_img_processor.remap_label(semantic_label)                 
                    self._write_label(self._compose_file_name(self.considered_sample, "instance_label", "temp"), instance_label_post_pro)
                    self._write_label(self._compose_file_name(self.considered_sample, "semantic_label", "temp"), semantic_label_post_pro)
                else:
                    label = self._read_label(self._compose_file_name(self.considered_sample, "label", "temp")).squeeze()
                    label_post_pro = self.pre_img_processor.remap_label(label)
                    self._write_label(self._compose_file_name(self.considered_sample, "label", "temp"), label_post_pro)


        if self.use_keep_pre_pro_img.isChecked() and train_sample_is_unlabeled:
            shutil.move(
                    self._compose_file_name(self.considered_sample, "sample", "temp"),
                    self._compose_file_name(self.considered_sample, "sample", "train_labeled"),
            )  
            os.remove(self._compose_file_name(self.considered_sample, "sample", "train_unlabeled"))
        elif self.data_frame.at[self.considered_sample, "dataset_type"] == "train_unlabeled":
            shutil.move(
                self._compose_file_name(self.considered_sample, "sample", "train_unlabeled"),
                self._compose_file_name(self.considered_sample, "sample", "train_labeled"),
            )

        if self.data_frame.at[self.considered_sample, "dataset_type"] == "test_labeled":
            if self.project["task"] == "panoptic_segmentation":
                shutil.move(
                    self._compose_file_name(self.considered_sample, "instance_label", "temp"),
                    self._compose_file_name(self.considered_sample, "instance_label", "test_labeled"),
                )
                shutil.move(
                    self._compose_file_name(self.considered_sample, "semantic_label", "temp"),
                    self._compose_file_name(self.considered_sample, "semantic_label", "test_labeled"),
                )
            else:
                shutil.move(
                        self._compose_file_name(self.considered_sample, "label", "temp"),
                        self._compose_file_name(self.considered_sample, "label", "test_labeled"),
                )
        else:
            if self.project["task"] == "panoptic_segmentation":
                shutil.move(
                    self._compose_file_name(self.considered_sample, "instance_label", "temp"),
                    self._compose_file_name(self.considered_sample, "instance_label", "train_labeled"),
                )
                shutil.move(
                    self._compose_file_name(self.considered_sample, "semantic_label", "temp"),
                    self._compose_file_name(self.considered_sample, "semantic_label", "train_labeled"),
                )
            else:
                shutil.move(
                        self._compose_file_name(self.considered_sample, "label", "temp"),
                        self._compose_file_name(self.considered_sample, "label", "train_labeled"),
                )
            
        self._clean_temp_dir()

    def split_train_test(self):
        if self.project["task"] == "panoptic_segmentation":
            data_type_lst = ["sample", "instance_label", "semantic_label"] 
        else:
            data_type_lst = ["sample", "label"]
        train_set_ind = self.data_module.labeled_train_dataset.indices
        test_set_ind = self.data_module.test_dataset.indices

        split_ratio = float(self.cfg["split_percent"]["selected"])/100
        num_to_shift = round(split_ratio*(len(train_set_ind)+len(test_set_ind))-len(test_set_ind))

        if num_to_shift < 1:
            self.statusBar().showMessage("Not enough new train samples to split")
            self.repaint()
        else:
            rand_samples = random.sample(train_set_ind, num_to_shift)
            for sample2shift in rand_samples:
                for dt in data_type_lst:
                    src = self._compose_file_name(sample2shift, dt, "train_labeled")
                    dst = self._compose_file_name(sample2shift, dt, "test_labeled")
                    shutil.move(src, dst)

                self.data_module.labeled_train_dataset.pop_sample(
                    self.data_module.labeled_train_dataset.get_samples().index(sample2shift)
                )
                self.data_module.test_dataset.add_sample(
                    sample2shift
                )
        self.reload()

    def _compose_file_name(self, sample_name, data_type, dataset_type):
        if data_type == "sample":
            file = f"{sample_name}.{self.data_module.samples_data_format}"
        elif data_type == "label":
            file = f"{self.data_module.label_prefix}{sample_name}{self.data_module.label_suffix}" \
                   f".{self.data_module.labels_data_format}"
        elif data_type == "instance_label":
            file = f"{self.data_module.instance_label_prefix}{sample_name}{self.data_module.instance_label_suffix}" \
                   f".{self.data_module.instance_labels_data_format}"
        elif data_type == "semantic_label":
            file = f"{self.data_module.semantic_label_prefix}{sample_name}{self.data_module.semantic_label_suffix}" \
                   f".{self.data_module.semantic_labels_data_format}"
        else:
            raise ValueError(f"Unexpected data type {data_type}")

        if dataset_type == "temp":
            file_path = os.path.join(self.project["data_dir"], "temp", file)
        elif dataset_type == "train_unlabeled":
            if data_type == "sample":
                file_path = os.path.join(self.data_module.unlabeled_train_dataset.samples, file)
            elif data_type == "instance_label":
                file_path = os.path.join(self.data_module.unlabeled_train_dataset.instance_labels, file)
            elif data_type == "semantic_label":
                file_path = os.path.join(self.data_module.unlabeled_train_dataset.semantic_labels, file)
            else:
                file_path = os.path.join(self.data_module.unlabeled_train_dataset.labels, file)
        elif dataset_type == "train_labeled":
            if data_type == "sample":
                file_path = os.path.join(self.data_module.labeled_train_dataset.samples, file)
            elif data_type == "instance_label":
                file_path = os.path.join(self.data_module.labeled_train_dataset.instance_labels, file)
            elif data_type == "semantic_label":
                file_path = os.path.join(self.data_module.labeled_train_dataset.semantic_labels, file)
            else:
                file_path = os.path.join(self.data_module.labeled_train_dataset.labels, file) 
        elif dataset_type == "test_labeled":
            if data_type == "sample":
                file_path = os.path.join(self.data_module.test_dataset.samples, file)
            elif data_type == "instance_label":
                file_path = os.path.join(self.data_module.test_dataset.instance_labels, file)
            elif data_type == "semantic_label":
                file_path = os.path.join(self.data_module.test_dataset.semantic_labels, file)
            else:
                file_path = os.path.join(self.data_module.test_dataset.labels, file)
        else:
            raise ValueError(f"Unexpected dataset type {dataset_type}")

        return file_path

    def edit_single_sample(self, sample_name, reply):
        if reply == 0 or reply == 1:
            self.data_module.labeled_train_dataset.pop_sample(
                self.data_module.labeled_train_dataset.get_samples().index(sample_name)
            )
            if self.project["task"] == "panoptic_segmentation":
                os.remove(self._compose_file_name(sample_name, "instance_label", "train_labeled"))
                os.remove(self._compose_file_name(sample_name, "semantic_label", "train_labeled"))
            else:
                os.remove(self._compose_file_name(sample_name, "label", "train_labeled"))
            if reply == 0:
                self.data_module.unlabeled_train_dataset.add_sample(sample_name)
                self.data_frame.at[sample_name, "dataset_type"] = "train_unlabeled"
                shutil.move(
                    self._compose_file_name(sample_name, "sample", "train_labeled"),
                    self._compose_file_name(sample_name, "sample", "train_unlabeled"),
                )
                self.data_frame.at[sample_name, "dataset_type"] = "train_unlabeled"
            else:
                self.data_frame = self.data_frame.drop(sample_name)
                os.remove(self._compose_file_name(sample_name, "sample", "train_labeled"))
            self._update_displays()