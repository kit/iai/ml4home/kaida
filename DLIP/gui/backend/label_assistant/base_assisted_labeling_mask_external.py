import os
import platform

import cv2
import tifffile
from PyQt5 import QtCore

from DLIP.gui.backend.label_assistant.base_assisted_labeling_masks import BaseAssistedLabelingGuiMasks
from DLIP.gui.backend.label_assistant.background_process import BackgroundProcess


class BaseAssistedLabelingGuiExternalMasks(BaseAssistedLabelingGuiMasks):
    def __init__(
            self,
            parent,
            project,
            path_project,
            data_module,
            task_property,
            path_seg_annotation_tool,
            open_gui
    ):
        super(BaseAssistedLabelingGuiExternalMasks, self).__init__(
            parent,
            project,
            path_project,
            data_module,
            task_property,
            path_seg_annotation_tool,
            open_gui
        )

        self.use_adapted_kaida_labeling_tool = True

        self.background_thread: QtCore.QThread = None
        self.background_process: BackgroundProcess = None

    def _read_label(self, file_path: str):
        return tifffile.imread(file_path) if (self.data_module.labels_data_format == "tif" or self.data_module.labels_data_format == "tiff" ) \
            else cv2.imread(file_path,-1)

    def _write_label(self, file_path, label):
        tifffile.imwrite(file_path, label) if (self.data_module.labels_data_format == "tif" or self.data_module.labels_data_format == "tiff" ) \
            else cv2.imwrite(file_path, label)

    def _update_label_ui(self):
        open(os.path.join(self.project["data_dir"], "temp", "update.txt"), "w").close()

    def _get_cmd_args(self):
        if platform.system() == "Linux":
            cmd = [
                "sh",
                self.path_seg_annotation_tool,
                "-i",
                os.path.join(self.project["data_dir"], "temp", "labeling.json")
            ]
        else:
            cmd = [self.path_seg_annotation_tool, "-i", os.path.join(self.project["data_dir"], "temp", "labeling.json")]

        if self.use_adapted_kaida_labeling_tool:
            cmd += [
                "--window-position",
                f"({self.annotation_tool_pos[0]},{self.annotation_tool_pos[1]})",
                "--window-size",
                f"({self.annotation_tool_shape[0]},{self.annotation_tool_shape[1]})"
            ]

        return cmd

    def _start_label_ui(self):
        cmd = self._get_cmd_args()

        self.background_thread = QtCore.QThread()
        self.background_process = BackgroundProcess(cmd)
        self.background_process.moveToThread(self.background_thread)

        self.background_process.finished.connect(self.background_thread.quit)
        self.background_process.finished.connect(self.on_label_ui_closed)

        self.background_process.failed.connect(self.background_thread.quit)
        self.background_process.failed.connect(self._on_background_process_error)
        self.background_process.failed.connect(self.on_label_ui_closed_without_saving)

        self.background_thread.finished.connect(self.background_thread.deleteLater)
        self.background_thread.started.connect(self.background_process.run)
        self.background_thread.start()

        self.label_ui_active = True

    def _save_label(self):
        open(os.path.join(self.project["data_dir"], "temp", "update.txt"), "w").close()

    def _on_background_process_error(self, error_code):
        if self.use_adapted_kaida_labeling_tool:
            # It seems, that the user still has the old version of the labeling tool
            # Therefore, try to use the old one next time
            self.use_adapted_kaida_labeling_tool = False
        else:
            raise ValueError(f"The labeling tool has thrown error code {error_code}")
