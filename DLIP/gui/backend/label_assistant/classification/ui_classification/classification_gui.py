import os
from typing import Tuple

import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets, uic

from DLIP.utils.helper_functions.create_qimage import array_to_qimage


class ClassificationToolGui(QtWidgets.QMainWindow):
    resized = QtCore.pyqtSignal()

    def __init__(self, par, ui_shape: Tuple[int, int] = None, ui_position: Tuple[int, int] = None):
        super(ClassificationToolGui, self).__init__(par)
        self.par = par
        uic.loadUi(os.path.expandvars(f"{self.par.path_project}/gui/layouts/gui_classification_tool.ui"), self)
        
        self.class_name = None
        self.next = False

        if ui_shape is not None:
            self.setMinimumWidth(ui_shape[0])
            self.setMinimumHeight(ui_shape[1])

        if ui_position is not None:
            self.move(*ui_position)

        self.q_img = None
        self.resizeEvent = self.resize_fcn

        self.buttons = dict()

        self.centralWidget = QtWidgets.QWidget()
        self.setCentralWidget(self.centralWidget)

        self.vLayout = QtWidgets.QVBoxLayout(self.centralWidget)

        self.buttonsWidget = QtWidgets.QWidget()
        self.buttonsWidgetLayout = QtWidgets.QHBoxLayout(self.buttonsWidget)
        for i in range(len(self.par.project["class_names"])):
            text = self.par.project["class_names"][i]
            self.buttons[str(i)] = QtWidgets.QPushButton(text, self)
            self.buttons[str(i)].clicked.connect(lambda _, i=i: self.function(i))
            self.buttonsWidgetLayout.addWidget(self.buttons[str(i)])

        self.image_lbl = QtWidgets.QLabel()
        self.vLayout.addWidget(self.image_lbl)
        self.vLayout.addWidget(self.buttonsWidget)

        self.classification_comboBox.hide()
        self.pushButton.hide()        

        self.show()

    def function(self,i):
        self.class_name = self.par.project["class_names"][i]
        self.next = True
        self.close()


    def button(self):
        return 
    def update_gui(self, img, img_file_name, label=None, reinspect=None):
        self.setWindowTitle(img_file_name)

        self.q_img = array_to_qimage(np.uint8(img / 65535.0 * 255.0) if img.dtype == np.uint16 else img)

        img_scaled = self.get_scaled_image()

        if label is None:
            self.change_styleSheet(True)
        elif reinspect:
            self.change_styleSheet(True, 'lightgreen', label)
        else:
            self.change_styleSheet(False, 'lightgreen', label)

        self.image_lbl.setPixmap(QtGui.QPixmap.fromImage(img_scaled))
        self.image_lbl.adjustSize()
    
    def change_styleSheet(self, enable_button, color_button = '', label = None):
        label_name = ''
        style = ''
        if label is not None:
            label_name = self.par.project["class_names"][int(np.where(label == 1)[0])] 

        if color_button is not None:
            style = 'background-color : ' + color_button

        for i in range(len(self.buttons)):
            self.buttons[str(i)].setEnabled(enable_button)
            if self.buttons[str(i)].text() == label_name:
                self.buttons[str(i)].setStyleSheet(style)
            else:
                self.buttons[str(i)].setStyleSheet('')

    def get_label(self):
        return self.class_name

    def resize_fcn(self, event):
        if self.q_img is not None:
            img_scaled = self.get_scaled_image()

            self.image_lbl.setPixmap(QtGui.QPixmap.fromImage(img_scaled))
            self.image_lbl.adjustSize()

    def get_scaled_image(self):
        return self.q_img.scaled(
                0.9 * self.frameGeometry().width(),
                0.9 * self.frameGeometry().height(),
                aspectRatioMode=QtCore.Qt.KeepAspectRatio,
                transformMode=QtCore.Qt.SmoothTransformation
            )

    def closeEvent(self, event):
        event.accept()
        self.par.on_label_ui_closed()
