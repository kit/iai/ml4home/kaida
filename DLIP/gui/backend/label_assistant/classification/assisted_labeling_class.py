import os
import random
import shutil

import numpy as np

from DLIP.gui.backend.label_assistant.base_assisted_labeling import BaseAssistedLabelingGui
from DLIP.gui.backend.label_assistant.classification.ui_classification.classification_gui import ClassificationToolGui
from DLIP.gui.backend.post_inspection.classification.post_insp_ui_class import PostInspectionClassGui
from DLIP.gui.backend.post_processing.classification.post_pro_ui_class import PostProcessingClassGui


class AssistedLabelingClassificationGui(BaseAssistedLabelingGui):
    def __init__(
        self,
        parent,
        project,
        path_project,
        data_module,
        task_property,
        path_seg_annotation_tool,
        open_gui
    ):

        super(AssistedLabelingClassificationGui, self).__init__(
            parent,
            project,
            path_project,
            data_module,
            task_property,
            open_gui
        )
        self.next_img_after_labelling = True
        self.path_seg_annotation_tool = path_seg_annotation_tool

        self.post_pro_gui = PostProcessingClassGui(path_project, project)
        self.post_insp_gui = PostInspectionClassGui(path_project, project)
        self.next_img_after_labelling = True

        self.setWindowTitle("KaIDA - Classification")


    def _update_label_ui(self, label = None):
        temp_folder = os.path.join(self.project["data_dir"], "temp")
        
        for file in os.listdir(temp_folder):
            if ".npy" in file:
                label_file = os.path.join(temp_folder, file)
                label = self._read_label(label_file)
            else:
                img_file = os.path.join(temp_folder, file)
                img_file_name = file
                img = self._read_img(img_file)

        self.inst_class_ui.update_gui(img, img_file_name, label, self.reinspect)

    def _start_label_ui(self, label = None):
        self.inst_class_ui = ClassificationToolGui(
            self,
            ui_shape=self.annotation_tool_shape,
            ui_position=self.annotation_tool_pos
        )

        self._update_label_ui(label)    
        self.label_ui_active = True

    def _save_label(self):
        label_name = self.inst_class_ui.get_label()
        '''if label_name not in self.data_module.class_lst:
            self.statusBar().showMessage("No label selected, please select a label")
            self.repaint()
            return False
        else:'''
        label = np.zeros((len(self.data_module.class_lst)))
        label[self.data_module.class_lst.index(label_name)] = 1.0
        self._write_label(os.path.join(self.project["data_dir"], "temp", "label.npy"), label)
        return True

    def _finalize_file_handling(self):
        target_class = self.data_module.class_lst[
            self._read_label(os.path.join(self.project["data_dir"], "temp", "label.npy")).argmax()
        ]

        train_sample_is_unlabeled = self.data_frame.at[self.considered_sample, "dataset_type"] == "train_unlabeled"
        if self.use_keep_pre_pro_img.isChecked() and train_sample_is_unlabeled:
            shutil.move(
                self._compose_file_name(self.considered_sample, "temp"),
                self._compose_file_name(self.considered_sample, "train_labeled", target_class),
            )
            os.remove(self._compose_file_name(self.considered_sample, "train_unlabeled"))
        elif self.data_frame.at[self.considered_sample, "dataset_type"] == "train_unlabeled":
            shutil.move(
                self._compose_file_name(self.considered_sample, "train_unlabeled"),
                self._compose_file_name(self.considered_sample, "train_labeled", target_class),
            )
        else:
            src = self._compose_file_name(
                self.considered_sample,
                self.data_frame.at[self.considered_sample, "dataset_type"]
            )

            dst = self._compose_file_name(
                self.considered_sample,
                self.data_frame.at[self.considered_sample, "dataset_type"],
                target_class
            )

            if src != dst:
                shutil.move(src, dst)

        self._clean_temp_dir()

    def _finalize_labeling(self):
        # postprocessing
        img = self._read_img(self._compose_file_name(self.considered_sample, "temp"))
        if self.post_processor is not None:
            self.statusBar().showMessage("Post-processing ...")
            self.repaint()
            label = self._read_label(os.path.join(self.project["data_dir"], "temp", "label.npy"))
            label_post_pro = self.post_processor.process(label, self.statusBar())
            self.statusBar().showMessage("Post-processing finished", self.time_duration_status_bar)
            self.repaint()
            if self.check_comparison.isChecked():
                self.post_pro_gui.compare_labels(label, label_post_pro, img)
                if self.post_pro_gui.accept_processing:
                    self._write_label(os.path.join(self.project["data_dir"], "temp", "label.npy"), label_post_pro)
            else:
                self._write_label(os.path.join(self.project["data_dir"], "temp", "label.npy"), label_post_pro)

        # post inspection
        reinspect = False
        if self.post_inspector is not None:
            self.statusBar().showMessage("Annotation inspection ...")
            self.repaint()
            if self.use_keep_pre_pro_img.isChecked():
                img = self._read_img(self._compose_file_name(self.considered_sample, "temp"))
            else:
                img = self._read_img(
                    self._compose_file_name(
                        self.considered_sample,
                        self.data_frame.at[self.considered_sample, "dataset_type"]
                    )
                )
            label = self._read_label(os.path.join(self.project["data_dir"], "temp", "label.npy"))

            reinspect = self.do_post_inspection(img, label)

        final_label_ind = self._read_label(
                os.path.join(self.project["data_dir"], "temp", "label.npy")
                ).argmax()
        self._finalize_file_handling()

        if self.data_frame.at[self.considered_sample, "dataset_type"] == "train_unlabeled":        
            self.data_module.unlabeled_train_dataset.pop_sample(
                self.data_module.unlabeled_train_dataset.get_samples().index(self.considered_sample)
            )
            self.data_module.labeled_train_dataset.add_sample(
                self.considered_sample, final_label_ind
            )
            self.data_frame.at[self.considered_sample, "dataset_type"] = "train_labeled"
        elif self.data_frame.at[self.considered_sample, "dataset_type"] == "train_labeled":
            ind = self.data_module.labeled_train_dataset.get_samples().index(
                self.considered_sample)
            self.data_module.labeled_train_dataset.labels[ind] = final_label_ind
        elif self.data_frame.at[self.considered_sample, "dataset_type"] == "test_labeled":
            ind = self.data_module.test_dataset.get_samples().index(
                self.considered_sample)
            self.data_module.test_dataset.labels[ind] = final_label_ind

        self._update_displays()

        return reinspect

    def on_label_ui_closed(self):
        self.label_ui_active = False
        if self.inst_class_ui.next:
            save_before_quit = True
        else:
            save_before_quit = False
        #save_before_quit = self.save_before_quit_dialog()

        if save_before_quit:
            succesful_saved = self._save_label()
            if not succesful_saved:
                reinspect_needed = True
            else:
                reinspect_needed = self._finalize_labeling()

            if reinspect_needed:
                sample_name_reinspect = self.considered_sample
                self.considered_sample = None
                self._label_img(sample_name_reinspect)
                return
        self.considered_sample = None

        if save_before_quit and not self.reinspect:
            self.next_img_after_labelling = True
            super().next_sample()

        self.reinspect = False

    def _load_label(self, sample_name):
        if self.data_frame.at[sample_name, "dataset_type"] == "train_labeled":
            class_index = self.data_module.labeled_train_dataset.labels[
                self.data_module.labeled_train_dataset.indices.index(sample_name)
            ]
        else:
            try:
                class_index = self.data_module.test_dataset.labels[self.data_module.test_dataset.indices.index(sample_name)]
            except: 
                return None

        label = np.zeros(len(self.data_module.class_lst))
        label[class_index] = 1.0

        return label

    def _label_img(self, sample_name: str):
        label = self._load_label(sample_name)

        df = self.data_frame.copy()
        df['row'] = np.arange(len(df))
        
        idx = df.at[sample_name,'row']
        prev_img_status = None

        while prev_img_status != 'labeled':
            if idx > 0:
                prev_img_status = df.iloc[idx-1][0].split('_', 1)[1]
            else:
                prev_img_status = 'labeled' #None

            if prev_img_status == 'labeled':
                self.considered_sample = None
                break
            idx -= 1

        #self.current_index = idx
        sample_name = df.index[idx]


        if label is not None: 
            self.considered_sample = sample_name
            mode = self.data_frame.at[sample_name, "dataset_type"].split('_', 1)[0] 
            img_class = self.data_module.class_lst[int(np.where(label == 1)[0])] 

            img = self._read_img(
                os.path.join(self.data_module.root_dir,mode,img_class,sample_name + '.' + self.data_module.samples_data_format))

            dir = os.path.join(self.data_module.root_dir, 'temp')
            for f in os.listdir(dir):
                os.remove(os.path.join(dir, f))

            self._write_img(self._compose_file_name(sample_name, "temp"), img)
        
        # process and save previous labeled sample
        elif self.next_img_after_labelling or prev_img_status == 'labeled':
            self.next_img_after_labelling = False

            if self.considered_sample is not None:
                succesful_saved = self._save_label()
                if not succesful_saved:
                    reinspect_needed = True
                else:
                    reinspect_needed = self._finalize_labeling()
                if reinspect_needed:
                    sample_name_reinspect = self.considered_sample
                    self.considered_sample = None
                    self._label_img(sample_name_reinspect)
                    return None

            # start processing of next sample
            self.considered_sample = sample_name

            img = self._read_img(
                self._compose_file_name(sample_name, self.data_frame.at[self.considered_sample, "dataset_type"])
                )

            if self.use_keep_pre_pro_img.isChecked(): 
                if self.data_frame.at[sample_name, "dataset_type"] != "train_unlabeled":
                    label = self._load_label(sample_name)
                else:
                    img, label = self._do_preprocessing_and_pre_labeling(img, use_pre_processor=True)
            else:
                if self.data_frame.at[sample_name, "dataset_type"] != "train_unlabeled":
                    label = self._load_label(sample_name)
                else:
                    img, label = self._do_preprocessing_and_pre_labeling(img, use_pre_processor=False)

                    if self.pre_img_processor is not None:
                        self.statusBar().showMessage("Pre-processing ...")
                        self.repaint()
                        img = self.pre_img_processor.process(img, self.statusBar())
                        self.statusBar().showMessage("Pre-processing finished", self.time_duration_status_bar)
                        self.repaint()

            # save pre-processed img
            if self.pre_img_processor is not None:
                self._write_img(self._compose_file_name(sample_name, "temp"), img)
            else:
                shutil.copy(
                    self._compose_file_name(sample_name,  self.data_frame.at[sample_name, "dataset_type"]),
                    self._compose_file_name(sample_name, "temp")
                    )

            # save label information if available
            if label is not None:
                self._write_label(os.path.join(self.project["data_dir"], "temp", "label.npy"), label)
        
        else:
            return

        # start label ui if not active
        if not self.label_ui_active:
            self._start_label_ui(label)
        else:
            self._update_label_ui(label)

    def _compose_file_name(self, sample_name, folder, class_label=None):
        file = f"{sample_name}.{self.data_module.samples_data_format}"

        if folder == "temp":
            file_path = os.path.join(self.project["data_dir"], "temp", file)
        elif folder == "train_unlabeled":
            file_path = os.path.join(self.data_module.unlabeled_train_dataset.root_dir, file)
        elif folder == "train_labeled" or folder == "test_labeled":
            dataset = self.data_module.test_dataset if folder == "test_labeled" \
                else self.data_module.labeled_train_dataset

            if class_label is None:
                file_path = os.path.join(
                    dataset.root_dir,
                    dataset.class_lst[dataset.labels[dataset.indices.index(sample_name)]],
                    file
                )
            else:
                file_path = os.path.join(dataset.root_dir, class_label, file)
        else:
            raise ValueError(f"Unexpected folder name: {folder}")

        return file_path

    def split_train_test(self):
        train_set_ind = self.data_module.labeled_train_dataset.indices
        test_set_ind = self.data_module.test_dataset.indices

        split_ratio = float(self.cfg["split_percent"]["selected"])/100
        num_to_shift = round(split_ratio*(len(train_set_ind)+len(test_set_ind))-len(test_set_ind))

        if num_to_shift < 1:
            self.statusBar().showMessage("Not enough new train samples to split")
            self.repaint()
        else:
            rand_samples = random.sample(train_set_ind, num_to_shift)
            for sample2shift in rand_samples:
                src = self._compose_file_name(sample2shift, "train_labeled")
                sample, label = self.data_module.labeled_train_dataset.pop_sample(
                        self.data_module.labeled_train_dataset.get_samples().index(sample2shift)
                )
                self.data_module.test_dataset.add_sample(sample, label)
                dst = self._compose_file_name(sample2shift, "test_labeled")
                shutil.move(src, dst)

        self.reload()
        
    def _read_label(self, file_path: str):
        return np.load(file_path)

    def _write_label(self, file_path, label):
        np.save(file_path, label)

    def edit_single_sample(self, sample_name, reply):
        if reply == 0 or reply == 1:
            if reply == 0:
                self.data_module.unlabeled_train_dataset.add_sample(sample_name, None)
                self.data_frame.at[sample_name, "dataset_type"] = "train_unlabeled"
                shutil.move(self._compose_file_name(sample_name, "train_labeled"),
                            self._compose_file_name(sample_name, "train_unlabeled"))
                self.data_frame.at[sample_name, "dataset_type"] = "train_unlabeled"
            else:
                self.data_frame = self.data_frame.drop(sample_name)
                os.remove(self._compose_file_name(sample_name, "train_labeled"))
            self.data_module.labeled_train_dataset.pop_sample(
                self.data_module.labeled_train_dataset.get_samples().index(sample_name)
            )
            self._update_displays()
