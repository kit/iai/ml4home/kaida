import os

import cv2
import tifffile

from DLIP.gui.backend.label_assistant.base_assisted_labeling_masks import BaseAssistedLabelingGuiMasks
from DLIP.gui.backend.label_assistant.object_detection.ui_object_detection.obj_detect_ui import ObjectDetectionUi

from DLIP.gui.backend.label_assistant.object_detection.ui_object_detection.obj_detect_ui import PascalVocReader
from libs.pascal_voc_io import PascalVocWriter



class AssistedLabelingObjectDetectionGui(BaseAssistedLabelingGuiMasks):
    def __init__(
        self,
        parent,
        project,
        path_project,
        data_module,
        task_property,
        path_seg_annotation_tool,
        open_gui
    ):

        super(AssistedLabelingObjectDetectionGui, self).__init__(
            parent,
            project,
            path_project,
            data_module,
            task_property,
            path_seg_annotation_tool,
            open_gui
        )

        self.setWindowTitle("KaIDA - Object Detection")

        self.ui = None

    def _read_label(self, file_path: str):
        pascal_voc_data = PascalVocReader(file_path)
        shapes = pascal_voc_data.shapes
        label = [ [box[0][0], box[0][1], box[1][0], box[2][1], class_label] for class_label, box,_,_,_ in shapes]
        return label

    def _write_label(self, file_path, label):
        image_shape = [1,1,1]
        writer = PascalVocWriter(
            os.path.split(file_path)[0], 
            os.path.split(file_path)[1],
            image_shape
        )

        for label_box in label:
            writer.add_bnd_box(label_box[0], label_box[1], label_box[2], label_box[3], label_box[4], "no")

        writer.save(file_path)

    def _update_label_ui(self):
        temp_folder = os.path.join(self.project["data_dir"], "temp")

        label_file = None
        img_file = None
        
        for file in os.listdir(temp_folder):
            if "_label" in file or ".xml" in file:
                label_file = os.path.join(temp_folder, file)
            else:
                img_file = os.path.join(temp_folder, file)

        
        self.ui.load_file(img_file)

        if label_file is not None:
            self.ui.show_bounding_box_from_annotation_file(label_file)

    def _start_label_ui(self):
        if self.ui is None:
            self.ui = ObjectDetectionUi(
                _save_label=self._save_label,
                on_label_ui_closed=self.on_label_ui_closed,
                class_names=self.project["class_names"],
            )
        
        self.ui.show()
        self._update_label_ui()
        self.label_ui_active = True

    def _save_label(self):
        extension = os.path.splitext(self.ui.file_path)[1]
        self.ui.save_labels(self.ui.file_path.replace(extension, "_label.xml"))