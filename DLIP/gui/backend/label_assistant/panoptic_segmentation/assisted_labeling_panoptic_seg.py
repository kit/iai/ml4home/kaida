import os

import cv2
import tifffile

from DLIP.gui.backend.label_assistant.base_assisted_labeling_masks import BaseAssistedLabelingGuiMasks
from DLIP.gui.backend.label_assistant.panoptic_segmentation.ui_panoptic_seg.gui import io
from DLIP.gui.backend.label_assistant.panoptic_segmentation.ui_panoptic_seg.gui.panoptic_segmentation_gui import PanopticSegUI

class AssistedLabelingPanopticSegmentationGui(BaseAssistedLabelingGuiMasks):
    def __init__(
        self,
        parent,
        project,
        path_project,
        data_module,
        task_property,
        path_seg_annotation_tool,
        open_gui
    ):
        super(AssistedLabelingPanopticSegmentationGui, self).__init__(
            parent,
            project,
            path_project,
            data_module,
            task_property,
            path_seg_annotation_tool,
            open_gui
        )

        self.setWindowTitle("KaIDA - Panoptic Segmentation")
        self.project = project
        self.panoptic_seg_ui = None

    def _read_label(self, file_path: str):
        return tifffile.imread(file_path) if self.data_module.samples_data_format == "tif" \
            else cv2.imread(file_path, -1)

    def _write_label(self, file_path, label):
        tifffile.imwrite(file_path, label) if self.data_module.samples_data_format == "tif" \
            else cv2.imwrite(file_path, label)

    def _update_label_ui(self):
        temp_folder = os.path.join(self.project["data_dir"], "temp")

        instance_label_file = None
        semantic_label_file = None
        img_file = None

        for file in os.listdir(temp_folder):
            if "_instance_label" in file:
                instance_label_file = os.path.join(temp_folder, file)
            elif "_semantic_label" in file:
                semantic_label_file = os.path.join(temp_folder, file)
            else:
                if not (".json" in file):
                    img_file = os.path.join(temp_folder, file)

        io._load_image(self.panoptic_seg_ui, img_file)

        if (instance_label_file and semantic_label_file) is not None:
            io._load_masks(self.panoptic_seg_ui, instance_label_file, semantic_label_file)

    def _start_label_ui(self):
        if (self.project["class_names"][1]) == '':
            del self.project["class_names"][1]
            for pan_class in range(1, self.project["n_classes"]):
                self.project["class_names"].append(str(pan_class))
        if self.panoptic_seg_ui is None:
            self.panoptic_seg_ui = PanopticSegUI(
                image=None,
                _save_label=self._save_label,
                on_label_ui_closed=self.on_label_ui_closed,
                ui_position=self.annotation_tool_pos,
                ui_shape=self.annotation_tool_shape,
                project=self.project
            )
        
        self._update_label_ui()
        self.label_ui_active = True

    def _save_label(self):
        io._save_label(self.panoptic_seg_ui)

