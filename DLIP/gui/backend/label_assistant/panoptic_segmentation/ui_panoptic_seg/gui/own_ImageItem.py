
from skimage import img_as_ubyte, measure
from PyQt5 import QtCore
from PyQt5 import QtWidgets
import pyqtgraph as pg
from PyQt5.QtCore import QObject, QThread, pyqtSignal
from pyqtgraph import Point
import diplib as dip

import numpy as np
import os
import random
from PyQt5.QtCore import QLibraryInfo
# from PySide2.QtCore import QLibraryInfo
from skimage import img_as_ubyte
import math
import cv2
import matplotlib.pyplot as plt
# os.environ["QT_QPA_PLATFORM_PLUGIN_PATH"] = QLibraryInfo.location(
#     QLibraryInfo.PluginsPath
# )
import copy

class ViewBoxNoRightDrag(pg.ViewBox):
    def __init__(self, parent=None, border=None, lockAspect=False, enableMouse=True, invertY=False, enableMenu=True, name=None, invertX=False):
        pg.ViewBox.__init__(self, None, border, lockAspect, enableMouse,
                            invertY, enableMenu, name, invertX)
        self.parent = parent
        self.axHistoryPointer = -1

    def mouseDragEvent(self, ev, axis=None):
        # if axis is specified, event will only affect that axis.
        if self.parent is None or (self.parent is not None and not self.parent.in_stroke):
            ev.accept()  # we accept all buttons

            pos = ev.pos()
            lastPos = ev.lastPos()
            dif = pos - lastPos
            dif = dif * -1

            # Ignore axes if mouse is disabled
            mouseEnabled = np.array(self.state['mouseEnabled'], dtype=np.float)
            mask = mouseEnabled.copy()
            if axis is not None:
                mask[1-axis] = 0.0

            # Scale or translate based on mouse button
            if ev.button() & (QtCore.Qt.LeftButton | QtCore.Qt.MidButton):
                if self.state['mouseMode'] == pg.ViewBox.RectMode:
                    if ev.isFinish():  # This is the final move in the drag; change the view scale now
                        # print "finish"
                        self.rbScaleBox.hide()
                        ax = QtCore.QRectF(
                            Point(ev.buttonDownPos(ev.button())), Point(pos))
                        ax = self.childGroup.mapRectFromParent(ax)
                        self.showAxRect(ax)
                        self.axHistoryPointer += 1
                        self.axHistory = self.axHistory[:self.axHistoryPointer] + [
                            ax]
                    else:
                        # update shape of scale box
                        self.updateScaleBox(ev.buttonDownPos(), ev.pos())
                else:
                    tr = dif*mask
                    tr = self.mapToView(tr) - self.mapToView(Point(0, 0))
                    x = tr.x() if mask[0] == 1 else None
                    y = tr.y() if mask[1] == 1 else None

                    self._resetTarget()
                    if x is not None or y is not None:
                        self.translateBy(x=x, y=y)
                    self.sigRangeChangedManually.emit(
                        self.state['mouseEnabled'])


class ImageDraw(pg.ImageItem):
    """
    **Bases:** :class:`GraphicsObject <pyqtgraph.GraphicsObject>`
    """
    # Overall description of ImageItem (including examples) moved to documentation text
    sigImageChanged = QtCore.pyqtSignal()

    def __init__(self, image=None, parent=None, num_of_classes=None, **kargs):

        super(ImageDraw, self).__init__()

        # self.view_box = viewbox
        self.current_class_color = parent.current_class_color
        self.contour = []
        self.show_contour_bool = False
        # self.colors_list = None
        self.instance_image = np.zeros(
            (image.shape[0], image.shape[1]), dtype=np.uint16)
        self.semantic_image = np.zeros(
            (image.shape[0], image.shape[1]), dtype=np.uint16)
        self.current_instance_number = 1
        self.in_some_other_contour = False
        self.in_some_other_contour_for_deletion = False
        self.intersecting_contours = []
        self.contours_dictionary = {}
        self.is_ctrl_pressed = False
        self.neighbour_size = 5
        self.left_neighbour = False
        self.current_class = parent.current_class
        self.num_of_classes = int(num_of_classes)
        self.instance_starting_point = Point()
        self.parent = parent
        self.levels = np.array([0, 255])
        self.lut = None
        self.autoDownsample = False
        self.is_right_mouse_clicked = False
        self.areas_of_contour_for_deletion = []
        self.areas_of_contour_for_deletion_keys = []
        self.brush_size = 3
        if image is not None:
            self.setImage(image, **kargs)
        else:
            self.setOpts(**kargs)
        self.copy_image = self.image.copy()

    def mouseDragEvent(self, ev):
        ev.ignore()

    def mouseClickEvent(self, ev):

        if ev.button() == QtCore.Qt.MouseButton.RightButton and self.is_right_mouse_clicked == False and self.current_class is not None and self.parent.hide_masks_checkbox.isChecked() == False:
            self.is_right_mouse_clicked = True
            self.instance_starting_point = ev.pos()
            self.setDrawKernel(self.brush_size)
        if ev.button() == QtCore.Qt.MouseButton.LeftButton and self.parent.hide_masks_checkbox.isChecked() == False:
            if self.show_contour_bool:
                contour_dialog = QtWidgets.QDialog()
                contour_label = QtWidgets.QLabel(
                    "Check off the show contour checkbox to delete annotations")
                contour_layout = QtWidgets.QVBoxLayout()
                contour_layout.addWidget(contour_label)
                contour_dialog.setLayout(contour_layout)
                contour_dialog.exec_()
                return
            if self.is_ctrl_pressed and self.contours_dictionary:
                # check if self.contours_dictionary is empty

                # iterate through all the contours and delete the one in which the mouse is clicked
                for instance_id, contour in self.contours_dictionary.items():
                    if cv2.pointPolygonTest(np.array(contour), (ev.pos().x(), ev.pos().y()), False) >= 0:
                        self.in_some_other_contour_for_deletion = True
                        self.areas_of_contour_for_deletion.append(
                            cv2.contourArea(np.array(contour)))
                        self.areas_of_contour_for_deletion_keys.append(
                            instance_id)
                    else:
                        self.areas_of_contour_for_deletion.append(1e9)
                        self.areas_of_contour_for_deletion_keys.append(
                            instance_id)
                # print(self.areas_of_contour_for_deletion_keys)
                min_index = self.areas_of_contour_for_deletion.index(
                    min(self.areas_of_contour_for_deletion))
                # print(self.areas_of_contour_for_deletion_keys[min_index])
                instance_id_to_delete = self.areas_of_contour_for_deletion_keys[min_index]
                contour_to_delete = self.contours_dictionary[instance_id_to_delete]

                instance_number_to_be_deleted = self.instance_image[int(
                    ev.pos().y()), int(ev.pos().x())]
                region_to_be_deleted = np.ones(
                    (self.image.shape[0], self.image.shape[1]), dtype=np.uint16)
                temp_self_instance_image_roi = copy.deepcopy(
                    self.instance_image)
                region_to_be_deleted[temp_self_instance_image_roi ==
                                        instance_number_to_be_deleted] = 0
                self.image[:, :, 0] = region_to_be_deleted * \
                    self.image[:, :, 0]
                self.image[:, :, 1] = region_to_be_deleted * \
                    self.image[:, :, 1]
                self.image[:, :, 2] = region_to_be_deleted * \
                    self.image[:, :, 2]
                self.instance_image = region_to_be_deleted * \
                    self.instance_image
                self.semantic_image = region_to_be_deleted * \
                    self.semantic_image

                self.copy_image = copy.deepcopy(
                    self.image)
                self.updateImage()
                if self.in_some_other_contour_for_deletion:
                    # delete that contour from the dictionary
                    del self.contours_dictionary[instance_id_to_delete]
                    self.areas_of_contour_for_deletion = []
                    self.areas_of_contour_for_deletion_keys = []
                    self.in_some_other_contour_for_deletion = False

        else:

            ev.ignore()

    def calDistance(self, p1, p2):
        return math.sqrt((p1.x() - p2.x())**2 + (p1.y() - p2.y())**2)

    def hoverEvent(self, ev):
        if (ev.isExit()):
            return
        if self.is_right_mouse_clicked:

            current_x = ev.pos().x()
            current_y = ev.pos().y()

            if (self.left_neighbour == False):
                if (self.calDistance(self.instance_starting_point, ev.pos()) > (self.neighbour_size + self.brush_size)):
                    self.left_neighbour = True

            self.contour.append([int(ev.pos().x()), int(ev.pos().y())])

            if (current_x > self.instance_starting_point.x() - ((self.brush_size + self.neighbour_size)/2) and
                current_x < self.instance_starting_point.x() + ((self.brush_size + self.neighbour_size)/2) and
                current_y > self.instance_starting_point.y() - ((self.brush_size + self.neighbour_size)/2) and
                current_y < self.instance_starting_point.y() + ((self.brush_size + self.neighbour_size)/2) and
                    self.left_neighbour):
                min_x = 1e9
                min_y = 1e9
                max_x = -1
                max_y = -1
                for x, y in self.contour:
                    min_x = min(min_x, x)
                    min_y = min(min_y, y)
                    max_x = max(max_x, x)
                    max_y = max(max_y, y)
                min_x = max(0, min_x - 1)
                min_y = max(0, min_y - 1)
                max_x = min(self.image.shape[1], max_x + 2)
                max_y = min(self.image.shape[0], max_y + 2)
                temp_contour = copy.deepcopy(self.contour)
                for i in range(len(temp_contour)):
                    temp_contour[i][0] -= min_x
                    temp_contour[i][1] -= min_y

                temp_self_instance_image = copy.deepcopy(
                    self.instance_image[min_y: max_y, min_x: max_x])

                temp_current_contour_img = np.zeros(
                    ((max_y - min_y), (max_x - min_x)), np.uint16)
                temp_zeros = np.zeros(
                    ((max_y - min_y), (max_x - min_x), 3), np.uint16)
                temp_instance_zeros = np.zeros(
                    ((max_y - min_y), (max_x - min_x)), np.uint16)
                temp_semantic_zeros = np.zeros(
                    ((max_y - min_y), (max_x - min_x)), np.uint16)
                temp_current_contour_img = cv2.fillPoly(
                    temp_current_contour_img, [np.array(temp_contour)], 255)
                temp_intersection = np.logical_and(
                    temp_current_contour_img, temp_self_instance_image)

                self.in_some_other_contour = temp_intersection.any()
                if (self.in_some_other_contour):
                    temp__ = copy.deepcopy(temp_self_instance_image)
                    temp_self_instance_image = np.where(
                        temp_self_instance_image > 0, 0, 1)
                    temp_intersection_and = np.logical_and(
                        temp_current_contour_img, temp_self_instance_image)
                    temp_intersection_and = temp_intersection_and.astype(
                        np.uint16)


                    temp_zeros[:, :, 0] = temp_intersection_and * \
                        self.current_class_color[0]
                    temp_zeros[:, :, 1] = temp_intersection_and * \
                        self.current_class_color[1]
                    temp_zeros[:, :, 2] = temp_intersection_and * \
                        self.current_class_color[2]
                    temp_instance_zeros = temp_intersection_and * self.current_instance_number
                    temp_semantic_zeros = temp_intersection_and * self.current_class
                    self.current_instance_number += 1
                    self.image[min_y: max_y, min_x: max_x, :] += temp_zeros
                    self.instance_image[min_y: max_y,
                                        min_x: max_x] += temp_instance_zeros
                    self.semantic_image[min_y:max_y,
                                        min_x: max_x] += temp_semantic_zeros

                    cc = dip.GetImageChainCodes(temp_intersection_and)
                    for c in cc:
                        contour = np.array(c.Polygon()).astype(np.uint16)
                        contour[:, 0] += min_x
                        contour[:, 1] += min_y

                        self.contours_dictionary[self.current_instance_number] = contour.tolist(
                        )
                        if self.show_contour_bool:
                            for point in contour:
                                self.image[point[1], point[0], :] = (
                                    255, 255, 255)

                else:
                    self.contour = np.array(self.contour)
                    self.contour[:, 0] -= min_x
                    self.contour[:, 1] -= min_y
                    self.contour = self.contour.tolist()
                    cv2.fillPoly(temp_zeros, np.array([self.contour]), (
                        self.current_class_color[0], self.current_class_color[1], self.current_class_color[2]))
                    cv2.fillPoly(temp_instance_zeros, np.array(
                        [self.contour]), self.current_instance_number)
                    cv2.fillPoly(temp_semantic_zeros, np.array(
                        [self.contour]), self.current_class)
                    self.image[min_y: max_y, min_x: max_x, :] += temp_zeros
                    self.instance_image[min_y: max_y,
                                        min_x: max_x] += temp_instance_zeros
                    self.semantic_image[min_y:max_y,
                                        min_x: max_x] += temp_semantic_zeros
                    self.current_instance_number += 1
                    # temp_zeros = cv2.cvtColor(temp_zeros, cv2.COLOR_BGR2GRAY)

                    cc = dip.GetImageChainCodes(temp_instance_zeros)

                    for c in cc:
                        contour = np.array(c.Polygon()).astype(np.uint16)
                        contour[:, 0] += min_x
                        contour[:, 1] += min_y
                        self.contours_dictionary[self.current_instance_number] = contour.tolist(
                        )
                        if self.show_contour_bool:
                            for point in contour:
                                self.image[point[1], point[0], :] = (
                                    255, 255, 255)

                self.copy_image[min_y: max_y, min_x: max_x] = copy.deepcopy(
                    self.image[min_y: max_y, min_x: max_x])
                self.updateImage()
                self.contour = []
                self.is_right_mouse_clicked = False
                self.left_neighbour = False
                self.in_some_other_contour = False

    def drawAt(self, pos, ev=None):
        if self.axisOrder == "col-major":
            pos = [int(pos.x()), int(pos.y())]
        else:
            pos = [int(pos.y()), int(pos.x())]
        dk = self.drawKernel
        kc = self.drawKernelCenter
        sx = [0, dk.shape[0]]
        sy = [0, dk.shape[1]]
        tx = [pos[0] - kc[0], pos[0] - kc[0] + dk.shape[0]]
        ty = [pos[1] - kc[1], pos[1] - kc[1] + dk.shape[1]]

        for i in [0, 1]:
            dx1 = -min(0, tx[i])
            dx2 = min(0, self.image.shape[0]-tx[i])
            tx[i] += dx1+dx2
            sx[i] += dx1+dx2

            dy1 = -min(0, ty[i])
            dy2 = min(0, self.image.shape[1]-ty[i])
            ty[i] += dy1+dy2
            sy[i] += dy1+dy2

        ts = (slice(tx[0], tx[1]), slice(ty[0], ty[1]))
        ss = (slice(sx[0], sx[1]), slice(sy[0], sy[1]))
        src = dk

        self.image[ts] = src[ss]

        self.updateImage()
        
    def setDrawKernel(self, kernel_size=3, r=255, g=255, b=255, erase=False):
        bs = kernel_size
        kernel_red = np.ones((bs, bs), np.uint8)
        kernel_green = np.ones((bs, bs), np.uint8)
        kernel_blue = np.ones((bs, bs), np.uint8)

        if (erase):

            kernel = np.dstack((kernel_red, kernel_green, kernel_blue))
            kernel = 0 * kernel

        else:
            self.brush_size = kernel_size

            # randomize channel values
            kernel_red = kernel_red * r
            kernel_green = kernel_green * g
            kernel_blue = kernel_blue * b
            kernel = np.dstack((kernel_red, kernel_green, kernel_blue))

        self.drawKernel = kernel
        self.drawKernelCenter = [int(np.floor(kernel.shape[0]/2)),
                                 int(np.floor(kernel.shape[1]/2))]

    def hideMasks(self):
        self.image = np.zeros(
            (self.image.shape[0], self.image.shape[1]), dtype=np.uint16)
        self.updateImage()

    def showMasks(self):
        self.image = self.copy_image
        self.updateImage()

    def hideContours(self):
        for contour in self.contours_dictionary.values():
            for point in contour:
                semantic_class = self.semantic_image[point[1], point[0]]
                self.image[point[1], point[0],
                           :] = self.parent.colors_list[semantic_class]
                self.updateImage()

    def showContours(self):
        for contour in self.contours_dictionary.values():
            for point in contour:
                self.image[point[1], point[0], :] = (255, 255, 255)
                self.updateImage()
    # def removeOverlappingRegion(contour1, contour2):


######################### ImageItem for contours ##############################

class ContourDraw(pg.ImageItem):
    """
    **Bases:** :class:`GraphicsObject <pyqtgraph.GraphicsObject>`
    """
    # Overall description of ImageItem (including examples) moved to documentation text
    sigImageChanged = QtCore.pyqtSignal()

    def __init__(self, image=None, parent=None, num_of_classes=None, **kargs):

        super(ContourDraw, self).__init__()

        self.neighbour_size = 5
        self.current_class = parent.current_class
        self.left_neighbour = False
        self.instance_starting_point = Point()
        self.parent = parent
        self.levels = np.array([0, 255])
        self.is_right_mouse_clicked = False
        self.brush_size = 3
        self.setDrawKernel(kernel_size=self.parent.brush_size)
        if image is not None:
            self.setImage(image, **kargs)
        else:
            self.setOpts(**kargs)
        

    def mouseDragEvent(self, ev):
        ev.ignore()

    def mouseClickEvent(self, ev):
        if self.current_class is None and ev.button() == QtCore.Qt.MouseButton.RightButton:
            select_class_dialog = QtWidgets.QDialog()
            select_class_label = QtWidgets.QLabel(
                "Please select a class before starting annotation :)")
            select_class_layout = QtWidgets.QVBoxLayout()
            select_class_layout.addWidget(select_class_label)
            select_class_dialog.setLayout(select_class_layout)
            select_class_dialog.exec_()
            return
        if self.parent.hide_masks_checkbox.isChecked() == True:
            ev.ignore()
        elif ev.button() == QtCore.Qt.MouseButton.RightButton and self.is_right_mouse_clicked == False:

            self.is_right_mouse_clicked = True
            self.instance_starting_point = ev.pos()
            self.setDrawKernel(self.brush_size)

        else:
            ev.ignore()

    def calDistance(self, p1, p2):
        return math.sqrt((p1.x() - p2.x())**2 + (p1.y() - p2.y())**2)

    def hoverEvent(self, ev):
        if (ev.isExit()):
            return
        if self.is_right_mouse_clicked:
            current_x = ev.pos().x()
            current_y = ev.pos().y()

            if (self.left_neighbour == False):
                if (self.calDistance(self.instance_starting_point, ev.pos()) > (self.neighbour_size + self.brush_size)):
                    # print(self.calDistance(self.instance_starting_point, ev.pos()))
                    self.left_neighbour = True

            self.drawAt(ev.pos(), ev)

            if (current_x > self.instance_starting_point.x() - ((self.brush_size + self.neighbour_size)/2) and
                current_x < self.instance_starting_point.x() + ((self.brush_size + self.neighbour_size)/2) and
                current_y > self.instance_starting_point.y() - ((self.brush_size + self.neighbour_size)/2) and
                current_y < self.instance_starting_point.y() + ((self.brush_size + self.neighbour_size)/2) and
                    self.left_neighbour):

                self.image = np.zeros(
                    (self.image.shape[0], self.image.shape[1], 3))
                self.updateImage()

                self.is_right_mouse_clicked = False
                self.left_neighbour = False

    def drawAt(self, pos, ev=None):
        if self.axisOrder == "col-major":
            pos = [int(pos.x()), int(pos.y())]
        else:
            pos = [int(pos.y()), int(pos.x())]
        dk = self.drawKernel
        kc = self.drawKernelCenter
        sx = [0, dk.shape[0]]
        sy = [0, dk.shape[1]]
        tx = [pos[0] - kc[0], pos[0] - kc[0] + dk.shape[0]]
        ty = [pos[1] - kc[1], pos[1] - kc[1] + dk.shape[1]]

        for i in [0, 1]:
            dx1 = -min(0, tx[i])
            dx2 = min(0, self.image.shape[0]-tx[i])
            tx[i] += dx1+dx2
            sx[i] += dx1+dx2

            dy1 = -min(0, ty[i])
            dy2 = min(0, self.image.shape[1]-ty[i])
            ty[i] += dy1+dy2
            sy[i] += dy1+dy2

        ts = (slice(tx[0], tx[1]), slice(ty[0], ty[1]))
        ss = (slice(sx[0], sx[1]), slice(sy[0], sy[1]))
        src = dk

        self.image[ts] = src[ss]

        self.updateImage()
        # self.thread.start()

    def setDrawKernel(self, kernel_size=1, r=255, g=0, b=0):

        self.brush_size = kernel_size
        bs = self.brush_size
        kernel_red = np.ones((bs, bs), np.uint8)
        kernel_green = np.ones((bs, bs), np.uint8)
        kernel_blue = np.ones((bs, bs), np.uint8)

        # randomize channel values
        kernel_red = kernel_red * r
        kernel_green = kernel_green * g
        kernel_blue = kernel_blue * b
        kernel = np.dstack((kernel_red, kernel_green, kernel_blue))

        self.drawKernel = kernel
        self.drawKernelCenter = [int(np.floor(kernel.shape[0]/2)),
                                 int(np.floor(kernel.shape[1]/2))]
