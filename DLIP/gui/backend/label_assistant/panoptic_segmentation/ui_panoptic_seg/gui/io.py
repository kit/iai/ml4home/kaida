"""
Copyright © 2020 Howard Hughes Medical Institute

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
Neither the name of HHMI nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

import os, datetime, gc, warnings, glob
from natsort import natsorted
import numpy as np
import cv2
import tifffile
from skimage import measure
import diplib as dip
import logging

from DLIP.gui.backend.label_assistant.panoptic_segmentation.ui_panoptic_seg import utils, transforms
from DLIP.gui.backend.label_assistant.panoptic_segmentation.ui_panoptic_seg.io import imread, imsave, outlines_to_text
from DLIP.gui.backend.label_assistant.panoptic_segmentation.ui_panoptic_seg.utils import ncolorlabel

try:
    from PyQt5.QtWidgets import QFileDialog
    GUI = True
except:
    GUI = False

try:
    import matplotlib.pyplot as plt
    MATPLOTLIB = True
except:
    MATPLOTLIB = False

NCOLOR = False 
# WIP to make GUI use N-color masks. Tricky thing is that only the display should be 
# reduced to N colors; selection and editing should act on unique labels. 
    
def _load_image(parent, filename=None):
    # """ load image with filename; if None, open QFileDialog """
    # if filename is None:
    #     name = QFileDialog.getOpenFileName(
    #         parent, "Load image"
    #         )
    #     filename = name[0]
    # manual_file = os.path.splitext(filename)[0]+'_seg.npy'
    # if os.path.isfile(manual_file):
    #     print(manual_file)
    #     _load_seg(parent, manual_file, image=imread(filename), image_file=filename)
    #     return
    # elif os.path.isfile(os.path.splitext(filename)[0]+'_manual.npy'):
    #     manual_file = os.path.splitext(filename)[0]+'_manual.npy'
    #     _load_seg(parent, manual_file, image=imread(filename), image_file=filename)
    #     return
    # try:
    #     image = imread(filename)
    #     parent.loaded = True
    # except:
    #     print('images not compatible')
    if filename is None:
        name = QFileDialog.getOpenFileName(
            parent, "Load image"
            )
        filename = name[0]
    try:
        # print(filename)
        image = imread(filename)
        parent.loaded = True
    except:
        print('images not compatible')
    if parent.loaded:
        parent.clear_all()
        parent.reset()
        parent.filename = filename
        _initialize_images(parent, image)
        parent.loaded = True

def _initialize_images(parent, image):
    # print(parent)
    parent.setImage(image)

def _load_masks(parent, instance_filename=None, semantic_filename=None):
    """ load zeros-based masks (0=no cell, 1=cell 1, ...) """
    if instance_filename is None:
        name = QFileDialog.getOpenFileName(
            parent, "Load masks (PNG or TIFF)"
            )
        instance_filename = name[0]
    if semantic_filename is None:
        name = QFileDialog.getOpenFileName(
            parent, "Load masks (PNG or TIFF)"
        )
        semantic_filename = name[0]
    instance_mask = imread(instance_filename)
    semantic_mask = imread(semantic_filename)
    _masks_to_gui(parent, instance_mask, semantic_mask)

    #parent.update_plot()

def _masks_to_gui(parent, instance_mask, semantic_mask, outlines=None):
    """ masks loaded into GUI """
    parent.draw_layer.instance_image = instance_mask
    parent.draw_layer.semantic_image = semantic_mask

    cc = dip.GetImageChainCodes(instance_mask)
    d = {}
    for c in cc:
        d[c.objectID] = np.array(c.Polygon()).astype(np.uint8).tolist()
    # print((np.array(c.Polygon()).shape))
    parent.draw_layer.contours_dictionary = d
    parent.draw_layer.current_instance_number = int(np.amax(instance_mask)) + 1
    for semantic_class in np.unique(semantic_mask):
        # if semantic_class == 0:
        #     continue
        parent.draw_layer.image[semantic_mask ==
                                semantic_class] = parent.colors_list[semantic_class]
        parent.draw_layer.copy_image = parent.draw_layer.image.copy()
    parent.draw_layer.updateImage()
    # parent.update_plot()

def _save_label(parent):
    """ save masks to png or tiff (if 3D) """
    filename = parent.filename
    base = os.path.splitext(filename)[0]
    instance_img = measure.label(parent.draw_layer.instance_image)
    # print(instance_img.dtype)
    instance_img = instance_img.astype(np.uint8)
    imsave(base + '_instance_label.tif', instance_img)
    semantic_image = parent.draw_layer.semantic_image
    semantic_image = semantic_image.astype(np.uint8)
    imsave(base + '_semantic_label.tif', semantic_image)
    # print(np.unique(instance_img), np.unique(semantic_image))

def _save_outlines(parent):
    pass
    # filename = parent.filename
    # base = os.path.splitext(filename)[0]
    # if parent.NZ==1:
    #     print('saving 2D outlines to text file, see docs for info to load into ImageJ')    
    #     outlines = utils.outlines_list(parent.cellpix[0])
    #     outlines_to_text(base, outlines)
    # else:
    #     print('ERROR: cannot save 3D outlines')
    

def _save_sets(parent):
    """ save masks to *_seg.npy """
    pass
    # filename = parent.filename
    # base = os.path.splitext(filename)[0]
    # if parent.NZ > 1 and parent.is_stack:
    #     np.save(base + '_seg.npy',
    #             {'outlines': parent.outpix,
    #              'colors': parent.cellcolors[1:],
    #              'masks': parent.cellpix,
    #              'current_channel': (parent.color-2)%5,
    #              'filename': parent.filename,
    #              'flows': parent.flows,
    #              'zdraw': parent.zdraw})
    # else:
    #     image = parent.chanchoose(parent.stack[parent.currentZ].copy())
    #     if image.ndim < 4:
    #         image = image[np.newaxis,...]
    #     np.save(base + '_seg.npy',
    #             {'outlines': parent.outpix.squeeze(),
    #              'colors': parent.cellcolors[1:],
    #              'masks': parent.cellpix.squeeze(),
    #              'chan_choose': [parent.ChannelChoose[0].currentIndex(),
    #                              parent.ChannelChoose[1].currentIndex()],
    #              'img': image.squeeze(),
    #              'ismanual': parent.ismanual,
    #              'X2': parent.X2,
    #              'filename': parent.filename,
    #              'flows': parent.flows})
    # #print(parent.point_sets)
    # print('--- %d ROIs saved chan1 %s, chan2 %s'%(parent.ncells,
    #                                               parent.ChannelChoose[0].currentText(),
    #                                               parent.ChannelChoose[1].currentText()))
