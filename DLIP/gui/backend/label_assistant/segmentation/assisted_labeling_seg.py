from DLIP.gui.backend.label_assistant.base_assisted_labeling_mask_external import BaseAssistedLabelingGuiExternalMasks


class AssistedLabelingSegmentationGui(BaseAssistedLabelingGuiExternalMasks):
    def __init__(
        self,
        parent,
        project,
        path_project,
        data_module,
        task_property,
        path_seg_annotation_tool,
        open_gui
    ):

        super(AssistedLabelingSegmentationGui, self).__init__(
            parent,
            project,
            path_project,
            data_module,
            task_property,
            path_seg_annotation_tool,
            open_gui
        )

        self.setWindowTitle("KaIDA - Segmentation")

        text = [f"{ix}:{name}" for ix, name in enumerate(self.project["class_names"])] 

        self.label_lookup.setText('\n'.join(text))