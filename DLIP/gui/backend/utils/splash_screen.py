from typing import NoReturn

from PyQt5.QtCore import QCoreApplication
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QSplashScreen


class SplashScreen:
    def __init__(self, splash_screen_img_path: str, width_percentage: float = 0.33):
        self.width_percentage = width_percentage
        self.splash_screen = QSplashScreen(self._load_splash_screen_img(splash_screen_img_path))

    def _load_splash_screen_img(self, splash_screen_img_path: str) -> QPixmap:
        screen_geometry = QCoreApplication.instance().desktop().screenGeometry()
        screen_width, screen_height = screen_geometry.width(), screen_geometry.height()

        img = QPixmap(splash_screen_img_path)
        width_img, height_img = img.width(), img.height()

        width_scaled = self.width_percentage * screen_width
        height_scaled = height_img * width_scaled / width_img

        return img.scaled(width_scaled, height_scaled)

    def show(self) -> NoReturn:
        self.splash_screen.show()

    def close(self) -> bool:
        return self.splash_screen.close()
