from PyQt5.QtWidgets import QMessageBox


class WarningMessageBox:
    def __init__(self, par: 'PyQt5.QtWidgets.QWidget'):
        self.par = par

    def show(self, text: str, window_title: str = "Warning"):
        dlg = QMessageBox(self.par)
        dlg.setWindowTitle(window_title)
        dlg.setText(text)
        dlg.exec()
