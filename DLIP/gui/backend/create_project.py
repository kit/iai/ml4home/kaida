import json
import logging
import re
from pathlib import Path
import os

from PyQt5 import QtWidgets, uic

from .utils import WarningMessageBox


class CreateProjectGui(QtWidgets.QDialog):
    def __init__(
        self,
        path_project,
        task_properties
    ):
        super(CreateProjectGui,self).__init__()
        uic.loadUi(f"{path_project}/gui/layouts/gui_create_project.ui", self)
        
        self.project = dict()
        self.project["name"]            = None
        self.project["n_classes"]       = None
        self.project["data_dir"]        = None
        self.project["task"]            = None
        self.project["class_names"]     = None
        self.project["dvc_server_dir"]  = None

        self._output = None

        self.class_lst  = [str(ix) for ix in list(range(1,int(1e3+1)))]
        self.class_combo_box.addItems(self.class_lst)

        self.task_name_lst = list()
        self.task_lst = list()
        for task in task_properties.keys():
           self.task_name_lst.append(getattr(task_properties[task], "name"))
           self.task_lst.append(task)
        self.tasks_combo_box.addItems(self.task_name_lst)


    def control_class_selection(self,item):
        if item == "Segmentation" or item == "Classification" or item == "Panoptic Segmentation" or item == "Object Detection":
            self.class_combo_box.setDisabled(False)
            self.class_names_textEdit.setDisabled(False)
            self.label_2.setDisabled(False)
            self.label_4.setDisabled(False)
        else:
            self.class_combo_box.setDisabled(True)
            self.class_names_textEdit.setDisabled(True)
            self.label_2.setDisabled(True)
            self.label_4.setDisabled(True)

    def select_data_dir(self):
        self.project["data_dir"] = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory"))

    def select_dvc_server_dir(self):
        if self.dvc_check_box.isChecked():
            self.project["dvc_server_dir"] = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Name of Server Directory"))
        else:
            logging.info("Enable DVC first")

    def init_project(self):
        if int(self.class_lst[self.class_combo_box.currentIndex()])+1 != len(["Background"]+re.split('\n|, |; |,|;', self.class_names_textEdit.toPlainText())):
            WarningMessageBox(self).show("Number of classes and class names entered do not match!")
            return
        if self.project["data_dir"] is None:
            WarningMessageBox(self).show("Please select a data directory!")
            return

        if len([f for f in Path(self.project["data_dir"]).iterdir() if f.is_file()]) == 0:
            WarningMessageBox(self).show("The given data directory does not contain any data. "
                                         "Did you select the wrong directory?")
            return

        self.project["name"] = self.dataset_name.text()

        if self.project["name"] == "":
            WarningMessageBox(self).show("Please select a project name!")
            return

        self.project["n_classes"]   = int(self.class_lst[self.class_combo_box.currentIndex()])
        self.project["task"]        = self.task_lst[self.tasks_combo_box.currentIndex()]
        if int(self.class_lst[self.class_combo_box.currentIndex()]) == 1 or (not self.project["task"] == "segmentation" and not self.project["task"] == "panoptic_segmentation"):
            self.project["class_names"] = re.split('\n|, |; |,|;',self.class_names_textEdit.toPlainText())
        if self.project["task"] == "segmentation" or self.project["task"] == "panoptic_segmentation":
            self.project["n_classes"] = int(self.class_lst[self.class_combo_box.currentIndex()])+1
            self.project["class_names"]= ["Background"]+re.split('\n|, |; |,|;',self.class_names_textEdit.toPlainText())
        
        filename_project = self.project["name"].lower()
        filename_project = filename_project.replace(' ', '_')

        if self.check_init_project_status():
            project_path = os.path.join(self.project['data_dir'], filename_project + '.json')
            self.project["prj_file_name"] =  project_path #export_file_name
            self._output = self.project
            with open(project_path, 'w') as f:
                json.dump(self.project, f)
            logging.info("project init was successful")
            self.accept()
        else: 
            logging.info("fill in all fields")

    def check_init_project_status(self):
        if not self.dvc_check_box.isChecked():
            del self.project["dvc_server_dir"]
        check_status = all(value is not None for value in self.project.values())
        
        if not self.dvc_check_box.isChecked():
            self.project["dvc_server_dir"] = None
        return check_status

    def get_output(self):
        return self._output