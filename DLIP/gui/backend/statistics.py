from PyQt5 import QtCore, QtGui, QtWidgets, uic
import os
import tifffile
import cv2
import PIL
import statistics
import matplotlib as plt
import numpy as np
from skimage.measure import label, regionprops
import copy
plt.use('Qt5Agg')

import math

from ALS4L.data_source.datasets.generic_segmentation_dataset.gen_seg_data_module import GenericSegmentationDataModule

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.figure import Figure

class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes1 = fig.add_subplot(1,2,1)
        self.axes2 = fig.add_subplot(1,2,2,sharey=self.axes1)
        yax = self.axes2.get_yaxis()
        yax = yax.set_visible(False)

        super(MplCanvas, self).__init__(fig)

class StatisticGui(QtWidgets.QDialog):
    def __init__(self, project_root,project):
        super(StatisticGui,self).__init__()
        uic.loadUi(os.path.expandvars(f"{project_root}/hil_labeling/gui/layouts/gui_statistics.ui"), self)
        self.project=project
        self.project_root=project_root
        self.data_module = GenericSegmentationDataModule(
            root_dir=self.project["data_dir"],
        )

        #init samples and labels lists
        samples_train       = os.listdir(self.data_module.labeled_train_dataset.samples)
        self.samples_train  = [os.path.join(self.data_module.labeled_train_dataset.samples, file) for file in samples_train]
        samples_test        = os.listdir(self.data_module.test_dataset.samples)
        self.samples_test   = [os.path.join(self.data_module.test_dataset.samples, file) for file in samples_test]
        self.samples        = self.samples_train + self.samples_test
        labels_train        = os.listdir(self.data_module.labeled_train_dataset.labels)
        self.labels_train   = [os.path.join(self.data_module.labeled_train_dataset.labels, file) for file in labels_train]
        labels_test         = os.listdir(self.data_module.test_dataset.labels)
        self.labels_test    = [os.path.join(self.data_module.test_dataset.labels, file) for file in labels_test]
        self.labels         = self.labels_train + self.labels_test

        self.img_type       = self.data_module.unlabeled_train_dataset.samples_data_format
        
        self.curr_img        = 1
        self.curr_page_lbl.setText(f"{self.curr_img}/{len(self.samples)}")        

        self.class_names    = self.project["class_names"]
        self.gen_stats_names    = ["Train images", "Test images"]

        self.get_general_stats()
        self.get_img_specific_stats()

        self.next_btn.clicked.connect(lambda: self.change_img(1))
        self.prev_btn.clicked.connect(lambda: self.change_img(-1))

    def get_general_stats(self):
        self.num_img_train = len(self.samples_train)
        self.num_img_test = len(self.samples_test)
        gen_stats = [str(self.num_img_train),str(self.num_img_test)]

        self.gen_stats_tbl.setColumnCount(2)
        self.gen_stats_tbl.setRowCount(len(gen_stats))

        for i in range(0,len(gen_stats)):
            self.gen_stats_tbl.setItem(i,1,QtWidgets.QTableWidgetItem(gen_stats[i]))
            self.gen_stats_tbl.setItem(i,0,QtWidgets.QTableWidgetItem(self.gen_stats_names[i]))
        
        self.gen_stats_tbl.horizontalHeader().setStretchLastSection(True)
        self.gen_stats_tbl.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.gen_stats_tbl.horizontalHeader().hide()

        # init histogram figure canvas
        self.sc = MplCanvas(self, width=2, height=2, dpi=100)

        self.create_histogram(self.labels_train,"train",self.sc.axes1)
        self.create_histogram(self.labels_test,"test",self.sc.axes2)

        layout = QtWidgets.QVBoxLayout()
        layout.addWidget(self.sc)
        self.Histogram.setLayout(layout)           

        
    def get_img_specific_stats(self):

        img_path = self.samples[self.curr_img-1]
        if img_path in self.samples_train:
            img_set = "train"
        elif img_path in self.samples_test:
            img_set = "test"

        img = QtGui.QImage(img_path).scaled(200,200, aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation) 
        self.img_lbl.setPixmap(QtGui.QPixmap.fromImage(img))
        self.img_lbl.adjustSize()

        if len(self.labels) > 0:
            # loads label
            lbl_path=self.labels[self.curr_img-1]
            lbl = tifffile.imread(lbl_path) if self.img_type == "tif" else cv2.imread(lbl_path,-1)

            # colors and displays labels
            lbl_hue = np.uint8(179*lbl/np.max(lbl))
            blanc_ch = 255 * np.ones_like(lbl_hue)
            col_lbl = cv2.merge([lbl_hue, blanc_ch, blanc_ch])
            col_lbl= cv2.cvtColor(col_lbl,cv2.COLOR_HSV2BGR)
            col_lbl[lbl==0]=0
            QImg = QtGui.QImage(col_lbl.data, col_lbl.shape[1], col_lbl.shape[0], col_lbl.strides[0], QtGui.QImage.Format_RGB888).scaled(200,200,aspectRatioMode=QtCore.Qt.KeepAspectRatio,transformMode=QtCore.Qt.SmoothTransformation)
            self.label_lbl.setPixmap(QtGui.QPixmap.fromImage(QImg))
            self.label_lbl.adjustSize()

            # collects label information 
            num=len(regionprops(label(lbl)))
            classes = np.unique(lbl)
            classes=classes[1:]
            num_seg=list()
            area_integral=list()
            area_mean=list()
            class_labels = list()
            for cls in classes:
                temp = copy.deepcopy(lbl)
                temp[temp!=cls]=0
                temp_lbl = label(temp)
                r_props=regionprops(temp_lbl)
                area_lst = list()
                for region in r_props:
                    area_lst.append(region["area"])
                num_seg.append(len(r_props))
                area_integral.append(sum(area_lst))
                area_mean.append(np.mean(area_lst))
                try:
                    class_labels.append(self.class_names[cls])
                except:
                    class_labels.append(f"Class_{cls}")
                    self.class_names.append(f"Class_{cls}")

            # sets up table
            self.img_stats_tbl.clearContents()
            self.img_stats_tbl.setColumnCount(len(self.class_names)+1)
            self.img_stats_tbl.setRowCount(6)

            self.img_stats_tbl.setItem(0,1,QtWidgets.QTableWidgetItem(img_set))
            self.img_stats_tbl.setItem(0,0,QtWidgets.QTableWidgetItem("Set"))

            self.img_stats_tbl.setItem(1,0,QtWidgets.QTableWidgetItem("Total Segments"))
            self.img_stats_tbl.setItem(1,1,QtWidgets.QTableWidgetItem(str(num)))

            ver_header=["Segment classes", "Number of segments","Area: integral","Area: mean"]
            for i in range(0,len(ver_header)):
                self.img_stats_tbl.setItem(i+2,0,QtWidgets.QTableWidgetItem(ver_header[i]))
            for i in range(0,len(area_integral)):
                self.img_stats_tbl.setItem(2,i+1,QtWidgets.QTableWidgetItem(self.class_names[i+1]))
                self.img_stats_tbl.setItem(3,i+1,QtWidgets.QTableWidgetItem(str(num_seg[i])))
                self.img_stats_tbl.setItem(4,i+1,QtWidgets.QTableWidgetItem(str(area_integral[i])))
                self.img_stats_tbl.setItem(5,i+1,QtWidgets.QTableWidgetItem(str(area_mean[i])))
            
            self.img_stats_tbl.horizontalHeader().setStretchLastSection(True)
            self.img_stats_tbl.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
            self.img_stats_tbl.horizontalHeader().hide()
    
    def change_img(self, direction):
        new_img = self.curr_img + direction
        if new_img < 1 or new_img > len(self.samples):
            pass
        else:
            self.curr_img = new_img
        self.get_img_specific_stats()
        self.curr_page_lbl.setText(f"{self.curr_img}/{len(self.samples)}")

    
    def create_histogram(self, im_set ,title, ax):
        num_lbls = list()
        for i in range(0,len(im_set)):
            lbl_path = im_set[i]
            lbl = tifffile.imread(lbl_path) if self.img_type == "tif" else cv2.imread(lbl_path,-1)
            num, labels, stats, centroids = cv2.connectedComponentsWithStats(lbl,connectivity=8)
            num_lbls.append(num)
        
        ax.hist(num_lbls,bins=np.arange(min(num_lbls) -0.5, max(num_lbls) +1.5, 1),rwidth=0.8,color='grey', edgecolor='black',linewidth=2.0)
        ax.set_title(f"{title}")

                


                

