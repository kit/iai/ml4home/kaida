from PyQt5 import QtCore, QtGui, uic
import numpy as np
from skimage.color import label2rgb

from DLIP.gui.backend.post_inspection.post_insp_ui_base import PostInspectionBaseGui
from DLIP.utils.helper_functions.create_qimage import array_to_qimage

from PyQt5 import QtCore, QtGui
import numpy as np
from skimage.color import label2rgb

class PostInspectionMaskGui(PostInspectionBaseGui):
    def __init__(self, path_project,project):
        super(PostInspectionMaskGui,self).__init__(path_project, project)
        uic.loadUi(f"{path_project}/gui/layouts/gui_post_insp_mask.ui", self)
        self.overlay_img_raw = None

    def do_inspection(self, label, img):
        self.relabeling = False
        self.overlay_img_raw = array_to_qimage(np.uint8(label2rgb(label, image=img, bg_label=0)*255,))
        self.image.setPixmap(
            QtGui.QPixmap.fromImage(
                self.overlay_img_raw.scaled(
                    0.9*self.frameGeometry().width(),0.9*self.frameGeometry().height(), 
                    aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
        )
        self.exec()

    def resize_fcn(self, event):
        if self.overlay_img_raw is not None:
            self.image.setPixmap(
                QtGui.QPixmap.fromImage(
                    self.overlay_img_raw.scaled(
                        0.9*self.frameGeometry().width(),0.9*self.frameGeometry().height(), 
                        aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
            )