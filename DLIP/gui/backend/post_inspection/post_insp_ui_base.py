from PyQt5 import QtWidgets
from abc import abstractmethod

class PostInspectionBaseGui(QtWidgets.QDialog):
    def __init__(self, path_project,project):
        super(PostInspectionBaseGui,self).__init__()
        self.resizeEvent = self.resize_fcn
        self.project        = project
        self.path_project   = path_project
        self.relabeling     = False

    def keep(self):
        self.relabeling = False
        self.close()
    
    def relabel(self):
        self.relabeling = True
        self.close()

    @abstractmethod
    def do_inspection(self,label, img):
        pass

    @abstractmethod
    def resize_fcn(self, event):
        pass

