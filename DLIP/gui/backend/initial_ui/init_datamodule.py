import json
import os
import shutil
import copy

import filetype
from PyQt5.QtCore import QObject, pyqtSignal

from DLIP.utils.loading.load_class import load_class


class DataModuleInitializer(QObject):
    finished = pyqtSignal()
    data = pyqtSignal(dict, dict, object)
    info = pyqtSignal(str)

    def __init__(self, project, task_properties):
        super(DataModuleInitializer, self).__init__()

        self.project = copy.deepcopy(project)
        self.task_properties = copy.deepcopy(task_properties)

    def _make_folder_structure(self):
        train_dir = os.path.join(self.project["data_dir"], "train")
        test_dir = os.path.join(self.project["data_dir"], "test")

        if not os.path.exists(train_dir):
            os.makedirs(train_dir)

        if not os.path.exists(test_dir):
            os.makedirs(test_dir)

        unlabeled_dir = os.path.join(self.project["data_dir"], "unlabeled")
        if not os.path.exists(unlabeled_dir):
            os.makedirs(unlabeled_dir)

        if self.project["task"] == "classification":
            for class_name in self.project["class_names"]:
                train_sub_dir = os.path.join(train_dir, class_name)
                if not os.path.exists(train_sub_dir):
                    os.makedirs(train_sub_dir)
                test_sub_dir = os.path.join(test_dir, class_name)
                if not os.path.exists(test_sub_dir):
                    os.makedirs(test_sub_dir)

            for filename in os.listdir(self.project["data_dir"]):
                src = os.path.join(self.project["data_dir"], filename)
                dst = os.path.join(unlabeled_dir, filename)
                if os.path.isfile(src) and filetype.is_image(src):
                    shutil.move(src, dst)
        elif self.project["task"] == "panoptic_segmentation":
            train_samples = os.path.join(train_dir, "samples")
            train_instance_labels = os.path.join(train_dir, "instance_labels")
            train_semantic_labels = os.path.join(train_dir, "semantic_labels")

            test_samples = os.path.join(test_dir, "samples")
            test_instance_labels = os.path.join(test_dir, "instance_labels")
            test_semantic_labels = os.path.join(test_dir, "semantic_labels")


            if not os.path.exists(train_samples):
                os.makedirs(train_samples)
            if not os.path.exists(train_instance_labels):
                os.makedirs(train_instance_labels)
            if not os.path.exists(train_semantic_labels):
                os.makedirs(train_semantic_labels)
            if not os.path.exists(test_samples):
                os.makedirs(test_samples)
            if not os.path.exists(test_instance_labels):
                os.makedirs(test_instance_labels)
            if not os.path.exists(test_semantic_labels):
                os.makedirs(test_semantic_labels)

            unlabeled_samples = os.path.join(unlabeled_dir, "samples")
            unlabeled_instance_labels = os.path.join(unlabeled_dir, "instance_labels")
            unlabeled_semantic_labels = os.path.join(unlabeled_dir, "semantic_labels")

            if not os.path.exists(unlabeled_samples):
                os.makedirs(unlabeled_samples)

            if not os.path.exists(unlabeled_instance_labels):
                os.makedirs(unlabeled_instance_labels)

            if not os.path.exists(unlabeled_semantic_labels):
                os.makedirs(unlabeled_semantic_labels)

            for filename in os.listdir(self.project["data_dir"]):
                src = os.path.join(self.project["data_dir"], filename)
                dst = os.path.join(unlabeled_samples, filename)
                if os.path.isfile(src) and filetype.is_image(src):
                    shutil.move(src, dst)
        else:
            train_samples = os.path.join(train_dir, "samples")
            train_labels = os.path.join(train_dir, "labels")

            test_samples = os.path.join(test_dir, "samples")
            test_labels = os.path.join(test_dir, "labels")

            if not os.path.exists(train_samples):
                os.makedirs(train_samples)
            if not os.path.exists(train_labels):
                os.makedirs(train_labels)
            if not os.path.exists(test_samples):
                os.makedirs(test_samples)
            if not os.path.exists(test_labels):
                os.makedirs(test_labels)

            unlabeled_samples = os.path.join(unlabeled_dir, "samples")
            unlabeled_labels = os.path.join(unlabeled_dir, "labels")

            if not os.path.exists(unlabeled_samples):
                os.makedirs(unlabeled_samples)

            if not os.path.exists(unlabeled_labels):
                os.makedirs(unlabeled_labels)

            for filename in os.listdir(self.project["data_dir"]):
                src = os.path.join(self.project["data_dir"], filename)
                dst = os.path.join(unlabeled_samples, filename)
                if os.path.isfile(src) and filetype.is_image(src):
                    shutil.move(src, dst)

        temp_dir = os.path.join(self.project["data_dir"], "temp")
        if not os.path.exists(temp_dir):
            os.makedirs(temp_dir)

        if self.project["task"] == "segmentation" or self.project["task"] == "seed_detection" or self.project["task"] == "panoptic_segmentation":
            settings_labeling = {
                'currImageIndex': 0,
                'imageDir': temp_dir,
                'labelingMode': False,
                'outputDir': temp_dir
            }

            with open(os.path.join(temp_dir, 'labeling.json'), 'w', encoding='utf-8') as labeling_json:
                json.dump(settings_labeling, labeling_json, ensure_ascii=False, indent=4)

    def run(self):
        self.info.emit("Loading data...")
        if self.project is not None:
            self._make_folder_structure()
            task_property = self.task_properties[self.project["task"]]
            dm_class = load_class(f"DLIP.data.base_classes.{task_property.folder_name}", task_property.datamodule_name)
            data_module = dm_class(
                root_dir=self.project["data_dir"],
                n_classes=self.project["n_classes"],
                class_lst=self.project["class_names"],
                initial_labeled_ratio=-1,
            )
            self.data.emit(self.project, self.task_properties, data_module)
        else:
            self.data.emit(None, None, None)

        self.finished.emit()
