import json
from pathlib import Path
from typing import List, Dict, Any

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QFileDialog, QVBoxLayout, QPushButton, QRadioButton

from DLIP.data.version_control.dvc_wrapper.dvc_project import DvcProject, DvcWrapper
from DLIP.gui.backend.utils import WarningMessageBox


class LoadExternalProjectDialog(QDialog):
    FILES_TO_TRACK = ["test", "train", "unlabeled", "meta_info"]

    def __init__(self, parent=None):
        super(LoadExternalProjectDialog, self).__init__(parent)

        self.project: Dict[str, Any] = dict()
        self._output = None

        self.external_data_dir = None

        self.loaded_external_project_file = False
        self.use_dvc = True
        self.copy_files = True

        self.select_external_project_file_button = None
        self.radio_button_contains_data = None
        self.radio_button_download_data = None

        self.init_ui()

    def init_ui(self):
        select_external_project_file_button = QPushButton("select external project file")
        select_external_project_file_button.clicked.connect(self.select_external_project_file)

        select_local_data_dir_button = QPushButton("select local data directory")
        select_local_data_dir_button.clicked.connect(self.select_local_data_dir)

        self.radio_button_contains_data = QRadioButton("Already contains data")
        self.radio_button_contains_data.setEnabled(False)
        self.radio_button_contains_data.toggled.connect(self.radio_buttons_toggled)

        self.radio_button_download_data = QRadioButton("Download data")
        self.radio_button_download_data.setEnabled(False)
        self.radio_button_download_data.setChecked(True)
        self.radio_button_download_data.toggled.connect(self.radio_buttons_toggled)

        self.select_external_project_file_button = QPushButton("select external data directory")
        self.select_external_project_file_button.clicked.connect(self.select_external_data_dir)
        self.select_external_project_file_button.setEnabled(False)

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        layout = QVBoxLayout()
        layout.addWidget(select_external_project_file_button)
        layout.addWidget(select_local_data_dir_button)
        layout.addWidget(self.radio_button_contains_data)
        layout.addWidget(self.radio_button_download_data)
        layout.addWidget(self.select_external_project_file_button)
        layout.addWidget(button_box)

        self.setWindowTitle("Load external project")
        self.setLayout(layout)

    def radio_buttons_toggled(self):
        if self.radio_button_contains_data.isChecked():
            self.select_external_project_file_button.setEnabled(False)
            self.copy_files = False
        else:
            self.select_external_project_file_button.setEnabled(True)
            self.copy_files = True

    def accept(self):
        if not self.loaded_external_project_file:
            WarningMessageBox(self).show("No external project file loaded. Please do so!")
            return

        invalid_file_error = "Maybe the loaded external project file was invalid"

        missing_key_errors = {
            "data_dir": "No data dir selected. Please select one!",
            "name": f"No name given. {invalid_file_error}",
            "n_classes": f"No num of classes given. {invalid_file_error}",
            "task": f"No task type given. {invalid_file_error}",
            "class_names": f"No class names given. {invalid_file_error}"
        }

        for key in missing_key_errors.keys():
            if key not in self.project or self.project[key] is None:
                WarningMessageBox(self).show(missing_key_errors[key])
                return

        local_dir = Path(self.project["data_dir"])

        if not local_dir.exists():
            if not local_dir.parent.exists():
                WarningMessageBox(self).show("The local directory does not exist")
                return
            local_dir.mkdir()

        if self.use_dvc:
            ret = self.load_external_project_using_dvc()
        else:
            ret = self.load_external_project_without_dvc()

        if ret is False:
            return

        self.project["prj_file_name"] = str(Path(self.project["data_dir"])
                                            .joinpath(self.project["name"] + ".json")
                                            .absolute())

        # save project file
        with open(self.project["prj_file_name"], 'w') as f:
            json.dump(self.project, f)

        self._output = self.project
        super(LoadExternalProjectDialog, self).accept()

    def select_external_project_file(self):
        project_file_name = QFileDialog.getOpenFileName(self, "Select external project file")[0]

        if project_file_name == "":
            # cancel
            return

        if Path(project_file_name).exists():
            with open(project_file_name, "r") as f:
                external_project = json.load(f)

                if all([key in external_project.keys() for key in DvcProject.DVC_INFO_FILE_KEYS]):
                    # check if the selected file is a dvc version info and load the right content from it
                    external_project = external_project["project_file"]

                for key in external_project.keys():
                    if key in ["data_dir"]:
                        continue

                    self.project[key] = external_project[key]

            self.loaded_external_project_file = True

            dvc_not_available = (
                    "dvc_server_dir" not in self.project
                    or self.project["dvc_server_dir"] is None
                    or not Path(self.project["dvc_server_dir"]).exists()
            )

            if dvc_not_available:
                WarningMessageBox(self).show("The selected project file contains no dvc information. "
                                             "Therefore, you have to select the data that should be used by hand.",
                                             window_title="Info")
                self.select_external_project_file_button.setEnabled(True)
                self.radio_button_contains_data.setEnabled(True)
                self.radio_button_download_data.setEnabled(True)
                self.use_dvc = False
            else:
                self.select_external_project_file_button.setEnabled(False)
                self.radio_button_contains_data.setEnabled(False)
                self.radio_button_download_data.setEnabled(False)
                self.use_dvc = True

        else:
            print("Invalid file name!")

    def select_external_data_dir(self):
        self.external_data_dir = str(QFileDialog.getExistingDirectory(self, "Select Directory"))

    def select_local_data_dir(self):
        self.project["data_dir"] = str(QFileDialog.getExistingDirectory(self, "Select Directory"))

    def load_external_project_using_dvc(self) -> bool:
        dvc_server_dir = Path(self.project["dvc_server_dir"])

        if (
                not dvc_server_dir.exists()
                or not dvc_server_dir.joinpath("dvc").exists()
                or not dvc_server_dir.joinpath("versions").exists()
                or len([f for f in dvc_server_dir.joinpath("versions").iterdir() if f.is_dir()]) == 0
        ):
            WarningMessageBox(self).show("The server path specified in the project file is invalid. "
                                         "Please select a project file with a valid server path.")
            return False

        max_version = max([int(f.name) for f in dvc_server_dir.joinpath("versions").iterdir() if f.is_dir()])
        external_version_path = dvc_server_dir.joinpath("versions").joinpath(str(max_version))

        local_dir = Path(self.project["data_dir"])

        DvcProject.load_external_version_data(external_version_path, local_dir, self.FILES_TO_TRACK,
                                              full=True, load_git_files=True)

        if not DvcWrapper.is_available():
            WarningMessageBox(self).show("DVC is not available. Please install it!")
            return False

        dvc_wrapper = DvcWrapper(local_dir)
        dvc_wrapper.checkout(force=True)

        f = open(Path(self.project["data_dir"]).joinpath(".dvc_version"), "w")
        f.write(str(external_version_path.name))
        f.close()

        return True

    def load_external_project_without_dvc(self) -> bool:
        if self.copy_files and (self.external_data_dir is None or not Path(self.external_data_dir).exists()):
            WarningMessageBox(self).show("No external server directory selected!")
            return False

        if self.copy_files:
            missing_subdirectories = self.test_for_missing_subdirectories(self.external_data_dir, self.FILES_TO_TRACK)

            if len(missing_subdirectories) != 0:
                warning = "The selected external directory is no valid project directory!\n"
                warning += "Missing subdirectories:\n\t"
                warning += "\n\t".join(missing_subdirectories)

                WarningMessageBox(self).show(warning)
                return False

        local_path = Path(self.project["data_dir"])

        if not self.copy_files:
            missing_subdirectories = self.test_for_missing_subdirectories(self.project["data_dir"], self.FILES_TO_TRACK)

            if len(missing_subdirectories) != 0:
                warning = "The selected local directory does not contain all needed data!\n"
                warning += "Missing subdirectories:\n\t"
                warning += "\n\t".join(missing_subdirectories)

                WarningMessageBox(self).show(warning)
                return False

            return True

        external_path = Path(self.external_data_dir)

        files_to_copy = self.FILES_TO_TRACK.copy()
        if external_path.joinpath("temp").exists():
            files_to_copy.append("temp")

        DvcProject.download_files(external_path, local_path, files_to_copy)

        return True

    @staticmethod
    def test_for_missing_subdirectories(directory: str, needed_sub_folders: List[str]) -> List[str]:
        path = Path(directory)
        missing_subdirectories = [not path.joinpath(needed_sub_folder).exists()
                                  for needed_sub_folder in needed_sub_folders]

        return [name for idx, name in enumerate(needed_sub_folders)
                if missing_subdirectories[idx] is True]

    def get_output(self):
        return self._output
