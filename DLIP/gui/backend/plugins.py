import logging
from DLIP.utils.loading.load_submodule import load_submodule
from PyQt5.QtWidgets import QPushButton
from PyQt5 import QtWidgets, uic

class PluginGui(QtWidgets.QDialog):
    def __init__(self,path_project,project, parent):
        super(PluginGui, self).__init__()
        uic.loadUi(f"{path_project}/gui/layouts/gui_plugins.ui", self)
        self.project=project
        self.path_project=path_project
        self.plugins_lst = None
        self.parent_obj = parent
        self._load_load_plugins()

    def _load_load_plugins(self):
        self.plugins_lst = load_submodule(f"DLIP.plugins")

        for i in range(len(self.plugins_lst)):
            button = QPushButton(str(self.plugins_lst[i].display_name), self)
            button.setObjectName('button_' + str(i))
            button.clicked.connect(lambda _,idx=i: self.open_plugin(idx))
            self.verticalLayout.addWidget(button)
   
    def open_plugin(self, i):
        plugin = self.plugins_lst[i](self.path_project,self.project, parent_obj=self.parent_obj)
        plugin.exec_()