from typing import Dict, Any

from PyQt5.QtWidgets import QWidget, QDialog, QDialogButtonBox, QVBoxLayout, QComboBox, QLabel, QScrollArea
from PyQt5.QtCore import Qt

from DLIP.data.version_control.diff_parser import DiffParser


class SelectVersionDialog(QDialog):
    def __init__(self, parent: QWidget, versions: Dict[str, Dict[str, Any]], task: str):
        super(SelectVersionDialog, self).__init__(parent)

        self.parser = DiffParser(task)

        self.versions = versions
        self.version_names = list(self.versions.keys())
        self.selected_version = str(-1)

        self.version_predecessor_label = QLabel()
        self.version_message_label = QLabel()
        self.version_diff_label = QLabel()

        self._init_ui(parent.width())

    def _init_ui(self, width):
        combo_box = QComboBox()
        combo_box.addItems(self.versions.keys())

        combo_box.currentIndexChanged.connect(self._version_changed_callback)
        self._version_changed_callback(0)

        version_message_scroll_area = QScrollArea()
        version_message_scroll_area.setWidgetResizable(True)

        self.version_message_label.setWordWrap(True)
        self.version_message_label.setAlignment(Qt.AlignTop | Qt.AlignLeft)
        version_message_scroll_area.setWidget(self.version_message_label)

        version_diff_scroll_area = QScrollArea()
        version_diff_scroll_area.setWidgetResizable(True)
        version_diff_scroll_area.setWidget(self.version_diff_label)

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        layout = QVBoxLayout()
        layout.addWidget(combo_box)
        layout.addWidget(self.version_predecessor_label)
        layout.addWidget(QLabel("Message:"))
        layout.addWidget(version_message_scroll_area)
        layout.addWidget(QLabel("Version Info:"))
        layout.addWidget(version_diff_scroll_area)
        layout.addWidget(button_box)

        self.setWindowTitle("Select Version")
        self.setLayout(layout)
        self.setMinimumWidth(int(0.9 * width))

    def _version_changed_callback(self, idx):
        self.selected_version = self.version_names[idx]

        current_version_info = self.versions[self.selected_version]
        last_version = current_version_info["last_version"]
        self.version_predecessor_label.setText(f"Building on version {last_version}"
                                               if last_version != -1 else "This is the first version.")
        self.version_message_label.setText(current_version_info["message"])
        self.version_diff_label.setText(self.parser.parse(current_version_info["diff"]))

    def get_version(self):
        ok = self.exec_()
        return self.selected_version, ok
