from PyQt5.QtCore import QObject, pyqtSignal


class SaveVersionWorker(QObject):
    finished = pyqtSignal()
    commit_info = pyqtSignal(str)

    def __init__(self, dvc_project, commit_message, project):
        super(SaveVersionWorker, self).__init__()
        self.dvc_project = dvc_project
        self.commit_message = commit_message
        self.project = project

    def run(self):
        try:
            self.dvc_project.commit_version(self.commit_message, self.project)
            self.commit_info.emit("")
        except ValueError as err:
            self.commit_info.emit(str(err.args[0]))

        self.finished.emit()
