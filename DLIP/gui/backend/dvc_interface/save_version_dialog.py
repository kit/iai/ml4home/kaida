from typing import Dict, List

from PyQt5.QtWidgets import QWidget, QDialog, QDialogButtonBox, QVBoxLayout, QLabel, QPlainTextEdit, QScrollArea
from PyQt5.QtCore import QThread, QObject, pyqtSignal


from DLIP.data.version_control.diff_parser import DiffParser
from DLIP.utils.qtwaitingspinner import QtWaitingSpinner


class LoadDiffWorker(QObject):
    finished = pyqtSignal()
    data = pyqtSignal(dict)

    def __init__(self, dvc_project):
        super(LoadDiffWorker, self).__init__()
        self.dvc_project = dvc_project

    def run(self):
        self.data.emit(self.dvc_project.get_current_diff())
        self.finished.emit()


class SaveVersionDialog(QDialog):
    def __init__(self, parent: QWidget, task: str, diff: Dict[str, List[str]] = None, dvc_project=None):
        super(SaveVersionDialog, self).__init__(parent)

        if diff is None and dvc_project is None:
            raise ValueError("Either a diff or a dvc project is needed!")

        self.message_edit = QPlainTextEdit("")
        self.version_diff_label = QLabel("")
        self.wait_spinner = QtWaitingSpinner()

        self.task = task

        self._init_ui(parent.width())

        if diff is not None:
            self._set_diff_text(diff)
        else:
            self.load_diff_thread = QThread()
            self.load_diff_worker = LoadDiffWorker(dvc_project)
            self.load_diff_worker.moveToThread(self.load_diff_thread)

            self.load_diff_thread.started.connect(self.load_diff_worker.run)
            self.load_diff_worker.finished.connect(self.load_diff_thread.quit)
            self.load_diff_worker.data.connect(self._set_diff_text)

            self.load_diff_thread.start()

            self.wait_spinner.start()

    def _init_ui(self, width):
        message_scroll_area = QScrollArea()
        message_scroll_area.setWidgetResizable(True)
        message_scroll_area.setWidget(self.message_edit)
        self.message_edit.setWordWrapMode(False)

        button_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        button_box.accepted.connect(self.accept)
        button_box.rejected.connect(self.reject)

        version_diff_scroll_area = QScrollArea()
        version_diff_scroll_area.setWidgetResizable(True)
        version_diff_scroll_area.setWidget(self.version_diff_label)

        layout = QVBoxLayout()
        layout.addWidget(QLabel("Enter version description:"))
        layout.addWidget(message_scroll_area)
        layout.addWidget(version_diff_scroll_area)
        layout.addWidget(button_box)
        layout.addWidget(self.wait_spinner)

        self.setWindowTitle("Save Version")
        self.setLayout(layout)
        self.setMinimumWidth(int(0.9 * width))

    def _set_diff_text(self, diff):
        parser = DiffParser(self.task)
        self.version_diff_label.setText(parser.parse(diff))

        if self.wait_spinner.isSpinning():
            self.wait_spinner.stop()

    def get_message(self):
        ok = self.exec_()
        return self.message_edit.toPlainText(), ok
