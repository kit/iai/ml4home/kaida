from PyQt5.QtCore import QObject, pyqtSignal

from DLIP.data.version_control.dvc_wrapper.dvc_project import DvcProject
from DLIP.data.version_control.dvc_wrapper.git_wrapper import GitWrapper


class DvcInitializationWorker(QObject):
    finished = pyqtSignal()
    data = pyqtSignal(DvcProject)

    def __init__(self, server_path, local_dir2track_lst):
        super(DvcInitializationWorker, self).__init__()
        self.server_path = server_path
        self.local_dir2track_lst = local_dir2track_lst

    def run(self):
        dvc_project = DvcProject(
            self.server_path,
            self.local_dir2track_lst,
            do_init=not DvcProject.is_already_initialized(self.server_path, self.local_dir2track_lst),
            use_git=GitWrapper.is_available()
        )

        self.data.emit(dvc_project)
        self.finished.emit()
