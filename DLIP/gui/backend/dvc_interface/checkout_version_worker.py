from PyQt5.QtCore import QObject, pyqtSignal


class CheckoutVersionWorker(QObject):
    finished = pyqtSignal()
    checkout_info = pyqtSignal(str)

    def __init__(self, dvc_project, version):
        super(CheckoutVersionWorker, self).__init__()
        self.dvc_project = dvc_project
        self.version = version

    def run(self):
        try:
            self.dvc_project.checkout_version(self.version)
            self.checkout_info.emit("")
        except ValueError as err:
            self.checkout_info.emit(str(err.args[0]))

        self.finished.emit()
