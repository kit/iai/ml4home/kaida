from PyQt5.QtCore import QThread
from PyQt5.QtCore import pyqtSignal, QObject
from PyQt5.QtWidgets import QMessageBox

from DLIP.data.version_control.dvc_wrapper.dvc_project import DvcProject
from DLIP.gui.backend.dvc_interface.checkout_version_worker import CheckoutVersionWorker
from DLIP.gui.backend.dvc_interface.dvc_initialization_worker import DvcInitializationWorker
from DLIP.gui.backend.dvc_interface.save_version_dialog import SaveVersionDialog
from DLIP.gui.backend.dvc_interface.save_version_worker import SaveVersionWorker
from DLIP.gui.backend.dvc_interface.select_version_dialog import SelectVersionDialog


class DvcInterface(QObject):
    initialized = pyqtSignal()

    def __init__(
        self,
        server_path: str,
        local_dir2track_lst: list,
        par: "BaseAssistedLabelingGui"
    ):
        super(DvcInterface, self).__init__()

        self.par = par
        self.dvc_project = None

        self.dvc_initialization_worker = None
        self.datamodule_init_thread = None

        self.save_version_thread = None
        self.save_version_worker = None

        self.checkout_version_thread = None
        self.checkout_version_worker = None

        self.initialized.connect(self.par.dvc_initialized)

        self._start_dvc_project_initialization(server_path, local_dir2track_lst)

    def _start_dvc_project_initialization(self, server_path, local_dir2track_lst):
        self.dvc_initialization_worker = DvcInitializationWorker(server_path, local_dir2track_lst)
        self.dvc_initialization_thread = QThread()

        self.dvc_initialization_worker.moveToThread(self.dvc_initialization_thread)

        self.dvc_initialization_thread.started.connect(self.dvc_initialization_worker.run)
        self.dvc_initialization_worker.finished.connect(self.dvc_initialization_thread.quit)
        self.dvc_initialization_worker.data.connect(self.set_dvc_project)

        self.dvc_initialization_thread.start()

    def set_dvc_project(self, dvc_project: DvcProject):
        self.dvc_project = dvc_project
        self.initialized.emit()

    def check_for_newer_versions(self):
        if 0 <= self.dvc_project.last_version < self.dvc_project.get_newest_remote_version():
            reply = QMessageBox.question(self.par, "Checkout new version",
                                         "Remote is a newer version of the data. "
                                         "Do you want to checkout the newer version?")

            if reply == QMessageBox.Yes:
                self._checkout_version(str(self.dvc_project.get_newest_remote_version()))

    def load_version(self):
        versions = self.dvc_project.get_versions_with_info()

        if len(versions.keys()) == 0:
            msg = QMessageBox()
            msg.setText("There are no versions so far")
            msg.exec_()
            return

        version, ok = SelectVersionDialog(self.par, versions, task=self.par.project["task"]).get_version()

        if ok:
            self._checkout_version(version)

    def save_version(self):
        self.par.spinner_widget.start()
        text, ok = SaveVersionDialog(self.par, self.par.project["task"], dvc_project=self.dvc_project).get_message()

        if ok:
            self.par.statusBar().showMessage("Committing version...")

            self.save_version_thread = QThread()
            self.save_version_worker = SaveVersionWorker(self.dvc_project, text, self.par.project)
            self.save_version_worker.moveToThread(self.save_version_thread)

            self.save_version_thread.started.connect(self.save_version_worker.run)
            self.save_version_worker.finished.connect(self.save_version_thread.quit)
            self.save_version_worker.commit_info.connect(self._version_saved)

            self.save_version_thread.start()
        else:
            self.par.spinner_widget.stop()

    def _version_saved(self, commit_info: str):
        if commit_info == "":
            self.par.statusBar().showMessage("Version committed")
        else:
            self._show_warning(commit_info)
            self.par.statusBar().showMessage("")

        self.par.spinner_widget.stop()

    def _checkout_version(self, version: str):
        self.par.statusBar().showMessage("Loading version...")
        self.par.spinner_widget.start()

        self.checkout_version_thread = QThread()
        self.checkout_version_worker = CheckoutVersionWorker(self.dvc_project, version)
        self.checkout_version_worker.moveToThread(self.checkout_version_thread)

        self.checkout_version_thread.started.connect(self.checkout_version_worker.run)
        self.checkout_version_worker.finished.connect(self.checkout_version_thread.quit)
        self.checkout_version_worker.checkout_info.connect(self._checkout_finished)

        self.checkout_version_thread.start()

    def _checkout_finished(self, checkout_info: str):
        if checkout_info == "":
            # update parent window now that the file structure may have changed
            self.par.reload()

            self.par.statusBar().showMessage("Version loaded")
        else:
            self._show_warning(checkout_info)
            self.par.statusBar().showMessage("")

        self.par.spinner_widget.stop()

    def _show_warning(self, warning):
        dlg = QMessageBox(self.par)
        dlg.setWindowTitle("Error")
        dlg.setText(warning)
        dlg.exec()
