from PyQt5 import QtCore, QtGui, QtWidgets, uic
import os
from PIL import Image
import shutil

class ActiveLearningGui(QtWidgets.QDialog):
    def __init__(self, project_root,project):
        super(ActiveLearningGui,self).__init__()
        uic.loadUi(os.path.expandvars(f"{project_root}/gui/layouts/gui_active_learning.ui"), self)
        self.project        = project
        self.project_root   = project_root
        self.ls_img_num_page   = list()
        self.curr_page       = 1

        # init gui
        # get image list
        self.img_lst = self.find_high_discrepancy()

        # gets image indexs per page
        self.num_image       = len(self.img_lst)
        self.max_page=int(self.num_image / 12) + (self.num_image % 12 > 0)

        for i in range(1,self.max_page):
            self.ls_img_num_page.append(range((i-1)*12,i*12))

        self.ls_img_num_page.append(range((self.max_page-1)*12,self.num_image))

        # creates graphic view and scene
        self.ls_graphic=list()
        self.ls_scene=list()
        for i in range(1,13):
            item_name='graphicsView_'+str(i)
            self.ls_graphic.append(self.findChild(QtWidgets.QGraphicsView,item_name))
            self.ls_scene.append(QtWidgets.QGraphicsScene(self))

        # init next and previous button
        self.btn_next_page=self.findChild(QtWidgets.QPushButton,'next_btn')
        self.btn_next_page.clicked.connect(lambda: self.change_page(1))
        self.btn_prev_page=self.findChild(QtWidgets.QPushButton,'prev_btn')
        self.btn_prev_page.clicked.connect(lambda: self.change_page(-1))
        
        # init image presentation
        self.update_gui()
        self.show()
        
        
    def change_page(self,direction):
        if self.curr_page+direction < 1 or self.curr_page+direction > self.max_page:
            return
        else:
            self.curr_page= self.curr_page+direction
        self.update_gui()
        

    def update_gui(self):

        for i in range(0,12):
            self.ls_scene[i].clear()
            self.ls_scene[i]=QtWidgets.QGraphicsScene(self)

        curr_position=self.ls_img_num_page[self.curr_page-1]
        self.curr_page_lbl.setText(str(self.curr_page)+'/'+str(self.max_page))
            
        for i in range(0,len(curr_position)):
            img_path=self.img_lst[curr_position[i]]
            im=Image.open(img_path)
            w,h=im.size
            pix = QtGui.QPixmap(img_path)
            item = QtWidgets.QGraphicsPixmapItem(pix)
            self.ls_scene[i].addItem(item)
            self.ls_graphic[i].setScene(self.ls_scene[i])
            self.ls_graphic[i].fitInView(QtCore.QRectF(0, 0, w, h),QtCore.Qt.KeepAspectRatio)
            self.ls_scene[i].update()

    def find_high_discrepancy(self):
        ...
        img_ls = list()
        return img_ls