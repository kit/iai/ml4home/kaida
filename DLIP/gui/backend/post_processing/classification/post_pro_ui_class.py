from PyQt5 import QtCore, QtGui, uic, QtWidgets
import numpy as np

import matplotlib as plt
plt.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.figure import Figure

from DLIP.gui.backend.post_processing.post_pro_ui_base import PostProcessingBaseGui
from DLIP.utils.helper_functions.create_qimage import array_to_qimage



class MplCanvas(FigureCanvasQTAgg):

    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.ax = fig.add_subplot(1,1,1)
        super(MplCanvas, self).__init__(fig)

class PostProcessingClassGui(PostProcessingBaseGui):
    def __init__(self, path_project,project):
        super(PostProcessingClassGui,self).__init__(path_project, project)
        uic.loadUi(f"{path_project}/gui/layouts/gui_post_pro_class.ui", self)

        self.fig_anno = None
        self.fig_post_pro_anno = None

        self.fig_anno = MplCanvas(self, width=2, height=2, dpi=100)
        self.fig_post_pro_anno = MplCanvas(self, width=2, height=2, dpi=100)
        
        self.layout_anno = QtWidgets.QVBoxLayout()
        self.layout_anno.addWidget(self.fig_anno)
        self.annotation.setLayout(self.layout_anno) 

        self.layout_post_pro_anno = QtWidgets.QVBoxLayout()
        self.layout_post_pro_anno.addWidget(self.fig_post_pro_anno)
        self.annotation_post_pro.setLayout(self.layout_post_pro_anno) 


    def compare_labels(self,label, label_post_pro, img):
        self.accept_processing = False
        self.img_raw = array_to_qimage(np.uint8(img/65535.0*255.0) if img.dtype == np.uint16 else img)
        self.image.setPixmap(QtGui.QPixmap.fromImage(
            self.img_raw.scaled(0.30*self.frameGeometry().width(),0.95*self.frameGeometry().height(), 
            aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
            )

        # init histogram figure canvas
        self.create_class_histogram(label,self.fig_anno.ax)
        self.create_class_histogram(label_post_pro,self.fig_post_pro_anno.ax)

        self.fig_anno.draw()
        self.fig_post_pro_anno.draw()

        self.exec()

    def resize_fcn(self, event):
        if self.img_raw is not None:
            self.image.setPixmap(QtGui.QPixmap.fromImage(
                self.img_raw.scaled(0.30*self.frameGeometry().width(),0.95*self.frameGeometry().height(), 
                aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
                )

    def create_class_histogram(self, label, ax):
        ax.clear()
        ax.bar(self.project["class_names"], label.ravel(), color='steelblue')
        ax.set(frame_on=False) 
        ax.set_xticklabels(self.project["class_names"], rotation=45, ha='right')
        ax.set_ylabel("Class Probability")