from PyQt5 import QtCore, QtGui, QtWidgets
import numpy as np
from skimage.color import label2rgb

from DLIP.utils.helper_functions.create_qimage import array_to_qimage

class PostProcessingBaseGui(QtWidgets.QDialog):
    def __init__(self, path_project,project):
        super(PostProcessingBaseGui,self).__init__()
        self.resizeEvent = self.resize_fcn
        self.project = project
        self.path_project=path_project
        self.accept_processing = False
        self.label_img_overlay_raw          = None
        self.label_post_pro_img_overlay_raw = None

    def keep(self):
        self.accept_processing = False
        self.close()
    
    def accept_prediction(self):
        self.accept_processing = True
        self.close()

    def compare_labels(self,label, label_post_pro, img):
        self.accept_processing = False
        #cmap = discrete_cmap(2 if self.project["n_classes"]==1 else self.project["n_classes"],"gray")
        self.label_img_overlay_raw = array_to_qimage(np.uint8(label2rgb(label, image=img, bg_label=0)*255))
        self.label_post_pro_img_overlay_raw = array_to_qimage(np.uint8(label2rgb(label_post_pro, image=img, bg_label=0)*255))
        self.original_img_lbl.setPixmap(QtGui.QPixmap.fromImage(
            self.label_img_overlay_raw.scaled(0.48*self.frameGeometry().width(),0.95*self.frameGeometry().height(), 
            aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
            )
        self.processed_img_lbl.setPixmap(QtGui.QPixmap.fromImage(
            self.label_post_pro_img_overlay_raw.scaled(0.48*self.frameGeometry().width(),0.95*self.frameGeometry().height(), 
            aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
            )
        self.exec()

    def resize_fcn(self, event):
        if self.label_img_overlay_raw is not None:
            self.original_img_lbl.setPixmap(QtGui.QPixmap.fromImage(
                self.label_img_overlay_raw.scaled(0.48*self.frameGeometry().width(),0.95*self.frameGeometry().height(), 
                aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
                )
            self.processed_img_lbl.setPixmap(QtGui.QPixmap.fromImage(
                self.label_post_pro_img_overlay_raw.scaled(0.48*self.frameGeometry().width(),0.95*self.frameGeometry().height(), 
                aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
                )