from PyQt5 import QtCore, QtGui, uic
import numpy as np
from skimage.color import label2rgb

from DLIP.gui.backend.post_processing.post_pro_ui_base import PostProcessingBaseGui
from DLIP.utils.helper_functions.create_qimage import array_to_qimage

class PostProcessingMaskGui(PostProcessingBaseGui):
    def __init__(self, path_project,project):
        super(PostProcessingMaskGui,self).__init__(path_project, project)
        uic.loadUi(f"{path_project}/gui/layouts/gui_post_pro_mask.ui", self)

    def compare_labels(self,label, label_post_pro, img):
        self.accept_processing = False
        self.label_img_overlay_raw = array_to_qimage(np.uint8(label2rgb(label, image=img, bg_label=0)*255))
        self.label_post_pro_img_overlay_raw = array_to_qimage(np.uint8(label2rgb(label_post_pro, image=img, bg_label=0)*255))
        self.original_img_lbl.setPixmap(QtGui.QPixmap.fromImage(
            self.label_img_overlay_raw.scaled(0.48*self.frameGeometry().width(),0.95*self.frameGeometry().height(), 
            aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
            )
        self.processed_img_lbl.setPixmap(QtGui.QPixmap.fromImage(
            self.label_post_pro_img_overlay_raw.scaled(0.48*self.frameGeometry().width(),0.95*self.frameGeometry().height(), 
            aspectRatioMode=QtCore.Qt.KeepAspectRatio, transformMode=QtCore.Qt.SmoothTransformation))
            )
        self.exec()