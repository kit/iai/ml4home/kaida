import json
import os
from pathlib import Path

from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import QThread

from DLIP.gui.backend.create_project import CreateProjectGui
from DLIP.gui.backend.load_external_project import LoadExternalProjectDialog
from DLIP.gui.backend.plugins import PluginGui
from DLIP.utils.loading.load_class import load_class
from .initial_ui.init_datamodule import DataModuleInitializer


class initialUi(QtWidgets.QMainWindow):
    def __init__(
        self,
        path_project,
        path_seg_annotation_tool,
        task_properties,
        parent=None
    ):
        super(initialUi, self).__init__()
        uic.loadUi(f"{path_project}/gui/layouts/gui_initial.ui", self)
        self.actionCreate_project = self.findChild(QtWidgets.QAction, "actionCreate_project")
        self.actionLoad_project = self.findChild(QtWidgets.QAction, "actionLoad_project")
        self.create_project_action.triggered.connect(self.create_project)
        self.create_project_remote_action.triggered.connect(self.load_project_from_remote)
        self.action_load.triggered.connect(self.load_project)

        self.recent_projects_file = f"{path_project}/data/recent_projects/recent_projects.json"
        self.recent_projects_names = list()

        self.recent_projects = None
        self._init_recent_project()

        self.path_project = path_project
        self.task_properties = task_properties
        self.path_seg_annotation_tool = path_seg_annotation_tool

        self.project = None
        self.data_module = None

        self.assisted_labeling_gui = None

        self.datamodule_init_thread = None
        self.datamodule_init_worker = None

        self.open_gui = True
        self.btn_open_current_dataset = self.findChild(QtWidgets.QPushButton, "btn_open_current_dataset")
        #only visible if a project is loaded
        self.btn_open_current_dataset.setVisible(False)
        self.btn_open_current_dataset.clicked.connect(self.open_current_dataset)

    def _init_recent_project(self):
        recent_project_file_path = Path(self.recent_projects_file)
        if recent_project_file_path.exists():
            with open(self.recent_projects_file, "r") as f:
                self.recent_projects = json.load(f)
        else:
            if not recent_project_file_path.parent.exists():
                recent_project_file_path.parent.mkdir()
            self.recent_projects = {"names": [], "paths": {}}

        self.recent_projects_names = self.recent_projects["names"]
        self.recent_projects_paths = self.recent_projects["paths"]
        self.recent_project_submenus = dict()

        del_ind = list()
        for i in range(len(self.recent_projects_names)):
            prjct = self.recent_projects_names[i]
            prjct_path = self.recent_projects_paths[prjct]
            if os.path.exists(prjct_path) and os.access(prjct_path, os.W_OK):
                self.recent_project_submenus[prjct] = (self.menu_load_project.addAction(prjct))
            elif not os.path.exists(prjct_path):
                del_ind.append(i)
        
        for menu in self.recent_project_submenus:
            self.recent_project_submenus[menu].triggered.connect(
                lambda _, m=self.recent_projects_paths[menu]: self.load_project(m)
            )

        for i in sorted(del_ind, reverse=True):
            prjct = self.recent_projects_names[i]
            del self.recent_projects_names[i]
            del self.recent_projects_paths[prjct]

    def save_recent_projects(self):
        if self.project["name"] not in self.recent_projects_names:
            if len(self.recent_projects_names) == 5:
                del self.recent_projects_paths[self.recent_projects_names[4]]
            self.recent_projects_names = [self.project["name"]] + self.recent_projects_names[0:3]
            self.recent_projects["paths"][self.project["name"]] = self.project["prj_file_name"]
            self.recent_projects = dict()
            self.recent_projects["names"] = self.recent_projects_names
            self.recent_projects["paths"] = self.recent_projects_paths
            json.dump(self.recent_projects, open(self.recent_projects_file, "w"))
        else:
            self.recent_projects_names.remove(self.project["name"])
            self.recent_projects_names = [self.project["name"]] + self.recent_projects_names[0:3]
            self.recent_projects["names"] = self.recent_projects_names
            json.dump(self.recent_projects, open(self.recent_projects_file, "w"))

    def open_plugins(self):
        self.open_gui = False
        self.open_labeling_tool()
        self.open_gui = True
        app_gui = PluginGui(self.path_project, self.project, self)
        app_gui.exec_()
    
    def create_project(self):
        self.spinner_widget.start()

        create_project_gui = CreateProjectGui(self.path_project, self.task_properties)
        ok = create_project_gui.exec_()

        if not ok:
            self.spinner_widget.stop()
            return

        self.project = create_project_gui.get_output()
        self._init_datamodule()

    def load_project(self, project_file_name=False):
        self.spinner_widget.start()
        self.repaint()

        if project_file_name is False:
            project_file_name = str(QtWidgets.QFileDialog.getOpenFileName(self, "Select Project File")[0])

        if not os.path.exists(project_file_name):
            project_file_name = None

        if project_file_name is not None:
            with open(project_file_name, 'r') as f:
                self.project = json.load(f)

            self.project["prj_file_name"] = project_file_name
            self._init_datamodule()
        else:
            self.statusBar().showMessage('No project was selected')
            self.repaint()
            self.spinner_widget.stop()

    def load_project_from_remote(self):
        self.spinner_widget.start()
        load_external_project_dialog = LoadExternalProjectDialog(self)

        load_external_project_dialog.exec_()

        self.project = load_external_project_dialog.get_output()
        self._init_datamodule()

    def open_labeling_tool(self,):
        self.spinner_widget.start()
        self.statusBar().showMessage("Opening labeling tool...")
        self.repaint()
        # print(f"self.data_module {self.data_module}")
        if self.project is not None:
            task_property = self.task_properties[self.project["task"]]
            assisted_labeling_gui_class = load_class(
                f"DLIP.gui.backend.label_assistant.{task_property.folder_name}",
                task_property.gui_name
            )
            self.assisted_labeling_gui = assisted_labeling_gui_class(
                self,
                self.project,
                self.path_project,
                self.data_module,
                task_property,
                self.path_seg_annotation_tool,
                self.open_gui
            )
        else:
            self.statusBar().showMessage("Load or create a project first.")
            self.repaint()
            self.spinner_widget.stop()

    def _init_datamodule(self):
        if self.project is None:
            self.statusBar().showMessage("Load or create a project first.")
            self.repaint()
            return

        self.datamodule_init_thread = QThread()
        self.datamodule_init_worker = DataModuleInitializer(self.project, self.task_properties)
        self.datamodule_init_worker.moveToThread(self.datamodule_init_thread)

        self.datamodule_init_thread.started.connect(self.datamodule_init_worker.run)
        self.datamodule_init_worker.finished.connect(self.datamodule_init_thread.quit)
        self.datamodule_init_worker.data.connect(self.finished_data_module_initialization)
        self.datamodule_init_worker.info.connect(lambda text: self.statusBar().showMessage(text))

        self.datamodule_init_thread.start()

    def finished_data_module_initialization(self, project, task_properties, data_module):
        self.project = project
        self.task_properties = task_properties
        self.data_module = data_module

        if self.project is None:
            self.statusBar().showMessage("No project to load!")
            self.repaint()
            return

        self.statusBar().showMessage('Project: ' + self.project["name"])
        self.repaint()
        self.btn_open_current_dataset.setVisible(True)

        self.save_recent_projects()
        self.spinner_widget.stop()

    def open_current_dataset(self):
        #open the window with the current dataset
        dir = QtWidgets.QFileDialog.getOpenFileName(
            self, 'Open file', self.project["prj_file_name"], "Image files (*.jpg *.png *.tif *.tiff)")
