from PyQt5 import QtCore, QtGui, QtWidgets, uic
import os

class TrainGui(QtWidgets.QDialog):
    def __init__(self, path_project,project):
        super(TrainGui,self).__init__()
        uic.loadUi(f"{path_project}/gui/layouts/gui_train.ui", self)
        self.project=project
        self.path_project=path_project

    def get_config_file(self):
        self.config_file_path=str(QtWidgets.QFileDialog.getOpenFileUrl(self,"Select config file"))
        self.config_lbl.setText(self.config_file_path)
        self.config_lbl.adjustSize()
    
    def choose_output_folder(self):
        self.output_folder=str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select Directory"))
        self.output_lbl.setText(self.output_folder)
        self.output_lbl.adjustSize()
    
    def start_train(self):
        ...