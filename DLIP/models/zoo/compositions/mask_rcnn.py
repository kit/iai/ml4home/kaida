import torch
import numpy as np
import torchvision
from DLIP.models.zoo.compositions.base_composition import BaseComposition
from DLIP.utils.metrics.inst_seg_metrics import get_fast_aji_plus, remap_label
from DLIP.utils.data_preparation.label_pre_processing.mask_r_cnn.mask2boxes import masks_to_boxes

class MaskRCnn(BaseComposition):
    def __init__(
        self, 
        num_classes=2,
        min_mask_size_threshold=0, 
        mask_confidence_threshold=0.5,
        object_score_threshold=0.8,
        iou_overlap_threshold=0.41,
        *args, 
        **kwargs
    ):
        super().__init__()
        self.detector = torchvision.models.detection.maskrcnn_resnet50_fpn(
            num_classes=num_classes,
            pretrained=False, 
            image_mean=[0,0,0], # disable norm
            image_std=[1,1,1],
        )
        self.min_mask_size_threshold    = min_mask_size_threshold
        self.mask_confidence_threshold  = mask_confidence_threshold
        self.object_score_threshold     = object_score_threshold
        self.iou_overlap_threshold      = iou_overlap_threshold

    def forward(self, x,targets=None):
        return self.detector(x,targets)

    def training_step(self, batch, batch_idx):
        x, y_true   = batch
        y_true      = y_true.squeeze(-1)

        targets     = self._y_true_to_targets(y_true)

        preds       = self.forward(x,targets)

        loss        = sum(loss for loss in preds.values())

        for k, v in preds.items():
            self.log(f"train/{k}", v, prog_bar=False)

        self.log("train/loss", loss, prog_bar=True)
        return loss
    
    def validation_step(self, batch, batch_idx):
        x, y_true = batch
        y_true = y_true.squeeze(-1)

        targets     = self._y_true_to_targets(y_true)
       
        preds       = self.forward(x,targets)

        pred_masks = torch.stack([self._post_pro_prediction(pred) for pred in preds])

        val_metric = np.mean(
            [
                get_fast_aji_plus(remap_label(y_true[i].detach().cpu().numpy()),
                remap_label(pred_masks[i].detach().cpu().numpy())) for i in range(len(y_true))
            ]
        )


        val_metric = np.mean([get_fast_aji_plus(remap_label(y_true[i].detach().cpu().numpy()),remap_label(pred_masks[i].detach().cpu().numpy())) for i in range(len(y_true))])

        if np.isnan(val_metric):
            val_metric = 0
                
        self.log("val/aji", val_metric, prog_bar=True, on_epoch=True,on_step=False)
        self.log("val/loss", 1-val_metric, prog_bar=False, on_epoch=True,on_step=False)
        return 1-val_metric


    def test_step(self, batch, batch_idx):
        x, y_true   = batch
        y_true      = y_true.squeeze(-1)

        targets     = self._y_true_to_targets(y_true)

        preds       = self.forward(x,targets)

        pred_masks = torch.stack([self._post_pro_prediction(pred) for pred in preds])

        ajis = np.mean(
            [
                get_fast_aji_plus(remap_label(y_true[i].detach().cpu().numpy()),
                remap_label(pred_masks[i].detach().cpu().numpy())) for i in range(len(y_true))
            ]
        )

        self.log("test/aji", ajis, prog_bar=True, on_epoch=True,on_step=False)
        self.log("test/loss", 1-ajis, prog_bar=True, on_epoch=True,on_step=False)
        return 1-ajis

    def _y_true_to_targets(self,y_true):
        targets = []
        for k in range(len(y_true)):
            if torch.sum(y_true[k]) == 0 or len([(y_true[k]==i)*1 for i in range(1,int(torch.max(y_true[k])+1)) if torch.sum((y_true[k]==i)*1) > self.min_mask_size_threshold]) == 0:
                # negative sample
                masks = torch.zeros(0, 100, 100, dtype=torch.uint8)
                boxes = torch.zeros((0, 4), dtype=torch.float32)
                labels = torch.zeros(0, dtype=torch.int64)
            else:
                masks = torch.stack([((y_true[k]==i)*1).to(torch.uint8) for i in range(1,int(torch.max(y_true[k])+1)) if torch.sum((y_true[k]==i)*1) > self.min_mask_size_threshold])
                boxes =  masks_to_boxes(masks)
                labels = torch.ones((len(masks)))

            targets.append({
                'boxes': boxes.cuda(),
                'labels': labels.cuda().to(torch.int64),
                'masks': masks.cuda(),
            })
        
        return targets

    def _post_pro_prediction(self,prediction):
        scores  = prediction['scores']
        masks   = prediction['masks']   

        final_mask  = torch.zeros(masks.shape[2:], dtype=torch.float32).cuda()

        masks   = masks[(scores > self.object_score_threshold )]
        scores = scores[(scores > self.object_score_threshold )]

        local_instance_number = 1
        for i in range(len(masks)):
            for j in range(i+1, len(masks)):
                intersection = torch.logical_and(masks[i], masks[j])
                union = torch.logical_or(masks[i], masks[j])
                iou_score = torch.sum(intersection) / torch.sum(union)
                if iou_score > self.iou_overlap_threshold:
                    scores[j] = 0    

        for mask, score in zip(masks, scores):
            # as scores is already sorted        
            if score == 0:
                continue
            mask = mask.squeeze()
            mask[mask >= self.mask_confidence_threshold] = local_instance_number        
            mask[mask < self.mask_confidence_threshold] = 0
            if torch.sum(mask) > 0:
                local_instance_number += 1       
                temp_filter_mask = torch.where(final_mask > 1, 0., 1.).cuda()
                mask = mask * temp_filter_mask        
                final_mask += mask

        return final_mask.to(torch.int16)