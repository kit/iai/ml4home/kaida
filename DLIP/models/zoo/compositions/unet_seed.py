from typing import List
import torch
import torch.nn as nn
from DLIP.models.zoo.compositions.unet_base import UnetBase
import wandb
from DLIP.utils.metrics.seed_metrics import create_gt_area, metric_scores
from DLIP.utils.data_preparation.label_pre_processing.centroid_post_pro import centroid_postprocessing

class UnetSeed(UnetBase):
    def __init__(
        self,
        in_channels: int,
        loss_fcn: nn.Module,
        encoder_type = 'unet',
        encoder_filters: List = [64, 128, 256, 512, 1024],
        decoder_filters: List = [512, 256, 128, 64],
        decoder_type = 'unet',
        dropout_encoder: float = 0.0,
        dropout_decoder: float = 0.0,
        ae_mode = False,
        pretraining_weights = 'imagenet',
        encoder_frozen=False,
        **kwargs,
    ):
        num_classes = 1
        out_channels = num_classes
        super().__init__(
                in_channels,
                out_channels,
                loss_fcn,
                encoder_type,
                encoder_filters,
                decoder_filters,
                decoder_type,
                dropout_encoder,
                dropout_decoder,
                ae_mode,
                pretraining_weights,
                encoder_frozen,
                **kwargs)
        
        self.append(nn.Sigmoid())

        
    def training_step(self, batch, batch_idx):
        x, y_true   = batch
        y_true      = y_true.permute(0, 3, 1, 2)
        y_pred      = self.forward(x)
        loss_n_c    = self.loss_fcn(y_pred, y_true)
        loss        = torch.mean(loss_n_c)
        self.log("train/loss", loss, prog_bar=True)
        return loss

    def predict_seeds(self,x):
        y_pred = self.forward(x).squeeze(1)
        y_pred_seed = torch.zeros_like(y_pred)
        
        for ib in range(x.shape[0]):
            y_pred_seed[ib,:] = torch.from_numpy(centroid_postprocessing(y_pred[ib,:].detach().cpu().numpy(), do_binarize=True))
        return y_pred_seed

    def validation_step(self, batch, batch_idx):
        x, y_true = batch
        y_true = y_true.permute(0, 3, 1, 2)

        y_pred_seed = self.predict_seeds(x)

        score = 0
        for ib in range(x.shape[0]):
            y_true_seed_ib = y_true[ib,0,:].detach().cpu().numpy()
            score+=metric_scores(y_pred_seed[ib].cpu().numpy(), create_gt_area(y_true_seed_ib))

        loss = 1-score/x.shape[0]
        self.log("val/loss", loss, prog_bar=True, on_epoch=True)
        return  loss

    def test_step(self, batch, batch_idx):
        x, y_true = batch
        y_true = y_true.permute(0, 3, 1, 2)
        y_pred_seed = self.predict_seeds(x)

        score = 0
        for ib in range(x.shape[0]):
            y_true_seed_ib = y_true[ib,0,:].detach().cpu().numpy()
            score+=metric_scores(y_pred_seed[ib].cpu().numpy(), create_gt_area(y_true_seed_ib))

        score = score/x.shape[0]
        self.log("test/score", score, prog_bar=True, on_epoch=True, on_step=False)
        return score