from typing import List, Tuple
import logging

import numpy as np
import torch
import torch.nn as nn
import wandb

from DLIP.models.zoo.compositions.base_composition import BaseComposition
from DLIP.models.zoo.decoder.unet_decoder import UnetDecoder
from DLIP.models.zoo.encoder.resnet_encoder import ResNetEncoder
from DLIP.models.zoo.encoder.unet_encoder import UnetEncoder
from DLIP.objectives import WeightedDiceLoss
from DLIP.utils.data_preparation.label_pre_processing.generate_distance_maps import DistanceMapGenerator
from DLIP.utils.loading.split_parameters import split_parameters
from DLIP.utils.metrics.inst_seg_metrics import get_fast_aji_plus, remap_label
from DLIP.utils.metrics.panoptic_seg_metrics import confusion_matrix
from DLIP.utils.post_processing.panoptic_post_processor import PanopticPostProcessor


class UnetPanoptic(BaseComposition):
    def __init__(
        self,
        in_channels: int,
        num_classes: int,
        lambda_smooth_l1: float ,
        dice_loss_class_weights: List[float] = [],
        encoder_type: str = "unet",
        encoder_filters: List[int] = [64, 128, 256, 512, 1024],
        decoder_instance_filters: List[int] = [512, 256, 128, 64],
        decoder_instance_type: str = "unet",
        decoder_semantic_filters: List[int] = [512, 256, 128, 64],
        decoder_semantic_type: str = "unet",
        dropout_encoder: float = 0.0,
        dropout_decoder_instance: float = 0.0,
        dropout_decoder_semantic: float = 0.0,
        pretraining_weights: str = "imagenet",
        encoder_frozen: bool = False,
        **kwargs,
    ):
        super().__init__()

        if len(dice_loss_class_weights)==0:
            logging.warning("No class weights for dice loss specified. Using uniform weights.")
            self.loss_function_semantic = WeightedDiceLoss([1.0 for _ in range(num_classes)])
        else:
            self.loss_function_semantic = WeightedDiceLoss(dice_loss_class_weights)
            
        self.loss_function_instance = nn.SmoothL1Loss()
        self.lambda_smooth_l1 = lambda_smooth_l1

        self.composition = None
        self.append = lambda _: print("WARNING: UnetPanoptic does not support append function!")

        if encoder_type.lower() == "unet":
            self.encoder = UnetEncoder(
                input_channels=in_channels,
                encoder_filters=encoder_filters,
                dropout=dropout_encoder,
                bilinear=False,
            )
        elif "resnet" in encoder_type.lower():
            self.encoder = ResNetEncoder(
                input_channels=in_channels,
                encoder_type=encoder_type,
                pretraining_weights=pretraining_weights,
                encoder_frozen=encoder_frozen,
            )
        else:
            raise ValueError(f"Encoder type {encoder_type} not supported!")

        if decoder_semantic_type.lower() == "unet":
            self.decoder_semantic = nn.Sequential(
                UnetDecoder(
                    n_classes=num_classes,
                    encoder_filters=self.encoder.get_skip_channels(),
                    decoder_filters=decoder_semantic_filters,
                    dropout=dropout_decoder_semantic,
                    billinear_downsampling_used=False,
                    ae_mode=False,
                ),
                nn.Sigmoid() if num_classes == 1 else nn.Softmax(dim=1)
            )
        else:
            raise ValueError(f"Semantic decoder type '{decoder_semantic_type}' not supported!")

        if decoder_instance_type.lower() == "unet":
            self.decoder_instance = UnetDecoder(
                n_classes=1,
                encoder_filters=self.encoder.get_skip_channels(),
                decoder_filters=decoder_instance_filters,
                dropout=dropout_decoder_instance,
                billinear_downsampling_used=False,
                ae_mode=False,
            )
        else:
            raise ValueError(f"Semantic decoder type '{decoder_instance_type}' not supported!")

        self.confusion_matrices = list()

        self.num_classes = num_classes
        self.postprocessor = PanopticPostProcessor(**split_parameters(kwargs, ["post_pro"])["post_pro"])

    @staticmethod
    def get_class_weightings(train_dataset):
        num_classes = train_dataset[0][2].shape[2]
        class_sums = torch.zeros((num_classes,))

        for (_, _, y_true_sem) in train_dataset:
            class_sums += torch.sum(y_true_sem, dim=(0, 1))

        weightings = torch.zeros_like(class_sums)
        weightings[1:] = (2 - class_sums[1:] / torch.max(class_sums[1:]))**4
        weightings[0] = 1

        return weightings

    def predict_single_image(self, img: np.ndarray) -> np.ndarray:
        assert len(img.shape) == 3

        if not img.shape[0] == 3:
            if img.shape[2] != 3:
                raise ValueError(f"Expected image with 3 channels, but got image of shape {img.shape}")
            img = img.transpose((2, 0, 1))

        with torch.no_grad():
            x = torch.tensor(img).to(self.device)
            y_pred_instance, y_pred_semantic = self.forward(x[None, ...])

        return self.postprocessor.process(
            img,
            y_pred_instance[0].detach().cpu().numpy(),
            y_pred_semantic[0].detach().cpu().numpy()
        )

    def forward(self, x: torch.Tensor) -> Tuple[torch.Tensor, torch.Tensor]:
        x = self.encoder.forward(x)
        y_pred_instance = self.decoder_instance.forward(x)
        y_pred_semantic = self.decoder_semantic.forward(x)
        return y_pred_instance, y_pred_semantic

    def _create_dist_map(self, y_true_inst: torch.Tensor):
        y_true_inst = y_true_inst.detach().cpu().numpy().astype(float)

        for channel_idx in range(y_true_inst.shape[0]):
            y_true_inst[channel_idx] = DistanceMapGenerator.apply_dist_transform(
                y_true_inst[channel_idx].astype(np.uint8)
            )

        y_true_inst = y_true_inst.transpose((0, 3, 1, 2))
        return torch.tensor(y_true_inst).to(self.device)

    def _step(self, batch: Tuple[torch.Tensor, torch.Tensor, torch.Tensor]) -> torch.Tensor:
        x, y_true_inst, y_true_sem = batch

        #y_true_inst = self._create_dist_map(y_true_inst)
        y_true_inst = y_true_inst.permute((0, 3, 1, 2))
        y_true_sem = y_true_sem.transpose(1, 3).transpose(2, 3)

        y_pred_instance, y_pred_semantic = self.forward(x)

        return (
            self.lambda_smooth_l1 * self.loss_function_instance(y_pred_instance, y_true_inst)
            + 0*self.loss_function_semantic(y_pred_semantic, y_true_sem)
        )

    def training_step(self, batch: Tuple[torch.Tensor, torch.Tensor, torch.Tensor], _) -> torch.Tensor:
        loss = self._step(batch)

        if self.trainer is not None:
            self.log("train/loss", loss, prog_bar=True)

        return loss

    def validation_step(self, batch: Tuple[torch.Tensor, torch.Tensor, torch.Tensor], _) -> torch.Tensor:
        loss = self._step(batch)

        if self.trainer is not None:
            self.log("val/loss", loss, prog_bar=True, on_epoch=True)

        return loss

    def test_step(self, batch: Tuple[torch.Tensor, torch.Tensor, torch.Tensor], _) -> torch.Tensor:
        x, y_true_inst, y_true_sem = batch

        aji = list()
        acc = list()
        conf_mat = list()

        for batch_idx in range(x.shape[0]):
            y_pred_instance_i, y_pred_semantic_i = self.forward(x[batch_idx][None, ...])

            y_pred_semantic_i = y_pred_semantic_i[0].detach().cpu().numpy()
            y_true_semantic_i = y_true_sem[batch_idx].detach().cpu().numpy().transpose((2, 0, 1))

            y_pred = self.postprocessor.process(
                x[batch_idx].detach().cpu().numpy().transpose((1, 2, 0)),
                y_pred_instance_i[0].detach().cpu().numpy(),
                y_pred_semantic_i,
            )

            y_true_instance_i = y_true_inst[batch_idx].detach().cpu().numpy().squeeze()

            aji.append(get_fast_aji_plus(remap_label(y_true_instance_i), remap_label(y_pred[0])))

            acc.append(np.mean((y_pred_semantic_i > 0.5) == (y_true_semantic_i > 0)))

            conf_mat.append(confusion_matrix(
                y_pred[0],
                y_true_instance_i,
                y_pred[1],
                np.argmax(y_true_semantic_i, axis=0),
                list(range(self.num_classes)),
            ))

        if self.trainer is not None:
            self.log("test/aji+", np.mean(aji), prog_bar=True, on_epoch=True, on_step=False)
            self.log("test/acc", np.mean(acc), prog_bar=True, on_epoch=True, on_step=False)
            self.confusion_matrices.append(np.sum(conf_mat, axis=0))

        return (np.mean(aji) + np.mean(acc)) / 2

    def on_test_epoch_start(self) -> None:
        self.confusion_matrices = list()

    def on_test_epoch_end(self) -> None:
        confusion_matrix = np.sum(self.confusion_matrices, axis=0)
        wandb.log({"test confusion matrix": confusion_matrix})