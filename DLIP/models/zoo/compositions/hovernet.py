# -------------------------------------------------------------------------------
# Code adapted from: https://github.com/vqdang/hover_net
#
# MIT License
#
# Copyright (c) 2020 vqdang
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
# -------------------------------------------------------------------------------

from collections import OrderedDict
from typing import Dict

import numpy as np
import torch
import torch.nn.functional as F

from DLIP.models.zoo.compositions.hovernet_base import HoVerNetBase
from DLIP.models.zoo.hovernet.loss import HoVerNetLoss
from DLIP.models.zoo.hovernet.preprocessing import (
    cropping_center,
    gen_instance_hv_map,
)
from DLIP.utils.post_processing.hv2inst import HVMapPostProcessor


class HoVerNet(HoVerNetBase):
    def __init__(self, input_channels: int = 3, num_classes: int = None, freeze: bool = False, mode: str = "original"):
        super(HoVerNet, self).__init__(input_channels, num_classes, freeze, mode)

        self.loss_function = HoVerNetLoss(using_tp_branch=num_classes is not None)
        self.post_processor = HVMapPostProcessor(nr_types=num_classes)

    def prepare_y_true(self, y_true_inst: torch.Tensor, y_true_sem: torch.Tensor) -> Dict["str", torch.Tensor]:
        assert len(y_true_inst[0].squeeze().shape) == 2
        assert len(y_true_sem.shape) == 3 or len(y_true_sem.shape) == 4

        if self.nr_types is not None and len(y_true_sem.shape) == 4:
            assert y_true_sem.shape[3] == self.nr_types

        batch_size = y_true_inst.shape[0]

        true_hv = torch.zeros((batch_size, 80, 80, 2))

        for batch_idx in range(batch_size):
            y_true_inst_i = self.adapt_input_dims(y_true_inst[batch_idx].squeeze(), [0, 1])
            true_hv_i = torch.tensor(
                gen_instance_hv_map(y_true_inst_i.detach().cpu().numpy().astype(int), (80, 80)).astype(float)
            )
            true_hv[batch_idx, ..., 0] = cropping_center(true_hv_i[..., 0], (80, 80), batch=True)
            true_hv[batch_idx, ..., 1] = cropping_center(true_hv_i[..., 1], (80, 80), batch=True)

        # plt.imshow(true_hv[0, :, :, 0].numpy() * 255)
        # plt.show()
        # plt.imshow(true_hv[0, :, :, 1].numpy() * 255)
        # plt.show()

        y_true_sem = self.adapt_input_dims(y_true_sem, [1, 2]).type(torch.int64)

        if len(y_true_sem.shape) == 3:
            y_true_bin_seg = y_true_sem.clone()
            y_true_bin_seg[y_true_bin_seg > 0] = 1
            true_np_onehot = (
                F.one_hot(cropping_center(y_true_bin_seg, (80, 80), batch=True), num_classes=2)
            ).type(torch.float32)
        elif len(y_true_sem.shape) == 4:
            # the given semantic segmentation is already in one hot format (but not binary one hot)
            true_np_onehot = torch.zeros((batch_size, 80, 80, 2))
            y_true_sem_numpy = y_true_sem.detach().cpu().numpy()
            true_np_onehot[..., 0] = torch.tensor(cropping_center(y_true_sem_numpy[..., 0], (80, 80), batch=True))
            true_np_onehot[..., 1][true_np_onehot[..., 0] == 0] = 1
        else:
            raise ValueError(f"The given semantic y_true has the unexpected shape {y_true_sem.shape}")

        # plt.imshow(true_np_onehot[0, :, :, 0].numpy() * 255)
        # plt.show()
        # plt.imshow(true_np_onehot[0, :, :, 1].numpy() * 255)
        # plt.show()

        true_dict = {
            "np": true_np_onehot,
            "hv": true_hv,
        }

        if self.nr_types is not None:
            true_tp_onehot = torch.zeros((batch_size, 80, 80, self.nr_types))

            if len(y_true_sem.shape) == 3:
                for batch_idx in range(batch_size):
                    true_tp_onehot[batch_idx] = F.one_hot(
                        cropping_center(y_true_sem[batch_idx].type(torch.int64), (80, 80), batch=True),
                        num_classes=self.nr_types,
                    ).type(torch.float32)
            else:  # len(y_true_sem.shape) == 4
                for i in range(self.nr_types):
                    true_tp_onehot[..., i] = torch.tensor(
                        cropping_center(y_true_sem_numpy[..., i], (80, 80), batch=True)
                    )
            true_dict["tp"] = true_tp_onehot

            # plt.imshow(true_tp_onehot[0].cpu().detach().numpy())
            # plt.show()

        return true_dict

    @staticmethod
    def adapt_input_dims(x: torch.Tensor, dims: list, desired_size: int = 270) -> torch.Tensor:
        num_of_dims = len(x.shape)

        for dim in dims:
            given_size = x.shape[dim]

            if given_size < desired_size:
                dim_pad = (desired_size - given_size) / 2
                uneffected_pad = (num_of_dims - dim - 1) * 2

                pad = tuple([0] * uneffected_pad + [int(np.floor(dim_pad)), int(np.ceil(dim_pad))])

                x = F.pad(x, pad)
            elif given_size > desired_size:
                raise NotImplementedError("Not yet imlpemented for images greater 270x270")

        return x

    def forward(self, x: torch.Tensor) -> OrderedDict:
        assert len(x.shape) == 4

        x = self.adapt_input_dims(x, [2, 3])
        x = x.type(torch.float32)

        if torch.cuda.is_available():
            x = x.to(self.device)

        pred_dict = super().forward(x)
        pred_dict = OrderedDict(
            [(k, v.permute(0, 2, 3, 1)) for k, v in pred_dict.items()]
        )
        pred_dict["np"] = F.softmax(pred_dict["np"], dim=-1)
        pred_dict["hv"] = torch.tanh(pred_dict["hv"])
        if self.nr_types is not None:
            pred_dict["tp"] = F.softmax(pred_dict["tp"], dim=-1)

        return pred_dict

    def _step(self, batch: tuple) -> torch.Tensor:
        x, y_true_inst, y_true_sem = batch

        true_dict = self.prepare_y_true(y_true_inst, y_true_sem)

        if torch.cuda.is_available():
            for key in true_dict.keys():
                true_dict[key] = true_dict[key].to(self.device)

        pred_dict = self.forward(x)

        return self.loss_function(true_dict, pred_dict)

    def predict_whole_image(self, x: torch.Tensor):
        """Predicts whole image batches using a sliding window"""
        final_pred_dict = OrderedDict()
        final_pred_dict["np"] = torch.zeros((x.shape[0], x.shape[2], x.shape[3], 2))
        final_pred_dict["hv"] = torch.zeros((x.shape[0], x.shape[2], x.shape[3], 2))
        if self.nr_types is not None:
            final_pred_dict["tp"] = torch.zeros((x.shape[0], x.shape[2], x.shape[3], self.nr_types))

        orig_shape = x.shape
        padding = 190

        # pad the image
        x = self.adapt_input_dims(x, [2], orig_shape[2] + padding)
        x = self.adapt_input_dims(x, [3], orig_shape[3] + padding)

        for i in range(int(np.floor(orig_shape[2] / 80))):  # TODO: use ceil instead to not loose pixels
            for j in range(int(np.floor(orig_shape[3] / 80))):
                start_i = i * 80
                start_j = j * 80
                end_i = start_i + 80 + padding
                end_j = start_j + 80 + padding
                current_x = x[..., start_i:end_i, start_j:end_j]
                current_pred_dict = self.forward(current_x)

                final_pred_dict["np"][:, start_i:start_i + 80, start_j:start_j + 80] = current_pred_dict["np"]
                final_pred_dict["hv"][:, start_i:start_i + 80, start_j:start_j + 80] = current_pred_dict["hv"]

                if self.nr_types is not None:
                    final_pred_dict["tp"][:, start_i:start_i + 80, start_j:start_j + 80] = current_pred_dict["tp"]

        return final_pred_dict

    def training_step(self, batch: tuple, batch_idx: int) -> torch.Tensor:
        loss = self._step(batch)

        if self.trainer is not None:
            self.log("train/loss", loss, prog_bar=True)
        return loss

    def validation_step(self, batch: tuple, batch_idx: int) -> torch.Tensor:
        loss = self._step(batch)

        if self.trainer is not None:
            self.log("val/loss", loss, prog_bar=True, on_epoch=True)
        return loss

    def test_step(self, batch: tuple, batch_idx: int) -> torch.Tensor:
        loss = self._step(batch)

        if self.trainer is not None:
            self.log("test/loss", loss, prog_bar=True, on_epoch=True, on_step=False)
        return loss
