"""
    Models to be used must be specified here to be loadable.
"""
from .example_classifier import ExampleClassifier
from .hovernet import HoVerNet
from .unet_instance import UnetInstance
from .unet_panoptic import UnetPanoptic
from .unet_seed import UnetSeed
from .unet_semantic import UnetSemantic
#from .detectron_instance import Detectron2Instance
from .mask_rcnn import MaskRCnn
