from typing import List
import torch
import torch.nn as nn
from DLIP.models.zoo.compositions.unet_base import UnetBase
from DLIP.utils.post_processing.distmap2inst import DistMapPostProcessor
from DLIP.utils.loading.split_parameters import split_parameters
from DLIP.utils.metrics.inst_seg_metrics import get_fast_aji_plus, remap_label
from skimage.segmentation import watershed
from skimage import measure
import numpy as np
from DLIP.utils.metrics.seed_metrics import create_gt_area, metric_scores
from DLIP.objectives.dice_loss import DiceLoss

import slidingwindow as sw

def window_inference(model, input_tensor, n_classes=1, window_size=512, boarder=12):
    input_tensor = input_tensor[0]
    windows = sw.generate(data=input_tensor.cpu().numpy(),
                        dimOrder=sw.DimOrder.ChannelHeightWidth,
                        maxWindowSize=window_size,
                        overlapPercent=0.1)

    prediction = torch.zeros((n_classes, input_tensor.shape[1],input_tensor.shape[2]))

    for window in windows:
        h = boarder if window.indices()[1].start > 0 else 0
        w = boarder if window.indices()[2].start > 0 else 0
        hmin = window.indices()[1].start + h
        wmin = window.indices()[2].start + w
        net_input_window = input_tensor[window.indices()].cuda()
        prediction_window = model(net_input_window.unsqueeze(0)).detach().cpu()
        # Combine the sliding windows
        prediction[:, hmin:window.indices()[1].stop, wmin:window.indices()[2].stop] = prediction_window[0,:, h:, w:]

    return prediction.unsqueeze(0)



class UnetInstance(UnetBase):
    def __init__(
        self,
        in_channels: int,
        loss_fcn: nn.Module,
        encoder_type = 'unet',
        encoder_filters: List = [64, 128, 256, 512, 1024],
        decoder_filters: List = [512, 256, 128, 64],
        decoder_type = 'unet',
        dropout_encoder: float = 0.0,
        dropout_decoder: float = 0.0,
        ae_mode = False,
        pretraining_weights = 'imagenet',
        encoder_frozen=False,
        **kwargs,
    ):
        out_channels = 1
        super().__init__(
                in_channels,
                out_channels,
                loss_fcn,
                encoder_type,
                encoder_filters,
                decoder_filters,
                decoder_type,
                dropout_encoder,
                dropout_decoder,
                ae_mode,
                pretraining_weights,
                encoder_frozen,
                **kwargs)
 
        self.post_pro = DistMapPostProcessor(**split_parameters(kwargs, ["post_pro"])["post_pro"])
        
    def training_step(self, batch, batch_idx):
        x, y_true   = batch
        y_true      = y_true.permute(0, 3, 1, 2)
        y_pred      = self.forward(x)
        loss_n_c    = self.loss_fcn(y_pred, y_true)
        loss        = torch.mean(loss_n_c)
        self.log("train/loss", loss, prog_bar=True)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y_true = batch
        y_true = y_true.permute(0, 3, 1, 2)
        if x.shape[0]==1 and (x.shape[2]>256 or x.shape[3]>256):
            y_pred = window_inference(self.forward,x)
            y_true = y_true.cpu()   
        else:
            y_pred = self.forward(x)

        metric = calc_instance_metric(y_true,y_pred, self.post_pro, "hybrid")
        self.log("val/loss", 1-metric, prog_bar=True, on_epoch=True)
        return  1-metric

    def test_step(self, batch, batch_idx):
        x, y_true = batch
        y_true = y_true.permute(0, 3, 1, 2)
        if x.shape[0]==1 and (x.shape[2]>256 or x.shape[3]>256):
            y_pred = window_inference(self.forward,x)
            y_true = y_true.cpu()   
        else:
            y_pred = self.forward(x)
        metric = calc_instance_metric(y_true,y_pred, self.post_pro,"hybrid")

        self.log("test/hybrid", metric, prog_bar=True, on_epoch=True, on_step=False)
        return metric

def calc_instance_metric(y_true,y_pred, post_pro, metric_type="aji+"):
    dsc_loss = DiceLoss()
    metric = list()
    for i_b in range(y_true.shape[0]):
        seeds   = measure.label(y_true[i_b,0,:].cpu().numpy()>0.6, background=0)
        masks   = y_true[i_b,0,:].cpu().numpy()>0.0
        gt_mask = watershed(image=-y_true[i_b,0,:].cpu().numpy(), markers=seeds, mask=masks, watershed_line=False)
        pred_mask = post_pro.process(y_pred[i_b,0,:].cpu().numpy(),None)
        try:
            if metric_type=="aji+":
                # TODO improve runtime, no
                metric_i = get_fast_aji_plus(remap_label(gt_mask),remap_label(pred_mask))
                if np.isnan(metric_i) or np.isinf(metric_i):
                    metric_i = 0
            else:
                dsc_instance_level = metric_scores(remap_label(pred_mask), create_gt_area(remap_label(gt_mask)))
                dsc_seg = 1- dsc_loss(
                     torch.from_numpy(pred_mask>0).unsqueeze(0).unsqueeze(0).to(dtype=torch.float32),
                     torch.from_numpy(gt_mask>0).unsqueeze(0).unsqueeze(0).to(dtype=torch.float32),
                ).numpy()
                print(f"dsc_inst {dsc_instance_level}, dsc_seg {dsc_seg}")
                metric_i = 0.5*(dsc_instance_level+dsc_seg)
            metric.append(metric_i)
        except:
            metric.append(0)

    return np.mean(metric)