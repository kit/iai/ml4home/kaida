from pl_bolts.models.autoencoders import AE
from torch.nn import functional as F

class AutoEncoder(AE):
    def __init__(
        self,
        input_height: int,
        enc_type: str = 'resnet18',
        first_conv: bool = False,
        maxpool1: bool = False,
        enc_out_dim: int = 512,
        latent_dim: int = 256,
        lr: float = 0.0001,
        **kwargs
    ):
        """
        Args:
            input_height: height of the images
            enc_type: option between resnet18 or resnet50
            first_conv: use standard kernel_size 7, stride 2 at start or
                replace it with kernel_size 3, stride 1 conv
            maxpool1: use standard maxpool to reduce spatial dim of feat by a factor of 2
            enc_out_dim: set according to the out_channel count of
                encoder used (512 for resnet18, 2048 for resnet50)
            latent_dim: dim of latent space
            lr: learning rate for Adam
        """
        
        super().__init__(input_height, enc_type=enc_type, first_conv=first_conv, maxpool1=maxpool1, enc_out_dim=enc_out_dim, latent_dim=latent_dim, lr=lr, **kwargs)

    def add_pretrained(self, name, path):
        self.pretrained_urls[name] = path

    def step(self, batch):
        x = batch

        feats = self.encoder(x)
        z = self.fc(feats)
        x_hat = self.decoder(z)

        loss = F.mse_loss(x_hat, x, reduction='mean')

        return loss, {"loss": loss}

    def training_step(self, batch, batch_idx):
        loss, logs = self.step(batch)
        self.log_dict({f"train/{k}": v for k, v in logs.items()}, on_step=True, on_epoch=False)
        return loss

    def validation_step(self, batch, batch_idx):
        loss, logs = self.step(batch)
        self.log_dict({f"val/{k}": v for k, v in logs.items()})
        return loss

    def get_features(self, x):
        feats = self.encoder(x)
        z = self.fc(feats)
        return z
    