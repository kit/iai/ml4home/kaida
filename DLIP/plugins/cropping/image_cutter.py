#GUI for croping images into MxM sized fragments

from PIL import Image
from PyQt5 import QtWidgets, uic, QtCore, QtGui
import shutil
import sys
import os
from difflib import get_close_matches

from DLIP.sampler.generic.cherry_picking import SelectionWindow


class CropImageGui(QtWidgets.QDialog):
        display_name = "Crop Images"
        def __init__(self,path_project, project, **kwargs):
                super(CropImageGui, self).__init__()
                uic.loadUi(f"{path_project}/plugins/cropping/gui_image_cutter.ui", self)

                self._init_vars()
                
                self.project=project
                if project!=None:
                        self.dir_path=project["data_dir"]      

                self.choose_crops_button.clicked.connect(lambda: self.start('choose'))
                self.save_all_button.clicked.connect(lambda: self.start('all'))

        def _init_vars(self):
                self.input_lbl_btn.setVisible(False)
                self.label_input.setVisible(False)
                self.sample_input.setText("")
                self.sample_input.adjustSize()
                self.label_input.setText("")
                self.label_input.adjustSize()
                self.labels_on = False

                self.samples = None
                self.sample_path = None
                self.labels = None
                self.label_path = None

        def choose_samples(self):
                self.samples, self.sample_path, self.sample_type = self.choose_images()
                if self.samples == None or self.sample_path == None:
                        return
                self.main_path=self.sample_path.split("/")
                self.main_path="/".join(self.main_path[:-1])

                self.sample_input.setText(self.sample_path)
                self.sample_input.adjustSize()
                if self.label_path == None:
                        self.label_input.setText(os.path.join(self.main_path,'labels'))
                        self.label_input.adjustSize()
        
        def choose_labels(self):
                #optional
                self.labels, self.label_path, self.label_type = self.choose_images()
                if self.labels == None or self.label_path == None:
                        return
                self.label_input.setText(self.label_path)
                self.label_input.adjustSize()
        
        def choose_images(self):
                file_name = QtWidgets.QFileDialog()
                file_name.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
                if self.project!=None:
                        filenames=file_name.getOpenFileNames(self, "Select Directory",self.dir_path)
                else:
                        filenames=file_name.getOpenFileNames(self, "Select Directory")

                if len(filenames[0]) == 0:
                        return None, None, None
                filenames=list(filenames[0])
                file_path=filenames[0].split("/")
                file_path="/".join(file_path[:-1])
                filenames = [f.split("/")[-1] for f in filenames if os.path.isfile(f)]
                data_type=filenames[0].split('.')[-1]

                return filenames, file_path, data_type

        def start(self,mode):
                if self.check_user_input() == False:
                        return
                status = self.crop_img()
                if status is not None:
                        files = [f for f in os.listdir(os.path.join(self.temp_folder,'samples')) if os.path.isfile(os.path.join(self.temp_folder,'samples', f))]
                        files = [f.split('.')[0] for f in files]
                        if mode == 'choose':
                                self.picker=SelectionWindow(files,os.path.join(self.temp_folder,'samples'),self.sample_type)
                                ranking_dict = self.picker.get_selection()
                                picked = [f for f in ranking_dict if ranking_dict[f]==1]
                        else:
                                picked = files
                        for f in picked:
                                sample=f+'.'+self.sample_type
                                src=os.path.join(self.temp_folder,'samples',sample)
                                dst=os.path.join(self.sample_path,sample)
                                shutil.move(src,dst)

                                if self.labels_on == True:
                                        label=f+'_label.'+self.label_type
                                        src=os.path.join(self.temp_folder,'labels',label)
                                        dst=os.path.join(self.label_path,label)
                                        if os.path.isfile(src):
                                                shutil.move(src,dst)

                        shutil.rmtree(self.temp_folder)   

                        self._init_vars()            

        def crop_img(self):
                self.temp_folder=os.path.join(self.main_path,"temp")

                if not os.path.exists(self.temp_folder):
                        os.mkdir(self.temp_folder)

                if not os.path.exists(os.path.join(self.temp_folder,'samples')):
                        os.mkdir(os.path.join(self.temp_folder,'samples'))

                if not os.path.exists(os.path.join(self.temp_folder,'labels')):
                        os.mkdir(os.path.join(self.temp_folder,'labels'))

                crop_size=int(self.crop_size)
                for file in self.samples:
                        file_path=os.path.join(self.sample_path,file)
                        if self.labels_on:
                                if file in self.sample_label_dict:
                                        label_exists = True
                                        label_file = self.sample_label_dict[file]
                                        lbl_path=os.path.join(self.label_path,label_file)
                                        label = Image.open(lbl_path)
                                else:
                                        label_exists = False

                        im=Image.open(file_path)
                        width, height = im.size
                        
                        if crop_size >= width or crop_size >= height:
                                self.show_popup(("Crop-Size is bigger than width (" + str(width) +  ") or hight (" + str(height) + ") of the image"))
                                shutil.rmtree(self.temp_folder)  
                                return
                

                        num_height=height // crop_size
                        num_width=width // crop_size
                        num_total=num_height*num_width

                        count=1
                        for i in range(1,num_height+1):
                                bottom=i*crop_size
                                top=(i*crop_size)-crop_size
                                for j in range(1,num_width+1):
                                        left=(j*crop_size)-crop_size
                                        right=j*crop_size
                                        new_img=im.crop((left,top,right,bottom))

                                        num_dez=len(str(num_total))
                                        add_zeros=num_dez-len(str(count))
                                        file_ending='_'+('0'*add_zeros)+str(count)+"."
                                        filepath_save=os.path.join(self.temp_folder,'samples',file.replace(".",file_ending))
                                        new_img.save(filepath_save)

                                        if self.labels_on:
                                                if label_exists:
                                                        new_label = label.crop((left,top,right,bottom))
                                                        sample_name= file.split('.')[0]
                                                        lbl_name= sample_name+'_'+('0'*add_zeros)+str(count)+'_label.'+self.label_type
                                                        lbl_path_save = os.path.join(self.temp_folder,'labels', lbl_name)
                                                        new_label.save(lbl_path_save)
                                        count+=1

                        if not os.path.isdir(os.path.join(self.main_path,'originals')):
                                os.mkdir(os.path.join(self.main_path,'originals'))
                        if not os.path.isdir(os.path.join(self.main_path,'originals','samples')):
                                os.mkdir(os.path.join(self.main_path,'originals','samples'))
                        
                        src=os.path.join(self.sample_path,file)
                        dst=os.path.join(self.main_path, 'originals','samples',file)
                        im.close()
                        shutil.move(src,dst)

                        if self.labels_on:
                                if label_exists:
                                        if not os.path.isdir(os.path.join(self.main_path,'originals','labels')):
                                                os.mkdir(os.path.join(self.main_path,'originals','labels'))
                                        src=os.path.join(self.label_path,label_file)
                                        dst=os.path.join(self.main_path, 'originals','labels',label_file)
                                        label.close()
                                        shutil.move(src,dst)
                return True

        def set_visibility(self):
                if self.crop_lbls_checkbox.isChecked():
                        self.input_lbl_btn.setVisible(True)
                        self.label_input.setVisible(True)
                        self.labels_on = True
                else:
                        self.input_lbl_btn.setVisible(False)
                        self.label_input.setVisible(False) 
                        self.labels_on = False

        def check_user_input(self):
                if self.samples == None:
                        self.show_popup("Choose samples first!")
                        return False
                
                crop_size=self.edit_cropSize.text()
                self.crop_size=crop_size.strip()
                if not crop_size:
                        self.show_popup("Please enter crop-size!")
                        return False

                if self.labels_on == True:
                        if self.labels == None:
                                self.label_path=os.path.join(self.main_path,"labels")
                                if os.path.isdir(self.label_path):
                                        self.labels = [f for f in os.listdir(self.label_path) if os.path.isfile(os.path.join(self.label_path, f))]
                        self.find_labels()
                        if len(self.sample_label_dict.keys()) < len(self.samples):
                                dif = len(self.samples)-len(self.labels)
                                msg=f'Found {len(self.labels)} labels but {len(self.samples)} samples. Do you want to continue?'
                                rpl=QtWidgets.QMessageBox.question(self,'',msg,QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
                                if rpl == QtWidgets.QMessageBox.No:
                                        return False
        
        def find_labels(self):
                self.sample_label_dict=dict()
                for f in self.samples:
                        label=get_close_matches(f,self.labels,n=1,cutoff=0.4)
                        if len(label) > 0:
                                self.label_type=label[0].split('.')[-1]
                                self.sample_label_dict[f]=label[0]

        def show_popup(self,text):
                msg=QtWidgets.QMessageBox()
                msg.setText(text)
                msg.setIcon(QtWidgets.QMessageBox.Information)
                x = msg.exec_()

