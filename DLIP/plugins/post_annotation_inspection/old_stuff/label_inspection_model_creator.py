from PyQt5 import QtCore, QtWidgets, uic

class ModelCreator(QtWidgets.QDialog):
    def __init__(self,path_project, project=None):
        super(ModelCreator, self).__init__()
        uic.loadUi(f"{path_project}/plugins/post_annotation_inspection/gui_label_inspection_model_creator.ui", self)
