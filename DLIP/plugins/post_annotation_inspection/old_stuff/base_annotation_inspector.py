import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os
from DLIP.utils.loading.load_class import load_class
from DLIP.plugins.post_annotation_inspection.label_viewer import ParentInspector

class BaseAnnotationIntraInspection:
    def __init__(self, 
            project,
            path_project,
            data_module,
            df=None
            ):

        self.project = project
        self.path_project = path_project
        self.data_module = data_module

        # init dataframe

        if df is None:
            self.data_frame = pd.DataFrame(
                {'inspection_status': [0 for _ in range(len(self.data_module.labeled_train_dataset))],          #0 oder 1 je nachdem ob schon bearbeitet oder nicht
                'inspection_score':  [np.nan for _ in range(len(self.data_module.labeled_train_dataset))],     #Score nachdem sortiert wird
                'changed_annotation':  [np.nan for _ in range(len(self.data_module.labeled_train_dataset))]},  #1 wenn etwas geändert wurde
                index=self.data_module.labeled_train_dataset.get_samples()
            )
        else:
            self.data_frame = df


    def _load_inspection_status(self, data_frame_file_path):
        if os.path.exists(data_frame_file_path):
            df = pd.read_hdf(data_frame_file_path, "df")
            for ind in range(len(df)):
                if df.index.values[ind] in self.data_frame.index:
                    self.data_frame.at[df.index.values[ind],'inspection_score'] = df.loc[df.index.values[ind]]["inspection_score"]
                    self.data_frame.at[df.index.values[ind],'inspection_status'] = df.loc[df.index.values[ind]]["inspection_status"]
                    self.data_frame.at[df.index.values[ind],'changed_annotation'] = df.loc[df.index.values[ind]]["changed_annotation"]


    def _save_inspection_status(self, data_frame_file_path):
        self.data_frame.to_hdf(data_frame_file_path, key="df")

    def inspect(self):
        self.data_frame = self.data_frame.sort_values(by=['inspection_status','inspection_score'], ascending=[True, False])

        # open label assistant
        '''task_property = self.task_properties[self.project["task"]]
        assisted_labeling_class = load_class(f"DLIP.gui.backend.label_assistant.{task_property.folder_name}", task_property.gui_name)
        self.assisted_labeling = assisted_labeling_class(
            self.project,
            self.path_project,
            self.data_module,
            task_property,
            self.path_seg_annotation_tool
        )'''


        #print("hallo")

        # iterate over labeled dataset
        # for i in range(len(self.data_frame)) :
        #     if self.data_frame.loc[i, "inspection_status"]:
        #         break
        #     else:
        #         pass
                # load all data -> image
                # see assisted_labeling _compose_file_name(self, sample_name, data_type, dataset_type), _read_label, _read_image
                # read prediction from different folder
                # update gui
                # gui.update_window(image, label, prediction)

                # get decision
                # result = gui.output()

                # reinspect, use prediction or move ahead

                # update data frame + update label in case of changes
                # see _finalize_file_handling _write_label

        #parent = ParentInspector(self.path_project, self.data_frame, self.project,self.data_module, self.task_properties, self.path_seg_annotation_tool)
        #parent.exec_()

        #self.data_frame = parent.df

    def load_inspector(self):
        # needs to be adapated with real model
        self.inspector = lambda img: np.random.random()

    def calculate(self):
        for i in range(len(self.data_frame)):
            self.data_frame.at[self.data_frame.index.values[i],'inspection_score'] = np.random.random()

        self.data_frame = self.data_frame.sort_values(by=['inspection_status','inspection_score'], ascending=[True, False])
        
        #pass
        # inspector writes labels in a folder and sampling scores in dataframe
        # score, prediction = self.inspector.inspect(image)

    def reinspect(self, sample_name = "ISIC_0015295"):
        self.assisted_labeling._label_img(sample_name)
        # shift sample to a temp folder
        # check methods in label assistant
        # use _label_img in label assistant e.g. AssistedLabelingSegmentationGui, BaseAssistedLabelingGuiMasks

