import os
import tkinter as tk

from PyQt5 import QtWidgets, uic
from DLIP.utils.loading.load_class import load_class
from DLIP.plugins.post_annotation_inspection.base_annotation_inspector import BaseAnnotationIntraInspection
from DLIP.plugins.post_annotation_inspection.label_inspection_model_creator import ModelCreator
from DLIP.plugins.post_annotation_inspection.label_viewer import ParentInspector
import pandas as pd

from DLIP.plugins.post_annotation_inspection.parent_viewer import ParentViewer
from DLIP.plugins.post_annotation_inspection.segm_viewer import SegmentationInspector
from DLIP.plugins.post_annotation_inspection.classification_viewer import ClassificationInspector


class AnnotationInspectionGui(QtWidgets.QDialog):
    display_name = "Annotation Inspection Gui"
    def __init__(self,path_project,project, parent_obj):
        super(AnnotationInspectionGui, self).__init__()
        uic.loadUi(f"{path_project}/plugins/post_annotation_inspection/gui_label_inspector.ui", self)

        methods = {'Inter', 'Intra'}        
        self.method.addItems(methods)

        self.project=project
        self.path_project=path_project
        self.parent_obj = parent_obj
        self.data_frame = None
        
        self.inspection_btn.setStyleSheet("color: rgb(169, 169, 169);")
        self.inspection_btn.setEnabled(False)
        self.startCalc_btn.setStyleSheet("color: rgb(169, 169, 169);")
        self.startCalc_btn.setEnabled(False)


        # ACCESS TO DATAMODULE
        print(self.parent_obj.data_module)

        # ACCESS TO ASSISTED LABELING UI
        print(self.parent_obj.assisted_labeling_gui)

        # if self.project is not None:
        #     self.base_inspector = BaseAnnotationIntraInspection(
        #         self.project,
        #         self.path_project, 
        #         parent_obj.data_module, 
        #         self.data_frame
        #     )


    def createModel(self):
        #model_creator = ModelCreator(self.path_project, self.project)
        #model_creator.exec_()

        self.startCalc_btn.setStyleSheet("color: rgb(0, 0, 0);")
        self.startCalc_btn.setEnabled(True)
        pass


    def loadCalc(self):
        root = tk.Tk()
        root.withdraw()

        calculation = str(QtWidgets.QFileDialog.getOpenFileName(self,"Select file")[0].replace('/',os.sep))

        if calculation == '':
            return

        self.txt_calculation.setText(calculation)
        self.data_frame = pd.read_hdf(calculation)
        self.path_calc = calculation

        self.inspection_btn.setStyleSheet("color: rgb(0, 0, 0);")
        self.inspection_btn.setEnabled(True)

    def loadModel(self):
        root = tk.Tk()
        root.withdraw()

        self.model_file_path= str(QtWidgets.QFileDialog.getOpenFileName(self,"Select file")[0].replace('/',os.sep))
        if self.model_file_path == '':
            return

        self.startCalc_btn.setStyleSheet("color: rgb(0, 0, 0);")
        self.startCalc_btn.setEnabled(True)

    def startCalc(self):
        path_calc_check = os.path.join(self.project['data_dir'],'Calculation')
        if not os.path.exists(path_calc_check):
            os.makedirs(path_calc_check)
            self.path_calc = os.path.join(path_calc_check, self.project['name'] + '_calculation.h5')  
        else:
            self.path_calc = os.path.join(path_calc_check, self.project['name'] + '_calculation.h5')  

        path_pred_check = os.path.join(self.project['data_dir'],'Predictions')
        if not os.path.exists(path_pred_check):
            os.makedirs(path_pred_check)
            self.path_pred = path_pred_check 
        else:
            self.path_pred = path_pred_check

        path_pictures = self.project.get('data_dir')
        task = self.project.get('task')
        
        #ToDo: Check ob alle Bilder gelabelt sind -> Ansonsten Warnung ausgeben aber Programm laufen lassen

        self.base_inspector.calculate()
        self.data_frame = self.base_inspector.data_frame
        self.data_frame.to_hdf(self.path_calc, key='df', mode='w')

        self.inspection_btn.setStyleSheet("color: rgb(0, 0, 0);")
        self.inspection_btn.setEnabled(True)


    def startInsp(self):
        task = self.project.get("task")
        if task == 'segmentation':
            segm = SegmentationInspector(self.path_project, self.data_frame, self.project,self.data_module, self.task_properties, self.path_seg_annotation_tool, self.path_calc)
            segm.exec_()

            self.data_frame = segm.df
        elif task == 'classification':
            clas_inspect = ClassificationInspector(self.path_project, self.data_frame, self.project,self.data_module, self.task_properties, self.path_seg_annotation_tool, self.path_calc)
            clas_inspect.exec_() 

        self.close()           