from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import *
import os
import numpy as np
import tifffile
import cv2
from DLIP.utils.loading.load_class import load_class
from DLIP.utils.helper_functions.create_qimage import array_to_qimage
from skimage.color import label2rgb
import time

from threading import Thread


class classRelable(QtWidgets.QDialog):
    def __init__(self, path_project, classes, actual_label, parent=None):
        super(classRelable, self).__init__()
        uic.loadUi(f"{path_project}/plugins/post_annotation_inspection/Classification_relabeling.ui", self)

        self.classes_cb.addItems(classes)
        self.classes_cb.setCurrentText(actual_label)

    def save(self):
        self.close()

    def return_new_class(self):
        return self.classes_cb.currentText()
