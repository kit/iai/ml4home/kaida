from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import *
import os
import numpy as np
import tifffile
import cv2
from DLIP.utils.loading.load_class import load_class
from DLIP.utils.helper_functions.create_qimage import array_to_qimage
from skimage.color import label2rgb
import pandas as pd
import shutil


from DLIP.plugins.post_annotation_inspection.parent_viewer import ParentViewer
from DLIP.plugins.post_annotation_inspection.Classification_relabeling import classRelable


class ClassificationInspector(ParentViewer):
    def __init__(self, path_project, dataframe, project,data_module, task_properties, path_seg_annotation_tool, path_calc):
        path_ui = (f"{path_project}/plugins/post_annotation_inspection/classification_viewer.ui")
        super(ClassificationInspector,self).__init__(path_project, dataframe, project, data_module, task_properties, path_seg_annotation_tool, path_calc, path_ui)

        self.classes= self.project.get('class_names')

        project_path = self.project.get('data_dir')
        self.meta_df = pd.DataFrame(columns=['name', 'path', 'label','path_without_label'])
        for folder in ['train', 'test']:
            for file in os.walk(os.path.join(project_path, folder), topdown=True):
                for image in file[2]:
                    path = os.path.join(file[0],image)
                    name = os.path.splitext(image)[0]
                    label = os.path.split(file[0])[1]
                    path_without_label = os.path.split(file[0])[0]
                    new_row = {'name': name, 'path': path, 'label': label, 'path_without_label': path_without_label}
                    self.meta_df = self.meta_df.append(new_row, ignore_index = True)
        
        self.show_picture()

    def previous(self):
        
        pass

    def show_picture(self):
        picture_name = self.df.index[self.counter-1]
        index_nr =  self.meta_df.loc[self.meta_df['name'] == picture_name]
        filepath_img= index_nr.iloc[0]['path']

        img = tifffile.imread(filepath_img) if self.data_module.samples_data_format == "tif" else cv2.imread(filepath_img)
        self.original_img_lbl.setPixmap(QPixmap(filepath_img))

        self.currentLabel_txt.setText(index_nr.iloc[0]['label'])

        self.num_img.setText('Picture: ' + str(self.counter) + ' / ' + str(self.num_pictures))


    def relabel(self):
        picture_name = self.df.index[self.counter-1]
        index_nr =  self.meta_df.loc[self.meta_df['name'] == picture_name]
        actual_label = index_nr.iloc[0]['label']
        filepath_old = index_nr.iloc[0]['path']

        new = classRelable(self.path_project,self.classes, actual_label)
        new.exec()

        new_class = new.return_new_class()
        folder_new = os.path.join(index_nr.iloc[0]['path_without_label'],new_class)
        filepath_new = os.path.join(folder_new,os.path.split(filepath_old)[1])

        shutil.move(filepath_old, filepath_new)

        #self.meta_df.loc[self.meta_df['name'] == picture_name, 'path_without_label'] = folder_new
        self.meta_df.loc[self.meta_df['name'] == picture_name, 'path'] = filepath_new
        self.meta_df.loc[self.meta_df['name'] == picture_name, 'label'] = new_class

        self.update_df(1,1)
        self.df.to_hdf(self.path_calc, key = 'df', mode = 'w')
        self.next()
        


