from PyQt5 import QtWidgets, uic
from PyQt5.QtGui import *
import os
import numpy as np
import tifffile
import cv2
from DLIP.utils.loading.load_class import load_class
from DLIP.utils.helper_functions.create_qimage import array_to_qimage
from skimage.color import label2rgb
import time

from threading import Thread


class ParentViewer(QtWidgets.QDialog):
    def __init__(self, path_project, dataframe, project,data_module, task_properties, path_seg_annotation_tool, path_calc, path_ui, parent=None):
        super(ParentViewer, self).__init__()
        #uic.loadUi(f"{path_project}/plugins/post_annotation_inspection/parent_viewer.ui", self)
        uic.loadUi(path_ui, self)

        self.path_project = path_project
        self.df = dataframe
        self.project = project
        self.data_module = data_module
        self.task_properties = task_properties
        self.path_seg_annotation_tool = path_seg_annotation_tool
        self.num_pictures = self.df.shape[0]
        self.path_calc = path_calc

        for i in range(len(self.df)):
            if self.df.loc[self.df.index.values[i],'inspection_status'] == 0:
                self.counter=i + 1
                break

        task_property = self.task_properties[self.project["task"]]
        assisted_labeling_class = load_class(f"DLIP.gui.backend.label_assistant.{task_property.folder_name}", task_property.gui_name)

        self.assisted_labeling = assisted_labeling_class(
            self.project,
            self.path_project,
            self.data_module,
            task_property,
            self.path_seg_annotation_tool
        )

        self.lb_information.setText("")

        #self.show_picture()
        self.progressBar.setValue(round(self.df['inspection_status'].sum() / self.num_pictures,2)*100)

        #self.data_module.label_train_dataset.samples #-> Samples Dateipfad

    def next(self):
        self.lb_information.setText("")
        picture_name = self.df.index[self.counter-1]

        if self.counter < self.num_pictures:
            picture_name_next = self.df.index[self.counter]

            if self.df.loc[picture_name,'inspection_status'] == 1:
                self.counter += 1
                self.show_picture()
                if self.df.loc[picture_name_next,'inspection_status'] == 1:
                    self.lb_information.setText("Label is already edited.")
            else:
                self.lb_information.setText("Please edit label first.")
                self.show_picture()

        else:
            if self.df.loc[picture_name,'inspection_status'] == 1:
                self.lb_information.setText("Label is already edited.")
            self.show_picture()
                
            

    def previous(self):
        if self.counter > 1:
            self.counter -= 1
            self.lb_information.setText("Label is already edited.")
            self.show_picture()

    '''def show_picture(self):
        picture_name = self.df.index[self.counter-1]
        filepath = self.project.get('data_dir')

        filepath_img = self.assisted_labeling._compose_file_name(picture_name,"sample", self.assisted_labeling.data_frame.at[picture_name,"dataset_type"])
        #filepath_label = self.assisted_labeling._compose_file_name(picture_name,"label", self.assisted_labeling.data_frame.at[picture_name,"dataset_type"])

        img = tifffile.imread(filepath_img) if self.data_module.samples_data_format == "tif" else cv2.imread(filepath_img)
        #label = tifffile.imread(filepath_label)
        #self.overlay_img_raw = array_to_qimage(np.uint8(label2rgb(label, image=img, bg_label=0)*255,))


        self.original_img_lbl.setPixmap(QPixmap(filepath_img))
        #self.label_img_lbl.setPixmap(QPixmap.fromImage(self.overlay_img_raw))

        self.num_img.setText('Picture: ' + str(self.counter) + ' / ' + str(self.num_pictures))'''


    '''def relabel(self):
        picture_name = self.df.index[self.counter-1]
        self.assisted_labeling._label_img(picture_name)

        thread = Thread(target = self.check_status)
        thread.start()'''
        

    '''def check_status(self):
        while self.assisted_labeling.label_ui_active:
            time.sleep(1)
        
        self.relabel_finished()'''
        

    '''def relabel_finished(self):
        self.update_df(1,1)

        self.df.to_hdf(self.path_calc, key = 'df', mode = 'w')
        self.progressBar.setValue(round(self.df['inspection_status'].sum() / self.num_pictures,2)*100)

        self.next()'''
        

    def keep_label(self):
        self.update_df(1,0)

        print(self.path_calc)
        self.df.to_hdf(self.path_calc, key = 'df', mode = 'w')
        self.progressBar.setValue(round(self.df['inspection_status'].sum() / self.num_pictures,2)*100)

        self.next()

    def suggested_label(self):
        self.update_df(1,1)
        self.df.to_hdf(self.path_calc, key = 'df', mode = 'w')

        self.next()

    def update_df(self,inspection_status,changed_annotation):
        picture_name = self.df.index[self.counter-1]
        self.df.loc[picture_name,'inspection_status'] = inspection_status
        self.df.loc[picture_name,'changed_annotation'] = changed_annotation