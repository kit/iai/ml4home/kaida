from PyQt5 import QtWidgets, uic
from DLIP.plugins.post_annotation_inspection.intra.intra_annotation_inspection import IntraAnnotationInspectionGui
class AnnotationInspectionModeSelectionGui(QtWidgets.QDialog):
    display_name = "Annotation Inspection"
    def __init__(self,path_project,project, parent_obj):
        super(AnnotationInspectionModeSelectionGui, self).__init__()
        uic.loadUi(f"{path_project}/plugins/post_annotation_inspection/gui_anno_insp_mode.ui", self)
     
        self.project=project
        self.path_project=path_project
        self.parent_obj = parent_obj
        self._status_bar("Select Mode")


    def open_intra(self):
        if self.project is not None:
            if self.project["task"] == "segmentation" or self.project["task"] == "instance_segmentation":
                self.intra_anno_insp_ui = IntraAnnotationInspectionGui(self.path_project,self.project,self.parent_obj)
                self.intra_anno_insp_ui.exec()
            else:
                self._status_bar("Annotation nspection not available for this task")
        else:
            self._status_bar("No project loaded")

    def open_inter(self):
        self._status_bar("Not available yet")

    def _status_bar(self, text):
        self.info_window.setText(text)
