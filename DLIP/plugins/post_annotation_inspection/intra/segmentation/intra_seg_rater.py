import numpy as np
from typing import NoReturn


from DLIP.plugins.post_annotation_inspection.intra.segmentation.predictor_wrapper import Predictor
from DLIP.objectives import DiceLoss
import torch
import pickle

class SegmentationPerImageRater:
    def __init__(
            self,
            original_image: np.ndarray,
            user_annotated_label: np.ndarray,
            segmentation_network: Predictor,
            uncertainty_weighting_param: float = 0,
    ):
        self.pred, self.unc = segmentation_network.predict_with_uncertainty(original_image)
        _,self.user_annotated_label,_ = segmentation_network.trafo(original_image, mask=user_annotated_label.squeeze())
        self.user_annotated_label = self.user_annotated_label.numpy()
        self.uncertainty_weighting_param = uncertainty_weighting_param
        self.dice_loss = DiceLoss()
        self.matching = self.calc_matching()
        self.user_feedback = False

    def calc_matching(self):
        matching = 1-self.dice_loss(
                            torch.from_numpy(self.user_annotated_label).unsqueeze(0).unsqueeze(0), 
                            torch.from_numpy(self.pred).unsqueeze(0).unsqueeze(0)
                        )
        return matching

    def get_overall_score(self):
        return  1 - ((1-self.uncertainty_weighting_param)*self.matching + self.uncertainty_weighting_param*self.unc)

    def set_uncertainty_weighting_param(self, uncertainty_weighting_param: float) -> NoReturn:
        self.uncertainty_weighting_param = uncertainty_weighting_param

    def set_user_feedback(self, user_feedback: bool) -> NoReturn:
        self.user_feedback = user_feedback

    def save(self, file_name) -> NoReturn:
        with open(file_name, "wb") as f:
            pickle.dump(self, f)
       