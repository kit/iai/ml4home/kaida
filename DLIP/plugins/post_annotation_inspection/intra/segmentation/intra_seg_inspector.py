
from DLIP.plugins.post_annotation_inspection.intra.user_inspection_ui import UserInspectionUi
from DLIP.utils.helper_functions.create_qimage import array_to_qimage
from PyQt5.QtGui import *
import numpy as np
from skimage.color import label2rgb
import time
import os
import pickle
import cv2
import pandas as pd
from threading import Thread

class IntraSegmentationInspector(UserInspectionUi):
    def __init__(self, path_project, parent_obj, df, calc_path):
        path_ui = (f"{path_project}/plugins/post_annotation_inspection/intra/segmentation/seg_gui.ui")
        super(IntraSegmentationInspector,self).__init__(path_ui)
        self.parent_obj = parent_obj
        self.df = df
        self.counter = 0
        self.calc_path = calc_path
        self._update_ui()

    @staticmethod
    def load_from_precomputed_calculations(
            path_project,
            parent_obj,
            calculation_results_path,
            uncertainty_weighting_param: float = 0,
    ) -> "IntraSegmentationInspector":
        df = pd.DataFrame(columns=["name", "rater", "score"])

        for i in range(len(parent_obj.data_module.labeled_train_dataset)):
            name = parent_obj.data_module.labeled_train_dataset.indices[i]

            with open(os.path.join(calculation_results_path, f"{name}.pkl"), "rb") as f:
                rater_obj = pickle.load(f)

            rater_obj.set_uncertainty_weighting_param(uncertainty_weighting_param)

            df = df.append({
                "name": name,
                "rater": rater_obj,
                "score": rater_obj.get_overall_score()
            }, ignore_index=True)

        df = df.sort_values(by=["score"], ascending=False)

        return IntraSegmentationInspector(path_project, parent_obj, df, calculation_results_path)


    def _update_ui(self):
        self.num_img.setText('Picture: ' + str(self.counter+1) + ' / ' + str(self.df.shape[0]))
        num_feedback = np.sum([self.df["rater"].iloc[i].user_feedback for i in range(self.df.shape[0])])
        self.progress_bar.setValue(round(num_feedback / self.df.shape[0],2)*100)
        self.show_picture()


    def show_picture(self):
        img, mask = self.parent_obj.data_module.labeled_train_dataset[
            self.parent_obj.data_module.labeled_train_dataset.indices.index(self.df["name"].iloc[self.counter])
        ]

        self.noisy_img.setPixmap(
            QPixmap.fromImage(
                array_to_qimage(
                    np.uint8(
                        label2rgb(
                            cv2.resize(self.df["rater"].iloc[self.counter].user_annotated_label.squeeze(), (img.shape[1], img.shape[0])), 
                            image=img, bg_label=0)*255,)
                        )
                    )
                ) 

        self.pred_anno = cv2.resize(self.df["rater"].iloc[self.counter].pred.squeeze(), (img.shape[1], img.shape[0]))

        self.dnn_img.setPixmap(
            QPixmap.fromImage(
                array_to_qimage(
                    np.uint8(
                        label2rgb(
                            self.pred_anno, 
                            image=img, bg_label=0)*255,)
                        )
                    )
                ) 

        if self.df["rater"].iloc[self.counter].user_feedback:
            self.final_img.setPixmap(
                QPixmap.fromImage(
                    array_to_qimage(
                        np.uint8(
                            label2rgb(
                                mask.squeeze(), 
                                image=img, bg_label=0)*255,)
                            )
                        )
                    ) 
        else:
            self.final_img.setPixmap(
                QPixmap.fromImage(
                    array_to_qimage(np.zeros_like(img))
                    ))

    def next(self):
        if (self.counter+1)<self.df.shape[0]:
            self.counter += 1
            self._update_ui()

    def previous(self):
        if (self.counter-1)>=0:
            self.counter -= 1
            self._update_ui()

    def re_annotate(self):
        self.parent_obj.assisted_labeling_gui._label_img(self.df["name"].iloc[self.counter])

        thread = Thread(target = self.check_status)
        thread.start()

    def check_status(self):
        while self.parent_obj.assisted_labeling_gui.label_ui_active:
            time.sleep(1)
        
        self.update_rater()
        self.next()

    def accept_prediction(self):
        self.update_rater()
        self.parent_obj.assisted_labeling_gui._write_label(
            self.parent_obj.assisted_labeling_gui._compose_file_name(self.df["name"].iloc[self.counter], "label", "train_labeled"), 
            self.pred_anno)
        self.next()

    def keep_annotation(self):
        self.update_rater()
        self.next()

    
    def update_rater(self):
        self.df["rater"].iloc[self.counter].set_user_feedback(True)
        self.df["rater"].iloc[self.counter].save(
            os.path.join(self.calc_path, f"{self.df['name'].iloc[self.counter]}.pkl")
        )