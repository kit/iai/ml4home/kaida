from typing import List, Dict, Union, Tuple

import matplotlib.pyplot as plt
import numpy as np
import torch

from DLIP.utils.metrics.uncertainty.inverted_agreement import get_i_dais

class Predictor:
    def __init__(self, model, trafo, num_of_monte_carlo_predictions: int = 3):
        self.model = model
        if torch.cuda.is_available():
            self.model.cuda()
        self.model.eval()
        self.trafo = trafo

        self.num_of_monte_carlo_predictions = num_of_monte_carlo_predictions


    def predict(self, image: np.ndarray):
        image = self.trafo(image)[0]
        image = image.unsqueeze(0)
        image = image.to(self.model.device)

        with torch.no_grad():
            output = self.model(image)

        output = output.squeeze(0)
        output = output.cpu().numpy()

        return output

    def predict_with_uncertainty(self, image: np.ndarray):

        def apply_dropout(m):
            if type(m) == torch.nn.Dropout or type(m) == torch.nn.Dropout2d:
                m.train()

        self.model.eval()

        self.model.apply(apply_dropout)

        dummy_pred = self.predict(image)

        dropout_images = np.zeros((self.num_of_monte_carlo_predictions, *dummy_pred.shape))
        for i in range(self.num_of_monte_carlo_predictions):
            dropout_images[i,:] = self.predict(image)

        predicted_label = self.get_final_prediction(dropout_images)

        uncertainty = self.compute_uncertainty(dropout_images)

        self.model.eval()

        return predicted_label, uncertainty

    def get_final_prediction(self,dropout_images):
        return np.mean(dropout_images, axis=0).squeeze()

    def compute_uncertainty(self,dropout_images):
        return get_i_dais(dropout_images)
