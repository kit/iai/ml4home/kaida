import os
import pickle
from pathlib import Path
from typing import Optional, Union

from PyQt5 import QtWidgets, uic

from DLIP.objectives import DiceLoss
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.calculation import InstSegCalculator
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.intra_inst_seg_inspector import \
    IntraInstanceSegmentationInspector
from DLIP.plugins.post_annotation_inspection.intra.segmentation.intra_seg_inspector import IntraSegmentationInspector
from DLIP.plugins.post_annotation_inspection.intra.segmentation.intra_seg_rater import SegmentationPerImageRater
from DLIP.plugins.post_annotation_inspection.intra.segmentation.predictor_wrapper import Predictor as PredictorSeg
from DLIP.plugins.training.train_gui import TrainGui
from DLIP.utils.loading.load_model_tar import load_model_trafos_tar


class IntraAnnotationInspectionGui(QtWidgets.QDialog):
    display_name = "Intra Annotation Inspection"

    def __init__(self, path_project, project, parent_obj):
        super(IntraAnnotationInspectionGui, self).__init__()
        uic.loadUi(f"{path_project}/plugins/post_annotation_inspection/intra/intra_anno_gui.ui", self)

        self.project = project
        self.path_project = path_project
        self.parent_obj = parent_obj

        self.model = None
        self.trafos = None

        self.calc_path = None
        self.dice_loss = DiceLoss()

        self.inspection_ui: Optional[Union[IntraSegmentationInspector, IntraInstanceSegmentationInspector]] = None

        self.inst_seg_calculator = InstSegCalculator(self._status_bar, self.setEnabled)

        self.unc_weighting.setText("0.5")
        self._status_bar("Select Model and Calculation")
        self._model_status("Not available")
        self._calc_status("Not available")

    def load_model(self):
        model_path = str(QtWidgets.QFileDialog.getOpenFileName(self,"Select file", "*.tar")[0])
        if len(model_path) > 0:
            self.model, self.trafos = load_model_trafos_tar(model_path)

        if self.model is not None:
            self._model_status("Available")

    def train_model(self):
        # TODO: IMPEMENT MODEL TRAINING
        # TODO: CLEAN/OPTIMIZE MODEL TRAINING
        # TODO: SELECT ALWAYS THE UNC MODEL
        train_gui = TrainGui(self.path_project, self.project)
        train_gui.exec_()

    def load_calc(self):
        self.calc_path = str(QtWidgets.QFileDialog.getExistingDirectory(self, "Select anno inspection dir"))

        if len(self.calc_path) == 0:
            self.calc_path = None
            return

        if self._check_calc_status():
            self._calc_status("Available")
        else:
            self.calc_path = None

    def start_calc(self):
        if self.model is not None and self.calc_path is None:
            # TODO mcd param
            self.parent_obj.data_module.labeled_train_dataset.raw_mode = True
            self.calc_path = os.path.join(self.project["data_dir"], "anno_inspection")

            if not os.path.exists(self.calc_path):
                os.makedirs(self.calc_path)

            if self.project["task"] == "segmentation":
                predictor = PredictorSeg(self.model, self.trafos[0], 10)

                for i in range(len(self.parent_obj.data_module.labeled_train_dataset)):
                    img, noisy_anno = self.parent_obj.data_module.labeled_train_dataset[i]
                    name = self.parent_obj.data_module.labeled_train_dataset.indices[i]
                    out_file = Path(self.calc_path).joinpath(f"{name}.pkl")

                    if self.project["task"] == "segmentation":
                        rater_obj = SegmentationPerImageRater(img, noisy_anno, predictor)
                        with open(out_file, "wb") as f:
                            pickle.dump(rater_obj, f)
            elif self.project["task"] == "instance_segmentation":
                self.inst_seg_calculator.start_calculation(
                    self.model, self.trafos, self.parent_obj.data_module, self.calc_path
                )
            else:
                self._status_bar(f"The given task {self.project['task']} is not supported")
                return
        elif self.model is None:
            self._status_bar("No model was loaded")
        else:
            self._status_bar("Already loaded a calculation")

    def start_guided_user_inspection(self):
        uncertainty_weighting_param = float(self.unc_weighting.text())
        if self.calc_path is not None:
            if self.project["task"] == "segmentation":
                self.inspection_ui = IntraSegmentationInspector.load_from_precomputed_calculations(
                    self.path_project,
                    self.parent_obj, 
                    self.calc_path,
                    uncertainty_weighting_param)
                self.inspection_ui.exec_()
            elif self.project["task"] == "instance_segmentation":
                self.setEnabled(False)
                self._status_bar("Loading Inspector ...")
                self.inspection_ui = IntraInstanceSegmentationInspector.load_from_precomputed_calculations(
                    self.path_project,
                    self.parent_obj,
                    self.calc_path,
                    uncertainty_weighting_param
                )
                self.inspection_ui.exec_()
                self.setEnabled(True)
                self._status_bar("")
            else:
                self.info_window.setText("No support for this task")
        else:
            self.info_window.setText("No calculation available")

    def _check_calc_status(self):
        for i in range(len(self.parent_obj.data_module.labeled_train_dataset)):
            name = self.parent_obj.data_module.labeled_train_dataset.indices[i]
            if not os.path.exists(os.path.join(self.calc_path, f"{name}.pkl")):
                return False
        return True

    def _status_bar(self, text):
        self.info_window.setText(text)

    def _model_status(self, text):
        self.status_model.setText(f"Status: {text}")

    def _calc_status(self, text):
        self.status_calc.setText(f"Status: {text}")
