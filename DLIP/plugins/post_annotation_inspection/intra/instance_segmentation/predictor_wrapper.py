from typing import List, Dict, Union, Tuple, Callable

import numpy as np
import torch
import ttach as tta
from torchvision.transforms import ToTensor

from DLIP.models.zoo.compositions.unet_instance import UnetInstance
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.matching import InstanceMatching, \
    MatchingResults


class Predictor:
    def __init__(
            self,
            model: UnetInstance,
            transformations: Callable = None,
            num_of_monte_carlo_predictions: int = 3,
            uncertainty_method: str = "tta",
    ):
        self.model = model
        if torch.cuda.is_available():
            self.model.cuda()

        self.transformations = transformations

        self.model.eval()
        self.model.double()

        self.num_of_monte_carlo_predictions = num_of_monte_carlo_predictions
        self.uncertainty_method = uncertainty_method

        self.to_tensor = None
        self.tta_transforms = None

        if uncertainty_method == "tta":
            self.to_tensor = ToTensor()

            self.tta_transforms = tta.Compose(
                [
                    tta.HorizontalFlip(),
                    tta.VerticalFlip(),
                    tta.Rotate90(angles=[90, 180]),
                ]
            )

    @staticmethod
    def from_checkpoint_path(
            checkpoint_path: str,
            transformations: Callable = None,
            num_of_monte_carlo_predictions: int = 3,
            uncertainty_method: str = "tta",
    ) -> "Predictor":
        return Predictor(UnetInstance.load_from_checkpoint(
                checkpoint_path,
                input_channels=3,
                loss_fcn="SmoothL1Loss",
                dropout_decoder=[0.3, 0.2, 0.1, 0.0],
                dropout_encoder=[0, 0.1, 0.2, 0.3, 0.3]
            ),
            transformations=transformations,
            num_of_monte_carlo_predictions=num_of_monte_carlo_predictions,
            uncertainty_method=uncertainty_method,
        )

    @torch.no_grad()
    def predict(self, image: np.ndarray):
        if self.transformations is not None:
            image = self.transformations(image)[0]
            image = image.unsqueeze(0)
            image = image.double()
        else:
            image = torch.tensor(image.transpose((2, 0, 1)))[None, :, :, :].double()

        image = image.to(self.model.device)

        prediction = self.model.forward(image)[0, 0].cpu().detach().numpy()

        prediction_label = self.model.post_pro.process(prediction, None)

        return prediction_label

    @staticmethod
    def calculate_uncertainty_for_given_instances(
            instance_ids: Union[List[int], np.ndarray],
            prediction_ids_for_results: List[int],
            original_prediction_pairings: List[Dict[int, List[int]]],
            matching_results: List[MatchingResults]
    ):
        instance_iou = dict()

        # 1 / len(results) is the same as 2 / (T * (T - 1)) with T = len(images)
        pre_factor_denominator = len(matching_results)

        for instance_id in instance_ids:
            instance_iou[instance_id] = 1

            iou_sum = 0

            for image_id, result in zip(prediction_ids_for_results, matching_results):
                if instance_id not in original_prediction_pairings[image_id]:
                    continue

                for i, instance_mapping_id in enumerate(original_prediction_pairings[image_id][instance_id]):
                    iou_sum += result.get_iou_of_user_instance(instance_mapping_id)
                    if i > 0:
                        pre_factor_denominator += 1

            instance_iou[instance_id] -= 1 / pre_factor_denominator * iou_sum

        return instance_iou

    @staticmethod
    def compute_instance_wise_uncertainty(
            original_prediction: np.ndarray,
            monte_carlo_predictions: List[np.ndarray],
            unpredicted_instances_image: np.ndarray = None
    ) -> Tuple[Dict[int, float], Dict[int, float]]:
        results = []
        original_prediction_pairings = []
        unpredicted_instances_pairings = []
        prediction_ids_for_results = []

        for i, prediction_i in enumerate(monte_carlo_predictions):
            original_prediction_pairings.append(
                InstanceMatching.match(original_prediction, prediction_i).paired_instance_ids
            )

            if unpredicted_instances_image is not None:
                unpredicted_instances_pairings.append(
                    InstanceMatching.match(unpredicted_instances_image, prediction_i).paired_instance_ids
                )

            for j in range(i + 1, len(monte_carlo_predictions)):
                prediction_ids_for_results.append(i)
                results.append(InstanceMatching.match(prediction_i, monte_carlo_predictions[j]))

        instance_ids = np.delete(np.unique(InstanceMatching.remap_label(original_prediction)), 0)

        instance_uncertainty = Predictor.calculate_uncertainty_for_given_instances(
            instance_ids,
            prediction_ids_for_results,
            original_prediction_pairings,
            results
        )

        if unpredicted_instances_image is not None:
            unpredicted_instance_ids = np.delete(
                np.unique(InstanceMatching.remap_label(unpredicted_instances_image)), 0
            )

            unpredicted_instance_uncertainty = Predictor.calculate_uncertainty_for_given_instances(
                unpredicted_instance_ids,
                prediction_ids_for_results,
                unpredicted_instances_pairings,
                results
            )

            # here the uncertainty is one minus calculated uncertainty, as the network didn't predict the images
            unpredicted_instance_uncertainty = {
                key: 1 - unpredicted_instance_uncertainty[key] for key in unpredicted_instance_uncertainty.keys()
            }
        else:
            unpredicted_instance_uncertainty = dict()

        return instance_uncertainty, unpredicted_instance_uncertainty

    def _compute_uncertainty_predictions_mc(self, image: np.ndarray) -> List[np.ndarray]:
        def apply_dropout(m):
            if type(m) == torch.nn.Dropout or type(m) == torch.nn.Dropout2d:
                m.train()

        self.model.eval()
        self.model.apply(apply_dropout)

        dropout_predictions = list()

        for _ in range(self.num_of_monte_carlo_predictions):
            dropout_predictions.append(self.predict(image))

        self.model.eval()

        return dropout_predictions

    def _compute_uncertainty_predictions_tta(self, image: np.ndarray) -> List[np.ndarray]:
        tta_predictions = list()

        for transformer in self.tta_transforms:
            image = image.astype(np.float32)
            augmented_image = (
                    transformer.augment_image(self.to_tensor(image)[None, :]).numpy().squeeze() * 255
            ).astype(int)
            if len(augmented_image.shape) == 3:
                augmented_image = augmented_image.transpose((1, 2, 0))
            network_prediction = self.to_tensor(self.predict(augmented_image))[None, :]
            tta_predictions.append(transformer.deaugment_mask(network_prediction).numpy().squeeze())

        return tta_predictions

    def predict_with_uncertainty(self, image: np.ndarray, unpaired_user_prediction_mask: np.ndarray = None):
        self.model.eval()
        predicted_label = self.predict(image)

        if self.uncertainty_method == "mc":
            uncertainty_predictions = self._compute_uncertainty_predictions_mc(image)
        elif self.uncertainty_method == "tta":
            uncertainty_predictions = self._compute_uncertainty_predictions_tta(image)
        else:
            raise ValueError(f"The given uncertainty method {self.uncertainty_method} is not supported!")

        uncertainty = self.compute_instance_wise_uncertainty(
            predicted_label,
            uncertainty_predictions,
            unpaired_user_prediction_mask
        )

        return predicted_label, *uncertainty
