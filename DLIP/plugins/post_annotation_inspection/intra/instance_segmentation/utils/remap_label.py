import numpy as np
from scipy import ndimage


def remap_label(mask: np.ndarray, by_size=False, multiple_label_usage=False):
    """
    Rename all instance id so that the id is contiguous i.e [0, 1, 2, 3]
    not [0, 2, 4, 6]. The ordering of instances (which one comes first)
    is preserved unless by_size=True, then the instances will be reordered
    so that bigger nuclei have smaller IDs.

    Implementation based on https://github.com/vqdang/hover_net/blob/master/metrics/stats_utils.py

    Args:
        mask    : the 2d array contain instances where each instances is marked
                  by non-zero integer
        by_size : renaming with larger nuclei has smaller id (on-top)
        multiple_label_usage: defines if multiple usages of a label idx are allowed (or treated as errors)
    """

    if len(mask.shape) == 3:
        mask = mask.squeeze()

    mask_id = list(np.unique(mask))

    if 0 in mask_id:
        mask_id.remove(0)

    if len(mask_id) == 0:
        return mask  # no label

    mask = mask.copy()

    if not multiple_label_usage:
        # jitter that has the same number as another object, is labeled as its own instance here
        for idx in mask_id:
            labeled_array, num_of_objects = ndimage.label(mask == idx, structure=[[1, 1, 1], [1, 1, 1], [1, 1, 1]])

            if num_of_objects > 1:
                for new_idx in range(1, num_of_objects + 1):
                    if new_idx == 1:
                        mask[labeled_array == new_idx] = idx
                    else:
                        mask[labeled_array == new_idx] = max(mask_id) + 1
                        mask_id.append(max(mask_id) + 1)

    if by_size:
        mask_size = []

        for inst_id in mask_id:
            size = (mask == inst_id).sum()
            mask_size.append(size)

        # sort the id by size in descending order
        pair_list = zip(mask_id, mask_size)
        pair_list = sorted(pair_list, key=lambda x: x[1], reverse=True)
        mask_id, mask_size = zip(*pair_list)

    new_mask = np.zeros(mask.shape, np.int32)

    for idx, inst_id in enumerate(mask_id):
        new_mask[mask == inst_id] = idx + 1

    return new_mask
