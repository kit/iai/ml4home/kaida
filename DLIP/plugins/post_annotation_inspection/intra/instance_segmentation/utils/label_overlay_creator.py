import cv2
import numpy as np


class LabelOverlayCreator:
    def __init__(self, image_original: np.ndarray):
        image: np.ndarray = image_original.copy().squeeze()

        if len(image.shape) == 2:
            image = np.stack([image] * 3, axis=-1)
        else:
            for i in range(3):
                image[:, :, i] = np.mean(image_original.squeeze(), axis=2)
        image = (((image - np.min(image)) / (np.max(image) - np.min(image))) * 255).astype(np.uint8)

        self.image = image

        self.missing_mask = None
        self.additional_mask = None
        self.good_mask = None
        self.selected_instance_mask = None
        self.given_overlay = None

    def set_missing_mask(self, mask: np.ndarray):
        self.missing_mask = mask

    def set_additional_mask(self, mask: np.ndarray):
        self.additional_mask = mask

    def set_good_mask(self, mask: np.ndarray):
        self.good_mask = mask

    def set_selected_instance_mask(self, mask: np.ndarray):
        self.selected_instance_mask = mask

    def add_selected_instance_mask(self, mask: np.ndarray):
        if self.selected_instance_mask is None:
            self.selected_instance_mask = mask
        else:
            self.selected_instance_mask[mask != 0] = mask[mask != 0]

    def set_overlay(self, overlay: np.ndarray):
        if np.max(overlay) > 1:
            overlay /= np.max(overlay)
        self.given_overlay = overlay

    def get_overlay(self):
        if self.given_overlay is not None:
            return self.given_overlay

        overlay = np.zeros_like(self.image)

        if self.missing_mask is not None:
            # missing parts in red
            overlay[self.missing_mask != 0, 0] = 1

        if self.additional_mask is not None:
            # additional parts in purple
            overlay[self.additional_mask != 0, 0] = 1
            overlay[self.additional_mask != 0, 2] = 1

        if self.good_mask is not None:
            # good parts in green
            overlay[self.good_mask != 0, 1] = 1

        if self.selected_instance_mask is not None:
            # selected instance in white
            overlay[self.selected_instance_mask != 0, :] = 1

        return overlay

    def get_image_with_overlay(self):
        return cv2.addWeighted(self.image, 0.7, (self.get_overlay() * 255).astype(np.uint8), 0.3, 0)
