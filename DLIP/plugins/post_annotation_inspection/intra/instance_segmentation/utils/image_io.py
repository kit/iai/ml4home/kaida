from pathlib import Path
from typing import Union

import cv2
import numpy as np
import tifffile


def load_image(name: Union[str, Path], file_format: str):
    return tifffile.imread(name) if file_format in ["tif", "tiff"] else cv2.imread(str(name), -1)


def save_image(name: Union[str, Path], file_format: str, data: np.ndarray):
    if file_format in ["tif", "tiff"]:
        tifffile.imwrite(name, data)
    else:
        cv2.imwrite(name, data)
