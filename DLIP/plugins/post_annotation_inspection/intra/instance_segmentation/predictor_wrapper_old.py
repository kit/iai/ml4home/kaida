from typing import Dict, Union, Tuple, Callable
from typing import List

import numpy as np
import torch
import torch.nn as nn

from DLIP.models.zoo.compositions.base_composition import BaseComposition
from DLIP.models.zoo.decoder.unet_decoder import UnetDecoder
from DLIP.models.zoo.encoder.unet_encoder import UnetEncoder
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.matching import InstanceMatching, \
    MatchingResults
from DLIP.utils.post_processing.distmap2inst import DistMapPostProcessor


class UnetInstSegSupervised(BaseComposition):

    def __init__(
            self,
            input_channels: int,
            loss_fcn: nn.Module,
            encoder_filters: List = [64, 128, 256, 512, 1024],
            decoder_filters: List = [512, 256, 128, 64],
            dropout_encoder: float = 0.0,
            dropout_decoder: float = 0.0,
    ):
        super().__init__()
        self.loss_fcn = loss_fcn
        bilinear = False
        self.append(UnetEncoder(
            input_channels=input_channels,
            encoder_filters=encoder_filters,
            dropout=dropout_encoder,
            bilinear=bilinear
        ))
        self.append(UnetDecoder(
            n_classes=1,
            encoder_filters=encoder_filters,
            decoder_filters=decoder_filters,
            dropout=dropout_decoder,
            billinear_downsampling_used=bilinear
        ))

    def training_step(self, batch, batch_idx):
        x, y_true = batch
        y_true = y_true.permute(0, 3, 1, 2)
        y_pred = self.forward(x)
        loss_n_c = self.loss_fcn(y_pred, y_true)  # shape NxC
        loss = torch.mean(loss_n_c)
        self.log("train/loss", loss, prog_bar=True)
        return loss

    def validation_step(self, batch, batch_idx):
        x, y_true = batch
        y_true = y_true.permute(0, 3, 1, 2)
        y_pred = self.forward(x)
        loss_n_c = self.loss_fcn(y_pred, y_true)
        loss = torch.mean(loss_n_c)
        self.log("val/loss", loss, prog_bar=True, on_epoch=True)
        return loss

    def test_step(self, batch, batch_idx):
        x, y_true = batch
        y_true = y_true.permute(0, 3, 1, 2)
        y_pred = self.forward(x)
        loss_n_c = self.loss_fcn(y_pred, y_true)
        loss = torch.mean(loss_n_c)
        self.log("test/loss", loss, prog_bar=True, on_epoch=True, on_step=False)
        return loss



class Predictor:
    def __init__(
            self,
            model: UnetInstSegSupervised,
            transformations: Callable = None,
            num_of_monte_carlo_predictions: int = 3
    ):
        self.model = model
        self.transformations = transformations

        self.model.eval()
        self.model.double()

        self.dist_map_post_processor = DistMapPostProcessor(
            sigma_cell=1.00,
            th_cell=0.09,
            th_seed=0.37,
            do_splitting=True,
            do_area_based_filtering=True,
            do_fill_holes=True,
            valid_area_median_factors=[0.25, 3]
        )

        self.num_of_monte_carlo_predictions = num_of_monte_carlo_predictions

    @staticmethod
    def from_checkpoint_path(
            checkpoint_path: str,
            transformations: Callable = None,
            num_of_monte_carlo_predictions: int = 3
    ):
        return Predictor(UnetInstSegSupervised.load_from_checkpoint(
                checkpoint_path,
                input_channels=3,
                loss_fcn="SmoothL1Loss",
                dropout_decoder=[0.3, 0.2, 0.1, 0.0],
                dropout_encoder=[0, 0.1, 0.2, 0.3, 0.3]
            ),
            transformations=transformations,
            num_of_monte_carlo_predictions=num_of_monte_carlo_predictions
        )

    @torch.no_grad()
    def predict(self, image: np.ndarray):
        if self.transformations is not None:
            image = self.transformations(image)[0]
            image = image.unsqueeze(0)
        else:
            image = torch.tensor(image.transpose((2, 0, 1)))[None, :, :, :].double()

        image = image.to(self.model.device)

        prediction = self.model.forward(image)[0, 0].detach().numpy()

        prediction_label = self.dist_map_post_processor.process(prediction, image)

        return prediction_label

    @staticmethod
    def calculate_uncertainty_for_given_instances(
            instance_ids: Union[List[int], np.ndarray],
            prediction_ids_for_results: List[int],
            original_prediction_pairings: List[Dict[int, List[int]]],
            matching_results: List[MatchingResults]
    ):
        instance_iou = dict()

        # 1 / len(results) is the same as 2 / (T * (T - 1)) with T = len(images)
        pre_factor_denominator = len(matching_results)

        for instance_id in instance_ids:
            instance_iou[instance_id] = 1

            iou_sum = 0

            for image_id, result in zip(prediction_ids_for_results, matching_results):
                if instance_id not in original_prediction_pairings[image_id]:
                    continue

                for i, instance_mapping_id in enumerate(original_prediction_pairings[image_id][instance_id]):
                    iou_sum += result.get_iou_of_user_instance(instance_mapping_id)
                    if i > 0:
                        pre_factor_denominator += 1

            instance_iou[instance_id] -= 1 / pre_factor_denominator * iou_sum

        return instance_iou

    @staticmethod
    def compute_instance_wise_uncertainty(
            original_prediction: np.ndarray,
            monte_carlo_predictions: List[np.ndarray],
            unpredicted_instances_image: np.ndarray = None
    ) -> Tuple[Dict[int, float], Dict[int, float]]:
        results = []
        original_prediction_pairings = []
        unpredicted_instances_pairings = []
        prediction_ids_for_results = []

        for i, prediction_i in enumerate(monte_carlo_predictions):
            original_prediction_pairings.append(
                InstanceMatching.match(original_prediction, prediction_i).paired_instance_ids
            )

            if unpredicted_instances_image is not None:
                unpredicted_instances_pairings.append(
                    InstanceMatching.match(unpredicted_instances_image, prediction_i).paired_instance_ids
                )

            for j in range(i + 1, len(monte_carlo_predictions)):
                prediction_ids_for_results.append(i)
                results.append(InstanceMatching.match(prediction_i, monte_carlo_predictions[j]))

        instance_ids = np.delete(np.unique(InstanceMatching.remap_label(original_prediction)), 0)

        if unpredicted_instances_image is not None:
            unpredicted_instance_ids = np.delete(
                np.unique(InstanceMatching.remap_label(unpredicted_instances_image)), 0
            )

        instance_uncertainty = Predictor.calculate_uncertainty_for_given_instances(
            instance_ids,
            prediction_ids_for_results,
            original_prediction_pairings,
            results
        )

        if unpredicted_instances_image is not None:
            unpredicted_instance_uncertainty = Predictor.calculate_uncertainty_for_given_instances(
                unpredicted_instance_ids,
                prediction_ids_for_results,
                unpredicted_instances_pairings,
                results
            )

            # here the uncertainty is one minus calculated uncertainty, as the network didn't predict the images
            unpredicted_instance_uncertainty = {
                key: 1 - unpredicted_instance_uncertainty[key] for key in unpredicted_instance_uncertainty.keys()
            }
        else:
            unpredicted_instance_uncertainty = dict()

        return instance_uncertainty, unpredicted_instance_uncertainty

    def predict_with_uncertainty(self, image: np.ndarray, unpaired_user_prediction_mask: np.ndarray = None):

        def apply_dropout(m):
            if type(m) == torch.nn.Dropout or type(m) == torch.nn.Dropout2d:
                m.train()

        self.model.eval()

        predicted_label = self.predict(image)

        self.model.apply(apply_dropout)

        dropout_images = list()

        for _ in range(self.num_of_monte_carlo_predictions):
            dropout_images.append(self.predict(image))

        uncertainty = self.compute_instance_wise_uncertainty(
            predicted_label,
            dropout_images,
            unpaired_user_prediction_mask
        )

        self.model.eval()

        return predicted_label, *uncertainty


if __name__ == '__main__':
    import pickle
    from pathlib import Path

    import tifffile

    from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InstancePerImageRater

    def load_image(path):
        img = tifffile.imread(path)
        img = np.squeeze(img)

        img = (img - np.min(img)) / (np.max(img) - np.min(img))

        return img

    base_path = Path("~/Documents/data").expanduser()

    img_names = [
        "TCGA-18-5592-01Z-00-DX1_000.tif",
        "TCGA-18-5592-01Z-00-DX1_001.tif"
    ]

    for img_name in img_names:
        image_path = base_path.joinpath("2018_MoNuSeg_cropped/train/samples/" + img_name)
        user_mask_path = base_path.joinpath("2018_MoNuSeg_cropped/train_forget_gamma_1p0_beta_0p1/labels/" + img_name)
        model_path = base_path.joinpath("dnn_weights.ckpt")
        out_path = base_path.joinpath("kaida_test/anno_inspection")

        original_image = load_image(image_path)
        user_prediction = load_image(user_mask_path)  # ground truth

        model = Predictor.from_checkpoint_path(str(model_path))

        rater = InstancePerImageRater.create_using_network(
            original_image,
            user_prediction,
            model
        )

        with open(out_path.joinpath(img_name.replace(".tif", ".pkl")), "wb") as f:
            pickle.dump(rater, f)
