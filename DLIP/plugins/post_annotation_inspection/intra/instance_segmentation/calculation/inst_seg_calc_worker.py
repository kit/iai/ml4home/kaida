from pathlib import Path

from PyQt5.QtCore import QObject, pyqtSignal

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.predictor_wrapper import Predictor
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InstancePerImageRater
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.utils import load_image


class InstSegCalcWorker(QObject):
    finished = pyqtSignal()
    idx = pyqtSignal(int)

    def __init__(self, data_module, predictor: Predictor, calc_path):
        super(InstSegCalcWorker, self).__init__()
        self.data_module = data_module
        self.predictor = predictor
        self.calc_path = calc_path

    def run(self):
        for idx in range(len(self.data_module.labeled_train_dataset)):
            self.compute(idx)
            self.idx.emit(idx)

        self.finished.emit()

    def compute(self, idx):
        name = self.data_module.labeled_train_dataset.indices[idx]
        out_file = Path(self.calc_path).joinpath(f"{name}.pkl")

        if out_file.exists():
            # just calculate new ones
            return

        # data_module.labeled_train_dataset would use the dist map (and requires it) not the real label
        # therefore not use it to load the images

        image_name = str(Path(self.data_module.labeled_train_dataset.samples).joinpath(
            f"{name}.{self.data_module.samples_data_format}"
        ))

        label_name = str(Path(self.data_module.labeled_train_dataset.labels).joinpath(
            f"{self.data_module.label_prefix}{name}"
            f"{self.data_module.label_suffix}.{self.data_module.labels_data_format}"
        ))

        img = load_image(image_name, self.data_module.samples_data_format)
        noisy_anno = load_image(label_name, self.data_module.labels_data_format)

        InstancePerImageRater.create_using_network(img, noisy_anno, self.predictor).save(out_file)
