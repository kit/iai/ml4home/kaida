from typing import Callable

from PyQt5.QtCore import QThread

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.calculation import InstSegCalcWorker
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.predictor_wrapper import Predictor


class InstSegCalculator:
    def __init__(self, output_slot: Callable, enable_ui_slot: Callable):
        self.inst_seg_calc_worker = None
        self.inst_seg_calc_thread = None
        self.current_data_len = 0

        self.print_to_ui = output_slot
        self.set_ui_enabled = enable_ui_slot

    def start_calculation(self, model, trafos, data_module, calc_path):
        self.print_to_ui("Starting calculation ...")
        self.set_ui_enabled(False)
        self.current_data_len = len(data_module.labeled_train_dataset)

        self.inst_seg_calc_worker = InstSegCalcWorker(data_module, Predictor(model, trafos[0], 10), calc_path)
        self.inst_seg_calc_thread = QThread()

        self.inst_seg_calc_worker.moveToThread(self.inst_seg_calc_thread)

        self.inst_seg_calc_thread.started.connect(self.inst_seg_calc_worker.run)
        self.inst_seg_calc_worker.finished.connect(self.inst_seg_calc_thread.quit)
        self.inst_seg_calc_worker.finished.connect(self.finished)
        self.inst_seg_calc_worker.idx.connect(self.update_ui)

        self.inst_seg_calc_thread.start()

    def update_ui(self, idx):
        self.print_to_ui(f"Calculation progress: {idx + 1}/{self.current_data_len}")

    def finished(self):
        self.set_ui_enabled(True)
        self.print_to_ui("Calculation finished")
