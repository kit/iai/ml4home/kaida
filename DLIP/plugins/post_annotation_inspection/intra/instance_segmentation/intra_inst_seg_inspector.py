from os import remove as remove_file
from pathlib import Path
from typing import Optional, List

import numpy as np
import pandas as pd
import tifffile
from PyQt5.QtCore import Qt, QCoreApplication, QThread
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QLabel, QMessageBox, QScrollArea, QWidget, QPushButton, QStackedWidget
from skimage import io

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.gui_parts import (
    InstanceSelection, ProgressWidget, ThresholdSliderGroup, ImageLabelGroup, SelectedInstanceOptions,
    create_box_layout
)
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.gui_parts.loading_widget import LoadingWidget
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.gui_parts.relabel_dialog import RelabelDialog
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.gui_parts.worker.image_resort_worker import \
    ImageResortWorker
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InspectionRatingWrapper
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import Instance
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.utils import load_image, save_image


class IntraInstanceSegmentationInspector(QDialog):
    def __init__(self, path_project, parent_obj, data_frame, calculation_results_path):
        super(IntraInstanceSegmentationInspector, self).__init__(flags=Qt.Window)

        self.parent_obj = parent_obj
        self.data_frame = data_frame
        self.calculation_results_path = calculation_results_path

        self.images_widget: Optional[ImageLabelGroup] = None
        self.overall_score_label: Optional[QLabel] = None
        self.inspection_done_label: Optional[QLabel] = None
        self.sliders: Optional[ThresholdSliderGroup] = None
        self.odd_instances_widget: Optional[InstanceSelection] = None
        self.selected_instance_options: Optional[SelectedInstanceOptions] = None
        self.progress_widget: Optional[ProgressWidget] = None
        self.scroll_area: Optional[QScrollArea] = None
        self.resort_button: Optional[QPushButton] = None
        self.loading_widget: Optional[LoadingWidget] = None

        self.resort_images_worker: Optional[ImageResortWorker] = None
        self.resort_images_thread: Optional[QThread] = None

        self._init_ui()

    @staticmethod
    def load_from_precomputed_calculations(
            path_project,
            parent_obj,
            calculation_results_path,
            uncertainty_weighting_param: float = 0,
            boundary_weighting_param: float = 0
    ) -> "IntraInstanceSegmentationInspector":
        df = pd.DataFrame(columns=["name", "rater", "score"])

        for i in range(len(parent_obj.data_module.labeled_train_dataset)):
            name = parent_obj.data_module.labeled_train_dataset.indices[i]

            rater_file = Path(calculation_results_path).joinpath(f"{name}.pkl")
            rater = InspectionRatingWrapper.load(rater_file)
            rater.set_oddness_th(1.0)
            rater.set_missing_instances_th(1.0)
            rater.set_additional_instances_th(1.0)
            rater.set_uncertainty_weighting_param(uncertainty_weighting_param)
            rater.set_boundary_weighting_param(boundary_weighting_param)

            df = df.append({
                "name": name,
                "rater": rater,
                "score": rater.get_overall_score(),
                "inspection_done": int(rater.is_inspection_done())
            }, ignore_index=True)

        df = df.sort_values(by=["inspection_done", "score"], ascending=[False, True])

        return IntraInstanceSegmentationInspector(path_project, parent_obj, df, calculation_results_path)

    def _get_current_rater(self) -> InspectionRatingWrapper:
        return self.data_frame["rater"].iloc[self.progress_widget.current_image_idx]

    def _get_current_original_image(self) -> np.ndarray:
        file_name = self._get_image_file_name(self.progress_widget.current_image_idx)
        return load_image(file_name, self.parent_obj.data_module.labeled_train_dataset.samples_data_format)

    def _get_fist_uninspected_image(self):
        idxs = np.argwhere(np.array(self.data_frame["inspection_done"]) != 1)

        if len(idxs) == 0:
            return -1

        return idxs[0][0]

    def _init_ui(self) -> None:
        self.progress_widget = ProgressWidget(
            self.data_frame.shape[0],
            self._image_changed,
            num_of_already_inspected_images=np.sum(self.data_frame["inspection_done"]).item(),
            current_image_idx=self._get_fist_uninspected_image()
        )

        self.images_widget = ImageLabelGroup(self._get_current_original_image(), self._get_current_rater())

        self.sliders = ThresholdSliderGroup(self._get_current_rater(), self._sliders_changed)

        self.overall_score_label = QLabel()
        self.overall_score_label.setText(
            f"Overall Score: {self._get_current_rater().get_overall_score()}")

        self.inspection_done_label = QLabel()
        self.inspection_done_label.setText(
            "\u2713 Inspection done" if self._get_current_rater().is_inspection_done()
            else "\u2717 Inspection not done"
        )

        self.odd_instances_widget = InstanceSelection(
            "Incorrectly Annotated Instances",
            self._get_current_rater().get_wrong_instances(),
            self._instance_selected
        )

        self.selected_instance_options = SelectedInstanceOptions(
            self._get_current_rater(),
            self._reload_ui,
            self._relabel_image
        )

        self.resort_button = QPushButton("Resort Images")
        self.resort_button.pressed.connect(self.resort_images)

        layout = QVBoxLayout()
        layout.addWidget(self.images_widget)
        layout.addWidget(create_box_layout([self.selected_instance_options, self.odd_instances_widget], "horizontal"))
        layout.addWidget(self.overall_score_label)
        layout.addWidget(self.inspection_done_label)
        layout.addWidget(self.progress_widget)
        layout.addWidget(self.sliders)
        layout.addWidget(self.resort_button)

        self.enable_inputs(not self._get_current_rater().is_disabled(), rater_disabled_mode=True)

        widget = QWidget()
        widget.setLayout(layout)

        self.scroll_area = QScrollArea()
        self.scroll_area.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.scroll_area.setHorizontalScrollBarPolicy(Qt.ScrollBarAsNeeded)
        self.scroll_area.setWidgetResizable(True)
        self.scroll_area.setWidget(widget)

        self.loading_widget = LoadingWidget()

        self.stacked_widgets = QStackedWidget()
        self.stacked_widgets.addWidget(self.scroll_area)
        self.stacked_widgets.addWidget(self.loading_widget)

        main_layout = QVBoxLayout()
        main_layout.addWidget(self.stacked_widgets)

        self.setWindowTitle("Intra Instance Inspection")
        self.setLayout(main_layout)
        self.resize(
            int(QCoreApplication.instance().desktop().screenGeometry().width() / 4) * 3.6,
            int(QCoreApplication.instance().desktop().screenGeometry().height() * 0.8)
        )
        self.update()

    def _save_changed_label(self, rater: InspectionRatingWrapper, idx: int) -> None:
        self.data_frame["inspection_done"].iat[idx] = 1
        self.data_frame["score"].iat[idx] = rater.get_overall_score()
        self.progress_widget.img_saved(
            num_of_already_inspected_images=np.sum(self.data_frame["inspection_done"]).item()
        )

        # save changed rating
        rater.save(Path(self.calculation_results_path).joinpath(f"{self.data_frame['name'].iloc[idx]}.pkl"))

        user_label = rater.get_user_annotated_label()
        image_file_name = self._get_label_file_name(idx)

        # save changed label
        if self.parent_obj.data_module.samples_data_format == "tif":
            tifffile.imwrite(image_file_name, user_label)
        else:
            io.imsave(str(image_file_name), user_label)

    def _save_label(self, idx) -> bool:
        rater: InspectionRatingWrapper = self.data_frame["rater"].iloc[idx]

        if not rater.is_inspection_updated():
            # if there is nothing new to save, don't ask about it
            return False

        reply = QMessageBox.question(
            self, "Quit", "Do you want to save the changed annotation?",
            QMessageBox.Yes | QMessageBox.No,
            QMessageBox.No
        )

        if reply == QMessageBox.Yes:
            self._save_changed_label(rater, idx)
            return True
        else:
            # reload the original unchanged rating
            rating_file_name = Path(self.calculation_results_path).joinpath(f"{self.data_frame['name'].iloc[idx]}.pkl")
            self.data_frame["rater"].iat[idx] = InspectionRatingWrapper.load(rating_file_name)
            return False

    def _save_old_label(self) -> None:
        idx = self.progress_widget.last_image_idx
        self._save_label(idx)

    def _image_changed(self, already_saved_label: bool = False) -> None:
        if not already_saved_label:
            self._save_old_label()

        rater = self._get_current_rater()
        self.sliders.set_rater(rater)
        self.selected_instance_options.set_rater(rater)
        self.images_widget.change_image(self._get_current_original_image(), rater)
        self.enable_inputs(not rater.is_disabled(), rater_disabled_mode=True)
        self._reload_ui()
        self.scroll_area.verticalScrollBar().setValue(0)

    def _reload_ui(self, selected_instances: List[Instance] = None, label_changed: bool = False) -> None:
        self.images_widget.reload_label_overlay(selected_instances)
        self.selected_instance_options.instances_selected(selected_instances)

        self.inspection_done_label.setText(
            "\u2713 Inspection done" if self._get_current_rater().is_inspection_done()
            else "\u2717 Inspection not done"
        )

        if label_changed:
            self.images_widget.reload_labels()

        self.overall_score_label.setText(f"Overall Score: {self._get_current_rater().get_overall_score()}")

        if selected_instances is None:
            self.odd_instances_widget.set_items(self._get_current_rater().get_wrong_instances())

    def _sliders_changed(self) -> None:
        self._reload_ui()
        self.odd_instances_widget.clear_selection()

    def _instance_selected(self, instances: List[Instance]) -> None:
        self._reload_ui(instances)

    def disable_inputs(self):
        self.enable_inputs(False)

    def enable_inputs(self, mode: bool = True, rater_disabled_mode: bool = False):
        self.selected_instance_options.enable_inputs(mode, rater_disabled_mode)
        self.odd_instances_widget.enable_inputs(mode)
        self.sliders.enable_inputs(mode)

        if not rater_disabled_mode:
            self.progress_widget.enable_inputs(mode)

    def _get_image_file_name(self, idx, use_temp_folder: bool = False):
        if not use_temp_folder:
            return Path(self.parent_obj.data_module.labeled_train_dataset.samples).joinpath(
                f"{self.data_frame['name'].iloc[idx]}.{self.parent_obj.data_module.samples_data_format}"
            )
        else:
            return Path(self.parent_obj.project["data_dir"]).joinpath("temp").joinpath(
                f"{self.data_frame['name'].iloc[idx]}.{self.parent_obj.data_module.samples_data_format}"
            )

    def _get_label_file_name(self, idx):
        return Path(self.parent_obj.data_module.labeled_train_dataset.labels).joinpath(
            f"{self.parent_obj.data_module.label_prefix}{self.data_frame['name'].iloc[idx]}"
            f"{self.parent_obj.data_module.label_suffix}.{self.parent_obj.data_module.labels_data_format}"
        )

    def _relabel_image(self) -> None:
        idx = self.progress_widget.current_image_idx
        image_file_name = self._get_image_file_name(idx, use_temp_folder=True)
        label_file_name = self._get_label_file_name(idx)

        current_image = self._get_current_original_image().astype(np.float64)
        current_image = current_image - np.min(current_image)
        current_image = (current_image / np.max(current_image) * 255).astype(np.uint8)

        save_image(image_file_name, image_file_name.name.split(".")[-1], current_image)

        rater = self._get_current_rater()

        if rater.is_inspection_updated():
            # save label first if it was already changed before
            self._save_changed_label(rater, idx)

        RelabelDialog(self, str(image_file_name), str(label_file_name))
        self.disable_inputs()

    def relabeled_image(self, label_img: np.ndarray) -> None:
        idx = self.progress_widget.current_image_idx

        image_file_name = self._get_image_file_name(idx, use_temp_folder=True)
        remove_file(image_file_name)

        rater = self._get_current_rater()
        rater.use_relabeled_image(label_img)

        self._save_changed_label(rater, idx)
        self._reload_ui(label_changed=True)

    def relabel_image_finished(self):
        self.enable_inputs()
        self.enable_inputs(False, rater_disabled_mode=True)

    def resort_images(self):
        label_saved = self._save_label(self.progress_widget.current_image_idx)

        self.loading_widget.set_loading_test("Resorting images. This may take a while...")
        self.stacked_widgets.setCurrentIndex(1)

        self.resort_images_worker = ImageResortWorker(self.data_frame, self.sliders, label_saved)
        self.resort_images_thread = QThread()
        self.resort_images_worker.moveToThread(self.resort_images_thread)

        self.resort_images_thread.started.connect(self.resort_images_worker.run)
        self.resort_images_worker.finished.connect(self.resort_images_thread.quit)
        self.resort_images_worker.progress.connect(self.loading_widget.update_progress_bar)
        self.resort_images_worker.finished_info.connect(self._resort_finished)
        self.resort_images_thread.start()

    def _resort_finished(self, args: tuple) -> None:
        self.data_frame, label_saved = args

        if label_saved:
            self.progress_widget.next(emit_signal=False)

        self._image_changed(already_saved_label=True)

        self.stacked_widgets.setCurrentIndex(0)
        self.scroll_area.verticalScrollBar().setValue(0)

    def closeEvent(self, _):
        self._save_label(self.progress_widget.current_image_idx)
