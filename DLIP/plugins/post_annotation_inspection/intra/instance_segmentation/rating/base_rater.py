from typing import List, Dict

import numpy as np

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.matching import MatchingResults
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import (
    InstanceType, InstanceSource
)


class BaseInstancePerImageRater:
    def __init__(
            self,
            matching_results: MatchingResults,
            network_uncertainty: Dict[int, float],
            network_uncertainty_on_additional_user_instances: Dict[int, float],
            uncertainty_weighting_param: float = 0,
            boundary_weighting_param: float = 0,
            add_miss_size_weighting_param: float = 0,
            reference_instance_size: int = None,
            boundary_region_percentage: float = 0.05,
            oddness_th: float = 1,
            additional_instances_th: float = 1,
            missing_instances_th: float = 1
    ):
        """

        Args:
            original_image: Original image as ndarray
            user_annotated_label: User annotated label image as ndarray
            instance_segmentation_network: Trained network to predict an instance segmentation for the given image
            uncertainty_weighting_param: Weighting of the uncertainty for calculating instance scores
            boundary_weighting_param: Weighting of the border region for calculating instance scores.
                                      Weighting of 1 means that instances in the border region are not complained.
            reference_instance_size: References size used in the rating of missing and additional user instances
            boundary_region_percentage: Percentage of the image the boundary region occupies on each image side
        """

        self.matching_results = matching_results
        self.network_uncertainty = network_uncertainty
        self.network_uncertainty_on_additional_user_instances = network_uncertainty_on_additional_user_instances

        self.uncertainty_weighting_param = uncertainty_weighting_param

        self.add_miss_size_weighting_param = add_miss_size_weighting_param
        self.reference_instance_size = reference_instance_size
        if reference_instance_size is None:
            self.reference_instance_size = self._get_biggest_instance_size()

        self.boundary_weighting_param = boundary_weighting_param
        self.boundary_region = None
        self._initialize_boundary_region(boundary_region_percentage)

        self.oddness_th: float = oddness_th
        self.additional_instances_th: float = additional_instances_th
        self.missing_instances_th: float = missing_instances_th

    def _initialize_boundary_region(self, percentage: float) -> None:
        dim_0, dim_1 = self.get_user_annotated_label().shape
        dim_0_boundary = int(dim_0 * percentage)
        dim_1_boundary = int(dim_1 * percentage)

        self.boundary_region = np.zeros_like(self.get_user_annotated_label())
        self.boundary_region[dim_0_boundary:-dim_0_boundary, dim_1_boundary:-dim_1_boundary] = 1
        self.boundary_region = np.invert(self.boundary_region != 0).astype(int)

    def set_oddness_th(self, oddness_th: float) -> None:
        self.oddness_th = oddness_th

    def set_additional_instances_th(self, additional_instances_th: float) -> None:
        self.additional_instances_th = additional_instances_th

    def set_missing_instances_th(self, missing_instances_th: float) -> None:
        self.missing_instances_th = missing_instances_th

    def set_boundary_weighting_param(self, boundary_weighting_param: float) -> None:
        self.boundary_weighting_param = boundary_weighting_param

    def set_uncertainty_weighting_param(self, uncertainty_weighting_param: float) -> None:
        self.uncertainty_weighting_param = uncertainty_weighting_param

    def set_add_miss_size_weighting_param(self, add_miss_size_weighting_param: float) -> None:
        self.add_miss_size_weighting_param = add_miss_size_weighting_param
        self.reference_instance_size = self._get_biggest_instance_size()

    def get_user_annotated_label(self) -> np.ndarray:
        return self.matching_results.user_annotated_label

    def get_network_prediction(self) -> np.ndarray:
        return self.matching_results.network_prediction

    def _is_instance_in_boundary_region(self, instance_id: int, instance_source: InstanceSource) -> bool:
        if instance_source == InstanceSource.NETWORK:
            instance_label_map = self.matching_results.get_network_instance_label_map(instance_id)
        else:
            instance_label_map = self.matching_results.get_user_instance_label_map(instance_id)

        intersection = np.logical_and(self.boundary_region != 0, instance_label_map != 0)

        return np.sum(intersection) / np.sum(instance_label_map != 0) > 0.99

    def _get_user_instance_iou(self, instance_id) -> List[float]:
        if instance_id in self.matching_results.paired_instances_iou_dict.keys():
            ious = np.array(self.matching_results.paired_instances_iou_dict[instance_id])
            ious *= (1 - self.uncertainty_weighting_param)

            uncertainty = [
                self.uncertainty_weighting_param * self.network_uncertainty[key]
                for key in self.matching_results.paired_instance_ids[instance_id]
            ]

            weighted_ious = ious + uncertainty

            if self._is_instance_in_boundary_region(instance_id, InstanceSource.USER):
                weighted_ious = (
                        (1 - self.boundary_weighting_param) * weighted_ious
                        + self.boundary_weighting_param * 1
                )

            return weighted_ious
        return [0.]

    def _get_biggest_instance_size(self, eps: float = 10**-12) -> float:
        values = self.matching_results.get_user_annotation_ids()

        overall_max_size = 0
        unpaired_max_size = 0

        for instance_id in values:
            size = np.sum(self.get_user_annotated_label() == instance_id)

            if size > overall_max_size:
                overall_max_size = size

            if instance_id in self.matching_results.unpaired_user_annotations and size > unpaired_max_size:
                unpaired_max_size = size

        for instance_id in self.matching_results.unpaired_network_predictions:
            size = np.sum(self.get_network_prediction() == instance_id)

            if size > unpaired_max_size:
                unpaired_max_size = size
            if size > overall_max_size:
                overall_max_size = size

        return (
                (1 - self.add_miss_size_weighting_param) * overall_max_size
                + self.add_miss_size_weighting_param * unpaired_max_size
                + eps
        )

    def _get_network_ids_of_odd_instances(self):
        pairwise_iou_network_prediction = np.array(self.matching_results.pairwise_iou_network_prediction)
        pairwise_iou_network_prediction *= 1 - self.uncertainty_weighting_param

        pairwise_uncertainty = np.array([
            self.network_uncertainty[idx + 1] * self.uncertainty_weighting_param
            for idx in range(len(pairwise_iou_network_prediction))
        ])

        weighted_iou = pairwise_iou_network_prediction + pairwise_uncertainty

        for instance_id in self.matching_results.paired_network_prediction:
            if self._is_instance_in_boundary_region(instance_id, InstanceSource.NETWORK):
                weighted_iou[instance_id - 1] = (
                        (1 - self.boundary_weighting_param) * weighted_iou[instance_id - 1]
                        + self.boundary_weighting_param * 1
                )

        odd_instances = np.nonzero(weighted_iou <= self.oddness_th)[0]
        return np.array([
            idx + 1 for idx in odd_instances if idx + 1 not in self.matching_results.unpaired_network_predictions
        ])

    def _get_odd_instance_ids(self):
        odd_instances = self._get_network_ids_of_odd_instances()

        if len(odd_instances) == 0:
            return []

        return self.matching_results.paired_user_annotation[
            np.logical_or.reduce([
                self.matching_results.paired_network_prediction == instance_id for instance_id in odd_instances
            ])
        ]  # index to instance ID

    def _get_odd_instances_iou(self):
        odd_instances = self._get_odd_instance_ids()

        return np.array([
            self._get_user_instance_iou(odd_instance_id)
            for odd_instance_id in odd_instances
        ], dtype=object)

    def _get_missing_or_additional_instance_score(self, instance_id: int, instance_type: InstanceType):
        reference_image = (
            self.get_network_prediction() if instance_type == InstanceType.MISSING
            else self.get_user_annotated_label()
        )

        instance_score = (
                (1 - self.uncertainty_weighting_param)
                * (1 - np.sum(reference_image == instance_id) / self.reference_instance_size)
        )

        if instance_type == InstanceType.ADDITIONAL:
            instance_uncertainty = (
                    self.uncertainty_weighting_param
                    * self.network_uncertainty_on_additional_user_instances[instance_id]
            )
            instance_source = InstanceSource.USER
        elif instance_type == InstanceType.MISSING:
            instance_uncertainty = (
                    self.uncertainty_weighting_param
                    * self.network_uncertainty[instance_id]
            )
            instance_source = InstanceSource.NETWORK
        else:
            raise ValueError(f"Expected missing or additional instance type, but got {instance_type}!")

        weighted_score = instance_score + instance_uncertainty

        if self._is_instance_in_boundary_region(instance_id, instance_source):
            weighted_score = (
                    (1 - self.boundary_weighting_param) * weighted_score
                    + self.boundary_weighting_param * 1
            )

        return weighted_score

    def _get_additional_instance_ids(self) -> List[int]:
        instance_ids = self.matching_results.get_additional_instances()
        final_ids = list()

        for instance_id in instance_ids:
            if (
                self._get_missing_or_additional_instance_score(instance_id, InstanceType.ADDITIONAL)
                <= self.additional_instances_th
            ):
                final_ids.append(instance_id)

        return final_ids

    def _get_missing_instance_ids(self) -> List[int]:
        instance_ids = self.matching_results.get_missing_instances()

        final_ids = list()

        for instance_id in instance_ids:
            if (
                    self._get_missing_or_additional_instance_score(instance_id, InstanceType.MISSING)
                    <= self.missing_instances_th
            ):
                final_ids.append(instance_id)

        return final_ids
