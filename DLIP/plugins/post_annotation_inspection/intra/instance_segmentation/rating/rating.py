import pickle
from pathlib import Path
from typing import List, Union, Dict

import numpy as np

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.predictor_wrapper import Predictor
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.matching import (
    InstanceMatching, MatchingResults
)
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.base_rater import \
    BaseInstancePerImageRater
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import InstanceType, Instance


class InstancePerImageRater(BaseInstancePerImageRater):
    def __init__(
            self,
            matching_results: MatchingResults,
            network_uncertainty: Dict[int, float],
            network_uncertainty_on_additional_user_instances: Dict[int, float],
            uncertainty_weighting_param: float = 0,
            boundary_weighting_param: float = 0,
            add_miss_size_weighting_param: float = 0,
            reference_instance_size: int = None,
            boundary_region_percentage: float = 0.05,
            oddness_th: float = 1,
            additional_instances_th: float = 1,
            missing_instances_th: float = 1
    ):
        super(InstancePerImageRater, self).__init__(
            matching_results,
            network_uncertainty,
            network_uncertainty_on_additional_user_instances,
            uncertainty_weighting_param,
            boundary_weighting_param,
            add_miss_size_weighting_param,
            reference_instance_size,
            boundary_region_percentage,
            oddness_th,
            additional_instances_th,
            missing_instances_th
        )

        self.user_instance_ids_to_ignore: List[int] = list()
        self.network_instance_ids_to_ignore: List[int] = list()
        self.user_defined_unpaired_network_instance_ids: List[int] = list()

    @staticmethod
    def create_using_network(
        original_image: np.ndarray,
        user_annotated_label: np.ndarray,
        instance_segmentation_network: Predictor,
        uncertainty_weighting_param: float = 0,
        boundary_weighting_param: float = 0,
        add_miss_size_weighting_param: float = 0,
        reference_instance_size: int = None,
        boundary_region_percentage: float = 0.05,
        oddness_th: float = 1,
        additional_instances_th: float = 1,
        missing_instances_th: float = 1
    ) -> "InstancePerImageRater":
        network_prediction = instance_segmentation_network.predict(original_image)

        matching_results = InstanceMatching.match(
            user_annotated_label,
            network_prediction,
        )

        unpaired_user_prediction_mask = matching_results.get_mask_of_unpaired_user_prediction()

        _, network_uncertainty, network_uncertainty_on_additional_user_instances = \
            instance_segmentation_network.predict_with_uncertainty(
                original_image,
                unpaired_user_prediction_mask
            )

        # as remap label is used in matching 1...N labels have to be mapped back to the instance ids
        # as the instances in the user label are already remapped by size -> just go through them in order
        network_uncertainty_on_additional_user_instances = {
            instance_id: network_uncertainty_on_additional_user_instances[key]
            for instance_id, key in zip(
                np.delete(np.unique(unpaired_user_prediction_mask), 0),
                network_uncertainty_on_additional_user_instances.keys()
            )
        }

        return InstancePerImageRater(
            matching_results,
            network_uncertainty,
            network_uncertainty_on_additional_user_instances,
            uncertainty_weighting_param,
            boundary_weighting_param,
            add_miss_size_weighting_param,
            reference_instance_size,
            boundary_region_percentage,
            oddness_th,
            additional_instances_th,
            missing_instances_th
        )

    def add_user_instance_to_ignore(self, user_instance: Instance) -> None:
        self.user_instance_ids_to_ignore.append(user_instance.instance_id)

    def add_network_instance_to_ignore(self, network_instance: Instance):
        self.network_instance_ids_to_ignore.append(network_instance.instance_id)

    def add_user_instance_id_to_ignore(self, user_instance_id: int) -> None:
        self.user_instance_ids_to_ignore.append(user_instance_id)

    def add_network_instance_id_to_ignore(self, network_instance_id: int) -> None:
        self.network_instance_ids_to_ignore.append(network_instance_id)

    def add_user_defined_unpaired_network_instance_id(self, network_instance_id: int) -> None:
        self.user_defined_unpaired_network_instance_ids.append(network_instance_id)

    def _get_missing_instance_ids(self) -> List[int]:
        missing_instance_ids = super(InstancePerImageRater, self)._get_missing_instance_ids()

        return missing_instance_ids + self.user_defined_unpaired_network_instance_ids

    def _get_missing_or_additional_instances(self, instance_type: InstanceType) -> List[Instance]:
        instances = list()

        if instance_type == InstanceType.MISSING:
            instance_ids = list(set(self._get_missing_instance_ids()) - set(self.network_instance_ids_to_ignore))
        elif instance_type == InstanceType.ADDITIONAL:
            instance_ids = list(set(self._get_additional_instance_ids()) - set(self.user_instance_ids_to_ignore))
        else:
            raise ValueError(f"The given instance type '{instance_type}' is not supported!")

        for instance_id in instance_ids:
            instances.append(Instance(
                instance_id,
                [self._get_missing_or_additional_instance_score(instance_id, instance_type)],
                instance_type
            ))

        return instances

    def get_missing_instances(self) -> List[Instance]:
        return self._get_missing_or_additional_instances(InstanceType.MISSING)

    def get_additional_instances(self) -> List[Instance]:
        return self._get_missing_or_additional_instances(InstanceType.ADDITIONAL)

    def get_odd_instances(self) -> List[Instance]:
        odd_instances = list()

        odd_instance_ids, odd_ious = self._get_odd_instance_ids(), self._get_odd_instances_iou()

        unique_odd_instances, idx = np.unique(odd_instance_ids, return_index=True)
        unique_odd_ious = odd_ious[idx]

        for instance_id, iou in zip(unique_odd_instances, unique_odd_ious):
            if instance_id in self.user_instance_ids_to_ignore:
                continue
            odd_instances.append(Instance(instance_id, iou, InstanceType.ODD))

        return odd_instances

    def get_wrong_instances(self) -> List[Instance]:
        return (
                self.get_odd_instances()
                + self.get_additional_instances()
                + self.get_missing_instances()
        )

    def get_good_instances(self) -> List[Instance]:
        instances = list()

        correct_instance_ids = list(
                set(self.matching_results.get_user_annotation_ids())
                - set([i.instance_id for i in self.get_odd_instances()])
                - set([i.instance_id for i in self.get_additional_instances()])
        )

        for instance_id in correct_instance_ids:
            instances.append(Instance(instance_id, [1], InstanceType.CORRECT))

        return instances

    def get_all_instances(self) -> List[Instance]:
        return (
            self.get_good_instances()
            + self.get_wrong_instances()
        )

    def get_overall_score(self):
        overall_score = 0
        all_instances = self.get_all_instances()

        if len(all_instances) == 0:
            return 1

        for instance in all_instances:
            overall_score += np.min(instance.scores)

        return overall_score / len(all_instances)

    def save(self, file_name: Union[str, Path]) -> None:
        with open(file_name, "wb") as f:
            pickle.dump(self, f)
