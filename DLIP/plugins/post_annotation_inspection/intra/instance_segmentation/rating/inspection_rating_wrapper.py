import pickle
from pathlib import Path
from typing import List, Union

import numpy as np

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import (
    InstancePerImageRater, MaskCreator
)
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import Instance, InstanceType


class InspectionRatingWrapper:
    def __init__(self, rater: InstancePerImageRater):
        self.rater = rater
        self.mask_creator = MaskCreator(rater)

        self._relabeled_image = None

        self._inspection_done = False
        self._inspection_updated = False

    @staticmethod
    def load(file_name: Union[str, Path], reset_params: bool = True) -> "InspectionRatingWrapper":
        with open(file_name, "rb") as f:
            obj = pickle.load(f)

        if isinstance(obj, InstancePerImageRater):
            obj = InspectionRatingWrapper(obj)
        elif not isinstance(obj, InspectionRatingWrapper):
            raise ValueError("The given file does not contain an InstancePerImageRater object")

        if reset_params:
            obj.set_oddness_th(1.)
            obj.set_additional_instances_th(1.)
            obj.set_missing_instances_th(1.)
            obj.set_boundary_weighting_param(0.)
            obj.set_uncertainty_weighting_param(0.)

        return obj

    def set_oddness_th(self, oddness_th: float) -> None:
        self.rater.set_oddness_th(oddness_th)

    def set_add_miss_th(self, add_miss_th) -> None:
        self.set_additional_instances_th(add_miss_th)
        self.set_missing_instances_th(add_miss_th)

    def set_additional_instances_th(self, additional_instances_th: float) -> None:
        self.rater.set_additional_instances_th(additional_instances_th)

    def set_missing_instances_th(self, missing_instances_th: float) -> None:
        self.rater.set_missing_instances_th(missing_instances_th)

    def set_boundary_weighting_param(self, boundary_weighting_param: float) -> None:
        self.rater.set_boundary_weighting_param(boundary_weighting_param)

    def set_uncertainty_weighting_param(self, uncertainty_weighting_param: float) -> None:
        self.rater.set_uncertainty_weighting_param(uncertainty_weighting_param)

    def set_add_miss_size_weighting_param(self, add_miss_size_weighting_param: float) -> None:
        self.rater.set_add_miss_size_weighting_param(add_miss_size_weighting_param)

    def get_oddness_th(self) -> float:
        return self.rater.oddness_th

    def get_additional_instances_th(self) -> float:
        return self.rater.additional_instances_th

    def get_missing_instances_th(self) -> float:
        return self.rater.missing_instances_th

    def get_boundary_weighting_param(self) -> float:
        return self.rater.boundary_weighting_param

    def get_uncertainty_weighting_param(self) -> float:
        return self.rater.uncertainty_weighting_param

    def get_add_miss_size_weighting_param(self) -> float:
        return self.rater.add_miss_size_weighting_param

    def get_user_annotated_label(self) -> np.ndarray:
        if self._relabeled_image is not None:
            return self._relabeled_image
        return self.rater.get_user_annotated_label()

    def get_network_prediction(self) -> np.ndarray:
        return self.rater.get_network_prediction()

    def get_missing_mask(self) -> np.ndarray:
        if self._relabeled_image is not None:
            return np.zeros_like(self._relabeled_image)
        return self.mask_creator.get_missing_mask()

    def get_good_mask(self) -> np.ndarray:
        if self._relabeled_image is not None:
            return (self._relabeled_image != 0).astype(int)
        return self.mask_creator.get_good_mask()

    def get_additional_mask(self) -> np.ndarray:
        if self._relabeled_image is not None:
            return np.zeros_like(self._relabeled_image)
        return self.mask_creator.get_additional_mask()

    def get_network_instance_label_map(self, instance_id: int) -> np.ndarray:
        return self.rater.matching_results.get_network_instance_label_map(instance_id)

    def get_user_instance_label_map(self, instance_id: int) -> np.ndarray:
        return self.rater.matching_results.get_user_instance_label_map(instance_id)

    def get_wrong_instances(self) -> List[Instance]:
        return self.rater.get_wrong_instances()

    def get_overall_score(self) -> float:
        return self.rater.get_overall_score()

    def user_annotation_instances_present(self) -> bool:
        return len(self.rater.matching_results.get_user_annotation_ids()) > 0

    def save(self, file_name: Union[str, Path]) -> None:
        self._inspection_updated = False

        with open(file_name, "wb") as f:
            pickle.dump(self, f)

    def keep_user_annotation(self, instance: Instance):
        if instance.instance_type == InstanceType.MISSING:
            self.rater.add_network_instance_to_ignore(instance)
        elif len(instance.scores) > 1:
            # if there are multiple matches,
            # label network instances with just a small overlap as missing instead of just forgetting them

            for network_id in self.rater.matching_results.paired_instance_ids[instance.instance_id]:
                iou = self.rater.matching_results.pairwise_iou[network_id - 1, instance.instance_id - 1]

                if iou < 0.2:
                    self.rater.add_user_defined_unpaired_network_instance_id(network_id)

        self.rater.add_user_instance_to_ignore(instance)

        self._inspection_done = True
        self._inspection_updated = True

    def use_network_annotation(self, instance: Instance):
        if instance.instance_type == InstanceType.MISSING:
            if not self.user_annotation_instances_present():
                new_instance_id = 1
            else:
                new_instance_id = max(self.rater.matching_results.get_user_annotation_ids()) + 1
            instance_label_map = self.rater.matching_results.get_network_instance_label_map(instance.instance_id)

            self.rater.get_user_annotated_label()[instance_label_map != 0] = new_instance_id

            self.rater.add_user_instance_id_to_ignore(new_instance_id)
            self.rater.add_network_instance_to_ignore(instance)
        elif instance.instance_type == InstanceType.ADDITIONAL:
            instance_label_map = self.rater.matching_results.get_user_instance_label_map(instance.instance_id)
            self.rater.get_user_annotated_label()[instance_label_map != 0] = 0
            self.rater.add_user_instance_id_to_ignore(instance.instance_id)
        elif instance.instance_type == InstanceType.ODD:
            label_map_user = self.rater.matching_results.get_user_instance_label_map(instance.instance_id)

            network_ids = list()
            not_really_paired_network_ids = list()

            if len(self.rater.matching_results.paired_instance_ids[instance.instance_id]) > 1:
                # if there are multiple matches, use only network annotations with a 'high' overlay
                # label others as unpaired network annotations, so that the user can decide what to do with them

                for instance_id in self.rater.matching_results.paired_instance_ids[instance.instance_id]:
                    iou = self.rater.matching_results.pairwise_iou[instance_id - 1, instance.instance_id - 1]

                    if iou > 0.2:
                        network_ids.append(instance_id)
                    else:
                        not_really_paired_network_ids.append(instance_id)
            else:
                network_ids = self.rater.matching_results.paired_instance_ids[instance.instance_id]

            for network_id in network_ids:
                label_map_net = self.rater.matching_results.get_network_instance_label_map(network_id)

                self.rater.get_user_annotated_label()[label_map_user != 0] = 0
                self.rater.get_user_annotated_label()[label_map_net != 0] = instance.instance_id

                self.rater.add_user_instance_id_to_ignore(instance.instance_id)

            for network_id in not_really_paired_network_ids:
                self.rater.add_user_defined_unpaired_network_instance_id(network_id)
        else:
            raise ValueError(f"Unexpected instance: {str(instance)}")

        self._inspection_done = True
        self._inspection_updated = True

    def use_relabeled_image(self, label_img: np.ndarray):
        self._relabeled_image = label_img

        self._inspection_done = True
        self._inspection_updated = True

    def is_inspection_done(self):
        return self._inspection_done

    def is_inspection_updated(self):
        return self._inspection_updated

    def is_disabled(self):
        return self._relabeled_image is not None
