from typing import List

import numpy as np


class InstanceType:
    CORRECT = "correct"
    ODD = "odd"
    ADDITIONAL = "additional"
    MISSING = "missing"


class InstanceSource:
    USER = "user"
    NETWORK = "network"


class Instance:
    def __init__(self, instance_id: int, scores: List[float], instance_type: InstanceType):
        self.instance_id = instance_id
        self.scores = scores
        self.instance_type = instance_type

    def __str__(self):
        if len(self.scores) == 1:
            return f"Instance {self.instance_id} ({self.instance_type} instance with score {self.scores[0]:.3f})"
        return f"Instance {self.instance_id} ({self.instance_type} instance with scores " \
               f"{', '.join(f'{s:.3f}' for s in self.scores)})"

    def __lt__(self, other):
        return np.min(self.scores) < np.min(other.scores)
