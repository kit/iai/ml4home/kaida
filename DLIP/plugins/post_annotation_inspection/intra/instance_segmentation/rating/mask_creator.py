import numpy as np

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InstancePerImageRater
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import InstanceType


class MaskCreator:
    def __init__(self, rater: InstancePerImageRater):
        self.rater = rater

    def _get_unpaired_part_in_odd_instances(
            self,
            unpaired_type: InstanceType = InstanceType.MISSING
    ) -> np.ndarray:
        """
        Calculates the segmentation mask of unpaired parts in paired odd instances that are either missing or additional

        Args:
            unpaired_type: type of unpaired pixels to calculate the mask for

        Returns:
            a numpy array containing the mask of the missing/additional pixels using the value 1
        """

        if unpaired_type not in [InstanceType.MISSING, InstanceType.ADDITIONAL]:
            raise ValueError(f"The given type {unpaired_type} is not supported!")

        union_mask = np.zeros_like(self.rater.get_user_annotated_label())
        mask = np.zeros_like(self.rater.get_user_annotated_label())

        for instance in self.rater.get_odd_instances():
            for network_instance_id in self.rater.matching_results.paired_instance_ids[instance.instance_id]:
                user_instance = self.rater.matching_results.get_user_instance_label_map(instance.instance_id)
                network_instance = self.rater.matching_results.get_network_instance_label_map(network_instance_id)

                if unpaired_type == InstanceType.ADDITIONAL:
                    network_instance[network_instance == 0] = -1
                else:
                    user_instance[user_instance == 0] = -1

                union = user_instance == network_instance

                if unpaired_type == InstanceType.ADDITIONAL:
                    user_instance[union] = 0
                    mask[user_instance != 0] = 1
                else:
                    network_instance[union] = 0
                    mask[network_instance != 0] = 1

                union_mask[union] = 1

        mask[union_mask == 1] = 0

        return mask

    def get_good_mask(self):
        mask = np.zeros_like(self.rater.get_user_annotated_label())

        for good_instance in self.rater.get_good_instances():
            mask[self.rater.get_user_annotated_label() == good_instance.instance_id] = 1

        for instance in self.rater.get_odd_instances():
            for network_instance_id in self.rater.matching_results.paired_instance_ids[instance.instance_id]:
                user_instance = self.rater.matching_results.get_user_instance_label_map(instance.instance_id)
                network_instance = self.rater.matching_results.get_network_instance_label_map(network_instance_id)
                network_instance[network_instance == 0] = -1
                union = user_instance == network_instance
                mask[union] = 1

        return mask

    def get_missing_mask(self):
        mask = np.zeros_like(self.rater.get_user_annotated_label())

        for missing_instance in self.rater.get_missing_instances():
            mask[self.rater.get_network_prediction() == missing_instance.instance_id] = 1

        mask += self._get_unpaired_part_in_odd_instances(unpaired_type=InstanceType.MISSING)
        mask[mask > 0] = 1

        return mask

    def get_additional_mask(self):
        mask = np.zeros_like(self.rater.get_user_annotated_label())

        for additional_instance in self.rater.get_additional_instances():
            mask[self.rater.get_user_annotated_label() == additional_instance.instance_id] = 1

        mask += self._get_unpaired_part_in_odd_instances(unpaired_type=InstanceType.ADDITIONAL)
        mask[mask > 0] = 1

        return mask
