from typing import Callable

from PyQt5.QtWidgets import QWidget, QVBoxLayout

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.gui_parts import ThresholdSlider
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InspectionRatingWrapper


class ThresholdSliderGroup(QWidget):
    def __init__(self, rater: InspectionRatingWrapper, reload_ui_slot: Callable):
        super(ThresholdSliderGroup, self).__init__()

        self.rater = rater
        self.reload_ui = reload_ui_slot

        self.oddness_slider = ThresholdSlider(
            "Oddness Threshold", 0, 1, rater.get_oddness_th(), self._oddness_th_changed
        )
        self.add_and_miss_slider = ThresholdSlider(
            "Add./Miss. Threshold", 0, 1, rater.get_additional_instances_th(), self._add_miss_th_changed
        )
        self.uncertainty_weighting_slider = ThresholdSlider(
            "Uncertainty Weighting", 0, 1, rater.get_uncertainty_weighting_param(), self._uncertainty_weighting_changed
        )
        self.boundary_weighting_slider = ThresholdSlider(
            "Boundary Weighting", 0, 1, rater.get_boundary_weighting_param(), self._boundary_weighting_changed
        )

        self.add_miss_size_weighting_slider = ThresholdSlider(
            "Add/Miss Instances Size Weighting", 0, 1,
            rater.get_add_miss_size_weighting_param(),
            self._add_miss_size_weighting_changed
        )

        layout = QVBoxLayout()
        layout.addWidget(self.oddness_slider)
        layout.addWidget(self.add_and_miss_slider)
        layout.addWidget(self.uncertainty_weighting_slider)
        layout.addWidget(self.boundary_weighting_slider)
        layout.addWidget(self.add_miss_size_weighting_slider)
        self.setLayout(layout)

    def set_rater(self, rater: InspectionRatingWrapper) -> None:
        self.rater = rater

        self.rater.set_oddness_th(self.oddness_slider.value())
        self.rater.set_add_miss_th(self.add_and_miss_slider.value())
        self.rater.set_uncertainty_weighting_param(self.uncertainty_weighting_slider.value())
        self.rater.set_boundary_weighting_param(self.boundary_weighting_slider.value())

    def _oddness_th_changed(self) -> None:
        self.rater.set_oddness_th(self.oddness_slider.value())
        self.reload_ui()

    def _add_miss_th_changed(self) -> None:
        self.rater.set_add_miss_th(self.add_and_miss_slider.value())
        self.reload_ui()

    def _uncertainty_weighting_changed(self) -> None:
        self.rater.set_uncertainty_weighting_param(self.uncertainty_weighting_slider.value())
        self.reload_ui()

    def _boundary_weighting_changed(self) -> None:
        self.rater.set_boundary_weighting_param(self.boundary_weighting_slider.value())
        self.reload_ui()

    def _add_miss_size_weighting_changed(self) -> None:
        self.rater.set_add_miss_size_weighting_param(self.add_miss_size_weighting_slider.value())
        self.reload_ui()

    def enable_inputs(self, mode: bool = True):
        self.oddness_slider.setEnabled(mode)
        self.add_and_miss_slider.setEnabled(mode)
        self.uncertainty_weighting_slider.setEnabled(mode)
        self.boundary_weighting_slider.setEnabled(mode)
        self.add_miss_size_weighting_slider.setEnabled(mode)
