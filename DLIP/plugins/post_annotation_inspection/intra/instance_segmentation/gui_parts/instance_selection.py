from typing import List

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QListWidget, QLabel, QAbstractItemView
from PyQt5.QtCore import pyqtSignal

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import Instance


class InstanceSelection(QWidget):
    instance_selected_signal = pyqtSignal(list)  # List[Instance]

    def __init__(self, title: str, instances: List[Instance], item_selected_slot):
        super(InstanceSelection, self).__init__()

        self.instance_widget = QListWidget()
        self.instance_widget.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.instances = None
        self.set_items(instances)

        self.instance_widget.itemClicked.connect(self._instance_selected)
        self.instance_selected_signal.connect(item_selected_slot)

        layout = QVBoxLayout()
        layout.addWidget(QLabel(title))
        layout.addWidget(self.instance_widget)
        self.setLayout(layout)

    def set_items(self, instances):
        self.instance_widget.clear()
        self.instances = sorted(instances)
        self.instance_widget.addItems(str(instance) for instance in self.instances)

    def clear_selection(self):
        self.instance_widget.clearSelection()

    def _instance_selected(self, _):
        selected_instances = [self.instances[x.row()] for x in self.instance_widget.selectedIndexes()]
        self.instance_selected_signal.emit(selected_instances)

    def enable_inputs(self, mode: bool = True):
        self.instance_widget.setEnabled(mode)
