from typing import List

import numpy as np
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QLabel, QButtonGroup, QRadioButton
from PyQt5.QtCore import Qt
from skimage.color import label2rgb

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.gui_parts.utils import img_to_pixmap
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InspectionRatingWrapper
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import Instance, InstanceType
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.utils import LabelOverlayCreator


class LabeledImageType:
    USER_ANNOTATION = 0,
    NETWORK_ANNOTATION = 1,
    COMBINATION = 2


class OverlayTypeRadioButtons(QWidget):
    def __init__(
            self,
            parent: "LabeledImageWidget",
            labeled_image_type: LabeledImageType
    ):
        super(OverlayTypeRadioButtons, self).__init__()

        self.radio_button_overlay = QRadioButton("Overlay")
        self.radio_button_label = QRadioButton("Annotation")
        self.radio_button_image = QRadioButton("Image")

        if labeled_image_type == LabeledImageType.COMBINATION:
            self.radio_button_overlay.setChecked(True)
        else:
            self.radio_button_label.setChecked(True)

        self.radio_button_overlay.toggled.connect(lambda x: parent.reload(use_last_selected_instances=True))
        self.radio_button_label.toggled.connect(lambda x: parent.reload(use_last_selected_instances=True))
        self.radio_button_image.toggled.connect(lambda x: parent.reload(use_last_selected_instances=True))

        radio_button_group = QButtonGroup()
        radio_button_group.addButton(self.radio_button_overlay)
        radio_button_group.addButton(self.radio_button_label)
        radio_button_group.addButton(self.radio_button_image)

        layout = QHBoxLayout()
        layout.addWidget(self.radio_button_overlay)
        layout.addWidget(self.radio_button_label)
        layout.addWidget(self.radio_button_image)
        self.setLayout(layout)

    def show_overlay(self) -> bool:
        return self.radio_button_overlay.isChecked()

    def show_label(self) -> bool:
        return self.radio_button_label.isChecked()

    def show_image(self) -> bool:
        return self.radio_button_image.isChecked()


class LabeledImageWidget(QWidget):
    def __init__(
            self,
            original_image: np.ndarray,
            rater: InspectionRatingWrapper,
            image_width: int,
            labeled_image_type: LabeledImageType = LabeledImageType.COMBINATION
    ):
        super(LabeledImageWidget, self).__init__()
        self.image_width = image_width
        self.last_image_size_with_reload = image_width
        self.last_image_resize_direction = 1

        self.original_image = original_image
        self.rater = rater
        self.labeled_image_type = labeled_image_type

        self.image_label = QLabel()

        self.radio_buttons = OverlayTypeRadioButtons(self, labeled_image_type)

        layout = QVBoxLayout()
        layout.addWidget(self.image_label)
        layout.addWidget(self.radio_buttons)
        self.setLayout(layout)

        self.last_selected_instances = None

        self.reload()

    def get_overlay_creator(self, selected_instances: List[Instance] = None) -> LabelOverlayCreator:
        overlay_creator = LabelOverlayCreator(self.original_image)

        if self.labeled_image_type == LabeledImageType.USER_ANNOTATION:
            overlay_creator.set_overlay(label2rgb(self.rater.get_user_annotated_label(), bg_label=0))
        elif self.labeled_image_type == LabeledImageType.NETWORK_ANNOTATION:
            overlay_creator.set_overlay(label2rgb(self.rater.get_network_prediction(), bg_label=0))
        else:
            overlay_creator.set_missing_mask(self.rater.get_missing_mask())
            overlay_creator.set_additional_mask(self.rater.get_additional_mask())
            overlay_creator.set_good_mask(self.rater.get_good_mask())

            if selected_instances is not None:
                for selected_instance in selected_instances:
                    if selected_instance.instance_type == InstanceType.MISSING:
                        overlay_creator.add_selected_instance_mask(
                            self.rater.get_network_instance_label_map(selected_instance.instance_id)
                        )
                    else:
                        overlay_creator.add_selected_instance_mask(
                            self.rater.get_user_instance_label_map(selected_instance.instance_id)
                        )

        return overlay_creator

    def reload(self, selected_instances: List[Instance] = None, use_last_selected_instances: bool = False) -> None:
        if use_last_selected_instances:
            selected_instances = self.last_selected_instances
        self.last_selected_instances = selected_instances

        if self.radio_buttons.show_overlay():
            self.image_label.setPixmap(
                img_to_pixmap(self.get_overlay_creator(selected_instances).get_image_with_overlay(), self.image_width)
            )
        elif self.radio_buttons.show_label():
            if self.labeled_image_type == LabeledImageType.USER_ANNOTATION:
                label_map = label2rgb(self.rater.get_user_annotated_label(), bg_label=0)
            elif self.labeled_image_type == LabeledImageType.NETWORK_ANNOTATION:
                label_map = label2rgb(self.rater.get_network_prediction(), bg_label=0)
            else:
                label_map = self.get_overlay_creator(selected_instances).get_overlay()
            self.image_label.setPixmap(img_to_pixmap(label_map, self.image_width))
        elif self.radio_buttons.show_image():
            self.image_label.setPixmap(
                img_to_pixmap(self.get_overlay_creator(selected_instances).image, self.image_width)
            )

    def change_image(self, original_image: np.ndarray, rater: InspectionRatingWrapper):
        self.original_image = original_image
        self.rater = rater
        self.last_selected_instances = None
        self.reload()

    def resizeEvent(self, event):
        self.resize(0.9 * self.frameGeometry().width())
        super(LabeledImageWidget, self).resizeEvent(event)

    def resize(self, image_width):
        if (
                self.last_image_size_with_reload / image_width < 0.9
                or self.last_image_size_with_reload / image_width > 1.25
                or self.last_image_resize_direction * np.sign(self.image_width / image_width - 1) < 0
        ):
            # reloading the image takes longer. Therefore, do it (to prevent artifacts) only if:
            # - the new image size is way bigger/smaller than the size of the last reload
            # - the resize direction changed
            self.last_image_resize_direction = np.sign(self.image_width / image_width - 1)
            self.image_width = image_width
            self.last_image_size_with_reload = image_width
            self.reload()
        elif np.abs(self.image_width / image_width - 1) > 0:
            # otherwise just scale the image using the appropriate transformMode
            transform_mode = Qt.FastTransformation if self.image_width / image_width < 1 else Qt.SmoothTransformation
            self.image_label.setPixmap(
                self.image_label.pixmap().scaled(image_width, image_width, transformMode=transform_mode)
            )
            self.image_label.adjustSize()

            self.last_image_resize_direction = np.sign(self.image_width / image_width - 1)
            self.image_width = image_width
