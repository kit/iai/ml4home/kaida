from typing import List

import numpy as np
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.gui_parts import (
    LabeledImageWidget, LabeledImageType, create_box_layout
)
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InspectionRatingWrapper
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import Instance


class ImageLabelGroup(QWidget):
    def __init__(self, original_image: np.ndarray, rater: InspectionRatingWrapper):
        super(ImageLabelGroup, self).__init__()

        self.rater = rater

        self.image_width = 0.29 * self.frameGeometry().width()

        self.label_user_annotation = LabeledImageWidget(
            original_image, self.rater, self.image_width, LabeledImageType.USER_ANNOTATION
        )
        self.label_map_overlay = LabeledImageWidget(
            original_image, self.rater, self.image_width, LabeledImageType.COMBINATION
        )
        self.label_model_prediction = LabeledImageWidget(
            original_image, self.rater, self.image_width, LabeledImageType.NETWORK_ANNOTATION
        )

        self.reload_labels()

        layout = QHBoxLayout()
        layout.addWidget(self._get_widget_with_title("User Annotation", self.label_user_annotation))
        layout.addWidget(self._get_widget_with_title("Annotation Overlay", self.label_map_overlay))
        layout.addWidget(self._get_widget_with_title("Model Prediction", self.label_model_prediction))
        self.setLayout(layout)

    @staticmethod
    def _get_widget_with_title(title, sub_widget) -> QWidget:
        return create_box_layout([QLabel(title), sub_widget], "vertical")

    def change_image(self, original_image: np.ndarray, rater: InspectionRatingWrapper) -> None:
        self.rater = rater
        self.label_map_overlay.change_image(original_image, rater)
        self.label_user_annotation.change_image(original_image, rater)
        self.label_model_prediction.change_image(original_image, rater)

    def reload_labels(self):
        self.label_user_annotation.reload()
        self.label_model_prediction.reload()

    def reload_label_overlay(self, selected_instances: List[Instance] = None) -> None:
        self.label_map_overlay.reload(selected_instances)
