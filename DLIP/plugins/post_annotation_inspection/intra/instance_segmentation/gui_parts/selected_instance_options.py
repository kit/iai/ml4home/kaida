from typing import Callable, List

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating.instance import Instance
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InspectionRatingWrapper


class SelectedInstanceOptions(QWidget):
    def __init__(self, rater: InspectionRatingWrapper, reload_ui_slot: Callable, relabel_image_slot: Callable):
        super(SelectedInstanceOptions, self).__init__()

        self.rater = rater
        self.selected_instances = None
        self.reload_ui = reload_ui_slot

        self.button_keep_user_annotation = QPushButton("Keep User Annotation")
        self.button_keep_user_annotation.pressed.connect(self.button_keep_user_annotation_pressed)

        self.button_use_network_annotation = QPushButton("Use Network Annotation")
        self.button_use_network_annotation.pressed.connect(self.button_use_network_annotation_pressed)

        self.disable_instance_buttons()

        self.button_relabel_image = QPushButton("Reannotate Image")
        self.button_relabel_image.pressed.connect(relabel_image_slot)

        layout = QVBoxLayout()
        layout.addWidget(self.button_keep_user_annotation)
        layout.addWidget(self.button_use_network_annotation)
        layout.addWidget(self.button_relabel_image)
        self.setLayout(layout)

    def button_keep_user_annotation_pressed(self):
        for selected_instance in self.selected_instances:
            self.rater.keep_user_annotation(selected_instance)
        self.reload_ui()

    def button_use_network_annotation_pressed(self):
        for selected_instance in self.selected_instances:
            self.rater.use_network_annotation(selected_instance)
        self.reload_ui(label_changed=True)

    def set_rater(self, rater: InspectionRatingWrapper):
        self.rater = rater
        self.disable_instance_buttons()

    def enable_instance_buttons(self, enabled: bool = True):
        self.button_keep_user_annotation.setEnabled(enabled)
        self.button_use_network_annotation.setEnabled(enabled)

    def enable_inputs(self, mode: bool = True, rater_disabled_mode: bool = False):
        self.enable_instance_buttons(mode)
        self.button_relabel_image.setEnabled(mode)

        if rater_disabled_mode and mode is not True:
            self.button_relabel_image.setEnabled(True)

    def disable_instance_buttons(self):
        self.enable_instance_buttons(False)

    def instances_selected(self, instances: List[Instance]):
        self.selected_instances = instances
        self.enable_instance_buttons(instances is not None)
