from typing import List

import numpy as np
from PyQt5.QtGui import QImage, QPixmap
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout, QBoxLayout


def img_to_pixmap(img: np.ndarray, desired_width=-1, desired_height=-1) -> np.ndarray:
    if np.max(img) <= 1:
        img = (img * 255).astype(np.uint8)

    if len(img.shape) == 3:
        height, width, channels = img.shape
        bytes_per_line = width * channels
        img_format = QImage.Format_RGB888
    else:
        height, width = img.shape
        bytes_per_line = width
        img_format = QImage.Format_Grayscale8

    img = QPixmap(QImage(img, width, height, bytes_per_line, img_format))

    if desired_width == -1 and desired_height == -1:
        return img
    elif desired_width == -1:
        desired_width = int(img.width() * (desired_height / img.height()))
    elif desired_height == -1:
        desired_height = int(img.height() * (desired_width / img.width()))
    return img.scaled(desired_width, desired_height)


def create_box_layout(widgets: List[QWidget], layout_direction: str = "h"):
    if layout_direction.lower().startswith("h"):
        layout = QHBoxLayout()
    else:
        layout = QVBoxLayout()

    for widget in widgets:
        layout.addWidget(widget)

    widget = QWidget()
    widget.setLayout(layout)
    return widget
