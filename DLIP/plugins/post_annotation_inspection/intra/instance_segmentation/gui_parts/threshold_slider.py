from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QLabel, QSlider


class ThresholdSlider(QWidget):
    def __init__(self, title, min_val, max_val, initial_val, changed_slot):
        super(ThresholdSlider, self).__init__()

        self.min_val = min_val
        self.max_val = max_val

        label_title = QLabel()
        label_title.setText(f"{title}:")

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setRange(0, 100)
        self.slider.setValue((initial_val - min_val) / max_val * 100)
        self.slider.valueChanged.connect(changed_slot)
        self.slider.valueChanged.connect(self._value_changed)

        self.label_value = QLabel()
        self.label_value.setText(f"{initial_val:.3f}")

        layout = QHBoxLayout()
        layout.addWidget(label_title)
        layout.addWidget(self.slider)
        layout.addWidget(self.label_value)
        self.setLayout(layout)

    def _value_changed(self):
        self.label_value.setText(f"{self.value():.3f}")

    def value(self):
        return self.slider.value() / 100 * (self.max_val - self.min_val) + self.min_val
