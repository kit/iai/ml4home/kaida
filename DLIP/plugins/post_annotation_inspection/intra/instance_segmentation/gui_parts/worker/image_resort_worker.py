import time

from PyQt5.QtCore import QObject, pyqtSignal

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.rating import InspectionRatingWrapper


class ImageResortWorker(QObject):
    finished = pyqtSignal()
    progress = pyqtSignal(float)
    finished_info = pyqtSignal(tuple)

    def __init__(self, data_frame, sliders, label_saved: bool):
        super(ImageResortWorker, self).__init__()
        self.data_frame = data_frame
        self.sliders = sliders
        self.label_saved = label_saved

    def run(self):
        start_time = time.time()

        for n, (idx, row) in enumerate(self.data_frame.iterrows()):
            curr_rater: InspectionRatingWrapper = row["rater"]
            curr_rater.set_oddness_th(self.sliders.oddness_slider.value())
            curr_rater.set_add_miss_th(self.sliders.add_and_miss_slider.value())
            curr_rater.set_uncertainty_weighting_param(self.sliders.uncertainty_weighting_slider.value())
            curr_rater.set_boundary_weighting_param(self.sliders.boundary_weighting_slider.value())
            curr_rater.set_add_miss_size_weighting_param(self.sliders.add_miss_size_weighting_slider.value())
            self.data_frame["score"].at[idx] = curr_rater.get_overall_score()
            self.data_frame["rater"].at[idx] = curr_rater

            self.progress.emit(min(0., n / len(self.data_frame) - 0.05))

        self.data_frame = self.data_frame.sort_values(by=["inspection_done", "score"], ascending=[False, True])

        self.progress.emit(1)

        end_time = time.time()

        if end_time - start_time < 3:
            # let the resorting take at least 3 seconds
            # this way, the user doesn't have just a flashing screen and can read the loading information
            time.sleep(3 - (end_time - start_time))

        self.finished_info.emit((self.data_frame, self.label_saved))
        self.finished.emit()

