from PyQt5.QtWidgets import QWidget, QDialog
from PyQt5.QtCore import Qt

from DLIP.gui.backend.label_assistant.instance_segmentation.ui_inst_seg.gui.cellpose_gui import InstanceSegUi
from DLIP.gui.backend.label_assistant.instance_segmentation.ui_inst_seg.gui import io


class RelabelDialog(InstanceSegUi, QDialog):
    def __init__(self, parent: QWidget, img_file_name: str, label_file_name: str):
        super(RelabelDialog, self).__init__(
            image=None,
            _save_label=self._save_label,
            on_label_ui_closed=self._on_label_ui_closed,
            ui_position=(100, 100),
            ui_shape=(1000, 1000),
            parent=parent
        )

        self.parent_window = parent

        self.setWindowModality(Qt.ApplicationModal)

        self.img_file_name = img_file_name
        self.label_file_name = label_file_name

        io._load_image(self, self.img_file_name)
        io._load_masks(self, self.label_file_name)

    def _save_label(self):
        self.parent_window.relabeled_image(self.cellpix[0])

    def _on_label_ui_closed(self):
        self.parent_window.relabel_image_finished()
