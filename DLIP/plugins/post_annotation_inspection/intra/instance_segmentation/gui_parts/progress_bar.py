from typing import Callable

from PyQt5.QtWidgets import QWidget, QProgressBar, QHBoxLayout, QPushButton, QLabel
from PyQt5.QtCore import pyqtSignal


class ProgressWidget(QWidget):
    image_changed = pyqtSignal()

    def __init__(
            self,
            number_of_images: int,
            image_changed_slot: Callable,
            num_of_already_inspected_images: int = 0,
            current_image_idx: int = 0
    ):
        super(ProgressWidget, self).__init__()

        self.current_image_idx = current_image_idx if current_image_idx != -1 else number_of_images - 1
        self.last_image_idx = None
        self.number_of_images = number_of_images
        self.num_of_already_inspected_images = num_of_already_inspected_images

        self.image_changed.connect(image_changed_slot)

        self.progress_bar = QProgressBar()
        self._update_progress_bar()

        self.info_label = QLabel(f"Picture")
        self._update_info_label()

        self.button_next = QPushButton(">")
        self.button_next.clicked.connect(lambda x: self.next())
        self.button_previous = QPushButton("<")
        self.button_previous.clicked.connect(lambda x: self.previous())

        if self.current_image_idx == 0:
            self.button_previous.setEnabled(False)
        if self.current_image_idx + 1 == self.number_of_images:
            self.button_next.setEnabled(False)

        layout = QHBoxLayout()
        layout.addWidget(self.button_previous)
        layout.addWidget(self.info_label)
        layout.addWidget(self.button_next)
        layout.addWidget(self.progress_bar)
        self.setLayout(layout)

    def _update_info_label(self):
        self.info_label.setText(f"Picture {self.current_image_idx + 1}/{self.number_of_images}")

    def _update_progress_bar(self):
        self.progress_bar.setValue(round(self.num_of_already_inspected_images / self.number_of_images, 1) * 100)

    def img_saved(self, num_of_already_inspected_images: int):
        self.num_of_already_inspected_images = num_of_already_inspected_images
        self._update_progress_bar()

    def next(self, emit_signal: bool = True):
        if self.current_image_idx + 1 < self.number_of_images:
            self.last_image_idx = self.current_image_idx
            self.current_image_idx += 1
            self._update_info_label()
            if emit_signal:
                self.image_changed.emit()
            self.button_previous.setEnabled(True)

        if self.current_image_idx + 1 == self.number_of_images:
            self.button_next.setEnabled(False)

    def previous(self):
        if self.current_image_idx > 0:
            self.last_image_idx = self.current_image_idx
            self.current_image_idx -= 1
            self._update_info_label()
            self.image_changed.emit()
            self.button_next.setEnabled(True)

        if self.current_image_idx == 0:
            self.button_previous.setEnabled(False)

    def enable_inputs(self, mode: bool = True):
        self.button_next.setEnabled(mode)
        self.button_previous.setEnabled(mode)
