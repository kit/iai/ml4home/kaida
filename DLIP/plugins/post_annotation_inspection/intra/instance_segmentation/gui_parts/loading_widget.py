from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QProgressBar, QSpacerItem, QSizePolicy


class LoadingWidget(QWidget):
    def __init__(self):
        super(LoadingWidget, self).__init__()

        self.info_label = QLabel("Loading...")
        self.progress_bar = QProgressBar()

        layout = QVBoxLayout()
        layout.addItem(QSpacerItem(1, 1, QSizePolicy.Minimum, QSizePolicy.Expanding))
        layout.addWidget(self.info_label)
        layout.addWidget(self.progress_bar)
        layout.addItem(QSpacerItem(1, 1, QSizePolicy.Minimum, QSizePolicy.Expanding))
        self.setLayout(layout)

    def set_loading_test(self, text: str) -> None:
        self.info_label.setText(text)

    def update_progress_bar(self, progress: float):
        self.progress_bar.setValue(progress * 100)
