from typing import List, Union

import numpy as np

from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.utils import remap_label
from DLIP.plugins.post_annotation_inspection.intra.instance_segmentation.matching import MatchingResults


class InstanceMatching:
    @staticmethod
    def remap_label(label: np.ndarray) -> np.ndarray:
        return remap_label(label, by_size=True)

    @staticmethod
    def _get_mask_id_list(label_map: np.ndarray) -> List[int]:
        id_list = list(np.unique(label_map.astype(int)))

        if id_list[0] != 0:
            # if there is no background id, the script doesn't work
            id_list = [0] + id_list

        return id_list

    @staticmethod
    def _get_mask_list_of_instances(label_map: np.ndarray, instance_ids: List[int]) -> List[Union[None, np.ndarray]]:
        instance_masks = [None]
        instance_masks += [
            np.array(label_map == instance_id, np.uint8)
            for instance_id in instance_ids[1:]
        ]
        return instance_masks

    @staticmethod
    def _preallocate_pairwise_array(network_prediction_id_list, user_annotated_label_id_list) -> np.ndarray:
        return np.zeros([len(network_prediction_id_list) - 1, len(user_annotated_label_id_list) - 1], dtype=np.float64)

    @staticmethod
    def match(
            user_annotated_label: np.ndarray,
            network_prediction: np.ndarray
    ) -> MatchingResults:
        """
        Calculates an instance wise matching between two label maps

        Implementation based on https://github.com/vqdang/hover_net/blob/master/metrics/stats_utils.py

        Args:
            user_annotated_label: user annotated label image as ndarray
            network_prediction: predicted label image as ndarray

        Returns:
            object of class MatchingResults containing all relevant information about the matching
        """

        network_prediction = InstanceMatching.remap_label(network_prediction)
        user_annotated_label = InstanceMatching.remap_label(user_annotated_label)

        network_prediction_id_list = InstanceMatching._get_mask_id_list(network_prediction)
        user_annotated_label_id_list = InstanceMatching._get_mask_id_list(user_annotated_label)

        network_prediction_masks = InstanceMatching._get_mask_list_of_instances(
            network_prediction,
            network_prediction_id_list
        )
        user_annotated_masks = InstanceMatching._get_mask_list_of_instances(
            user_annotated_label,
            user_annotated_label_id_list
        )

        # preallocate arrays for intersection and union
        pairwise_inter = InstanceMatching._preallocate_pairwise_array(
            network_prediction_id_list,
            user_annotated_label_id_list
        )
        pairwise_union = InstanceMatching._preallocate_pairwise_array(
            network_prediction_id_list,
            user_annotated_label_id_list
        )

        # compute pairwise intersection and union
        for network_prediction_instance_id in network_prediction_id_list[1:]:  # 0-th is background
            network_prediction_instance = network_prediction_masks[network_prediction_instance_id]
            overlap = user_annotated_label[network_prediction_instance > 0]
            overlap_ids = list(np.unique(overlap))
            for user_annotation_instance_id in overlap_ids:
                if user_annotation_instance_id == 0:
                    continue  # ignore overlapping background

                user_annotation_instance = user_annotated_masks[user_annotation_instance_id]

                total = (network_prediction_instance + user_annotation_instance).sum()
                inter = (network_prediction_instance * user_annotation_instance).sum()

                pairwise_inter[network_prediction_instance_id - 1, user_annotation_instance_id - 1] = inter
                pairwise_union[network_prediction_instance_id - 1, user_annotation_instance_id - 1] = total - inter

        pairwise_iou = np.divide(
            pairwise_inter,
            pairwise_union,
            out=np.zeros_like(pairwise_inter),
            where=pairwise_union != 0
        )  # prevent division by zero, even if it shouldn't appear

        try:
            paired_user_annotation = np.argmax(pairwise_iou, axis=1)
        except Exception:
            # There are no paired instances
            return MatchingResults(
                user_annotated_label,
                network_prediction,
                user_annotated_label_id_list[1:] if len(user_annotated_label_id_list) > 1 else [],
                network_prediction_id_list[1:] if len(network_prediction_id_list) > 1 else [],
                {},  # paired instance ids
                {},  # # paired instances iou
                pairwise_union,
                pairwise_inter,
                pairwise_iou,
                np.array([]),
                np.array([])
            )

        pairwise_iou_network_prediction = np.max(pairwise_iou, axis=1)

        # exclude those don't have intersection
        paired_network_prediction = np.nonzero(pairwise_iou_network_prediction > 0.0)[0]
        paired_user_annotation = paired_user_annotation[paired_network_prediction]

        paired_instance_ids = {}
        for user_id, network_id in zip(paired_user_annotation, paired_network_prediction):
            if user_id + 1 not in paired_instance_ids.keys():
                paired_instance_ids[user_id + 1] = [network_id + 1]
            else:
                paired_instance_ids[user_id + 1].append(network_id + 1)

        paired_instances_iou = {
            user_id: [pairwise_iou_network_prediction[net_id - 1] for net_id in paired_instance_ids[user_id]]
            for user_id in paired_instance_ids.keys()
        }

        # index to instance ID
        paired_network_prediction = paired_network_prediction + 1
        paired_user_annotation = paired_user_annotation + 1

        unpaired_network_prediction = np.array(
            [idx for idx in network_prediction_id_list[1:] if idx not in paired_network_prediction]
        )
        unpaired_user_annotation = np.array(
            [idx for idx in user_annotated_label_id_list[1:] if idx not in paired_user_annotation]
        )

        return MatchingResults(
            user_annotated_label,
            network_prediction,
            unpaired_user_annotation,
            unpaired_network_prediction,
            paired_instance_ids,
            paired_instances_iou,
            pairwise_union,
            pairwise_inter,
            pairwise_iou,
            paired_network_prediction,
            paired_user_annotation
        )
