from typing import Union, List, Dict

import numpy as np


class MatchingResults:
    def __init__(
            self,
            user_annotated_label: np.ndarray,
            network_prediction: np.ndarray,
            unpaired_user_annotations: Union[np.ndarray, List[int]],
            unpaired_network_predictions: Union[np.ndarray, List[int]],
            paired_instance_ids: Dict[int, List[int]],
            paired_instances_iou: Dict[int, List[float]],
            pairwise_union: np.ndarray,
            pairwise_inter: np.ndarray,
            pairwise_iou: np.ndarray,
            paired_network_prediction: np.ndarray,
            paired_user_annotation: np.ndarray,
    ):
        """

        Args:
            user_annotated_label: user annotated label image as ndarray
            network_prediction: predicted label image as ndarray
            unpaired_user_annotations: array of all instance ids from the user annotation that are unpaired
            unpaired_network_predictions: array of all instance ids from the network prediction that are unpaired
            paired_instance_ids: dictionary mapping from user instance ids to network prediction instance ids
                                 (list, as multiple matches are possible)
            paired_instances_iou: dictionary mapping from user instance ids to their iou scores
                                  (list, as multiple matches are possible)
            pairwise_union: array pairwise_union[network_id - 1, user_id - 1] storing the union of two instances
            pairwise_inter: array pairwise_inter[network_id - 1, user_id - 1] storing the intersection of two instances
            pairwise_iou: array pairwise_iou[network_id - 1, user_id - 1] storing the iou of two instances
            paired_network_prediction: array of instance ids of paired network instances
            paired_user_annotation: array of instance ids of paired user instances
                                    (same order as paired_network_prediction)
        """

        self.user_annotated_label = user_annotated_label
        self.network_prediction = network_prediction

        self.unpaired_user_annotations = self._test_and_convert_input_type(
            unpaired_user_annotations,
            "unpaired_user_annotations"
        )

        self.unpaired_network_predictions = self._test_and_convert_input_type(
            unpaired_network_predictions,
            "unpaired_network_predictions"
        )

        self.paired_instance_ids = paired_instance_ids
        self.paired_instances_iou_dict = paired_instances_iou

        self.pairwise_union = pairwise_union
        self.pairwise_inter = pairwise_inter
        self.pairwise_iou = pairwise_iou

        if pairwise_iou.shape[0] == 0:
            self.pairwise_iou_network_prediction = np.array([])
        elif pairwise_iou.shape[1] == 1:
            self.pairwise_iou_network_prediction = np.array([0.] * pairwise_iou.shape[0])
        elif pairwise_iou.shape[1] == 0:
            self.pairwise_iou_network_prediction = np.array([])
        else:
            self.pairwise_iou_network_prediction = np.max(pairwise_iou, axis=1)

        self.paired_network_prediction = self._test_and_convert_input_type(
            paired_network_prediction,
            "paired_network_prediction"
        )
        self.paired_user_annotation = self._test_and_convert_input_type(
            paired_user_annotation,
            "paired_user_annotation"
        )

    @staticmethod
    def _test_and_convert_input_type(variable, name) -> np.ndarray:
        if isinstance(variable, list):
            variable = np.array(variable, dtype=int)
        elif not isinstance(variable, np.ndarray):
            raise TypeError(f"expected {name} to be np.ndarray but got {type(variable)}")
        elif variable.dtype != int:
            variable.astype(int)

        return variable

    def get_additional_instances(self) -> np.ndarray:
        return self.unpaired_user_annotations

    def get_missing_instances(self) -> np.ndarray:
        return self.unpaired_network_predictions

    def is_user_instance_matched(self, instance_id):
        if instance_id in self.unpaired_user_annotations:
            return False
        return True

    def is_network_instance_matched(self, instance_id):
        if instance_id in self.unpaired_network_predictions:
            return False
        return True

    def get_user_annotation_ids(self):
        return np.delete(np.unique(self.user_annotated_label), 0)

    def get_network_annotation_ids(self):
        return np.delete(np.unique(self.network_prediction), 0)

    def get_iou_of_user_instance(self, instance_id):
        if instance_id in self.paired_user_annotation:
            return np.max(self.pairwise_iou, axis=0)[instance_id - 1]
        elif instance_id in self.unpaired_user_annotations:
            return 0

        if instance_id not in self.get_user_annotation_ids():
            raise ValueError(f"The given instance id {instance_id} is not in the given label image")
        elif instance_id not in np.unique(
                np.concatenate((self.paired_user_annotation, self.unpaired_user_annotations))
        ):
            raise ValueError(f"The given instance id {instance_id} is neither paired nor unpaired, "
                             f"there must be a bug in your code!")
        raise ValueError(f"Unexpected error for instance id {instance_id}")

    def get_user_instance_label_map(self, instance_id, use_original_instance_id: bool = False) -> np.ndarray:
        """
        Returns the labelmap of the given user instance with 0 for background and 1 for the instance.

        Args:
            instance_id: id of the wanted user instance
            use_original_instance_id: if true the original instance id is used instead of 1

        Returns:
            np.ndarray of the same size as the given labelmap of the user with just one instance on it
        """
        if use_original_instance_id:
            mask = np.array(self.user_annotated_label == instance_id, dtype=int)
            mask[mask != 0] = instance_id
            return mask
        return np.array(self.user_annotated_label == instance_id, dtype=int)

    def get_network_instance_label_map(self, instance_id, use_original_instance_id: bool = False) -> np.ndarray:
        """
        Returns the labelmap of the given network instance with 0 for background and 1 for the instance.

        Args:
            instance_id: id of the wanted network instance
            use_original_instance_id: if true the original instance id is used instead of 1

        Returns:
            np.ndarray of the same size as the predicted labelmap of the network with just one instance on it
        """
        if use_original_instance_id:
            mask = np.array(self.network_prediction == instance_id, dtype=int)
            mask[mask != 0] = instance_id
            return mask
        return np.array(self.network_prediction == instance_id, dtype=int)

    def get_mask_of_unpaired_user_prediction(self) -> np.ndarray:
        unpaired_user_prediction_maps = [
            self.get_user_instance_label_map(instance_id, True)
            for instance_id in self.unpaired_user_annotations
        ]
        if len(unpaired_user_prediction_maps) == 0:
            return np.zeros_like(self.user_annotated_label)
        return np.sum(unpaired_user_prediction_maps, axis=0)

    def get_mask_of_unpaired_network_prediction(self) -> np.ndarray:
        unpaired_network_prediction_masks = [
            self.get_network_instance_label_map(instance_id)
            for instance_id in self.unpaired_network_predictions
        ]
        if len(unpaired_network_prediction_masks) == 0:
            return np.zeros_like(self.network_prediction)
        return np.sum(unpaired_network_prediction_masks, axis=0)
