import ctypes
import os
import platform
import shutil
import subprocess
import sys
from pathlib import Path

from PyQt5 import QtWidgets, uic, QtCore
from paramiko.client import SSHClient

if platform.system() == "Windows":
    CP_console = f"cp{ctypes.cdll.kernel32.GetConsoleOutputCP()}"

from DLIP.data.version_control.dvc_wrapper.file_utils import onerror
from DLIP.plugins.training.util_fcn import create_temporary_copy, lsdf_path_converter, add_param_to_cfg
from DLIP.utils.helper_gui.yaml_to_param_table import ParameterTable


class TrainGui(QtWidgets.QDialog):
    display_name = "DNN Training"

    def __init__(self, path_project, project, **kwargs):
        super(TrainGui, self).__init__()
        uic.loadUi(f"{path_project}/plugins/training/train_gui.ui", self)
        self.path_project = path_project
        self.project=project

        if self.project is None:
            self.data_dir_btn.setEnabled(False)
            self.result_dir_btn.setEnabled(False)
            self.cfg_btn.setEnabled(False)
            self.train_btn.setEnabled(False)
            self._mode_status(f'Select Project First')
            return

        gpu_server_lst = ["Local", "HPC HoreKa"]
        self.gpu_server.addItems(gpu_server_lst)

        self.result_dir = None
        self.dataset_dir = None
        self.data_mode = None

        model_lst = list()
        self.temp_files = dict()
        self.model_cfg_path = os.path.join(path_project, "experiments", "configurations", "models", self.project['task'])
        for model_name in os.listdir(self.model_cfg_path):
            if "base" in model_name:
                continue
            model_lst.append(model_name.replace(".yaml", ""))
            self.temp_files[model_name.replace(".yaml", "")] = create_temporary_copy(
                self.project["data_dir"],os.path.join(self.model_cfg_path, model_name)
            )

        self.model.addItems(model_lst)

        self.mode.addItems(["Supervised", "Pre-Trained Model"])

        self.process_yaml = QtCore.QProcess(self)
        self.process_yaml.readyRead.connect(self.data_printer_yaml)
        self.process_yaml.started.connect(lambda: self.train_btn.setEnabled(False))

        self.process_dmap = QtCore.QProcess(self)
        self.process_dmap.readyRead.connect(self.data_printer_dmap)

        self.process_train = QtCore.QProcess(self)
        self.process_train.readyRead.connect(self.data_printer_train)

    def select_data_dir(self):
        input_path = str(Path(QtWidgets.QFileDialog().getExistingDirectory(self, "Select Dataset Directory")))

        if len(input_path):
            self.data_mode = "custom"
            self.dataset_dir = input_path
        else:
            self._mode_status(f'Selection was not successful!')

    def select_result_dir(self):
        input_path = str(Path(QtWidgets.QFileDialog().getExistingDirectory(self, "Select Result Directory")))

        if len(input_path):
            self.result_dir = input_path
        else:
            self._mode_status(f'Selection was not successful!')

    def open_cfg(self):
        cfg_file = self.temp_files[self.model.currentText()]
        
        """if platform.system() == "Linux":
            subprocess.call(["xdg-open", cfg_file])
        else:
            os.startfile(cfg_file)"""

        edible_cfg = ParameterTable(self.path_project, cfg_file)
        edible_cfg.exec_()

    def start_training(self):
        if self.data_mode != "custom":
            if self.gpu_server.currentText() == "Local":
                self.data_mode = "local"
                self.dataset_dir = str(Path(self.project["data_dir"]))
            elif self.project["dvc_server_dir"] is not None:
                self.data_mode = "dvc"
                self.dataset_dir = self._prepare_remote_dvc_dir()
            else:
                self._mode_status(f'Select Remote Data Dir First')
                self.repaint()
                return

        if self.result_dir is None:
            self.result_dir = os.path.join(self.dataset_dir, "results")
            if not os.path.exists(self.result_dir):
                os.makedirs(self.result_dir)

        add_params = dict()
        if self.mode.currentText() == "Supervised":
            add_params["train.aem.method"] = {"value": "supervised"}
        elif self.mode.currentText() == "Pre-Trained Model":
            pre_trained_model, _ = QtWidgets.QFileDialog().getOpenFileName(self, "Select Pre-Trained Model File")
            add_params = dict()
            add_params["train.aem.method"] = {"value": "pretrained_model"}
            if self.gpu_server.currentText() == "HPC HoreKa":
                add_params["train.aem.method.params.path"] = {"value": lsdf_path_converter(pre_trained_model)}
            else:
                add_params["train.aem.method.params.path"] = {"value": pre_trained_model}

        add_param_to_cfg(self.temp_files[self.model.currentText()], add_params)

        cfg_dir = os.path.join(self.dataset_dir, "train_cfg")
        if not os.path.exists(cfg_dir):
            os.makedirs(cfg_dir)

        self.cfg_add_path = os.path.join(self.dataset_dir, "train_cfg", "selected_train_cfg.yaml")
        shutil.copy(self.temp_files[self.model.currentText()], self.cfg_add_path)

        if self.gpu_server.currentText() == "HPC HoreKa":
            self._submit_job_to_hpc()
        else:
            self._train_local()
            
    def _submit_job_to_hpc(self):
        try:
            self._mode_status("Submission to HPC ...")
            client = SSHClient()
            client.load_system_host_keys()

            self.dataset_dir    = lsdf_path_converter(self.dataset_dir)
            self.result_dir     = lsdf_path_converter(self.result_dir)

            dvc_status = "1" if self.data_mode == "dvc" else "0" 

            client.connect("horeka.scc.kit.edu", port=22, username="sc1357", key_filename="C:\\AIDA_Software\\ssh\\id_rsa")
            export_vars = f"DVC={dvc_status},DATASET={self.dataset_dir},RESULT_DIR={self.result_dir},MODEL={self.project['task']}/{self.model.currentText()},"
            cmd = f"sbatch --export={export_vars} /home/hk-project-sppo/sc1357/devel/kaida/DLIP/scripts/start_train_hpc.sh"
            stdin, stdout, stderr = client.exec_command(cmd)

            self._processing_status(stdout.readlines())

            self._mode_status("Submission to HPC was successful")
            self._processing_status("Wait till training on HPC is finished")
        except:
            self._mode_status("Submission to HPC failed")

    def _train_local(self):
        # self.process.finished.connect(lambda: self.train_btn.setEnabled(True))

        self.cfg_base_path = os.path.join(self.path_project, "experiments", "configurations", "models", 
                self.project['task'], self.model.currentText()+"_base.yaml")

        cmd_write_yaml = [
            f"{self.path_project}/scripts/write_parameter_to_yaml.py",
            f"--cfg_file_path", 
            f"{self.cfg_base_path}",
            f"--dataset_path", 
            f"{self.dataset_dir}",
            f"--device", 
            "local"]

        self._mode_status(f'Write Parameter to YAML')

        self.process_yaml.finished.connect(self.dmap_gen)
        self.process_yaml.start(sys.executable, cmd_write_yaml)

    def dmap_gen(self):
        dmap_models = ["u_net", "u_net_uncertainty"]

        if (self.project['task'] == "instance_segmentation" or self.project['task'] == "panoptic_segmentation" )and self.model.currentText() in dmap_models:
            labels_dir = "labels" if self.project['task'] == "instance_segmentation" else "instance_labels"
            cmd_dmap_yaml = [
                f"{self.path_project}/utils/data_preparation/label_pre_processing/generate_distance_maps.py",
                f"--dataset_path", 
                f"{self.dataset_dir}",
                f"--instance_labels_dir", 
                f"{labels_dir}",
            ]
            self._mode_status(f"Generate Distance Maps")
            self._processing_status("")
            self.process_dmap.finished.connect(self.train_start)
            self.process_dmap.start(sys.executable, cmd_dmap_yaml)
        elif self.project['task'] == "seed_detection" and self.model.currentText() in dmap_models:
            cmd_dmap_yaml = [
                f"{self.path_project}/utils/data_preparation/label_pre_processing/generate_seed_maps.py",
                f"--dataset_path", 
                f"{self.dataset_dir}"
            ]
            self._mode_status(f"Generate Seed Masks")
            self._processing_status("")
            self.process_dmap.finished.connect(self.train_start)
            self.process_dmap.start(sys.executable, cmd_dmap_yaml)
        else:
            self.train_start()
        
    def train_start(self):
        cmd_train = [
            f"{self.path_project}/scripts/train.py",
            f"--config_files", 
            f"{self.cfg_base_path} {self.cfg_add_path}",
            f"--result_dir", 
            f"{self.result_dir}"
        ]

        self._mode_status(f'Training ...')
        self.process_train.setStandardErrorFile(os.path.join(self.result_dir, "train_log.txt"))
        self.process_train.finished.connect(self.train_end)
        self.process_train.start(sys.executable, cmd_train)

    def train_end(self):
        self._mode_status(f'Training process finished')
        self._processing_status('')
        self.train_btn.setEnabled(True)

    def _prepare_remote_dvc_dir(self):
        element2copy = [".dvc", ".git", "train.dvc", "test.dvc", "unlabeled.dvc", ".dvc_version"]
        dvc_path = str(Path(self.project["dvc_server_dir"]))
        local_path = str(Path(self.project["data_dir"]))
        if not os.path.exists(os.path.join(dvc_path, "selected_version")):
            os.makedirs(os.path.join(dvc_path, "selected_version"))
        for element in element2copy:
            src = os.path.join(local_path, element)
            dst = os.path.join(dvc_path, "selected_version", element)
            if os.path.exists(dst):
                if os.path.isfile(dst):
                    os.remove(dst)
                else:
                    try:
                        shutil.rmtree(dst, ignore_errors=False)
                    except PermissionError:
                        # On Windows the .dvc folder cannot be removed, due to read only files
                        # Therefore an errorhandler has to be used that changes the rights for the needed files
                        shutil.rmtree(dst, ignore_errors=False, onerror=onerror)
            try:
                shutil.copy2(src, dst)
            except:
                shutil.copytree(src, dst)

        return os.path.join(dvc_path, "selected_version")

    def data_printer_dmap(self):
        return self.data_printer(self.process_dmap)

    def data_printer_yaml(self):
        return self.data_printer(self.process_yaml)

    def data_printer_train(self):
        return self.data_printer(self.process_train)

    def data_printer(self, process):
        if platform.system() == "Windows":
            text = str(process.readAll().data().decode(CP_console))
        else:
            text = str(process.readAll().data().decode('utf-8'))

        self._processing_status(text)

    def open_logging(self):
        try:
            log_file = os.path.join(self.result_dir, "train_log.txt")
            if platform.system() == "Linux":
                subprocess.call(["xdg-open", log_file])
            else:
                os.startfile(log_file)
        except:
            self._mode_status("Logging file does not exist")

    def _mode_status(self, text):
        self.mode_info.setText(text)
        self.repaint()

    def _processing_status(self, text):
        self.processing_status.setText(text)
        self.repaint()
    
    def closeEvent(self, event):
        event.accept()
        try:
            self.process_dmap.kill()
            self.process_yaml.kill()
            self.process_train.kill()
        except:
            pass
