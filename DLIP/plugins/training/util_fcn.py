import os
from pathlib import Path
import yaml
import shutil

LOCAL_LSDF_PATH = "/lsdf"

def create_temporary_copy(path_project,path):
    temp_dir = os.path.join(path_project, "temp")
    temp_path = os.path.join(temp_dir, os.path.basename(path))
    shutil.copy(path, temp_path)
    return temp_path

def lsdf_path_converter(input_path):
    """ Get LSDF path from local user path. """
    # WARNING: This would crash if LSDF is not mounted
    #          somewhere with a different name than 'kit'
    # TODO: Avoid path conversion and directly
    #       suggest LSDF valid paths in GUI
    index = input_path.find(os.path.sep+"kit")
    return LOCAL_LSDF_PATH +  str(Path(input_path[index:]).as_posix())

def add_param_to_cfg(path, params):
    with open(path, "r") as file_descriptor:
        cfg_file = yaml.safe_load(file_descriptor)

    cfg_file.update(params)

    with open(path, 'w') as file_descriptor:
        yaml.safe_dump(cfg_file, file_descriptor)