from PyQt5 import QtWidgets, uic
import pandas as pd
import os
import yaml

class YamlFuserGui(QtWidgets.QDialog):
    display_name = "YAML Fuser"
    def __init__(self,path_project,project, parent_obj):
        super(YamlFuserGui, self).__init__()
        uic.loadUi(f"{path_project}/plugins/yaml_fuser/gui_yaml_fuser.ui", self)
     
    def select(self):
        filter = "yaml(*.yaml)"
        self.file_lst, _ = QtWidgets.QFileDialog().getOpenFileNames(self, "Select Files To Process", filter=filter)
  
    def fuse(self):
        if len(self.file_lst)>0: 
            summary = pd.DataFrame()
        
            for file in self.file_lst:
                file_name, ext = os.path.splitext(os.path.basename(file))

                if ext != ".yaml":
                    continue
            
                with open(file, 'r') as f:
                    properties= yaml.safe_load(f)
                    properties["name"] = file_name.replace("_props", "")

                summary = summary.append(properties, ignore_index=True)

            summary = summary.set_index("name")
            summary.to_csv(os.path.join(os.path.dirname(self.file_lst[0]), "summary.csv"))
            self.status_bar.setText("Fusing finished")
        else:
            self.status_bar.setText("No files were selected")
        
        self.repaint()