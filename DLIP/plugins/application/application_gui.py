
from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QPushButton
from DLIP.utils.loading.load_submodule import load_submodule

class ApplicationGui(QtWidgets.QDialog):
    display_name = "Application"
    def __init__(self,path_project, project, **kwargs):
        super(ApplicationGui, self).__init__()
        uic.loadUi(f"{path_project}/plugins/application/application_pipe_zoo.ui", self)
        self.path_project = path_project
        self.project=project
        self.rest_apis_lst = None
        self._load_rest_apis()

    def _load_rest_apis(self):
        self.rest_apis_lst = load_submodule(f"DLIP.plugins.application.rest_apis")

        for i in range(len(self.rest_apis_lst)):
            button = QPushButton(str(self.rest_apis_lst[i].display_name), self)
            button.setObjectName('button_' + str(i))
            button.clicked.connect(lambda _,idx=i: self.open_api(idx))
            self.verticalLayout.addWidget(button)
                
    def open_api(self, i):
        rest_api = self.rest_apis_lst[i](self.path_project,project=self.project)
        rest_api.exec_()