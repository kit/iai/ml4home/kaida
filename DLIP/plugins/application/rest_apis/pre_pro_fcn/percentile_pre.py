import numpy as np

class PercentilePrePro:
    display_name = "Percentile Normalization"
    def __init__(self):
        pass

    def pre_pro(img,**kwargs):
        min_val = int(np.percentile(img, 1)/2)
        max_val = np.percentile(img, 99)

        img[img<min_val] = min_val
        img[img>max_val] = max_val
        img_norm = (img - min_val) / (max_val - min_val)
        return img_norm