import numpy as np
import cv2
import tifffile
import os
import pandas
from skimage.measure import label, regionprops
from scipy.ndimage.morphology import binary_fill_holes

class MicrogliaPrePro:
    display_name = "Microglia Channel Stiching"
    def __init__(self):
        pass

    def pre_pro(img, result_folder,img_name):
        dapi_series = img[:,0,:,:].astype("uint8")
        dapi = np.maximum.reduce(dapi_series)

        microglia_series = img[:,1,:,:].astype("uint8")
        microglia = np.maximum.reduce(microglia_series)

        astrocytes_series = img[:,2,:,:].astype("uint8")
        astrocytes = np.maximum.reduce(astrocytes_series)

        ros_series = img[:,3,:,:].astype("uint8")
        ros = np.maximum.reduce(ros_series)

        img = np.dstack((dapi,microglia))
        #img = np.moveaxis(img,-1,0)


        #img = np.transpose(img, (1,2,0))
        #img = img[:,:,0:2]

        th=np.percentile(dapi,40)

        seg = ((dapi>th)).astype("uint8")

        seg=cv2.morphologyEx(seg,cv2.MORPH_CLOSE,kernel=np.ones((3,3)),iterations=1)
        seg =np.invert(seg)
        seg_label = label(seg)
        regions = regionprops(seg_label)
        areas = [region.area for region in regions]
        labels = [region.label for region in regions]
        for i,area in enumerate(areas):
            if area < dapi.shape[1]*dapi.shape[0]*0.005:
                seg[seg_label==labels[i]]=254
        seg=np.invert(seg)

        seg_label = label(seg)
        regions = regionprops(seg_label)
        areas = [region.area for region in regions]
        labels = [region.label for region in regions]
        for i,area in enumerate(areas):
            if area < dapi.shape[1]*dapi.shape[0]*0.005:
                seg[seg_label==labels[i]]=0
        
        seg=binary_fill_holes(seg)
        seg=seg.astype("uint8")
        seg[seg==1]=255

        img2 = np.dstack((dapi,microglia,astrocytes,ros,seg))
        img2 = np.moveaxis(img2,-1,0)

        tifffile.imsave(os.path.join(result_folder,img_name+'_dorsal_horn.tif'),img2,metadata={'axes':'CYX'},imagej=True)

        mask = np.bool_(seg)
        size_mask = cv2.countNonZero(seg)
        intensity_microglia = np.sum(microglia[mask])
        intensity_astrocytes = np.sum(astrocytes[mask])
        intensity_ros = np.sum(ros[mask])
        result = {'Image': [img_name],
                    'Number_of_Microglia': [0],
                    'Size': [size_mask],
                    'Intensity_Channel_1': [intensity_microglia], 
                    'Intensity_Astrocytes': [intensity_astrocytes], 
                    'Intensity_ROS': [intensity_ros]}
        result = pandas.DataFrame(data=result)
        if os.path.isfile(os.path.join(result_folder,'results.csv')):
            saved_result = pandas.read_csv(os.path.join(result_folder,'results.csv'),sep=';') 
            saved_result = pandas.concat([saved_result,result])
        else:
            saved_result = result
        saved_result.to_csv(os.path.join(result_folder,'results.csv'),sep=';',index=False)
        return img