from PyQt5 import QtCore, QtWidgets, uic
import requests
import json
import os
from io import BytesIO
import tifffile

ADDRESS_SERVER = "http://141.52.44.110:8000"

class DmaCpsRestApi(QtWidgets.QDialog):
    display_name = "DMA Cells Per Spot BF"
    def __init__(self,path_project, project=None):
        super(DmaCpsRestApi, self).__init__()
        uic.loadUi(f"{path_project}/plugins/application/rest_apis/layouts/dma_cps.ui", self)
        self.mode.addItems(["LSDF", "Local"])
        self.file_lst = list()
        self.result_folder = None

    def select_files(self):
        self.file_lst, _ = QtWidgets.QFileDialog().getOpenFileNames(self, "Select Files To Process")

        if len(self.file_lst)==0: 
            self.status.setText('No files were selected')
            self.repaint()

    def select_result_folder(self):
        self.result_folder = str(QtWidgets.QFileDialog.getSaveFileName(self, 'Insert Result Dir')[0])
        if len(self.result_folder)==0:
            self.status.setText('No result folder was selected')
            self.result_folder = None

    def process(self):
        if len(self.file_lst)>0: 
            headers = {
                    'Content-type': 'application/json',
            }
            for i, file in enumerate(self.file_lst):
                self.status.setText(f'Processing file {i+1} of {len(self.file_lst)}')
                self.repaint()
                if self.mode.currentText() == "LSDF": 
                    if self.result_folder is None:
                        data = json.dumps({"file_path": file}) 
                    else:
                        data = json.dumps({"file_path": file, "result_dir": self.result_folder}) 

                    response = requests.post(f'{ADDRESS_SERVER}/predict/lsdf', headers=headers, data=data)
                else:
                    with open(file, 'rb') as f:
                        data = f.read()

                    headers = {
                        'Content-Type': 'multipart/form-data',
                        'accept': 'application/json'
                    }

                    files = {
                        'image_file': (os.path.splitext(os.path.basename(file))[0], open(file, 'rb'), 'image/tiff'),
                    }

                    response = requests.post(f'{ADDRESS_SERVER}/predict/file', files=files)
                    image_type = response.headers.get('content-type')
                    image_raw = tifffile.imread(BytesIO(response.content))
                    if self.result_folder is None:
                        tifffile.imwrite(
                            file.replace(".tif", "_result.tif"),
                            image_raw, imagej=True)
                    else:
                        tifffile.imwrite(
                            os.path.join(
                                self.result_folder,
                                os.path.basename(file)[0].replace(".tif", "_result.tif")),
                            image_raw, imagej=True)

            response = requests.post(f'{ADDRESS_SERVER}/cmd/clear', headers=headers)
            self.status.setText(f'Processing finish - cleaning container memory')
            self.repaint()
        else:
            self.status.setText('Processing not possible - no files were selected')
            self.repaint()