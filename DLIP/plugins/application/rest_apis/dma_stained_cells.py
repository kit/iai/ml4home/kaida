from cgitb import lookup
from PyQt5 import QtCore, QtWidgets, uic
import requests
import json
import os
from io import BytesIO
import tifffile
from collections import Counter
import numpy as np
from DLIP.utils.helper_functions.dma_clustering import get_dma_list


ADDRESS_SERVER = "http://141.52.44.110:5000"

class DmaStainedCellsRestApi(QtWidgets.QDialog):
    display_name = "DMA Stained Cells"
    def __init__(self,path_project, project=None):
        super(DmaStainedCellsRestApi, self).__init__()
        uic.loadUi(f"{path_project}/plugins/application/rest_apis/layouts/dma_stained_cells.ui", self)
        self.cell_type_cb.addItems(["cll", "su-dhl"])
        self.mode.addItems(["LSDF"])
        ch_lst = ["None","ch00", "ch01", "ch02", "ch03"]
        self.hoechst_cb.addItems(ch_lst)
        self.calcein_cb.addItems(ch_lst)
        self.pi_cb.addItems(ch_lst)
        self.mag_cb.addItems(["10", "20"])
        self.file_lst = list()
        self.result_folder = None

    def select_files(self):
        self.file_lst, _ = QtWidgets.QFileDialog().getOpenFileNames(self, "Select Files To Process")

        if len(self.file_lst)==0: 
            self.status.setText('No files were selected')
            self.repaint()

    def process(self):
        channel_lookup = self._read_channel_lookup()

        if lookup is not None:
            if len(self.file_lst)>0: 
                headers = {
                        'Content-type': 'application/json',
                }

                dma_spot_lst = get_dma_list(self.file_lst)

                for i, dma_spot in enumerate(dma_spot_lst):
                    self.status.setText(f'Processing file {i+1} of {len(dma_spot_lst)}')
                    self.repaint()

                    data = json.dumps({
                        "dma_file_path": dma_spot, 
                        "channel_lookup": channel_lookup, 
                        "magnification":  self.mag_cb.currentText(), 
                        "cell_line": self.cell_type_cb.currentText()}
                        ) 
                    response = requests.post(f'{ADDRESS_SERVER}/predict/lsdf', headers=headers, data=data)

                response = requests.post(f'{ADDRESS_SERVER}/cmd/clear', headers=headers)
                self.status.setText(f'Processing finish - cleaning container memory')
                self.repaint()
            else:
                self.status.setText('Processing not possible - no files were selected')
                self.repaint()
        else:
            self.status.setText('Processing not possible - channel assignment wrong')
            self.repaint()
        

    def _read_channel_lookup(self):
        channel_lookup = {
            "Hoechst": self.hoechst_cb.currentText(), 
            "Calcein": self.calcein_cb.currentText(), 
            "PI": self.pi_cb.currentText(), 
        }

        if len(np.unique(list(channel_lookup.values())))==3 and "None" not in channel_lookup.values():
            return channel_lookup
        else:
            return None