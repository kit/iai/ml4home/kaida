import os
import numpy as np
import tifffile
from skimage.measure import label, regionprops_table
import yaml
import pandas as pd

prop_lst = ['centroid','area','axis_major_length','axis_minor_length', 'eccentricity']

from skimage.color import label2rgb

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)

class DropExport:
    display_name = "Drop Export"
    def __init__(self):
        pass

    def post_pro(img, pred, img_name, result_dir):
        if pred.shape==(3368,6000):
            pred_post_pro = np.zeros_like(pred)
            start_y = int(600/3368*pred_post_pro.shape[0])
            delta_y = int(2600/3368*pred_post_pro.shape[0])
            start_x = int(1800/6000*pred_post_pro.shape[1])
            delta_x = int(2400/6000*pred_post_pro.shape[1])
            pred_post_pro[start_y:start_y+delta_y,start_x:start_x+delta_x] = pred[start_y:start_y+delta_y,start_x:start_x+delta_x]
        else:
            pred_post_pro = pred.copy()

        overlay = label2rgb(pred_post_pro, image=normalize(img))
        tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), (255*overlay).astype(np.uint8))

        mask = label(pred_post_pro)
        props_table =  regionprops_table(mask, img, properties=prop_lst) 

        df = pd.DataFrame(props_table)
        df.to_csv(os.path.join(result_dir, img_name+"_props.csv"))