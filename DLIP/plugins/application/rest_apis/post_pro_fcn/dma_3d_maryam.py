import os
import numpy as np
import tifffile
import pandas as pd

from skimage.color import label2rgb
from skimage.measure import label, regionprops
import yaml
import cv2

prop_lst = ['centroid','orientation','axis_major_length','axis_minor_length']

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)

class Dma3dMaryam:
    display_name = "DMA 3D Maryam"
    def __init__(self):
        pass

    def post_pro(img, pred, img_name, result_dir):
        mask = label(pred)
        regions =  sorted(regionprops(mask), key=lambda r: r.area, reverse=True) 

        properties = {
            "num_instances": None, "area": None, "axis_major_length": None, "axis_minor_length": None, 
            "mean": None, "std": None, "norm_std": None, "norm_edge_ratio": None,
        }
        properties["num_instances"] = len(regions)
        if properties["num_instances"] > 0:
            for props in regions:
                properties["area"] = int(props.area)
                properties["axis_major_length"] = props.axis_major_length
                properties["axis_minor_length"] = props.axis_minor_length
                mask[mask!=props.label] = 0
                break
            properties["mean"], properties["std"] = float(np.mean(img[mask>0])), float(np.std(img[mask>0]))
            properties["norm_std"] = float(properties["std"]/properties["mean"])
            img_uint8 = (img/255).astype("uint8")
            edge = cv2.Canny(img_uint8,60/255*np.max(img_uint8),120/255*np.max(img_uint8))
            edge = cv2.morphologyEx(edge, cv2.MORPH_CLOSE, np.ones((3,3)), iterations=3)
            properties["norm_edge_ratio"] = float(np.sum(edge[mask>0]>0)/properties["area"])
        else:
            properties["area"], properties["axis_major_length"], properties["axis_minor_length"], \
            properties["mean"], properties["std"],properties["norm_edge_ratio"], properties["norm_std"]  = -1, -1, -1, -1, -1,-1, -1

        overlay = label2rgb(mask>0, image=normalize(img))

        tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), (255*overlay).astype(np.uint8))

        with open(os.path.join(result_dir, img_name+"_props.yaml"), 'w') as f:
            yaml.dump(properties, f)