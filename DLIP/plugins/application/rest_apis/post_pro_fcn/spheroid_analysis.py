import os
import numpy as np
import tifffile
import pandas as pd
import yaml

from skimage.color import label2rgb
from skimage.measure import label, regionprops
from skimage.color import label2rgb


prop_lst = ["area","bbox", "centroid", "area_convex","eccentricity","equivalent_diameter_area","euler_number","extent","feret_diameter_max"]


def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)

class SpheroidAnalysis:
    display_name = "Spheroid Analysis"
    def __init__(self):
        pass

    def post_pro(img, pred, img_name, result_dir):
        mask = label(pred)
        regions =  sorted(regionprops(mask), key=lambda r: r.area, reverse=True) 

        properties = {"num_instances": None, "area": None, "axis_major_length": None, "axis_minor_length": None,}
        properties["num_instances"] = len(regions)
        if properties["num_instances"] > 0:
            for props in regions:
                properties["area"] = int(props.area)
                properties["axis_major_length"] = props.axis_major_length
                properties["axis_minor_length"] = props.axis_minor_length
                mask[mask!=props.label] = 0
                break
        else:
            properties["area"], properties["axis_major_length"], properties["axis_minor_length"] = -1, -1, -1

        with open(os.path.join(result_dir, img_name+"_props.yaml"), 'w') as f:
            yaml.dump(properties, f)

        overlay = label2rgb(mask>0, image=normalize(img))
        tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), (255*overlay).astype(np.uint8))
