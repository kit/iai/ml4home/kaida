import os
import numpy as np
import tifffile
import pandas as pd

from skimage.color import label2rgb
from skimage.measure import label, regionprops_table
from skimage.color import label2rgb

prop_lst = ['centroid','orientation','axis_major_length','axis_minor_length']

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)

class RegionProps:
    display_name = "Region Proposals"
    def __init__(self):
        pass

    def post_pro(img, pred, img_name, result_dir):
        mask = label(pred)
        props_table =  regionprops_table(mask, img, properties=prop_lst) 

        df = pd.DataFrame(props_table)
        df.to_csv(os.path.join(result_dir, img_name+"_props.csv"))

        overlay = label2rgb(pred>0, image=normalize(img))
        tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), (255*overlay).astype(np.uint8))