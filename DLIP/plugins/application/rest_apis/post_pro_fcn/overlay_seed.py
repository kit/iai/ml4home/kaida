import os
import numpy as np
import tifffile
import yaml
from scipy.ndimage import binary_dilation
from skimage.morphology import disk

from skimage.color import label2rgb

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)

class SeedExport:
    display_name = "Seed Export"
    def __init__(self):
        pass

    def post_pro(img, pred, img_name, result_dir):
        properties = {
                    "num_instances": None, 
                }

        properties["num_instances"] = int(np.sum(pred))

        pred = binary_dilation(pred,  disk(5)) > 0
        overlay = label2rgb(pred, image=normalize(img))
        tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), (255*overlay).astype(np.uint8))

        with open(os.path.join(result_dir, img_name+"_props.yaml"), 'w') as f:
            yaml.dump(properties, f)