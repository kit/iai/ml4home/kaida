import os
import numpy as np
import tifffile
from skimage.measure import label, regionprops
import yaml
import cv2
from DLIP.utils.visualization.inst_seg_contour import visualize_instances_map

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)

def normalize_percentile(img):
    min_val = np.percentile(img, 1)
    max_val = np.percentile(img, 99)
            
    img[img<min_val] = min_val
    img[img>max_val] = max_val
    img_norm = ((img - min_val) / (max_val - min_val))
         
    return (img_norm*255).astype(np.uint8)

class Dma2dMaryam:
    display_name = "DMA 2D Maryam"
    def __init__(self):
        pass

    def post_pro(img, pred, img_name, result_dir):
        mask = label(pred)
        regions =  regionprops(mask)

        properties = {
            "num_instances": None, 
        }

        properties["num_instances"] = int(len(regions))

        overlay = visualize_instances_map(cv2.cvtColor(normalize_percentile(img), cv2.COLOR_GRAY2RGB),mask)

        tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), overlay)

        with open(os.path.join(result_dir, img_name+"_props.yaml"), 'w') as f:
            yaml.dump(properties, f)