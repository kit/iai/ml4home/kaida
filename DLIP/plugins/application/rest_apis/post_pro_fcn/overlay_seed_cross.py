import os
import numpy as np
import tifffile
import yaml
from scipy.ndimage import binary_dilation
from skimage.morphology import disk
import cv2
from DLIP.utils.data_preparation.label_pre_processing.centroid_post_pro import centroid_postprocessing

def draw_cross(pred,img):
    y_coords,x_coords = np.where(centroid_postprocessing(pred)>0)

    for i in range(len(x_coords)):
        cv2.drawMarker(img, (int(x_coords[i]),int(y_coords[i])), color=(255,0,0), markerType=cv2.MARKER_CROSS, thickness=1, markerSize = 10)
        
    return img

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)

class SeedExportCross:
    display_name = "Seed Export Cross"
    def __init__(self):
        pass

    def post_pro(img, pred, img_name, result_dir):
        properties = {
                    "num_instances": None, 
                }

        properties["num_instances"] = int(np.sum(pred))

        pred = binary_dilation(pred,  disk(5)) > 0
        overlay = draw_cross(pred, cv2.cvtColor(normalize(img),cv2.COLOR_GRAY2RGB))
        tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), overlay.astype(np.uint8))

        with open(os.path.join(result_dir, img_name+"_props.yaml"), 'w') as f:
            yaml.dump(properties, f)