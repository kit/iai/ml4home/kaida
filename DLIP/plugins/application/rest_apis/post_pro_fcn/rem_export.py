import os
import numpy as np
import tifffile
from skimage.measure import label, regionprops_table
import pandas as pd
from skimage.color import label2rgb

# List of properties to extract from labeled regions (including bbox and coords for internal use)
prop_lst = ['area', 'feret_diameter_max', 'bbox', 'coords']

# Function to normalize an image to a range of 0 to 255
def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val != max_val:
        img_norm = (img - min_val) / (max_val - min_val)
    else:
        img_norm = np.zeros_like(img)
    return (img_norm * 255).astype(np.uint8)

class RemExport:
    display_name = "REM Export"
    def __init__(self):
        pass

    @staticmethod
    def post_pro(img, pred, img_name, result_dir):
        if pred.shape == (3368, 6000):
            pred_post_pro = np.zeros_like(pred)
            start_y = int(600 / 3368 * pred_post_pro.shape[0])
            delta_y = int(2600 / 3368 * pred_post_pro.shape[0])
            start_x = int(1800 / 6000 * pred_post_pro.shape[1])
            delta_x = int(2400 / 6000 * pred_post_pro.shape[1])
            pred_post_pro[start_y:start_y + delta_y, start_x:start_x + delta_x] = pred[start_y:start_y + delta_y, start_x:start_x + delta_x]
        else:
            pred_post_pro = pred.copy()
        
        # Initial labeling to detect edge particles
        mask = label(pred_post_pro)
        props = regionprops_table(mask, img, properties=prop_lst)
        
        # Remove particles touching the borders
        for i in range(len(props["coords"])):
            if (props["bbox-0"][i] == 0 or props["bbox-1"][i] == 0 or 
                props["bbox-2"][i] == pred_post_pro.shape[0] or 
                props["bbox-3"][i] == pred_post_pro.shape[1]):
                for pix in props["coords"][i]:
                    pred_post_pro[pix[0], pix[1]] = 0  # Set edge particle pixels to zero

        # Re-label the mask after removing edge particles
        mask = label(pred_post_pro)
        
        # Extract properties from the updated mask with edge particles excluded
        props_table = regionprops_table(mask, img, properties=prop_lst)
        
        # Convert properties to DataFrame and calculate the equivalent-area diameter (X_A)
        df = pd.DataFrame(props_table)
        df = df.drop(columns=[col for col in df.columns if 'bbox' in col or 'coords' in col])

        # Calculate the equivalent-area diameter and add it as a new column
        df['equal_area_diameter'] = 2 * np.sqrt(df['area'] / np.pi)

        # Save the DataFrame to a CSV file
        df.to_csv(os.path.join(result_dir, img_name + "_props.csv"))

        # Create an overlay image and save it as a TIFF file
        overlay = label2rgb(mask, image=normalize(img))
        tifffile.imwrite(os.path.join(result_dir, img_name + "_overlay.tif"), (255 * overlay).astype(np.uint8))
