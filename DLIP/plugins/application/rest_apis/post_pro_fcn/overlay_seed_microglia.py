import os
import numpy as np
import tifffile
import yaml
from scipy.ndimage import binary_dilation
from skimage.morphology import disk
import cv2
import pandas
from skimage.color import label2rgb
from DLIP.utils.data_preparation.label_pre_processing.centroid_post_pro import centroid_postprocessing

def draw_cross(pred,img):
    y_coords,x_coords = np.where(centroid_postprocessing(pred)>0)

    for i in range(len(x_coords)):
        cv2.drawMarker(img, (int(x_coords[i]),int(y_coords[i])), color=(0,0,255), markerType=cv2.MARKER_CROSS, thickness=2)
        
    return img

def normalize(img):
    min_val = np.min(img)
    max_val = np.max(img)
    if min_val!=max_val:
        img_norm = (img-min_val)/(max_val-min_val)
    return (img_norm*255).astype(np.uint8)

class SeedExportMicroglia:
    display_name = "Seed Export Microglia"
    def __init__(self):
        pass

    def post_pro(img, pred, img_name, result_dir):
        properties = {
                    "num_instances": None, 
                }

        properties["num_instances"] = int(np.sum(pred))

        pred = binary_dilation(pred,  disk(5)) > 0
        img_new = np.zeros((img.shape[0],img.shape[1],3), dtype=np.uint8)
        img_new[:,:,0:2] = img
        overlay = draw_cross(pred, img_new)
        tifffile.imwrite(os.path.join(result_dir, img_name+"_overlay.tif"), overlay)

        """with open(os.path.join(result_dir, img_name+"_props.yaml"), 'w') as f:
            yaml.dump(properties, f)"""
        saved_result = pandas.read_csv(os.path.join(result_dir,'results.csv'),sep=';') 
        saved_result.loc[(saved_result.Image == img_name),'Number_of_Microglia']=properties["num_instances"]
        saved_result.to_csv(os.path.join(result_dir,'results.csv'),sep=';',index=False)