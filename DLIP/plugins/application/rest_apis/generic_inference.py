import os
import tarfile
from PyQt5 import QtWidgets, uic, QtCore
from DLIP.utils.loading.load_submodule import load_submodule
import tifffile
import cv2
import numpy as np
import torch
from copy import deepcopy

from DLIP.utils.helper_functions.window_inference import window_inference
from pytorch_lightning.utilities.seed import seed_everything
from DLIP.utils.loading.initialize_wandb import initialize_wandb
from DLIP.utils.loading.load_transforms import load_transforms
from DLIP.utils.loading.load_model import load_model
from DLIP.utils.loading.merge_configs import merge_configs
from DLIP.utils.loading.split_parameters import split_parameters
from DLIP.utils.data_preparation.label_pre_processing.centroid_post_pro import centroid_postprocessing
import imageio as io

class ProcessingThread(QtCore.QThread):
    progress = QtCore.pyqtSignal(int)
    def __init__(self, model, file_lst, trafo, result_folder, pre_pro_fcn, post_pro_fcn, parent=None):
        super(ProcessingThread, self).__init__(parent)
        self.model = model
        self.file_lst = file_lst
        self.trafo = trafo
        self.result_folder = result_folder
        self.pre_pro_fcn = pre_pro_fcn
        self.post_pro_fcn = post_pro_fcn

    def run(self):
        ind = 0
        for file in self.file_lst:
            if ".tif" in file:
                img = tifffile.imread(file)
            else:
                img = io.imread(file)

            if self.pre_pro_fcn is not None:
                img = self.pre_pro_fcn.pre_pro(img, self.result_folder,os.path.splitext(os.path.basename(file))[0])

            if len(img.shape)==2:
                img_shape_0 = img.shape
            else: 
                channel_id = np.argmin(img.shape)
                img_shape_0 = img.shape[:2] if channel_id==2 else img.shape[1:] 

            input_tensor,_,_ = self.trafo[0](img)
            prediction = window_inference(self.model,input_tensor, n_classes=1, window_size=512, use_gpu=torch.cuda.is_available()).squeeze().numpy()
            if self.model.__class__.__name__ == "UnetInstance":
                prediction = self.model.post_pro.process(prediction, None)
            elif self.model.__class__.__name__ == "UnetSemantic":
                prediction =  (prediction>0.5).astype(np.uint8)
            elif self.model.__class__.__name__ == "UnetSeed":
                prediction =  centroid_postprocessing(prediction, do_binarize=True).astype(np.uint8)
            
            prediction = cv2.resize(prediction, (img_shape_0[1],img_shape_0[0]), interpolation=cv2.INTER_NEAREST)
            self.post_pro_fcn.post_pro(
                img, 
                prediction, 
                os.path.splitext(os.path.basename(file))[0], 
                self.result_folder
            )

            self.progress.emit(ind)
            ind +=1

def get_model_trafo(tar_file):
    tar = tarfile.open(tar_file, "r:")
    base_path = os.path.dirname(tar_file)
    tar.extractall(path=base_path)
    tar.close()

    # load model
    cfg_yaml = merge_configs(os.path.join(base_path, "cfg.yaml"))

    config = initialize_wandb(
        cfg_yaml=cfg_yaml,
        disabled=True,
        experiment_dir=None,
        config_name=None,
    )

    seed_everything(seed=cfg_yaml['experiment.seed']['value'])
    parameters_splitted = split_parameters(config, ["model", "train", "data"])

    model = load_model(
        parameters_splitted["model"], 
        checkpoint_path_str=os.path.join(base_path, "dnn_weights.ckpt")
    )

    if torch.cuda.is_available():
        model.cuda()
    model.eval()

    _, _, test_trafos = load_transforms(parameters_splitted["data"])

    os.remove(os.path.join(base_path, "dnn_weights.ckpt"))
    os.remove(os.path.join(base_path, "cfg.yaml"))

    return model, test_trafos


class GenericInference(QtWidgets.QDialog):
    display_name = "Generic Inference"
    def __init__(self,path_project, project=None):
        super(GenericInference, self).__init__()
        uic.loadUi(f"{path_project}/plugins/application/rest_apis/layouts/generic_inference.ui", self)
        self.computing_device.addItems(["Local"])
        self.data_mode.addItems(["Local"])

        self._read_pre_pro_fcn()
        self._read_post_pro_fcn()

        self.file_lst = list()
        self.result_folder = None
        self.model = None
        self.trafo = None

    def select_files(self):
        self.file_lst, _ = QtWidgets.QFileDialog().getOpenFileNames(self, "Select Files To Process")
        if len(self.file_lst)==0: 
            self.status.setText('No files were selected')
            self.repaint()

    def process(self):
        if self.model is None or len(self.file_lst)==0:
            self.status.setText('Processing not possible')
            self.repaint()
        else:
            if self.result_folder is None:
                self.result_folder = os.path.join(os.path.dirname(self.file_lst[0]), "results")
            if not os.path.exists(self.result_folder):
                os.mkdir(self.result_folder)

            trafo = deepcopy(self.trafo)
            if not self.resize_cb.isChecked():
                trafo_names = [elem.__class__.__name__ for elem in trafo[0].transform["pre"].transforms.transforms]
                if "Resize" in trafo_names:
                    ind = trafo_names.index("Resize")
                    del trafo[0].transform["pre"].transforms.transforms[ind]

            post_pro_fcn = self.post_pro_fcn_lst[self.post_pro.currentIndex()]
            pre_pro_fcn = self.pre_pro_fcn_lst[self.pre_pro.currentIndex()]

            filter_lst = self.exclude_filter.text().split(",")

            if len(filter_lst[0])>0:
                self.file_lst = [elem for elem in self.file_lst if not any([filter in elem for filter in filter_lst])]

            self.thread = ProcessingThread(self.model,self.file_lst,trafo,self.result_folder,pre_pro_fcn,post_pro_fcn)
            self.thread.progress.connect(self.img_processing_done)
            self.thread.finished.connect(self.on_finished)
            self.thread.start()
            
            self.result_folder = None

    def img_processing_done(self, ind):
        self.status.setText(f"Processing {ind+1}/{len(self.file_lst)}")
        self.repaint()

    def on_finished(self):
        self.status.setText(f"Processing done!")
        self.repaint()

    def select_result_folder(self):
        selected_folder = str(QtWidgets.QFileDialog().getExistingDirectory(self, "Select Folder To Save Results"))
        if len(selected_folder) == 0:
            self.status.setText('No folder was selected')
            self.repaint()
        else:
            self.result_folder = selected_folder

    def _read_post_pro_fcn(self):
        self.post_pro_fcn_lst = load_submodule(f"DLIP.plugins.application.rest_apis.post_pro_fcn")
        self.post_pro.addItems([fcn.display_name for fcn in self.post_pro_fcn_lst])

    def _read_pre_pro_fcn(self):
        self.pre_pro_fcn_lst = [None] + load_submodule(f"DLIP.plugins.application.rest_apis.pre_pro_fcn")
        self.pre_pro.addItems([fcn.display_name if fcn is not None else "None" for fcn in self.pre_pro_fcn_lst])

    def load_model(self):
        tar_file = str(QtWidgets.QFileDialog().getOpenFileName(self, "Select Model File")[0])
        if len(tar_file)==0:
            self.status.setText('No model was selected')
            self.repaint()
            return
        else:
            self.model, self.trafo = get_model_trafo(tar_file)