from DLIP.post_inspection.abstract_post_inspector import AbstractPostInspector
import numpy as np

class DummyInspector(AbstractPostInspector):
    def __init__(self, project):
        super(DummyInspector, self).__init__(project)

    def inspect(self, label, img, status_bar):
        return 0.0