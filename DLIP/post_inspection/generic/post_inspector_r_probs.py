from DLIP.post_inspection.abstract_post_inspector import AbstractPostInspector
from skimage.measure import label as label_fcn
from skimage.measure import regionprops
import numpy as np

class RegionProposalInspector(AbstractPostInspector):
    def __init__(self, project, proposal, acceptance_intervall):
        super(RegionProposalInspector, self).__init__(project)
        self.proposal = proposal
        self.acceptance_intervall = acceptance_intervall

    def inspect(self, label, img, status_bar):
        r_props = regionprops(label_fcn(label))
        criteria_lst = list()
        for prop in r_props:
            if np.min(self.acceptance_intervall) <= prop[self.proposal] <= np.max(self.acceptance_intervall):
                criteria_lst.append(1)
            else:
                criteria_lst.append(0)
        
        if len(criteria_lst)>0:
            return np.mean(criteria_lst)
        else:
            return 1.0 