from DLIP.post_inspection.abstract_post_inspector import AbstractPostInspector
from skimage.measure import label as label_fcn
import numpy as np

class SegmentCounter(AbstractPostInspector):
    def __init__(self, project, acceptance_intervall):
        super(SegmentCounter, self).__init__(project)
        self.acceptance_intervall = acceptance_intervall

    def inspect(self, label, img, status_bar):  
        if not (np.min(self.acceptance_intervall) <=np.max(label_fcn(label)) <=np.max(self.acceptance_intervall)):
            return 0.0 
        else:
            return 1.0