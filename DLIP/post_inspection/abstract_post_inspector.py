from abc import ABC, abstractmethod

class AbstractPostInspector(ABC):
    def __init__(self, project):
        super().__init__()
        self.project = project

    @abstractmethod
    def inspect(self, label, img, status_bar):
        raise NotImplementedError("Process fcn needs to be implemented")