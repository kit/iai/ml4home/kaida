# Testing UI functionality

If parts of the UI were changed, it's not possible to test them using e.g. unit tests.
Therefore, we are collecting different use cases of the ui to be considered when manually testing the ui.


Different kinds of projects (each time for all four different labeling tasks):
- new project without version control
- new project with version control
- loaded project
- project from remote


Labeling GUI:
- with or without preprocessing and/or pre-annotation
- with or without post-assistance
- go one sample back (<)
- go to the next sample (>)
- reinspect a sample
- remove label
- remove a sample
- add a new sample


Version Control:
- try to save a version without changes
- save a version
- load a version
