# KaIDA - Karlsruhe Image Data Annotation Tool

[Marcel P. Schilling](mailto:marcel.schilling@kit.edu), Svenja Schmelzer, Lukas Klinger, Markus Reischl

Institute for Automation and Applied Informatics, Karlsruhe Institute of Technology, Germany
![Overview](img/kaida_logo.png)

This repository is intended to be used as a generic tool for assisted image annotation.

## Citation
BIBTEX
```
@article{SchillingSchmelzerKlingerReischl+2022,
    url = {https://doi.org/10.1515/jib-2022-0018},
    title = {KaIDA: a modular tool for assisting image annotation in deep learning},
    title = {},
    author = {Marcel P. Schilling and Svenja Schmelzer and Lukas Klinger and Markus Reischl},
    pages = {20220018},
    journal = {Journal of Integrative Bioinformatics},
    doi = {doi:10.1515/jib-2022-0018},
    year = {2022},
    lastchecked = {2022-09-16}
}
```
MLA

Schilling, Marcel P., Schmelzer, Svenja, Klinger, Lukas and Reischl, Markus. "KaIDA: a modular tool for assisting image annotation in deep learning" Journal of Integrative Bioinformatics, 2022, pp. 20220018. https://doi.org/10.1515/jib-2022-0018


## Installation
### Prerequisites
- [Anaconda Distribution](https://www.anaconda.com/products/individual)
- Installation of [`git`](https://git-scm.com/downloads)
- Download of needed [Image Labeling Tool](https://bitbucket.org/abartschat/imagelabelingtool/) from A. Bartschat -> please use compatible version available [here](https://osf.io/5zcye/) (Windows or Ubuntu) 

### Clone Repository
```
git clone https://gitlab.kit.edu/kit/iai/ml4home/kaida.git
```

### Create Conda Environment and Install Required Packages including KaIDA
```
conda env create -f kaida/env_kaida.yml
```
Note: Use conda to gurantee full functionality (e.g. GPU support) of used libraries 

### Unpack Image Labeling Tool
Unpack Image Labeling Tool to a folder parallel to cloned git repository leading to the following folder structure:
```
├── kaida
│   ├── ...  
│   ...
├── ImageLabelingTool [Windows] or ImageLabelingTool_Ubuntu [Ubuntu]
│   ├── ...  
│   ...
```

## Usage
Activate conda environment

```
conda activate kaida
```
and start KaIDA via executing `python` script

```
python kaida/DLIP/scripts/run_kaida.py
```

## Start - Project Selection
It is possible to 
- create a new project,
- load a already initialized project, or
- load a project from a remote cloud server.

<img img src="img/start_project.png" width="800"/>

### Create Project
Add all meta-information to project

<img img src="img/create_project.png" width="400"/>

Number and names of classes have to be entered without considering the background as separate class.  

There is the possibility to use Data Version Control for the dataset. With Data Version Control, different versions of the dataset can be saved to a remote directory. Furthermore, one can roll back to an older dataset version. Data Version Control can be selected using the corresponding checkbox.

If Data Version Control is selected, an external directory has to be specified using the "Select Server Dir" button.


### Load a project from a remote server

Selecting "Create project remote..." (or Ctrl+R) opens the following dialog window:

<img img src="img/load_external_project_dialog.png" width="300"/>

The button "select external project file" opens a file dialog, where one can specify the project file, that should be used. A project file can be:
- the json file containing all information about the project, that is saved on creating a project
- a version info file. This one is saved at the remote directory as "versions/<version_number>/dvc_project_version_info.json"

The button "select local data dir" opens a file dialog, where one can specify the local directory where the project should be initialized.

If the remote project is not using dvc, one has to specify an external directory to copy the data from or the data has to be inside the local directory already.

On clicking "Ok", the project file is tested for validity and optionally the needed data is loaded from the external directory.


## Image Annotation
After loading or initializing a project, annotation can be started.

<img img src="img/start_labeling.png" width="400"/>

### General
![Instance Segmentation Image](img/label_assistant_gui.png)  
In the first part of the GUI, configurational settings can be altered. A configuration is saved even if the GUI is closed. The saved configuration will be loaded next time the GUI is opened.  
The second part of the GUI is for navigating through the images and checking the annotation progress.
#### Sampler
Choose the sampler which will sort the not annotated images.
#### Pre-Assistance
Pre-processing functions and pre-annotation functions can be specified. Those function will be applied on each image before it is shown for annotation.
#### Post-Assistance
An annotation inspection function gives a feedback if the warning threshold is reached. Inconsistent annotations will be displayed for reinspection.  
Post annotation processing function is applied after the manual annotation. If "Show Comparison" is activated, the manual annotation and the correction of the post annotation processing function is displayed. The user can decide which one to keep.
#### Test/Train Dataset Ratio
Define the percentage of images of the train dataset which will be transfered to the test dataset when the "Split into Train/Test Dataset" Button is used.
#### Image handling with KaIDA
Start sampling by pressing the "Sampling" Button to initialize the specified sampler in order to sort the not annotated images.  
Start the image annotation tool by clicking "Open". With the buttons left and right of it one can switch between the images. The progess bar shows how many percent of the dataset is already annotated.  
Already annotated images can be reinspected by selecting the image in the processed samples list (eventually one has to change the dataset from train to test with the drop down menu). Clicking the "Reinspect Sample" button then opens the image annotation tool for reinspection and annotation correction.
Further, by double clicking an annotated sample, it can be "deleted" in two ways. First, a selection of abort deletes the sample completely and removes it from the dataset. Second, by selecting yes, the sample is shifted back to the not annotated dataset (only removing annotation).

#### Data Versioning
![Data Versioning Menu](img/data_versioning_menu.png)

In the menu bar is the drop-down menu 'Data Versioning', which has two sub items 'Save Version' and 'Load Version'.

![Save Dataset Version Dialog](img/sava_dataset_version_dialog.png)

Clicking on 'Save Version' opens the dialog window seen above. There, one can enter a commit message describing the changes in the current version. On clicking 'Ok', the version is saved and the changed data is pushed to the remote directory. If there is a lot of new data in the new version (e.g. on the first commit), this can take some minutes.

![Load Dataset Version Dialog](img/load_dataset_version_dialog.png)

Choosing 'Load Version' opens the dialog window above, where one can select the version to load in a drop-down menu.

Below the drop-down menu are additional information about the selected version. At first, there is the previous version on which the selected one is building on followed by the commit message.

Furthermore, there is a list of the new labeled data, changed labels and added data. In case of image classification the class label is written behind the image name.


### Image Processing Tasks

#### Instance Segmentation
![Instance Segmentation Image](img/instance_seg.png)
1) Display properties
2) Drawing properties  
brush size: size of used brush
erase: erases a part of the segment -> start and end drawing the contours of the part to erase by right-clicking  
cross hairs: displays a crosshair  
masks on: displays masks 
opacity: define masks opacity  
outlines on: marks the outlines of each segment
1) Drawing area  
Drawing: start and end drawing by right mouse click (if the segment is big enough and single stroke is activated drawing is stopped automatically when the start point is reached again)
Delete total instance: Ctrl + left mouse click  
Merge 2 instances: select one instance by left-click and alt + left-click on second instance
4) Image sector navigation

#### Semantic Segmentation
![Semantic Segmentation Image](img/semantic_segmentation.png)
Chose class by clicking on the colored buttons (C1-C9). Deselect class by clicking on the "background" button and press and hold ctrl while drawing.  
Draw by holding the left mouse-button (or by touch in the tablet-mode).
Undo last drawn segment by clicking "Undo" button.  
Change pen size by mouse wheel scroll or by entering the size (Pen size) or by using the slider.  
Press ctrl while scrolling zooms in and out.  
The buttons "Open Image", "Image Dir", "Output Dir", "<<" and ">>" have no function. The GUI will be updated in the future.
#### Classification
![Classification Image](img/classification.png)  
Use buttons to select class.
#### Seed Detection
![Seed Detection Image](img/seed_detection.png)
Chose class by clicking on the colored buttons (C1-C9). Deselect class by clicking on the "background" button and press and hold ctrl while drawing.  
Draw by holding the left mouse-button (or by touch in the tablet-mode).
Undo last drawn segment by clicking "Undo" button.  
Change pen size by mouse wheel scroll or by entering the size (Pen size) or by using the slider.  
Press ctrl while scrolling zooms in and out.  
The buttons "Open Image", "Image Dir", "Output Dir", "<<" and ">>" have no function. The GUI will be updated in the future.


## Plugins
<img img src="img/start_plugins.png" width="800"/>  

### Annotation Inspection

![Annotation Inspection](img/annotation_inspection.png) 

Annotation Inspection is a plugin to improve the annotation quality of datasets. Therefore, either the annotation quality within a dataset of one annotator (Intra) or the variability between two datasets annotated by two different persons (Inter) can be inspected.

#### Intra Annotator Variability

![Intra Annotation Inspection](img/intra_annotation_inspection.png)

For the Intra Annotator Variability each image in a dataset is compared to the prediction of a network trained on the dataset. This way annotation errors can be detected.

To do this a user has multiple possibilities:
- train a model and calculate the model predictions
- load an already trained model and calculate the model predictions
- load already calculated model predictions

Once the network predictions are available, the user inspection can be started.

##### Inspection for Instance Segmentation

![Instance Intra Annotation Inspection GUI Top](img/instance_intra_annotation_top.png)

The inspection gui for the instance segmentation case is shown above. At the top of the gui the user annotation, an overlay of user annotation and network prediction as well as the network prediction are shown. There, one can select whether the label itself should be shown, an overlay of label and image or the image on it's own.

The label map overlay of user annotation and model network prediction shows correctly labeled areas in green, missing areas in red and additionally labeled areas in pink.

Below the images, a list of all instances is shown that are not sufficiently labeled. If an instance is selected, it is highlighted in the label map overlay. The user can decide to either keep the user annotation for a selected instance or to use the network annotation instead.

Another possibility is to relabel the whole image. However, this will disable further comparisons between user and network instances.

Below the instances list, the overall score of the selected image is shown. This score is used to sort the images during the annotation inspection as the image with the lowest score is shown first.

If an image was already inspected by the user, this is shown on the gui. Below this, the number of the current image as well as the overall inspection progress is shown. To switch between images, there are the buttons "<" and ">".

![Instance Intra Annotation Inspection GUI Bottom](img/instance_intra_annotation_bottom.png)

By scrolling down, on gets further options for the annotation inspection. Using the oddness and add./miss. thresholds, on can adjust how early a difference between user annotation and network prediction is seen as an error. Furthermore, there are different weighting parameters.

Using the uncertainty weighting, one can adjust, how much the network uncertainty for each instance is taken into account:
```
score = (1 - uncertainty_weighting) * score + uncertainty_weighting * uncertainty
```

The boundary weighting takes into account that images may contain only partially covered objects at the image boundary. Based on the labeling guideline for a dataset they might be labeled by a user, but the network was unable to learn them. Or maybe they should not be labeled, but the network predicts labels for them. In both cases, this could result in low scores even if the user annotation is correct. Using the boundary weighting, one can adjust how strictly objects in a border region (inside a 5% margin at each side of the image) are rated:
```
if instance is in boundary region:
    weighted_score = (1 - boundary_weighting) * score + boundary_weighting * 1
```

Finally, there is a size weighting for additional and missing instances. The score of an additional or missing instance is calculated by dividing the instance size by a reference size. The size weighting is used to calculate the reference size:
```
reference_size = (1 - size_weighting) * overall_max_size
                + size_weighting * unpaired_max_size
```

All these parameters directly affect the instance scores and the overall score of the current image. However, the overall sorting of the images is based on the initial scores. To also change the sorting, there is a "Resort Images" button.

##### Inspection for Semantic Segmentation

...


#### Inter Annotator Variability

...


### Application
![Application](img/application.png)  
Collection of analysis pipelines and GUIs for sending processing jobs to a server.

### Crop Image
![Crop Image](img/crop_image.png)  
Chose images by clicking on "Input Image".
Activate "Crop Labels" if corresponding annotations exist.
Enter crop size M (Images will be crop into MxM sized fragments).
Start croping by pressing "Choose Crops" or "Save All". "Choose Crops" allows for selecting specific crops in a separate GUI. "Save All" saves all crops. Original images will be moved to a separate folder.
  
### Train  
![Train Image](img/train.png)  
Enter configuration file by clicking "Config file".
Define output folder by clicking "Output folder".
Start training by clicking "Train".
## Customization
ToDo: Img + Text to explain
### Project Structure
Overview of this repository:
```
.
├── DLIP
│   ├── sampler  
│   |   ├── generic 
│   |   ├── segmentation
│   |   ├── instance_segmentation
│   |   ├── classification  
│   |   ├── seed_detection
│   ├── pre_img_processing
│   |   ├── generic 
│   |   ├── segmentation
│   |   ├── ...
│   ├── pre_labeling
│   |   ├── generic 
│   |   ├── segmentation
│   |   ├── ...  
│   ├── post_processing
│   |   ├── generic 
│   |   ├── segmentation
│   |   ├── ... 
│   ├── post_inspection
│   |   ├── generic 
│   |   ├── segmentation
│   |   ├── ...
|   ... 
│   ├── experiments
│   |   ├── configurations
│   |   |   ├── sampler.yaml
│   |   |   ├── pre_img_processing.yaml
│   |   |   ├── pre_labeling.yaml
│   |   |   ├── post_processing.yaml
│   |   |   ├── post_inspection.yaml
│   |   ├── ...
...
```

In the following the structure/interfaces are shown which are needed to customize KaIDA.

### Sampler

```
from DLIP.sampler.abstract_query_strategy import AbstractQueryStrategy

class YourSampler(AbstractQueryStrategy):
    def __init__(self):
        super().__init__()

        ...

    def rank_samples_to_label(self, unlabeled_dataset):

        ...

        return ranking_dict  
```
### Pre-Image-Processing
```
from DLIP.pre_img_processing.abstract_img_pre_processor import AbstractImgPreProcessor

class YourImgPreProcessor(AbstractImgPreProcessor):
    def __init__(
        self,
        project,
    ):
        super(YourImgPreProcessor,self).__init__(project)

        ...

    def process(self, img, status_bar):

        ...

        return pre_pro_img
```

### Pre-Labeling
```
from DLIP.pre_labeling.abstract_pre_labeling import AbstractPreLabeler

class YourPreLabeler(AbstractPreLabeler):
    def __init__(
        self,
        project,
    ):
        super(YourPreLabeler,self).__init__(project)

        ...

    def predict(self, img, status_bar):

        ...

        return pre_labeled_img
```

### Post-Label-Processing
```
from DLIP.post_processing.abstract_post_processor import AbstractPostProcessor

class YourLabelPostProcessor(AbstractPostProcessor):
    def __init__(
        self,
        project,
    ):
        super(YourLabelPostProcessor,self).__init__(project)

        ...

    def process(self, label, status_bar):

        ...

        return post_pro_label
```
### Label Inspection
```
from DLIP.post_inspection.abstract_post_inspector import AbstractPostInspector

class YourLabelInspector(AbstractPostInspector):
    def __init__(
        self,
        project,
    ):
        super(YourLabelInspector,self).__init__(project)

        ...

    def inspect(self, label, img, status_bar):

        ...

        return rating
```


Please be aware to add you custom python file to the corresponding `__init__.py` in order to enable auto-load of custom features, e.g. for the sampler

```
from .random_selector import RandomSelector

...

from .your_sampler import YourSampler
```
