from setuptools import setup, find_packages

setup(
    name="KaIDA",
    version="0.1.1",
    author='Marcel Schilling',
    author_email='marcel.schilling@kit.edu',    
    url="https://gitlab.kit.edu/kit/iai/ml4home/kaida",
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    packages=find_packages(),
    install_requires=[],
)
